import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { GetStaticProps } from 'next';

import { Main } from 'landing/pages/Main';
import { withUserOnStaticPage } from 'hocs/withUserOnStaticPage';

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale!, ['common', 'header.component', 'footer.component', 'utils'])),
  },
});

export default withUserOnStaticPage(Main);

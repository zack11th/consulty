import Pusher from 'pusher';
import type { NextApiRequest, NextApiResponse } from 'next';
import { api } from 'api';

export const pusher = new Pusher({
  appId: process.env.PUSHER_APP_ID!,
  key: process.env.NEXT_PUBLIC_PUSHER_KEY!,
  secret: process.env.PUSHER_SECRET!,
  cluster: process.env.NEXT_PUBLIC_PUSHER_CLUSTER!,
  useTLS: true,
});

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const token: string = req.cookies.token;
  //   console.log(`token`, token);
  const { socket_id, channel_name } = req.body;
  const { data } = await api.PusherApi.pusherControllerAuth(
    // @ts-ignore
    { socket_id, channel_name },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
  //   console.log(`data`, data);
  res.send(data);
  res.end();
}

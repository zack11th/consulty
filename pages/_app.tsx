import '../styles/globals.scss';
import { appWithTranslation } from 'next-i18next';
import { Router } from 'next/router';
import type { AppProps, AppContext } from 'next/app';
import NextApp from 'next/app';
import Head from 'next/head';
import { createGlobalStyle } from 'styled-components';
import { Provider, useSelector } from 'react-redux';
import { useEffect, useRef } from 'react';
import NProgress from 'nprogress';
import { ToastContainer } from 'react-toastify';
import mpegEncoder from 'audio-recorder-polyfill/mpeg-encoder';
import GoogleAnalytics from 'react-ga';
import { IntercomProvider } from 'react-use-intercom';

import { actions, selectors } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';
import { store } from '../store';
import { useUserPrivateCannel } from 'hooks/useUserPrivateChannel';
import { usePresenceChannel } from 'hooks/usePresenceChannel';
import { getCookie } from 'utils';
import { AUDIO_MESSAGE_EXT, COOKIE_KEY, ENVIRONMENT_TYPE, INTERCOM_APP_ID } from 'common/constants';
import { AxiosInterceptors } from 'api/httpClient';
import { useUserPrivateConsultationsCannel } from 'hooks/useUserPrivateConsultationsChannel';
import { YMInitializer } from 'react-yandex-metrika';
import { useFacebookPixel } from 'hooks/useFacebookPixel';

Router.events.on('routeChangeStart', (url) => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

Router.events.on('routeChangeStart', (url) => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const GlobalStyle = createGlobalStyle`
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
`;

AxiosInterceptors.setup(store);

function MyApp(props: AppProps) {
  const renderMetrics = () => {
    if (ENVIRONMENT_TYPE === 'production') {
      return (
        <>
          <YMInitializer accounts={[66496642, 85906455]} />
          {GoogleAnalytics.initialize('UA-177534398-1')}
        </>
      );
    }
  };

  return (
    <Provider store={store}>
      <IntercomProvider appId={INTERCOM_APP_ID} autoBoot>
        <App {...props} />
        {renderMetrics()}
      </IntercomProvider>
    </Provider>
  );
}

const App = ({ Component, pageProps: { token, ...props } }: AppProps) => {
  const dispatch = useAppDispatch();
  const tokenFromStore = useSelector(selectors.profile.selectToken);
  const user = useSelector(selectors.profile.selectUser);
  const oldToken = useRef<string | null>(null);
  useUserPrivateCannel();
  useUserPrivateConsultationsCannel();
  usePresenceChannel();

  useEffect(() => {
    oldToken.current = tokenFromStore;
  }, [tokenFromStore]);

  useEffect(() => {
    if (token && oldToken.current !== token) {
      dispatch(actions.profile.setToken(token));
      dispatch(actions.profile.fetchMe());
      dispatch(actions.profile.fetchWallet());
      dispatch(actions.profile.fetchSelfPromocode());
      dispatch(actions.profile.fetchConsultationsWithoutReview());
    }
  }, [token]);

  useEffect(() => {
    if (user.id) {
      dispatch(actions.chatRooms.getChatRooms({ user }));
      dispatch(
        actions.consultationRequests.fetchManyConsultationsRequests({
          filter: [`clientId||$eq||${user.id}`, `status||$eq||active`],
        }),
      );
      user.isExpert && Boolean(user.categories?.length)
        ? dispatch(
            actions.consultationRequests.fetchManyClientsRequests({
              filter: [
                `categoryId||$in||${user.categories?.map((c) => c.categoryId).join(',')}`,
                `clientId||$ne||${user.id}`,
                `status||$in||active`,
              ],
            }),
          )
        : dispatch(actions.consultationRequests.clearClientRequests());
    }
  }, [user]);

  useEffect(() => {
    import('audio-recorder-polyfill').then((audioRecorder) => {
      audioRecorder.default.encoder = mpegEncoder;
      audioRecorder.default.prototype.mimeType = `audio/${AUDIO_MESSAGE_EXT}`;
      window.MediaRecorder = audioRecorder.default;
    });
  }, []);

  const appendFbPixelScript = () => {
    if (ENVIRONMENT_TYPE === 'production') {
      return (
        <script
          dangerouslySetInnerHTML={{
            __html: `<!-- Facebook Pixel Code -->
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '470297194334893');
fbq('track', 'PageView')
<!-- End Facebook Pixel Code -->`,
          }}
        ></script>
      );
    }
  };

  const appendGaScript = () => {
    if (ENVIRONMENT_TYPE === 'production') {
      return (
        <>
          <script async src={`https://www.googletagmanager.com/gtag/js?id=UA-177534398-1`}></script>
          <div
            dangerouslySetInnerHTML={{
              __html: `          
              <script>
                window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments)}
                gtag('js', new Date()); gtag('config', 'UA-177534398-1' );
              </script>`,
            }}
          ></div>
        </>
      );
    }
  };

  return (
    <>
      <GlobalStyle />
      <Head>
        <link rel="stylesheet" type="text/css" href="/nprogress.css" />
        <meta name="facebook-domain-verification" content="harlq5tch0u63fvj57kmyenzjs7evd" />
        {appendFbPixelScript()}
        {appendGaScript()}
      </Head>
      <Component {...props} />
      <ToastContainer
        position="top-center"
        autoClose={3000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover={false}
        closeButton={false}
        limit={1}
        enableMultiContainer
      />
      <ToastContainer
        containerId="chatToastContainer"
        enableMultiContainer
        toastClassName="chat-toast-container"
        position="bottom-left"
        autoClose={5000}
        hideProgressBar
        pauseOnHover
        limit={5}
      />
    </>
  );
};
MyApp.getInitialProps = async (appContext: AppContext) => {
  const { ctx } = appContext;
  const appProps = await NextApp.getInitialProps(appContext);
  let accessToken;

  if (ctx.req) {
    accessToken = getCookie(COOKIE_KEY.accessToken, ctx.req);
  }

  return { ...appProps, pageProps: { ...appProps.pageProps, token: accessToken || '' } };
};

export default appWithTranslation(MyApp);

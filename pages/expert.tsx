import { withUserOnStaticPage } from 'hocs/withUserOnStaticPage';
import { Landing } from 'landing/pages/ExpertLanding';
import { GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale!, ['common', 'header.component', 'footer.component', 'utils'])),
  },
});

export default withUserOnStaticPage(Landing);

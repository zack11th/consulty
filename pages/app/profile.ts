import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { api } from 'api';
import { withAuthGSSP } from 'hocs/withAuthGSSP';
import ProfilePage from 'components/pages/profile';
import { withAuthClientSide } from 'hocs/withAuthClientSide';
import { ProfileProps } from 'components/pages/profile/profile';

export default withAuthClientSide<ProfileProps>(ProfilePage);

export const getServerSideProps = withAuthGSSP(async ({ locale, req }) => {
  const token: string = req.cookies.token;
  if (!token) {
    return {
      redirect: '/',
      props: {},
    };
  }
  const profileData = await api.V1UsersApi.usersControllerGetMe({
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const categories = await api.V1CategoriesApi.categoriesControllerGetManyCategories();
  const rubrics = await api.V1RubricsApi.rubricsControllerGetManyRubrics();

  if (!profileData || !categories) {
    return {
      redirect: '/',
      props: {},
    };
  }
  return {
    props: {
      ...(await serverSideTranslations(locale!, [
        'profile.page',
        'header.component',
        'footer.component',
        'errors.messages',
        'expert.preview',
        'utils',
      ])),
      profile: profileData.data,
      categories: categories.data,
      rubrics: rubrics.data,
    },
  };
});

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { api } from 'api';
import ExpertPage from 'components/pages/expert';
import { GetServerSideProps } from 'next';

export default ExpertPage;

export const getServerSideProps: GetServerSideProps = async ({ locale, query, req, res }) => {
  const { data: profile } = await api.V1UsersApi.usersControllerGetUserOne(Number(query.id));
  const { data: rubrics } = await api.V1RubricsApi.rubricsControllerGetManyRubrics();
  const { data: categories } = await api.V1CategoriesApi.categoriesControllerGetManyCategories(undefined);

  if (!profile.isExpert) {
    return {
      props: {},
      notFound: true,
    };
  }

  return {
    props: {
      profile: profile,
      rubrics: rubrics,
      categories: categories,
      achievements: profile.achievements,
      ...(await serverSideTranslations(locale!, [
        'header.component',
        'footer.component',
        'expert.page',
        'expert.preview',
        'utils',
      ])),
    },
  };
};

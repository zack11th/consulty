import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import ContractPage from 'components/pages/contract';
import { withAuthClientSide } from 'hocs/withAuthClientSide';
import { withAuthGSSP } from 'hocs/withAuthGSSP';

export default withAuthClientSide(ContractPage);

export const getServerSideProps = withAuthGSSP(async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['contract.page', 'header.component', 'footer.component', 'utils'])),
    },
  };
});

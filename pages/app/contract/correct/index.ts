import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import CorrectContract from 'components/pages/contract-correct';
import { withAuthClientSide } from 'hocs/withAuthClientSide';
import { withAuthGSSP } from 'hocs/withAuthGSSP';

export default withAuthClientSide(CorrectContract);

export const getServerSideProps = withAuthGSSP(async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['contract.page', 'header.component', 'footer.component', 'utils'])),
    },
  };
});

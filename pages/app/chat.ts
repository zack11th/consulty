import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { withAuthGSSP } from 'hocs/withAuthGSSP';
import { withAuthClientSide } from 'hocs/withAuthClientSide';
import ChatPage from 'components/pages/chat';

export default withAuthClientSide(ChatPage);

export const getServerSideProps = withAuthGSSP(async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['chat.page', 'header.component', 'utils', 'errors.messages'])),
    },
  };
});

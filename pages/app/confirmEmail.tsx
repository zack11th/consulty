import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { GetStaticProps } from 'next';

export { default } from 'components/pages/confirm-email';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale!, [
        'confirm-email.page',
        'header.component',
        'footer.component',
        'utils',
      ])),
    },
  };
};

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { GetServerSideProps } from 'next';
import { api, Rubric } from 'api';

export { default } from 'components/pages/topics';

export const getServerSideProps: GetServerSideProps = async ({ locale }) => {
  const result = await api.V1RubricsApi.rubricsControllerGetManyRubrics();
  let upRubrics: Rubric[] = [];
  let downRubrics: Rubric[] = [];

  if (result.data) {
    const sortedRubrics = result.data.sort((item) => (item.isUpcoming ? 1 : -1));

    upRubrics = sortedRubrics.filter((_, index) => index < 6);
    downRubrics = sortedRubrics.filter((_, index) => index >= 6);
  }

  return {
    props: {
      upRubrics,
      downRubrics,
      ...(await serverSideTranslations(locale!, [
        'topics.page',
        'header.component',
        'footer.component',
        'expert.preview',
        'utils',
      ])),
    },
  };
};

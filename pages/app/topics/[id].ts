import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { GetServerSideProps } from 'next';
import { api, Category } from 'api';
import { captureError } from 'utils/captureError';

export { default } from 'components/pages/category';

export const getServerSideProps: GetServerSideProps = async ({ locale, query }) => {
  try {
    const result = await api.V1CategoriesApi.categoriesControllerGetOneCategory(Number(query.id));
    if (!result?.data) {
      return {
        props: {},
        notFound: true,
      };
    }

    const getRootCategory = (category: Category): Category => {
      if (category.rootCategory) {
        return getRootCategory(category.rootCategory);
      } else {
        return category;
      }
    };
    const firstLevelCategory = getRootCategory(result.data.category);

    const { data: rubrics } = await api.V1RubricsApi.rubricsControllerGetManyRubrics();
    if (
      rubrics
        .filter((rubric) => rubric.categories.some((category) => category.id === firstLevelCategory?.id))
        .every((rubric) => rubric.isUpcoming)
    ) {
      return {
        props: {},
        notFound: true,
      };
    }

    return {
      props: {
        category: result.data,
        ...(await serverSideTranslations(locale!, [
          'category.page',
          'header.component',
          'footer.component',
          'ask-expert-form.component',
          'expert.preview',
          'utils',
        ])),
      },
    };
  } catch (error) {
    captureError(error);

    return {
      props: {},
      notFound: true,
    };
  }
};

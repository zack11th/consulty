import React, { memo, useCallback, useState } from 'react';
import styled from 'styled-components';
import { Form } from 'react-final-form';

import { SUPPORT_EMAIL } from 'common/constants';
import { InputField } from 'landing/fields/InputField';
import { Button } from 'landing/components/Button';

export const OfferRubric = memo(() => {
  const [isSent, setIsSent] = useState(false);

  const onSubmit = useCallback(
    async (values: {
      name: string;
    }): Promise<any> => {
      window.open(`mailto:${SUPPORT_EMAIL}?subject='Предложить  рубрику'&body=${encodeURIComponent(values.name)}`);
      setIsSent(true);
    },
    [],
  );

  if (isSent) {
    return (
      <>
        <StyledTitle>Отправлено</StyledTitle>

        <StyledDescription>Мы расмотрим предложенную рубрику и добавим, если она нужна пользователям.</StyledDescription>
      </>
    );
  }

  return (
      <>
      <StyledTitle>Предложить рубрику</StyledTitle>

      <StyledDescription>Если считаете, что не хватает востребованной рубрики - напишите нам</StyledDescription>

      <Form onSubmit={onSubmit}>
        {({ handleSubmit, submitting, values }) => (
          <StyledForm onSubmit={handleSubmit}>
            <InputField name="name" placeholder="Например астрология" />

            <StyledButtonWrapper>
              <Button type="submit" $block disabled={submitting || !values.name}>
                Предложить
              </Button>
            </StyledButtonWrapper>
          </StyledForm>
        )}
      </Form>
      </>
  );
});
  
  const StyledTitle = styled.div({
    fontSize: 19,
    fontWeight: 'bold',
    lineHeight: 1.05,
    textAlign: 'center',
    color: '#232832',
    marginBottom: 15,
  });
  
  const StyledDescription = styled.div({
    fontSize: 13,
    fontWeight: 500,
    lineHeight: 1.54,
    textAlign: 'center',
    color: '#232832',
  });
  
  const StyledForm = styled.form({
    marginTop: 30,
  });
  
  const StyledButtonWrapper = styled.div({
    marginTop: 15,
  });
import React, { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { Button } from '../../ui/button';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { About, api } from '../../../api';
import { Link as ScrollLink } from 'react-scroll';
import { captureError } from 'utils/captureError';

export const Support: FC = () => {
  const { t } = useTranslation('profile.page');

  const [about, setAbout] = useState<About | null>(null);

  const getAbout = async () => {
    try {
      const { data } = await api.V1AboutServiceApi.aboutControllerGetOne();
      setAbout(data);
    } catch (e) {
      captureError(e);
      return;
    }
  };

  useEffect(() => {
    getAbout();
  }, []);

  if (!about) {
    return null;
  }

  return (
    <Container>
      <MainTitle>{t('supportTab.title')}</MainTitle>
      <Title>{t('supportTab.mostValued')}</Title>

      <ItemsContainer>
        {about.faq.map((item, index) => (
          <Item key={index}>
            <ScrollLink to={index.toString()} smooth={true} offset={-100} duration={500}>
              {item.question}
            </ScrollLink>
          </Item>
        ))}
      </ItemsContainer>

      <Block $mb={90}>
        <Title>{t('supportTab.connectWithSupport')}</Title>
        <Note>{t('supportTab.connectWithSupportNote')}</Note>
        <a href="mailto:support@consulty.online" target="_blank">
          <Button bordered>{t('supportTab.write')}</Button>
        </a>
      </Block>

      {about.faq.map((item, index) => (
        <Block $mb={60} key={index} id={index.toString()}>
          <Title>{item.question}</Title>

          {item.answer.map((a, i) => (
            <Note key={i}>{a}</Note>
          ))}
        </Block>
      ))}
    </Container>
  );
};

const Container = styled.div`
  padding-left: 0;
  padding-bottom: 90px;

  ${CONDITION_DESKTOP} {
    padding-left: 95px;
  }
`;

const Title = styled.div`
  font-size: 17px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 20px;
`;

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  color: var(--gray);
  margin-bottom: 20px;
  max-width: 600px;
`;

const MainTitle = styled(Title)`
  font-size: 25px;
  margin-bottom: 40px;
`;

const Item = styled.div`
  & > a {
    color: var(--purple);

    &:hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }
`;

const ItemsContainer = styled.div`
  margin-bottom: 60px;

  & > div {
    margin-bottom: 15px;
  }

  & > div:last-child {
    margin-bottom: 0;
  }
`;

const Block = styled.div<{ $mb: number }>`
  margin-bottom: ${({ $mb }) => $mb}px;
`;

import React, { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';

import { Button } from 'components/ui/button';
import { HeaderFreeConsult } from 'components/common/header-free-consult';
import { Camera, StoreApple, StoreGoogle } from 'assets/svg';
import { useUserPromocodes } from 'hooks/useUserPromocodes';
import { useAppDispatch } from 'hooks/redux';
import { actions, selectors } from 'store/ducks';
import { coverImageStyle } from 'utils';

import { authMenuListExpert, authMenuListClient, notAuthMenuList } from './menuList';
import { routes } from '../../../common/routes';
import ym from 'react-yandex-metrika';
import { ENVIRONMENT_TYPE } from 'common/constants';

interface HeaderMobileMenuProps {
  onCloseMenu: () => void;
}

const sendDataToMetrics = () => {
  if (ENVIRONMENT_TYPE === 'production') {
    ym('reachGoal', ['Support']);
    window.gtag('event', 'Support');
    window.fbq('trackCustom', 'Support');
  }
};

export const HeaderMobileMenu: FC<HeaderMobileMenuProps> = ({ onCloseMenu }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('header.component');
  const router = useRouter();
  const user = useSelector(selectors.profile.selectUser);
  const wallet = useSelector(selectors.profile.selectWallet);
  const isAuthenticate = useSelector(selectors.profile.selectIsAuthentication);
  const [menuList, setMenuList] = useState<{ textKey: string; route: string }[]>([]);
  const { freeConsultCount, freeConsultDays } = useUserPromocodes();

  useEffect(() => {
    setMenuList(isAuthenticate ? (user.isExpert ? authMenuListExpert : authMenuListClient) : notAuthMenuList);
  }, [isAuthenticate]);

  const logout = () => {
    dispatch(actions.profile.signOut());
    onCloseMenu();
  };

  const onClickNavigateMenuButton = (route: string) => {
    router.push(route);
    onCloseMenu();
  };

  return (
    <Container>
      {isAuthenticate && <AvatarContainer $url={user.avatarUrl}>{user.avatarUrl ? null : <Camera />}</AvatarContainer>}
      {isAuthenticate && (
        <NameContainer>
          <Name>{`${user.firstName || ''} ${user.lastName || ''}`}</Name>
          {user.isExpert && (
            <Money>{t('dropdown.money', { quantity: wallet?.balance || 0, currency: '\u20bd' })}</Money>
          )}
        </NameContainer>
      )}
      <StyledButton block onClick={() => onClickNavigateMenuButton(routes.topics)}>
        {t('mobileMenu.findExperts')}
      </StyledButton>
      {freeConsultCount > 0 && <HeaderFreeConsult freeCount={freeConsultCount} freeDays={freeConsultDays} />}

      <MenuList>
        {menuList.map((item, index) => {
          return (
            <MenuItem key={index} onClick={() => onClickNavigateMenuButton(item.route)}>
              {t(item.textKey)}
            </MenuItem>
          );
        })}
        <MenuItem
          onClick={() => {
            onClickNavigateMenuButton(`${routes.profile}?activeTab=support`);
            sendDataToMetrics();
          }}
        >
          {t('dropdown.support')}
        </MenuItem>
        {isAuthenticate ? (
          <MenuItem onClick={logout}>{t('dropdown.logout')}</MenuItem>
        ) : (
          <MenuItem onClick={onCloseMenu}>{t('dropdown.about')}</MenuItem>
        )}
      </MenuList>

      <Footer>
        <FooterText>{t('mobileMenu.downloadApp')}</FooterText>
        <FooterStores>
          {/*TODO: add link to apple store */}
          <a>
            <StoreApple />
          </a>
          <StoreGap />
          <a href="https://play.google.com/store/apps/details?id=com.consulty" target="_blank">
            <StoreGoogle />
          </a>
        </FooterStores>
      </Footer>
    </Container>
  );
};

const Container = styled.div`
  position: absolute;
  top: calc(100% + 1px);
  width: 100vw;
  background-color: var(--white);
  overflow-y: auto;
  height: calc(100vh - 60px);
  padding: 30px 20px 60px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const AvatarContainer = styled.div<{ $url?: string }>`
  width: 60px;
  height: 60px;
  background-color: var(--purple);
  border-radius: 15px 15px 3px 15px;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  flex-shrink: 0;

  ${({ $url }) => $url && { ...coverImageStyle($url) }}
  img {
    max-width: 100%;
  }
`;

const NameContainer = styled.div`
  text-align: center;
  margin-bottom: 20px;
`;

const Name = styled.p`
  font-weight: bold;
  font-size: 16px;
  line-height: 25px;
`;

const Money = styled.p`
  font-size: 16px;
  line-height: 20px;
  color: var(--gray);
`;

const StyledButton = styled(Button)`
  flex-shrink: 0;
`;

const MenuList = styled.ul`
  margin-top: 30px;
  border-top: 1px solid var(--gray5);
  width: 100%;
`;

const MenuItem = styled.li`
  font-size: 16px;
  line-height: 50px;
  border-bottom: 1px solid var(--gray5);
`;

const Footer = styled.div`
  margin-top: 60px;
`;

const FooterText = styled.p`
  font-weight: bold;
  font-size: 15px;
  line-height: 25px;
`;

const FooterStores = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 10px;
`;

const StoreGap = styled.div`
  width: 15px;
`;

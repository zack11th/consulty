import { routes } from 'common/routes';

export const authMenuListExpert = [
  {
    textKey: 'dropdown.settings',
    route: `${routes.profile}?activeTab=settings`,
  },
  {
    textKey: 'dropdown.profile',
    route: `${routes.profile}?activeTab=expertProfile`,
  },
  {
    textKey: 'dropdown.wallet',
    route: `${routes.profile}?activeTab=wallet`,
  },
  {
    textKey: 'dropdown.notifications',
    route: `${routes.profile}?activeTab=notifications`,
  },
];

export const authMenuListClient = [
  {
    textKey: 'dropdown.settings',
    route: `${routes.profile}?activeTab=settings`,
  },
  {
    textKey: 'dropdown.profile',
    route: `${routes.profile}?activeTab=expertProfile`,
  },
  {
    textKey: 'dropdown.notifications',
    route: `${routes.profile}?activeTab=notifications`,
  },
];

export const notAuthMenuList = [
  {
    textKey: 'mobileMenu.becomeExpert',
    route: `${routes.profile}?activeTab=expertProfile`,
  },
];

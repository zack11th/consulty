import React, { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import ExpertPreview from '../expert-preview';
import { api, User } from '../../../api';
import { useRouter } from 'next/router';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { TabButton } from '../../ui/tab-button';
import { useTranslation } from 'next-i18next';
import { Button } from '../../ui/button';

interface ExpertsInCategoryProps {
  sortCriteria: SortCriteria;
  title: string;
  note: string;
}

type SortCriteria = 'rating' | 'countOfConsultationExpert';

const getExperts = async (id: number, sortCriteria: SortCriteria, limit: number ,filters?: "freeConsultations"[]) => {
  return await api.V1CategoriesApi.categoriesControllerGetExpertsByCategory(id, sortCriteria, 'DESC', limit, undefined,filters);
};

//TODO: удалить когда допишут сваггер
interface ExpertsByCategoryResponse {
  count: number;
  data: User[];
  page: number;
  pageCount: number;
  total: number;
}

enum ExpertTab {
  ALL,
  WITH_FREE_CONSULT,
}

export const ExpertsInCategory: FC<ExpertsInCategoryProps> = ({ sortCriteria, title, note }) => {
  const [limit, setLimit] = useState<number>(6);
  const [total, setTotal] = useState<number>(0);
  const [count, setCount] = useState<number>(0);
  const [experts, setExperts] = useState<User[]>([]);
  const [freeLimit, setFreeLimit] = useState<number>(6);
  const [freeTotal, setFreeTotal] = useState<number>(0);
  const [freeCount, setFreeCount] = useState<number>(0);
  const [freeExperts, setFreeExperts] = useState<User[]>([]);
  const [tab, setTab] = useState<ExpertTab>(ExpertTab.ALL);
  const router = useRouter();
  const { query } = router;
  const { t } = useTranslation('category.page');
  
  useEffect(() => {
    if (!query.id) {
      return;
    }
    getExperts(Number(query.id), sortCriteria, limit).then((res) => {
      //TODO: remove any when GetManyCategoryExpertsDto return User[]
      const data = res.data as any;
      setTotal(data.total);
      setCount(data.count);
      setExperts(data.data);
    });
  }, [limit, query.id]);

  useEffect(() => {
    if (!query.id) {
      return;
    }
      getExperts(Number(query.id), sortCriteria, limit, ["freeConsultations"] ).then((res) => {
        const data = res.data as ExpertsByCategoryResponse;
        setFreeTotal(data.total);
        setFreeCount(data.count);
        setFreeExperts(data.data);
      });
  }, [freeLimit, query.id]);
  
  const onMoreExperts = (free?: boolean) => {
    free? setLimit((l) => l + 6) : setFreeLimit((l) => l + 6);
  };

  if(freeExperts.length === 0 && experts.length === 0) return null;

  return (
    <Container className={'container'}>
      <Title>{title}</Title>
      <Note>{note}</Note>

      <ToggleContainer>
        <TabButton index={ExpertTab.ALL} activeIndex={tab} setActiveIndex={setTab}>
          {t('experts.allExperts')}
        </TabButton>
        <TabButton
          index={ExpertTab.WITH_FREE_CONSULT}
          icon={'/img/coin.png'}
          iconRetina={'/img/coin.png'}
          activeIndex={tab}
          setActiveIndex={setTab}
        >
          {t('experts.freeConsult')}
        </TabButton>
      </ToggleContainer>

      <Table>
        {tab === ExpertTab.ALL ? experts.map(
          ({
            firstName,
            lastName,
            expertConsultationsCount,
            expertRating,
            reviewsCount,
            isProfileVerified,
            avatarUrl,
            consultationAveragePrice,
            achievements,
            videoUrl,
            id,
          }) => (
            <Item key={id}>
              <ExpertPreview
                name={`${firstName} ${lastName}`}
                consultationCount={expertConsultationsCount}
                id={id}
                rating={expertRating || 0}
                reviewCount={reviewsCount}
                averagePrice={consultationAveragePrice || 0}
                avatar={avatarUrl || null}
                isVerification={isProfileVerified}
                achievements={achievements?.map((a) => a.achievement)}
                videoUrl={videoUrl}
              />
            </Item>
          ),
        ): 
        freeExperts.map(
          ({
            firstName,
            lastName,
            expertConsultationsCount,
            expertRating,
            reviewsCount,
            isProfileVerified,
            avatarUrl,
            consultationAveragePrice,
            achievements,
            videoUrl,
            id,
          }) => (
            <Item key={id}>
              <ExpertPreview
                name={`${firstName} ${lastName}`}
                consultationCount={expertConsultationsCount}
                id={id}
                rating={expertRating || 0}
                reviewCount={reviewsCount}
                averagePrice={consultationAveragePrice || 0}
                avatar={avatarUrl || null}
                isVerification={isProfileVerified}
                achievements={achievements?.map((a) => a.achievement)}
                videoUrl={videoUrl}
              />
            </Item>
          ),
        )}
      </Table>

      {tab === ExpertTab.ALL ? total - count !== 0 && (
        <ButtonContainer>
          <Button bordered onClick={() => onMoreExperts()}>
            {t('experts.moreExperts', { count: total - count > 10 ? 10 : total - count })}
          </Button>
        </ButtonContainer>
      ): 
      freeTotal - freeCount !== 0 && (
        <ButtonContainer>
          <Button bordered onClick={() => onMoreExperts(true)}>
            {t('experts.moreExperts', { count: freeTotal - freeCount > 10 ? 10 : freeTotal - freeCount })}
          </Button>
        </ButtonContainer>
      )}
    </Container>
  );
};

const Container = styled.div`
  &&&{
    margin-top: 120px;
    margin-bottom: 120px;
  }
`;

const Title = styled.h2`
  text-align: center;
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.13;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 6px;

  ${CONDITION_DESKTOP} {
    font-size: 40px;
  }
`;

const Note = styled.h2`
  text-align: center;
  font-size: 15px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.76;
  letter-spacing: normal;
  color: var(--gray);
  margin-bottom: 30px;

  ${CONDITION_DESKTOP} {
    font-size: 17px;
  }
`;

const Table = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin-bottom: 40px;

  ${CONDITION_DESKTOP} {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    margin-bottom: 60px;
  }
`;

const Item = styled.div`
  display: flex;
  justify-content: center;
`;

const ToggleContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 40px;
  text-align: center;

  span {
    font-size: 13px;
  }

  img {
    display: none;
  }

  & > div {
    padding: 10px 4px;
  }

  ${CONDITION_DESKTOP} {
    img {
      display: unset;
    }

    span {
      font-size: 15px;
    }

    & > div {
      padding: 10px 15px;
    }

    margin-bottom: 60px;
  }
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;

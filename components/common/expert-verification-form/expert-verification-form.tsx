import React, { FC } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import VerificationMark from '../../../assets/svg/VerificationMark';
import ExpertPreview from '../expert-preview';
import { Controller, useForm } from 'react-hook-form';
import { Input } from '../../ui/input';
import { Button } from '../../ui/button';
import { Achievement, AchievementNameEnum } from '../../../api';
import { toast } from 'react-toastify';
import { VideoCamera } from '../../../assets/svg';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { useSelector } from 'react-redux';
import { selectVerificationRequest } from '../../../store/ducks/profile/selectors';
import { selectors } from '../../../store/ducks/profile';
import StatusVerificationRequest from '../status-verification-request';
import { useAppDispatch } from '../../../hooks/redux';
import { sendVerificationRequest } from '../../../store/ducks/profile/actions';
import { linkPattern } from '../../../constants';
import { captureError } from 'utils/captureError';

interface ExpertVerificationFormValues {
  videoCard: string;
  instagram: string;
  facebook: string;
  website: string;
  diplomas: string;
  reviews: string;
}

const achievements: Achievement[] = [
  {
    id: 1,
    name: AchievementNameEnum.Profi,
    updatedAt: '',
    deletedAt: '',
    createdAt: '',
  },
  {
    id: 2,
    name: AchievementNameEnum.OnThePositive,
    updatedAt: '',
    deletedAt: '',
    createdAt: '',
  },
  {
    id: 3,
    name: AchievementNameEnum.Smartest,
    updatedAt: '',
    deletedAt: '',
    createdAt: '',
  },
  {
    id: 4,
    name: AchievementNameEnum.RespondsQuickly,
    updatedAt: '',
    deletedAt: '',
    createdAt: '',
  },
];

export const ExpertVerificationForm: FC = () => {
  const { t } = useTranslation('profile.page');

  const { t: error } = useTranslation('errors.messages');

  const verificationRequest = useSelector(selectVerificationRequest);
  const user = useSelector(selectors.selectUser);

  const dispatch = useAppDispatch();

  const {
    control,
    handleSubmit,
    formState: { isDirty },
  } = useForm<ExpertVerificationFormValues>({
    defaultValues: {
      videoCard: verificationRequest?.videoUrl || user.videoUrl || '',
      instagram: verificationRequest?.instagramUrl || user.instagramUrl || '',
      facebook: verificationRequest?.facebookUrl || user.facebookUrl || '',
      website: verificationRequest?.personalSite || user.personalSite || '',
      diplomas: verificationRequest?.diplomasUrl || user.diplomasUrl || '',
      reviews: verificationRequest?.reviewsUrl || '',
    },
  });

  const onSubmit = async ({
    videoCard,
    reviews,
    diplomas,
    website,
    facebook,
    instagram,
  }: ExpertVerificationFormValues) => {
    try {
      await dispatch(
        sendVerificationRequest({
          diplomasUrl: diplomas.length !== 0 ? diplomas : undefined,
          videoUrl: videoCard.length !== 0 ? videoCard : undefined,
          instagramUrl: instagram.length !== 0 ? instagram : undefined,
          facebookUrl: facebook.length !== 0 ? facebook : undefined,
          reviewsUrl: reviews.length !== 0 ? reviews : undefined,
          personalSite: website.length !== 0 ? website : undefined,
        }),
      );

      toast.success(t('requestHasBeenSend'));
    } catch (e) {
      captureError(e);
      toast.error(t('notSuccessSaveInfo'));
    }
  };

  return (
    <Container>
      <Title>
        {t('verificationProfile')}
        <VerificationMark />
      </Title>
      <Note>{t('verificationProfileNote')}</Note>

      <ExampleContainer>
        <div>
          <ExampleTitle>{t('beforeVerification')}</ExampleTitle>
          <ExpertPreview
            isVerification={false}
            rating={4.75}
            name={t('exampleUser')}
            consultationCount={14}
            reviewCount={12}
            averagePrice={600}
            avatar={'/img/verificationProfileExampleAvatar.png'}
            id={1}
            isExample={true}
            achievements={[]}
            videoUrl={''}
          />
        </div>
        <div>
          <ExampleTitle>{t('afterVerification')}</ExampleTitle>
          <ExpertPreview
            isVerification={true}
            rating={4.75}
            name={t('exampleUser')}
            consultationCount={14}
            reviewCount={12}
            averagePrice={600}
            avatar={'/img/verificationProfileExampleAvatar.png'}
            id={1}
            isExample={true}
            achievements={achievements}
            videoUrl={''}
          />
        </div>
      </ExampleContainer>

      <FieldContainer>
        <VideoCard>
          <VideoCamera />
          <VideoCardText>{t('videoCard')}</VideoCardText>
        </VideoCard>
        <VideoCardNote>{t('videoCardNote')}</VideoCardNote>
        <Controller
          name={'videoCard'}
          rules={{
            pattern: { value: linkPattern, message: error('incorrectLink') },
          }}
          control={control}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <Input
              type={'url'}
              label={t('videoUrl')}
              value={value}
              onChange={onChange}
              error={error?.message}
              placeholder={'youtube.com/'}
            />
          )}
        />
      </FieldContainer>

      <FieldContainer>
        <FieldTitle>{t('linksOnYou')}</FieldTitle>
        <FieldNote>{t('shareLinks')}</FieldNote>
        <Controller
          name={'instagram'}
          control={control}
          rules={{
            pattern: { value: linkPattern, message: error('incorrectLink') },
          }}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <Input
              label={'Instagram'}
              value={value}
              onChange={onChange}
              error={error?.message}
              placeholder={'Instagram.com/'}
            />
          )}
        />
        <InputContainer>
          <Controller
            name={'facebook'}
            control={control}
            rules={{
              pattern: { value: linkPattern, message: error('incorrectLink') },
            }}
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <Input
                label={'Facebook'}
                value={value}
                onChange={onChange}
                error={error?.message}
                placeholder={'facebook.com/'}
              />
            )}
          />
        </InputContainer>
        {/*TODO return website field*/}
        <InputContainer>
          <Controller
            name={'website'}
            control={control}
            rules={{
              pattern: { value: linkPattern, message: error('incorrectLink') },
            }}
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <Input label={t('selfWebsite')} value={value} onChange={onChange} error={error?.message} />
            )}
          />
        </InputContainer>
      </FieldContainer>

      <FieldContainer>
        <FieldTitle>{t('diplomasTitle')}</FieldTitle>
        <FieldNote>{t('diplomasTitleNote')}</FieldNote>
        <Controller
          name={'diplomas'}
          control={control}
          rules={{
            pattern: { value: linkPattern, message: error('incorrectLink') },
          }}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <Input label={t('linkOnArchive')} value={value} onChange={onChange} error={error?.message} />
          )}
        />
      </FieldContainer>

      <FieldContainer>
        <FieldTitle>{t('reviewsOnYou')}</FieldTitle>
        <FieldNote>{t('reviewsOnYouNote')}</FieldNote>
        <Controller
          name={'reviews'}
          control={control}
          rules={{
            pattern: { value: linkPattern, message: error('incorrectLink') },
          }}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <Input label={t('linkOnArchive')} value={value} onChange={onChange} error={error?.message} />
          )}
        />
      </FieldContainer>

      <Button disabled={!isDirty} onClick={handleSubmit(onSubmit)}>
        {t('sendVerification')}
      </Button>
      {!!verificationRequest && <StatusVerificationRequest verificationRequest={verificationRequest} />}
    </Container>
  );
};

const Container = styled.div`
  padding: 30px 50px 40px 29px;
  border-radius: 12px;
  box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.1);

  & > button {
    margin-top: 40px;
    width: 100%;
    ${CONDITION_DESKTOP} {
      width: unset;
    }
  }
`;

const Title = styled.div`
  margin: 0;
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--black2);
  padding-bottom: 10px;
  display: flex;
  align-items: center;
  & > svg {
    margin-left: 3px;
  }
`;

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  max-width: 500px;
  letter-spacing: normal;
  color: var(--gray);
  & > a {
    color: var(--purple);
    text-decoration: underline;
    :hover {
      text-decoration: none;
    }
  }
`;

const ExampleContainer = styled.div`
  display: flex;
  width: 100%;
  padding-top: 30px;
  flex-direction: column;
  align-items: center;

  ${CONDITION_DESKTOP} {
    flex-direction: row;
    align-items: flex-start;
  }
  & > div {
    width: 50%;
    display: flex;
    align-items: center;
    flex-direction: column;
  }
`;

const ExampleTitle = styled(Note)`
  text-align: center;
  margin-bottom: 8px;
`;
const VideoCard = styled.div`
  font-size: 17px;
  font-weight: bold;
  padding-bottom: 25px;
  display: flex;
  align-items: center;
  & > svg {
    margin-right: 15px;
  }
`;

const VideoCardText = styled.span`
  background: linear-gradient(to right, #812df6 5%, #993dfa 48%, #ae4cfe);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  display: inline-block;
`;

const VideoCardNote = styled(Note)`
  padding-bottom: 30px;
`;

const FieldTitle = styled(Title)`
  font-size: 17px;
`;

const FieldNote = styled(Note)`
  font-size: 14px;
  padding-bottom: 30px;
`;
const FieldContainer = styled.div`
  padding-top: 60px;
`;
const InputContainer = styled.div`
  padding-top: 30px;
`;

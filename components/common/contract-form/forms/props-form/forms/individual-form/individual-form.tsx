import React, { ChangeEvent, FC, useState } from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import { ContractFormValues } from '../../../../contract-form';
import { Input } from '../../../../../../ui/input';
import { datePattern, digitsPattern, emailPattern, fioPattern, passportNumberPattern, passportSeriesPattern } from '../../../../../../../constants';
import PhotoInput from '../../../../../../ui/photo-input';
import { Button } from '../../../../../../ui/button';
import styled from 'styled-components';
import { ContractFormSteps } from '../../../../../../pages/contract/contract';
import { useAppDispatch } from '../../../../../../../hooks/redux';
import { sendIndividualContract } from '../../../../../../../store/ducks/profile/actions';
import { uploadFile } from '../../../../../../../utils/uploadFile';
import { captureError } from 'utils/captureError';
import { useTranslation } from 'react-i18next';
import { api, CreateIndividualContractDto } from 'api';
import { ContractSignCodeModal } from 'components/common/contract-form/components/contract-sign-code-modal';
import { ContractSignSmsCodePayload } from 'common/types';
import dayjs from 'dayjs';
import { unwrapResult } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import { InputDate } from 'components/ui/input-date';
import { InputPhone } from 'components/ui/input-phone';

interface IndividualFormProps {
  changeStepHandle: (step: ContractFormSteps) => void;
}

type FileInputsName = 'registrationInPassportPhoto' | 'selfWithPassport' | 'passportPhoto';

export const IndividualForm: FC<IndividualFormProps> = ({ changeStepHandle }) => {
  const { t } = useTranslation();

  const { handleSubmit, setValue, control, clearErrors, trigger } = useFormContext<ContractFormValues>();

  const [isSignSmsModalOpen, setIsSignSmsModalOpen] = useState(false);

  const dispatch = useAppDispatch();

  const onChangePhoto = (name: FileInputsName) => (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files) {
      const file = files.item(0);
      if (file) {
        setValue(name, file);
        clearErrors(name);
      }
    }
  };

  const handleSendSignSms = async () => {
    const isFormOk = await trigger();

    if (!isFormOk) {
      return;
    }

    try {
      await api.V1ContractsApi.contractsControllerSendSignSmsCode();
      setIsSignSmsModalOpen(true);
    } catch (e: any) {
      toast.error(e.message || 'Что-то пошло не так...');
    }
  };

  const handleSubmitSignSms = async (payload: ContractSignSmsCodePayload) => {
    setIsSignSmsModalOpen(false);

    await handleSubmit(async (values) => await onSubmit({ ...values, ...payload }))();
  };

  const onSubmit = async (values: ContractFormValues & ContractSignSmsCodePayload) => {
    try {
      const selfiePhotoUrl = await uploadFile(values.selfWithPassport, t);

      const res = await dispatch(
        sendIndividualContract({
          signSmsCode: values.smsCode,
          timestampEnterSignSmsCode: values.timestampEnterSmsCode,
          timestampAcceptExpertOffer: values.agencyAgreementTimestamp,
          timestampAcceptPrivacyPolicy: values.dataPolicyAgreementTimestamp,
          payload: {
            birthDate: dayjs(values.birthday, { format: 'DD.MM.YYYY' }).toISOString(),
            birthPlace: values.birthPlace,
            contactPhone: values.phone,
            email: values.email,
            fullName: values.fullName,
            passportNumber: values.passportNumber,
            passportSeries: values.passportSeries,
            passportIssuedBy: values.whoPassportIssued,
            passportWhenGiven: dayjs(values.whenPassportIssued, { format: 'DD.MM.YYYY' }).toISOString(),
            physicalAddress: values.physicalAddress,
            residencePlaceRegistrationAddress: values.addressOfRegistration,
            selfieWithPassportSecondAndThirdPageUrl: selfiePhotoUrl,
          },
        }),
      );
      unwrapResult(res);
      changeStepHandle(ContractFormSteps.final);
    } catch (e: any) {
      toast.error(e.message || 'Что-то пошло не так...');
      captureError(e);
      return;
    }
  };

  return (
    <>
      <ContractSignCodeModal isVisible={isSignSmsModalOpen} onSubmitSmsCode={handleSubmitSignSms} />
      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern:{value: fioPattern,message: 'Не корректный формат'}
        }}
        name={'fullName'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'ФИО'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="Иванов Иван Иванович"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: datePattern, message: 'Не корректная дата' },
        }}
        name={'birthday'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <InputDate
            value={value}
            label={'Дата рождения'}
            placeholder={'01.05.1992'}
            onChange={onChange}
            required
            error={error?.message}
          />
        )}
      />

      <Controller
        control={control}
        name={'birthPlace'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Место рождения'}
            placeholder={'г. Москва'}
            onChange={onChange}
            error={error?.message}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: passportSeriesPattern, message: 'Серия паспорта должна быть из 4 цифр' },
        }}
        name={'passportSeries'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            type={'number'}
            label={'Серия паспорта'}
            onChange={onChange}
            placeholder="4444"
            required
            error={error?.message}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: passportNumberPattern, message: 'Номер паспорта должен быть из 6 цифр' },
        }}
        name={'passportNumber'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Номер паспорта'}
            type={'number'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="555555"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: datePattern, message: 'Не корректная дата' },
        }}
        name={'whenPassportIssued'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <InputDate
            value={value}
            label={'Когда выдан'}
            placeholder={'01.01.2021'}
            onChange={onChange}
            required
            error={error?.message}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        name={'whoPassportIssued'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Кем выдан'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="Отделом внутренних дел Октябрьского округа города Архангельска"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        name={'addressOfRegistration'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Адрес регистрации по месту жительства'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="Кемеровская обл., гор. Ленинск-Кузнецкий, ул. Суворова, д. 9, кв. 1"
          />
        )}
      />

      <Controller
        control={control}
        name={'physicalAddress'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Физический адрес'}
            onChange={onChange}
            error={error?.message}
            placeholder="Кемеровская обл., гор. Ленинск-Кузнецкий, ул. Суворова, д. 9, кв. 1"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        name={'phone'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <InputPhone
            value={value}
            label={'Контактный номер телефона'}
            type={'number'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="+79088034444"
          />
        )}
      />

      <Controller
        control={control}
        name={'email'}
        defaultValue={''}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: emailPattern, message: 'Некорректный Email' },
        }}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Электронная почта для уведомлений'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="example@gmail.com"
          />
        )}
      />

      <Title><span>Селфи с второй и третьей страницей паспорта (должно быть видно паспорт и лицо целиком)</span>
      <Required>*</Required></Title>
      <Controller
        control={control}
        name={'selfWithPassport'}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        render={({ fieldState: { error } }) => (
          <PhotoInput
            prefix={'selfWithPassport'}
            label={'Выбрать'}
            width={'50%'}
            error={error?.message}
            onChange={onChangePhoto('selfWithPassport')}
            sizeLimitMb={15}
            largeSizeError={'Файл больше 15 МБ'}
          />
        )}
      />

      <Button type="button" onClick={handleSendSignSms}>
        Подписать договор SMS-сообщением
      </Button>
    </>
  );
};

const Title = styled.span`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 22px;
  display: block;
`;
const Required = styled.span`
  color: var(--purple);
`;
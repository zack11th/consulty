import React, { ChangeEvent, FC, useState } from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import { ContractFormValues } from '../../../../contract-form';
import { Input } from '../../../../../../ui/input';
import {
  datePattern,
  emailPattern,
  innPathPattern,
  passportNumberPattern,
  passportSeriesPattern,
  fioPattern,
} from '../../../../../../../constants';
import PhotoInput from '../../../../../../ui/photo-input';
import { Button } from '../../../../../../ui/button';
import styled from 'styled-components';
import { ContractFormSteps } from '../../../../../../pages/contract/contract';
import { useAppDispatch } from '../../../../../../../hooks/redux';
import { sendSelfEmployedContract } from '../../../../../../../store/ducks/profile/actions';
import { uploadFile } from '../../../../../../../utils/uploadFile';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import dayjs from 'dayjs';
import { unwrapResult } from '@reduxjs/toolkit';
import { InputPhone } from 'components/ui/input-phone';
import { InputDate } from 'components/ui/input-date';
import { captureError } from 'utils/captureError';
import { api } from 'api';
import { ContractSignSmsCodePayload } from 'common/types';
import { ContractSignCodeModal } from 'components/common/contract-form/components/contract-sign-code-modal';

interface IndividualFormProps {
  changeStepHandle: (step: ContractFormSteps) => void;
}

export const SelfEmpoyedForm: FC<IndividualFormProps> = ({ changeStepHandle }) => {
  const { handleSubmit, setValue, control, clearErrors, trigger } = useFormContext<ContractFormValues>();

  const [isSignSmsModalOpen, setIsSignSmsModalOpen] = useState(false);

  const { t: u } = useTranslation('utils');

  type FileInputsName = 'registrationInPassportPhoto' | 'selfWithPassport' | 'passportPhoto';

  const dispatch = useAppDispatch();

  const onChangePhoto = (name: FileInputsName) => (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files) {
      const file = files.item(0);
      if (file) {
        setValue(name, file);
        clearErrors(name);
      }
    }
  };

  const handleSendSignSms = async () => {
    const isFormOk = await trigger();

    if (!isFormOk) {
      return;
    }

    try {
      await api.V1ContractsApi.contractsControllerSendSignSmsCode();
      setIsSignSmsModalOpen(true);
    } catch (e: any) {
      toast.error(e.message || 'Что-то пошло не так...');
    }
  };

  const handleSubmitSignSms = async (payload: ContractSignSmsCodePayload) => {
    setIsSignSmsModalOpen(false);

    await handleSubmit(async (values) => await onSubmit({ ...values, ...payload }))();
  };

  const onSubmit = async (values: ContractFormValues & ContractSignSmsCodePayload) => {
    try {
      const selfWithPassport = await uploadFile(values.selfWithPassport, u);

      const res = await dispatch(
        sendSelfEmployedContract({
          signSmsCode: values.smsCode,
          timestampEnterSignSmsCode: values.timestampEnterSmsCode,
          timestampAcceptExpertOffer: values.agencyAgreementTimestamp,
          timestampAcceptPrivacyPolicy: values.dataPolicyAgreementTimestamp,
          payload: {
            birthDate: dayjs(values.birthday, 'DD.MM.YYYY').toISOString(),
            birthPlace: values.birthPlace,
            contactPhone: values.phone,
            email: values.email,
            fullName: values.fullName,
            inn: values.INN,
            passportIssuedBy: values.whoPassportIssued,
            passportNumber: values.passportNumber,
            passportSeries: values.passportSeries,
            passportWhenGiven: dayjs(values.whenPassportIssued, 'DD.MM.YYYY').toISOString(),
            physicalAddress: values.physicalAddress,
            residencePlaceRegistrationAddress: values.addressOfRegistration,
            selfieWithPassportSecondAndThirdPageUrl: selfWithPassport,
          },
        }),
      );
      unwrapResult(res);
      changeStepHandle(ContractFormSteps.final);
    } catch (e: any) {
      toast.error(e.message || u('failedSendContract'));
      captureError(e);
      return;
    }
  };

  return (
    <>
      <ContractSignCodeModal isVisible={isSignSmsModalOpen} onSubmitSmsCode={handleSubmitSignSms} />
      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern:{value: fioPattern,message: 'Не корректный формат'}
        }}
        name={'fullName'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'ФИО'}
            placeholder="Иванов Иван Иванович"
            onChange={onChange}
            required
            error={error?.message}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: datePattern, message: 'Дата должна быть в формате 31.01.2000' },
        }}
        name={'birthday'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <InputDate
            value={value}
            label={'Дата рождения'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder={'01.05.1992'}
          />
        )}
      />

      <Controller
        control={control}
        name={'birthPlace'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Место рождения'}
            placeholder={'г. Москва'}
            onChange={onChange}
            error={error?.message}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: passportSeriesPattern, message: 'Серия паспорта должна быть из 4 цифр' },
        }}
        name={'passportSeries'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            type={'text'}
            label={'Серия паспорта'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="4444"
            maxLength={4}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: passportNumberPattern, message: 'Номер паспорта должен быть из 6 цифр' },
        }}
        name={'passportNumber'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Номер паспорта'}
            type={'text'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="555555"
            maxLength={6}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: datePattern, message: 'Дата должна быть в формате 31.01.2000' },
        }}
        name={'whenPassportIssued'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <InputDate
            value={value}
            label={'Когда выдан'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder={'01.01.2021'}
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        name={'whoPassportIssued'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Кем выдан'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="Отделом внутренних дел Октябрьского округа города Архангельска"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        name={'addressOfRegistration'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Адрес регистрации по месту жительства'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="Кемеровская обл., гор. Ленинск-Кузнецкий, ул. Суворова, д. 9, кв. 1"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          pattern: { value: innPathPattern, message: 'ИНН должен быть из 12 цифр' },
        }}
        name={'INN'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            type={'text'}
            label={'ИНН'}
            onChange={onChange}
            error={error?.message}
            placeholder="123456789012"
            maxLength={12}
          />
        )}
      />

      <Controller
        control={control}
        name={'physicalAddress'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Физический адрес'}
            onChange={onChange}
            error={error?.message}
            placeholder="Кемеровская обл., гор. Ленинск-Кузнецкий, ул. Суворова, д. 9, кв. 1"
          />
        )}
      />

      <Controller
        control={control}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        name={'phone'}
        defaultValue={''}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <InputPhone
            value={value}
            label={'Контактный номер телефона'}
            type={'text'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="+79088034444"
          />
        )}
      />

      <Controller
        control={control}
        name={'email'}
        defaultValue={''}
        rules={{
          required: { value: true, message: 'Обязательно' },
          pattern: { value: emailPattern, message: 'Некорректный Email' },
        }}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input
            value={value}
            label={'Электронная почта для уведомлений'}
            onChange={onChange}
            required
            error={error?.message}
            placeholder="example@gmail.com"
          />
        )}
      />

      <Title>
        <span>Селфи с второй и третьей страницей паспорта (должно быть видно паспорт и лицо целиком)</span>
        <Required>*</Required>
      </Title>
      <Controller
        control={control}
        name={'selfWithPassport'}
        rules={{
          required: { value: true, message: 'Обязательно' },
        }}
        render={({ fieldState: { error } }) => (
          <PhotoInput
            prefix={'selfWithPassport'}
            label={'Выбрать'}
            width={'50%'}
            error={error?.message}
            onChange={onChangePhoto('selfWithPassport')}
            sizeLimitMb={15}
            largeSizeError={'Файл больше 15 МБ'}
          />
        )}
      />

      <Button type="button" onClick={handleSendSignSms}>
        Подписать договор SMS-сообщением
      </Button>
    </>
  );
};

const Title = styled.span`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 22px;
  display: block;
`;

const Required = styled.span`
  color: var(--purple);
`;

import React from 'react';
import { ContractFromVariants } from '../../contract-form';
import IndividualForm from './forms/individual-form';
import IpForm from './forms/ip-form';
import OooForm from './forms/ooo-form';
import { ContractFormSteps } from '../../../../pages/contract/contract';
import SelfEmpoyedForm from './forms/self-employed';

export const renderPropsForm = (variant: ContractFromVariants, changeStepHandle: (step: ContractFormSteps) => void) => {
  switch (variant) {
    case ContractFromVariants.INDIVIDUAL:
      return <IndividualForm changeStepHandle={changeStepHandle} />;
    case ContractFromVariants.IP:
      return <IpForm changeStepHandle={changeStepHandle} />;
    case ContractFromVariants.OOO:
      return <OooForm changeStepHandle={changeStepHandle} />;
    case ContractFromVariants.SELF_EMPLOYED:
      return <SelfEmpoyedForm changeStepHandle={changeStepHandle} />;
    default:
      return null;
  }
};

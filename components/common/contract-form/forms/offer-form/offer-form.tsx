import React, { FC, useEffect } from 'react';
import styled from 'styled-components';
import { ContractFormSteps, StepsState } from '../../../../pages/contract/contract';
import { useTranslation } from 'next-i18next';
import { Controller, useFormContext, useWatch } from 'react-hook-form';
import { ContractFormValues } from '../../contract-form';
import { Checkbox } from '../../../../ui/checkbox';
import { Button } from '../../../../ui/button';

interface OfferFormProps {
  changeStepHandle: (step: ContractFormSteps) => void;
  changeProgressBarStateHandle: (data: Partial<StepsState>) => void;
}

export const OfferForm: FC<OfferFormProps> = ({ changeProgressBarStateHandle, changeStepHandle }) => {
  const { t } = useTranslation('contract.page');
  const { control, watch, setValue, getValues } = useFormContext<ContractFormValues>();
  const [agencyAgreement, dataPolicyAgreement] = watch(['agencyAgreement', 'dataPolicyAgreement']);

  const onContinue = () => {
    changeProgressBarStateHandle({
      variant: true,
      offer: true,
      props: false,
    });
    changeStepHandle(ContractFormSteps.props);
  };

  useEffect(() => {
    if (agencyAgreement) {
      setValue('agencyAgreementTimestamp', new Date().toISOString());
    }
  }, [agencyAgreement]);

  useEffect(() => {
    if (dataPolicyAgreement) {
      setValue('dataPolicyAgreementTimestamp', new Date().toISOString());
    }
  }, [dataPolicyAgreement]);

  return (
    <Container>
      <Title>{t('form.readTheOffer')}</Title>

      <Document>
        Значимость этих проблем настолько очевидна, что начало повседневной работы по формированию позиции требует
        определения и уточнения дальнейших направлений развития проекта. Практический опыт показывает, что рамки и место
        обучения кадров играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных
        задач. С другой стороны дальнейшее развитие различных форм деятельности играет важную роль в формировании
        направлений прогрессивного развития.
        <ul style={{ listStyle: 'inside' }}>
          <li>Практический опыт показывает, что начало повседневно</li>
          <li>и позволяет оценить значение модели разви</li>
          <li>проверки ключевых компонентов пл</li>
          <li>проверки ключевых компонентов пл</li>
          <li>проверки енить значение всесторонне сбаля пр</li>
        </ul>
        Практический опыт показывает, что начало повседневной работы по формированию позиции представляет собой
        интересный эксперимент проверки ключевых компонентов планируемого обновления! Повседневная практика показывает,
        что выбранный нами инновационный путь обеспечивает широкому кругу специалистов участие в формировании позиций,
        занимаемых участниками в отношении поставленных задач! Разнообразный и богатый опыт дальнейшее развитие
        различных форм деятельности позволяет оценить значение модели развития? Повседневная практика показывает, что
        рамки и место обучения кадров требует от нас системного анализа позиций, занимаемых участниками в отношении
        поставленных задач.
        <p>
          аемых участниками в отношении поставленных задач! Разнообразный и богатый опыт дальнейшее развитие различных
          форм деятельности позволяет оценить значение модели развития? Повседневная практика показывает, что рамки и
          место обучения кадро
        </p>
        <ul style={{ listStyle: 'inside' }}>
          <li>Практический опыт показывает, что начало повседневно</li>
          <li>и позволяет оценить значение модели разви</li>
          <li>проверки ключевых компонентов пл</li>
          <li>проверки ключевых компонентов пл</li>
          <li>проверки енить значение всесторонне сбаля пр</li>
        </ul>
        Задача организации, в особенности же постоянный количественный рост и сфера нашей активности позволяет оценить
        значение новых предложений! Соображения высшего порядка, а также дальнейшее развитие различных форм деятельности
        позволяет оценить
        <p>
          Задача организации, в особенности же постоянный количественный рост и сфера нашей активности позволяет оценить
          значение новых предложений! Соображения высшего порядка, а также дальнейшее развитие различных форм
          деятельности позволяет оценить
        </p>
        <p>
          значение всесторонне сбалансированных нововведений. Разнообразный и богатый опыт реализация намеченного плана
          развития обеспечивает актуальность дальнейших направлений развития проекта? Соображения высшего порядка, а
          также реализация намеченного плана развития представляет собой интересный эксперимент проверки всесторонне
          сбалансированных нововведений?
        </p>
        Повседневная практика показывает,...
      </Document>

      <PointsContainer>
        <Controller
          control={control}
          name={'agencyAgreement'}
          render={({ field: { onChange, value } }) => (
            <Point>
              <Checkbox checked={!!value} onChange={onChange} />
              <PointName>
                {t('form.agreeWith')}
                <a href={'#'}>{t('form.agencyAgreementOffer')}</a>
              </PointName>
            </Point>
          )}
        />

        <Controller
          control={control}
          name={'dataPolicyAgreement'}
          render={({ field: { onChange, value } }) => (
            <Point>
              <Checkbox checked={!!value} onChange={onChange} />
              <PointName>
                {t('form.agreeWith')}
                <a href={'#'}>{t('form.dataProcessingPolicy')}</a>
              </PointName>
            </Point>
          )}
        />
      </PointsContainer>

      <Button disabled={!(!!agencyAgreement && !!dataPolicyAgreement)} onClick={onContinue}>
        {t('form.continue')}
      </Button>
    </Container>
  );
};

const Container = styled.div``;

const Document = styled.div`
  font-size: 13px;
  color: var(--gray3);
  margin-bottom: 40px;
`;

const Title = styled.div`
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.07;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 40px;
`;

const Point = styled.div`
  display: flex;
  gap: 10px;
  margin-bottom: 22px;
`;

const PointName = styled.div`
  font-size: 15px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: var(--text);

  & > a {
    cursor: pointer;
    color: var(--purple);
    text-decoration: underline;

    &:hover {
      text-decoration: none;
    }
  }
`;

const PointsContainer = styled.div`
  margin-bottom: 40px;

  & > div:last-child {
    margin: 0;
  }
`;

import React, { FC } from 'react';
import styled from 'styled-components';
import { Checkbox } from '../../../../ui/checkbox';
import { Controller, useFormContext } from 'react-hook-form';
import { ContractFormValues, ContractFromVariants } from '../../contract-form';
import { WarningGrayIcon } from '../../../../../assets/svg';
import { useTranslation } from 'next-i18next';
import { Button } from '../../../../ui/button';
import { ContractFormSteps, StepsState } from '../../../../pages/contract/contract';
import { CONDITION_DESKTOP } from '../../../../../common/constants';

interface VariantFormProps {
  changeStepHandle: (step: ContractFormSteps) => void;
  changeProgressBarStateHandle: (data: Partial<StepsState>) => void;
}

export const VariantForm: FC<VariantFormProps> = ({ changeStepHandle, changeProgressBarStateHandle }) => {
  const { control, watch } = useFormContext<ContractFormValues>();
  const { t } = useTranslation('contract.page');
  const variantValue = watch('variant');

  const onContinue = () => {
    changeProgressBarStateHandle({
      variant: true,
      props: false,
      offer: false,
    });
    changeStepHandle(ContractFormSteps.offer);
  };

  return (
    <Container>
      <Title>{t('form.selectHowWork')}</Title>
      <PointsContainer>
        <Controller
          control={control}
          name={'variant'}
          render={({ field: { value, onChange } }) => (
            <>
              <Point>
                <Checkbox
                  checked={value === ContractFromVariants.INDIVIDUAL}
                  onChange={() => {
                    onChange(ContractFromVariants.INDIVIDUAL);
                  }}
                />
                <PointName>{t('form.individual')}</PointName>
              </Point>
              <Point>
                <Checkbox
                  checked={value === ContractFromVariants.SELF_EMPLOYED}
                  onChange={() => {
                    onChange(ContractFromVariants.SELF_EMPLOYED);
                  }}
                />
                <PointName>{t('form.selfEmployed')}</PointName>
              </Point>

              {/*<Point>*/}
              {/*  <Checkbox checked={value === ContractFromVariants.IP} onChange={() => {*/}
              {/*    onChange(ContractFromVariants.IP);*/}
              {/*  }} />*/}
              {/*  <PointName>{t('form.IP')}</PointName>*/}
              {/*</Point>*/}

              {/*<Point>*/}
              {/*  <Checkbox checked={value === ContractFromVariants.OOO} onChange={() => {*/}
              {/*    onChange(ContractFromVariants.OOO);*/}
              {/*  }} />*/}
              {/*  <PointName>{t('form.OOO')}</PointName>*/}
              {/*</Point>*/}
            </>
          )}
        />
      </PointsContainer>

      <Warning>
        <div>
          <WarningGrayIcon fill={'#ffcc00'} />
        </div>
        <div>{t('form.warning')}</div>
      </Warning>

      <Button disabled={!variantValue} onClick={onContinue} type="button">
        {t('form.continue')}
      </Button>
    </Container>
  );
};

const Container = styled.div``;

const Title = styled.div`
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.07;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 40px;
`;

const PointsContainer = styled.div`
  margin-bottom: 40px;

  & > div:last-child {
    margin: 0;
  }
`;

const Point = styled.div`
  display: flex;
  gap: 10px;
  margin-bottom: 22px;

  &:hover {
    cursor: pointer;
  }
`;

const PointName = styled.div`
  font-size: 18px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.11;
  letter-spacing: normal;
  color: var(--text);
`;

const Warning = styled.div`
  padding: 17px 40px 20px 15px;
  border-radius: 8px;
  background-color: rgb(255 204 0 / 20%);
  display: flex;
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.15;
  letter-spacing: normal;
  color: var(--gray2);
  gap: 10px;
  width: 100%;
  margin-bottom: 40px;

  ${CONDITION_DESKTOP} {
    width: 70%;
  }
`;

import React from 'react';
import { ContractFormSteps, StepsState } from '../../pages/contract/contract';
import VariantForm from './forms/variant-form';
import OfferForm from './forms/offer-form';
import PropsForm from './forms/props-form';

export function renderContractForm(
  step: ContractFormSteps,
  changeStepHandle: (step: ContractFormSteps) => void,
  changeProgressBarStateHandle: (data: Partial<StepsState>) => void,
) {
  switch (step) {
    case ContractFormSteps.variant:
      return (
        <VariantForm changeProgressBarStateHandle={changeProgressBarStateHandle} changeStepHandle={changeStepHandle} />
      );
    case ContractFormSteps.offer:
      return (
        <OfferForm changeProgressBarStateHandle={changeProgressBarStateHandle} changeStepHandle={changeStepHandle} />
      );
    case ContractFormSteps.props:
      return <PropsForm changeStepHandle={changeStepHandle} />;
    default:
      break;
  }
}

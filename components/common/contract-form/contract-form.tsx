import React, { FC } from 'react';
import styled from 'styled-components';
import { ContractFormSteps, StepsState } from '../../pages/contract/contract';
import { useForm, FormProvider } from 'react-hook-form';
import { renderContractForm } from './renderContractForm';

interface ContractFormProps {
  step: ContractFormSteps;
  changeStepHandle: (step: ContractFormSteps) => void;
  changeProgressBarStateHandle: (data: Partial<StepsState>) => void;
}

export enum ContractFromVariants {
  INDIVIDUAL = 'INDIVIDUAL',
  IP = 'IP',
  OOO = 'OOO',
  SELF_EMPLOYED = 'SELF_EMPLOYED',
}

export interface ContractFormValues {
  variant: ContractFromVariants;
  agencyAgreement?: boolean;
  agencyAgreementTimestamp?: string;
  dataPolicyAgreement?: boolean;
  dataPolicyAgreementTimestamp?: string;
  fullCompanyName: string;
  INN: string;
  selfEmployedId: string;
  KPP: string;
  directorFullNameNominativeCase: string;
  directorFullNameParentCase: string;
  directorPost: string;
  legalAddress: string;
  physicalAddress: string;
  phone: string;
  email: string;
  bankName: string;
  BIK: string;
  correspondedAccount: string;
  paymentAccount: string;
  EQRULPhoto: string;
  certificateOfRegistrationPhoto: File;
  fullName: string;
  OGRNIP: string;
  OGRNIPDate: string;
  addressOfRegistration: string;
  passportPhoto: File;
  selfWithPassport: File;
  registrationInPassportPhoto: File;
  EQURL: File;
  birthday: string;
  birthPlace: string;
  passportSeries: string;
  passportNumber: string;
  whenPassportIssued: string;
  whoPassportIssued: string;
}

export const ContractForm: FC<ContractFormProps> = ({ step, changeProgressBarStateHandle, changeStepHandle }) => {
  const methods = useForm<ContractFormValues>({
    defaultValues: {},
  });

  return (
    <Container>
      <FormProvider {...methods}>
        {renderContractForm(step, changeStepHandle, changeProgressBarStateHandle)}
      </FormProvider>
    </Container>
  );
};

const Container = styled.form`
  width: 80%;
  margin: 0 auto;
`;

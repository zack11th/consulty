import { StoreApple, StoreGoogle } from 'assets/svg';
import { CONDITION_DESKTOP } from 'common/constants';
import { routes } from 'common/routes';
import { ImageWithRetina } from 'components/ui/image-with-retina';
import Link from 'next/link';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

const Footer: FC = () => {
  const { t } = useTranslation('footer.component');

  return (
    <Wrapper>
      <Container className="container">
        <LinksArea>
          <ColumnLinks>
            <Link href={routes.expertLanding}>
              <StyledLink href={routes.expertLanding}>{t('links.howToBeExpert')}</StyledLink>
            </Link>
            {/*TODO: add link to соглашение на обработку персональных данных */}
            <StyledLink href="">{t('links.personalSecurity')}</StyledLink>
            <StyledLink href="https://storage.yandexcloud.net/consulty-prod/terms_and_conditions.pdf" target="_blank">
              {t('links.termsOfUse')}
            </StyledLink>
          </ColumnLinks>
          <ColumnLinks>
            <Link href={routes.home}>
              <StyledLink href={routes.home}>{t('links.aboutProject')}</StyledLink>
            </Link>
            {/*TODO: add link to договор-оферта */}
            <Link href={''}>
              <StyledLink href={''}>{t('links.offerAgreement')}</StyledLink>
            </Link>
            <StyledLink href="https://storage.yandexcloud.net/consulty-prod/privacy_policy.pdf" target="_blank">
              {t('links.cookieAgreement')}
            </StyledLink>
          </ColumnLinks>
        </LinksArea>

        <MobileAppArea>
          <ImageWrapper>
            <ImageWithRetina src="/img/phone_mockup.png" alt="phone" />
          </ImageWrapper>
          <MobileAppLinks>
            <MobileAppText>{t('takeMobile')}</MobileAppText>
            {/*TODO: add link to markets */}
            <Link href={''}>
              <StoreLinkWrapper>
                <StoreApple />
              </StoreLinkWrapper>
            </Link>
            <StoreLinkWrapper href="https://play.google.com/store/apps/details?id=com.consulty" target="_blank">
              <StoreGoogle />
            </StoreLinkWrapper>
          </MobileAppLinks>
        </MobileAppArea>

        <BottomArea>
          <HelpText>
            {t('support.ifYouHaveAsk')}{' '}
            <span>
              <SupportLink href="mailto:support@consulty.online" target="_blank">
                support@consulty.online
              </SupportLink>{' '}
              {t('support.or')}{' '}
              <SupportLink
              //TODO: add link to telegram
              >
                Telegram
              </SupportLink>
            </span>
          </HelpText>
          <Copyright>{t('copyright')}</Copyright>
        </BottomArea>
      </Container>
    </Wrapper>
  );
};

export default Footer;

const Wrapper = styled.div`
  background-color: var(--gray5);
`;
const Container = styled.footer`
  &&&{
    display: grid;
    grid-template-areas:
      'links'
      'mobile'
      'bottom';
    padding: 20px;
    ${CONDITION_DESKTOP} {
      padding: 40px 20px 0;
      grid-template-columns: 3fr 2fr;
      grid-template-areas:
        'links mobile'
        'bottom mobile';
    }
  }
`;
const LinksArea = styled.div`
  grid-area: links;
  display: flex;
  flex-direction: column;
  ${CONDITION_DESKTOP} {
    flex-direction: row;
  }
`;
const ColumnLinks = styled.div`
  display: flex;
  flex-direction: column;
  ${CONDITION_DESKTOP} {
    margin-right: 50px;
  }
`;
const StyledLink = styled.a`
  font-size: 15px;
  line-height: 20px;
  margin-bottom: 20px;
  &:hover {
    color: var(--purple3);
  }
  ${CONDITION_DESKTOP} {
    margin-bottom: 15px;
  }
`;
const BottomArea = styled.div`
  grid-area: bottom;
`;
const HelpText = styled.p`
  margin-top: 20px;
  margin-bottom: 20px;
  font-size: 13px;
  line-height: 20px;
  span {
    display: block;
  }
  ${CONDITION_DESKTOP} {
    margin-top: 10px;
    margin-bottom: 15px;
    span {
      display: inline;
    }
  }
`;
const SupportLink = styled.a`
  color: var(--purple);
`;
const Copyright = styled.p`
  font-size: 13px;
  line-height: 20px;
  color: var(--gray);
`;
const MobileAppArea = styled.div`
  grid-area: mobile;
  display: flex;
  align-items: center;
  margin-top: 30px;
  ${CONDITION_DESKTOP} {
    margin-top: 0;
  }
`;
const ImageWrapper = styled.div`
  width: 33%;
  img {
    width: 100%;
  }
`;
const MobileAppLinks = styled.div`
  flex: 1;
  margin-left: 30px;
  display: flex;
  flex-direction: column;
`;
const MobileAppText = styled.p`
  font-weight: 600;
  font-size: 13px;
  line-height: 20px;
  margin-bottom: 16px;
  ${CONDITION_DESKTOP} {
    font-size: 15px;
    line-height: 35px;
  }
`;
const StoreLinkWrapper = styled.a`
  margin-bottom: 11px;
  cursor: pointer;
`;

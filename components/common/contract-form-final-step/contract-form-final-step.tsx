import React, { FC } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { Button } from '../../ui/button';
import { Badge } from '../../ui/badge';
import { useRouter } from 'next/router';
import { routes } from '../../../common/routes';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';
import { getDigitsEventDateFromString } from '../../../utils';

export const ContractFormFinalStep: FC = () => {
  const { t, i18n } = useTranslation('contract.page');
  const router = useRouter();
  const contract = useSelector(selectors.selectContract);

  const onContinue = () => {
    router.push(`${routes.profile}?activeTab=wallet`);
  };

  return (
    <Container>
      <Title>{t('form.almostDone')}</Title>

      <div>
        <SmallTitle>
          {t('form.contractData', {
            number: contract.id,
            date: getDigitsEventDateFromString(contract.createdAt, i18n.language),
          })}
        </SmallTitle>

        <Status>
          <Text>{t('form.status')}</Text>
          {/*TODO Contract status*/}
          <Badge label={t(`contractStatus.${contract.status}`)} />
        </Status>

        <Text>{t('form.finalNote')}</Text>
      </div>

      <Button onClick={onContinue}>{t('form.continue')}</Button>
    </Container>
  );
};

const Container = styled.div`
  width: 80%;
  margin: 40px auto;

  & > div > div {
    margin-bottom: 10px;
  }

  & > div > div:last-child {
    margin-bottom: 0;
  }
  
  &>button{
    margin-top: 40px;
  }
  
  ${CONDITION_DESKTOP}{
    width: 50%;
    margin: 0 auto;
  }
  
}
`;

const Title = styled.div`
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.07;
  letter-spacing: normal;
  margin-bottom: 40px;
  color: var(--paleText);
`;

const SmallTitle = styled.div`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: var(--text);
`;

const Text = styled(SmallTitle)`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  line-height: 1.43;
`;

const Status = styled.div`
  display: flex;
  gap: 10px;
`;

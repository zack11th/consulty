import React, { FC, useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Input } from '../../ui/input';
import { useTranslation } from 'next-i18next';
import styled from 'styled-components';
import { Select } from '../../ui/select';
import { Button } from '../../ui/button';
import { InputPhone } from '../../ui/input-phone';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { generateMouthOptions } from './generateMouthOptions';
import { getClearTime } from '../../../utils';
import { useAppDispatch } from '../../../hooks/redux';
import { getFullYear } from '../../../utils';
import { toast } from 'react-toastify';
import { fetchMe, fetchUpdateIamProfile } from '../../../store/ducks/profile/actions';
import { unwrapResult } from '@reduxjs/toolkit';
import { emailPattern, namePattern } from '../../../constants';
import { api, User } from 'api';
import { useDebounce } from 'use-debounce/lib';
import { captureError } from 'utils/captureError';

interface SettingFormValues {
  firstName: string;
  lastName: string;
  number: string;
  email: string;
  birthDay?: {
    day: { label: string; value: string };
    mouth: { label: string; value: string };
    year: { label: string; value: string };
  };
}

const optionsDays: any[] = [];
const optionsYears: any[] = [];

for (let i = 1; i < 32; i++) {
  optionsDays.push({ value: `${i}`, label: `${i}` });
}
const year = new Date().getFullYear();
for (let i = year - 100; i < year; i++) {
  optionsYears.push({ value: String(i), label: String(i) });
}
optionsYears.reverse();

interface SettingFormProps {
  user: User;
}

export const SettingForm: FC<SettingFormProps> = ({ user }) => {
  const { t } = useTranslation('profile.page');
  const { t: error } = useTranslation('errors.messages');
  const { t: u } = useTranslation('utils');

  const mouthOptions = generateMouthOptions(t);

  let birthDate:
    | {
        day: { value: string; label: string };
        mouth: { value: string; label: string };
        year: { value: string; label: string };
      }
    | undefined;

  if (user.birthDate) {
    const fullYear = getFullYear(user.birthDate);
    birthDate = {
      day: { value: fullYear.day, label: fullYear.day },
      mouth: { value: fullYear.mouth, label: mouthOptions.find((o) => o.value === fullYear.mouth)?.label || '' },
      year: { value: fullYear.year, label: fullYear.year },
    };
  } else {
    birthDate = undefined;
  }

  const {
    handleSubmit,
    control,
    formState: { isDirty, errors },
    reset,
    watch,
    clearErrors,
    setError,
  } = useForm<SettingFormValues>({
    defaultValues: {
      firstName: user.firstName,
      lastName: user.lastName,
      number: user.phone,
      birthDay: birthDate,
      email: user.email,
    },
  });

  const dispatch = useAppDispatch();
  const email = watch('email');
  const [emailForCheck] = useDebounce(email, 500);

  useEffect(() => {
    const checkEmailIsUniq = async () => {
      try {
        const { data } = await api.V1UsersApi.usersControllerEmailIsUniq(emailForCheck);
        if (!data) {
          setError('email', { message: u('emailIsNoUniq') });
        } else {
          clearErrors('email');
        }
      } catch (e: any) {
        captureError(e);
      }
    };
    if (Boolean(emailForCheck) && user.email !== email) {
      checkEmailIsUniq();
    }
  }, [emailForCheck]);

  const onSubmit = async ({ birthDay, firstName, lastName, email }: SettingFormValues) => {
    let day;
    let mouth;
    let year;
    let date;
    
    if(birthDay?.day && birthDay.mouth && birthDay.year){
       day = parseInt(birthDay.day.value);
       mouth = parseInt(birthDay.mouth.value);
       year = parseInt(birthDay.year.value);
       date = new Date();
       date.setFullYear(year, mouth, day);
    }

    const sameEmail = user.email === email;
    
    try {
      const res = await dispatch(
        fetchUpdateIamProfile({
          firstName: firstName,
          lastName: lastName,
          email: sameEmail ? undefined : email,
          birthDate: date && getClearTime(date),
        }),
      );
      if (unwrapResult(res)) {
        toast.success(t('saveInfo'));
        api.V1UsersApi.usersControllerSendVerificationEmail();
        dispatch(fetchMe());
      }
    } catch (e: any) {
      captureError(e);
      toast.error(e.message || t('notSuccessSaveInfo'));
      reset();
    }
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Title>{t('title')}</Title>
        <Controller
          name={'firstName'}
          control={control}
          rules={{
            pattern: { value: namePattern, message: error('incorrectData') },
            required: { value: true, message: error('required') },
          }}
          render={({ field, fieldState }) => (
            <Input
              {...field}
              placeholder={t('form.firstName')}
              label={t('form.firstName')}
              error={fieldState.error?.message}
            />
          )}
        />

        <Controller
          name={'lastName'}
          control={control}
          rules={{
            pattern: { value: namePattern, message: error('incorrectData') },
            required: { value: true, message: error('required') },
          }}
          render={({ field, fieldState }) => (
            <Input
              {...field}
              placeholder={t('form.lastName')}
              label={t('form.lastName')}
              error={fieldState.error?.message}
            />
          )}
        />
        <Label>{t('form.birthDay.title')}</Label>
        <BirthDayField>
          <Controller
            name={'birthDay.day'}
            control={control}
            render={({ field, fieldState }) => (
              <Select
                {...field}
                options={optionsDays}
                placeholder={t('form.birthDay.day')}
                error={fieldState.error?.message}
                instanceId="select-birth-day"
              />
            )}
          />
          <Controller
            control={control}
            name={'birthDay.mouth'}
            render={({ field, fieldState }) => (
              <Select
                {...field}
                placeholder={t('form.birthDay.mouth')}
                options={mouthOptions}
                error={fieldState.error?.message}
                instanceId="select-birth-mouth"
              />
            )}
          />
          <Controller
            control={control}
            name={'birthDay.year'}
            render={({ field, fieldState }) => (
              <Select
                {...field}
                options={optionsYears}
                placeholder={t('form.birthDay.year')}
                error={fieldState.error?.message}
                instanceId="select-birth-year"
              />
            )}
          />
        </BirthDayField>

        <Controller
          name={'number'}
          control={control}
          render={({ field, fieldState }) => (
            <InputPhone {...field} label={t('form.number')} disabled={true} error={fieldState.error?.message} />
          )}
        />

        <Controller
          name={'email'}
          control={control}
          rules={{
            pattern: {
              value: emailPattern,
              message: error('incorrectEmail'),
            },
            required: { value: true, message: error('required') },
          }}
          render={({ field, fieldState }) => (
            <Input
              {...field}
              label={t('form.email')}
              placeholder={t('form.email')}
              error={fieldState.error?.message}
              successMessage={user.isEmailVerified && !fieldState.error?.message ? t('form.emailVerified') : undefined}
            />
          )}
        />
        <Label>{t('form.emailReason')}</Label>

        <Button type={'submit'} disabled={!isDirty}>
          {t('form.submit')}
        </Button>
      </Form>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 90px;

  ${CONDITION_DESKTOP} {
    width: calc(270px * 2 + 20px);
  }
`;

const Form = styled.form`
  max-width: 420px;
  width: 100%;
  margin: 0 10px;

  & > div {
    margin-top: 25px;
  }

  & > button {
    margin-top: 60px;
    width: 100%;

    ${CONDITION_DESKTOP} {
      width: unset;
    }
  }
`;

const BirthDayField = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  gap: 25px;

  ${CONDITION_DESKTOP} {
    flex-direction: row;
    gap: 0;
  }

  & > div {
    width: 100%;

    ${CONDITION_DESKTOP} {
      width: 30%;
    }
  }
`;

const Title = styled.h2`
  text-align: left;
  margin: 0;
`;

const Label = styled.label`
  font-weight: 500;
  font-size: 15px;
  color: var(--gray);
  margin-top: 10px;
  display: block;
`;

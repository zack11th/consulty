import React, { FC, useMemo } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { Button } from '../../ui/button';
import { ImageWithRetina } from '../../ui/image-with-retina';
import { api, InvoiceStatusEnum, User, UserCategoryMeta } from '../../../api';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks';
import { toast } from 'react-toastify';
import { useAppDispatch } from '../../../hooks/redux';
import { actions } from '../../../store/ducks';
import { routes } from '../../../common/routes';
import { useRouter } from 'next/router';
import { checkCanUseFreeToken, findFirstNotUsedPromocode } from 'utils';
import { captureError } from 'utils/captureError';

interface ConsultationPaymentModalProps {
  userCategoryMeta: UserCategoryMeta;
  price: number;
  theme: string;
  hasFreeConsultations: boolean;
  expert: User;
  onRefuse: () => void;
}

export const ConsultationPaymentModal: FC<ConsultationPaymentModalProps> = ({
  price,
  theme,
  hasFreeConsultations,
  userCategoryMeta,
  expert,
  onRefuse,
}) => {
  const { t } = useTranslation('expert.page');

  const dispatch = useAppDispatch();
  const router = useRouter();
  const userPromocodes = useSelector(selectors.profile.selectUserPromocodes);
  const chatRooms = useSelector(selectors.chatRooms.selectChatRooms);

  const notUsedPromocode = findFirstNotUsedPromocode(userPromocodes);

  const redirectUrl = useMemo(() => {
    const chatRoomWithThisExpert = chatRooms?.find((room) => room.expertId === expert.id);
    const redirectUrl = chatRoomWithThisExpert
      ? `${window.location.origin}${routes.chat}?activeTab=consult&roomId=${chatRoomWithThisExpert.id}`
      : `${window.location.origin}${routes.chat}?activeTab=consult&expertId=${expert.id}`;
    return redirectUrl;
  }, [chatRooms, expert]);

  const onPayWithToken = async () => {
    if (!!notUsedPromocode) {
      try {
        const { data } = await api.V1ConsultationsApi.consultationsControllerCreateOneFromUserMetaWithPromocode({
          userCategoryMetaId: userCategoryMeta.id,
          userPromocodeId: notUsedPromocode.id,
          redirectUrl,
        });
        dispatch(actions.chatRooms.addChatRoom(data.chatRoom));
        dispatch(actions.profile.fetchSelfPromocode());
        await router.push({
          pathname: routes.chat,
          query: {
            activeTab: 'consult',
            roomId: data.chatRoomId,
          },
        });
      } catch (e: any) {
        captureError(e);
        toast.error(e.message || t('notSuccessPayWithToken'));
      }
    }
  };

  const onPayWithMoney = async () => {
    try {
      const { data } = await api.V1ConsultationsApi.consultationsControllerCreateOneFromUserCategoryMeta({
        userCategoryMetaId: userCategoryMeta.id,
        redirectUrl,
      });
      const paymentUrl = data.invoices.find((invoice) => invoice.invoice?.status === InvoiceStatusEnum.Pending)?.invoice
        ?.checkoutUrl;

      if (paymentUrl) {
        window.location.href = paymentUrl;
      }
      onRefuse();
    } catch (e: any) {
      captureError(e);
      toast.error(e.message || t('somethingWrong'));
    }
  };

  return (
    <Container>
      <Title>{t('consultationPayment')}</Title>
      <Note>{t('ifConsultationDidntLike')}</Note>

      <Content>
        <ContentItem>
          <PropertyLabel>{t('theme')}</PropertyLabel>
          <PropertyValue>{theme}</PropertyValue>
        </ContentItem>

        <ContentItem>
          <PropertyLabel>{t('price')}</PropertyLabel>
          <PropertyValue>{t('priceNote', { price: price })}</PropertyValue>
        </ContentItem>

        <ContentItem>
          <PropertyLabel>{t('duration')}</PropertyLabel>
          <PropertyValue>{t('messagesOrHour')}</PropertyValue>
        </ContentItem>
      </Content>

      <ButtonsContainer>
        {!!hasFreeConsultations && checkCanUseFreeToken(userPromocodes, expert) && (
          <Button onClick={onPayWithToken} rightIcon={<CoinIcon src="/img/coin.png" alt="coin" />}>
            {t('payWithToken')}
          </Button>
        )}

        <Button onClick={onPayWithMoney}>{t('payWithPrice', { price: price })}</Button>

        <Button bordered onClick={onRefuse}>
          {t('refuse')}
        </Button>
      </ButtonsContainer>
    </Container>
  );
};

const Container = styled.div``;

const Title = styled.div`
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 10px;
`;

const Note = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.38;
  letter-spacing: normal;
  color: var(--text);
`;

const Content = styled.div`
  padding: 30px 0;

  & > div {
    margin-bottom: 15px;
  }
  & > div:last-child {
    margin-bottom: 0;
  }
`;

const PropertyLabel = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.54;
  letter-spacing: normal;
  color: var(--gray2);
`;

const PropertyValue = styled(PropertyLabel)`
  color: var(--text);
`;

const ContentItem = styled.div``;

const ButtonsContainer = styled.div`
  & > button {
    margin-bottom: 10px;
    width: 100%;
  }

  & > button:last-child {
    margin-bottom: 0;
  }
`;
const CoinIcon = styled(ImageWithRetina)`
  position: absolute;
  left: 0;
  top: 0;
  transform: translateY(-50%);
`;

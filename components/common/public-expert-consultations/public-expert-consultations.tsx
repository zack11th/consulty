import React, { FC, useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import { Category, Rubric, User, UserCategoryMeta } from '../../../api';
import { getUserConsultationsRubricMap } from './getUserConsultationsRubricMap';
import { useTranslation } from 'next-i18next';
import { Button } from '../../ui/button';
import { Wallet } from '../../../assets/svg';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { ImageWithRetina } from '../../ui/image-with-retina';
import Tip from '../../ui/tip';
import Image from 'next/image';
import Modal from '../modal';
import { ConsultationPaymentModal } from './consultation-payment-modal';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';
import { checkCanUseFreeToken } from 'utils';
import { useAppSelector } from 'hooks/redux';

interface PublicExpertConsultationsProps {
  profile: User;
  rubrics: Rubric[];
  categories: Category[];
}

interface PaymentModalState {
  price: number;
  theme: string;
  userCategoryMeta: UserCategoryMeta;
}

const dots = new Array(200).fill('.').join('');

export const PublicExpertConsultations: FC<PublicExpertConsultationsProps> = ({ profile, rubrics, categories }) => {
  const { t } = useTranslation('expert.page');

  const categoriesMap = new Map<number, Category>();
  const rubricsMap = new Map<number, Rubric>();

  rubrics.forEach((r) => rubricsMap.set(r.id, r));
  categories.forEach((c) => categoriesMap.set(c.id, c));

  const consultationsRubrics = useMemo(
    () => getUserConsultationsRubricMap(profile.categories, categoriesMap),
    [profile.categories, categoriesMap],
  );

  const [isPaymentModalOpen, setIsPaymentModalOpen] = useState<boolean>(false);
  const [paymentModalState, setPaymentModalState] = useState<PaymentModalState>({} as PaymentModalState);
  const userPromocodes = useSelector(selectors.selectUserPromocodes);
  const [canUseFreeToken, setCanUseFreeToken] = useState(false);
  const isUserAuthorized = useAppSelector(selectors.selectIsAuthentication);

  const onPayment = (state: PaymentModalState) => () => {
    setPaymentModalState({ ...state });
    setIsPaymentModalOpen(true);
  };

  const onClosePaymentModal = () => {
    setIsPaymentModalOpen(false);
  };

  useEffect(() => {
    setCanUseFreeToken(checkCanUseFreeToken(userPromocodes, profile));
  }, [userPromocodes]);

  return (
    <>
      <Modal isVisible={isPaymentModalOpen} onClose={onClosePaymentModal}>
        <ConsultationPaymentModal
          price={paymentModalState.price}
          theme={paymentModalState.theme}
          userCategoryMeta={paymentModalState.userCategoryMeta}
          hasFreeConsultations={!!profile.hasFreeConsultations}
          expert={profile}
          onRefuse={onClosePaymentModal}
        />
      </Modal>
      <Container>
        <Title>{t('expertConsultations')}</Title>

        {Array.from(consultationsRubrics.keys()).map((rubricId) => (
          <RubricContainer key={rubricId}>
            <RubricTitle>{rubricsMap.get(rubricId)!.name}</RubricTitle>

            {consultationsRubrics.get(rubricId)!.map((category) => (
              <CategoryContainer key={category.categoryId}>
                <CategoryName>
                  <div>
                    <Name>{category.category.name}</Name>
                    <Dots>{dots}</Dots>
                  </div>
                </CategoryName>

                <CategoryPrice>
                  <span>{t('averagePrice', { price: category.price })}</span>
                  <ButtonContainer>
                    <MobileButton
                      leftIcon={<Wallet />}
                      color={'gray'}
                      onClick={onPayment({
                        price: category.price,
                        theme: category.category.name,
                        userCategoryMeta: category,
                      })}
                    />

                    {isUserAuthorized && (
                      <DesktopButton
                        size="small"
                        bordered
                        color={'gray'}
                        onClick={onPayment({
                          price: category.price,
                          theme: category.category.name,
                          userCategoryMeta: category,
                        })}
                      >
                        {t('pay')}
                      </DesktopButton>
                    )}
                    {!!profile.hasFreeConsultations && canUseFreeToken && (
                      <CoinContainer>
                        <Tip text={t('canPayWithToken')}>
                          <ImageWithRetina src="/img/coin.png" alt="coin" />
                        </Tip>
                      </CoinContainer>
                    )}
                  </ButtonContainer>
                </CategoryPrice>
              </CategoryContainer>
            ))}
          </RubricContainer>
        ))}

        <MoneyGuard>
          <div>
            <Image src={'/img/shield@3x.png'} width={35} height={45} alt={'money shield'} />
          </div>
          <div>
            <ShieldTitle>{t('moneyShieldTitle')}</ShieldTitle>
            <ShieldNote>{t('moneyShieldNote')}</ShieldNote>
          </div>
        </MoneyGuard>
      </Container>
    </>
  );
};

const Container = styled.div`
  padding-bottom: 60px;

  ${CONDITION_DESKTOP} {
    max-width: 700px;
  }
`;

const RubricContainer = styled.div`
  padding-bottom: 30px;

  ${CONDITION_DESKTOP} {
    padding-bottom: 40px;
  }
`;

const Title = styled.div`
  font-size: 21px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.19;
  letter-spacing: normal;
  color: var(--text);
  padding-bottom: 20px;

  ${CONDITION_DESKTOP} {
    padding-bottom: 40px;
    font-size: 22px;
  }
`;

const RubricTitle = styled(Title)`
  font-size: 17px;
  line-height: 1.1;
  letter-spacing: normal;
  padding-bottom: 15px;

  ${CONDITION_DESKTOP} {
    font-size: 19px;
  }
`;
const CategoryContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  gap: 10px;
  padding: 10px 0;
  max-width: 80vw;

  ${CONDITION_DESKTOP} {
    max-width: unset;
  }
`;
const CategoryName = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.1;
  letter-spacing: normal;
  color: var(--text);
  display: flex;
  align-items: center;
  width: 60%;
  position: relative;
  background-color: var(--white);

  ${CONDITION_DESKTOP} {
    font-size: 15px;
  }

  & > div {
    overflow: hidden;
  }
`;

const CategoryPrice = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
  justify-content: space-between;

  ${CONDITION_DESKTOP} {
    gap: 20px;
  }

  & > span {
    font-size: 14px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.1;
    letter-spacing: normal;
    color: var(--gray);
    width: 60px;
  }
`;
const Dots = styled.span`
  color: var(--gray3);
`;
const Name = styled.span`
  ${CONDITION_DESKTOP} {
    white-space: nowrap;
  }
`;

const DesktopButton = styled(Button)`
  display: none;
  padding: 5px 10px;
  color: var(--purple);
  background-color: var(--white);
  border: 1px solid var(--purple);

  ${CONDITION_DESKTOP} {
    display: unset;
  }
`;
const MobileButton = styled(Button)`
  display: unset;
  padding: 5px 16px;
  justify-content: center;
  height: 25px;
  background-color: var(--gray7);

  :hover {
    background-color: var(--gray3);
  }

  & > div {
    margin: 0;
  }

  & > span {
    display: none;
  }

  ${CONDITION_DESKTOP} {
    display: none;
  }
`;

const CoinContainer = styled.div`
  display: none;

  ${CONDITION_DESKTOP} {
    display: unset;
  }
`;

const MoneyGuard = styled.div`
  padding: 20px;
  border-radius: 8px;
  background-color: rgb(255 204 0 / 15%);
  width: 90%;
  display: flex;
  align-items: center;
  gap: 20px;
`;

const ShieldTitle = styled.div`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 5px;
`;

const ShieldNote = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--gray);
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`;

import React, { FC } from 'react';
import styled from 'styled-components';
import { buildRoute, coverImageStyle } from '../../../utils';
import { useTranslation } from 'next-i18next';
import VerificationMark from '../../../assets/svg/VerificationMark';
import { ChatSmallIcon, GrayWallet, Star } from '../../../assets/svg';
import { Achievement } from '../../../api';
import { renderAchievement } from './renderAchievement';
import { CONDITION_DESKTOP, DEFAULT_AVATAR } from '../../../common/constants';
import { routes } from '../../../common/routes';
import { useRouter } from 'next/router';

interface ExpertPreviewProps {
  isVerification?: boolean;
  name: string;
  consultationCount: number;
  rating: number;
  reviewCount: number;
  averagePrice: number;
  avatar: string | null;
  videoUrl?: string;
  achievements?: Achievement[];
  id: number;
  isExample?: boolean;
}

export const ExpertPreview: FC<ExpertPreviewProps> = ({
  isVerification,
  name,
  consultationCount,
  reviewCount,
  rating,
  averagePrice,
  avatar,
  videoUrl,
  achievements,
  id,
  isExample = false,
}) => {
  const { t } = useTranslation('expert.preview');
  const router = useRouter();

  const onExpertClick = () => {
    if (isExample) return;
    router.push(buildRoute(routes.expert, { id: id }));
  };

  return (
    <Container>
      <AvatarContainer>
        <GradientBorder isVerification={isVerification && !!videoUrl}>
          <Avatar onClick={onExpertClick} $url={avatar || DEFAULT_AVATAR} />
        </GradientBorder>
        <Video isVisible={isVerification && !!videoUrl}>
          <a href={videoUrl} target={'_blank'}>
            {t('videoCard')}
          </a>
        </Video>
      </AvatarContainer>
      <NameContainer>
        <Name onClick={onExpertClick}>
          <NameWrapper>
            <span>{name}</span>
          </NameWrapper>
          <IconWrapper>{isVerification && <VerificationMark />}</IconWrapper>
        </Name>
        <ConsultationCount>{`${consultationCount} ${t('consultations')}`}</ConsultationCount>
      </NameContainer>
      <FastInfoContainer>
        <Bold>
          <IconContainer>
            <Star />
          </IconContainer>
          <div>{rating !== 0 ? rating.toFixed(1) : "Нет рейтинга"}</div>
        </Bold>
        <Bold>
          <IconContainer>
            <ChatSmallIcon />
          </IconContainer>
          <div>{`${reviewCount} ${t('reviews')}`}</div>
        </Bold>
        <Bold>
          <GrayWallet />
          <div>{`${Math.round(averagePrice)} \u20bd`}</div>
        </Bold>
        <AchievementContainer>{achievements?.map((a) => renderAchievement(a))}</AchievementContainer>
      </FastInfoContainer>
    </Container>
  );
};

const Container = styled.div`
  max-width: 120px;

  ${CONDITION_DESKTOP} {
    max-width: 160px;
  }
`;

interface AvatarProps {
  $url: string | null;
}

const AvatarContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Avatar = styled.div<AvatarProps>`
  border-radius: 50%;
  width: 122px;
  height: 122px;
  margin: 3px;

  &:hover {
    cursor: pointer;
  }

  ${({ $url }) => ({ ...coverImageStyle($url) })}
  ${CONDITION_DESKTOP} {
    width: 142px;
    height: 142px;
  }
`;

interface GradientBorderProps {
  isVerification?: boolean;
}

const GradientBorder = styled.div<GradientBorderProps>`
  ${({ isVerification }) =>
    isVerification &&
    `
  background:
          linear-gradient(var(--white),var(--white)) padding-box,
          linear-gradient(360deg,#5806c9 16%,#993dfa 27%,#ac45ff 57%) border-box;
  `}
  color: #313149;
  padding: 1px;
  border: 3px solid transparent;
  border-radius: 50%;
`;

interface VideoProps {
  isVisible?: boolean;
}

const Video = styled.div<VideoProps>`
  background-image: linear-gradient(to right, #ae4cfe 5%, #993dfa 51%, #812df6);
  border-radius: 12.5px;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: translateY(-12px);
  visibility: ${({ isVisible }) => (isVisible ? 'visible' : 'hidden')};

  & > a {
    color: var(--white);
    font-size: 11px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    padding: 3px 9px;
  }

  text-align: center;
`;

const Name = styled.div`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--black2);
  display: flex;
  align-items: center;
  gap: 5px;

  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }

  & > svg {
    margin-left: 3px;
  }
`;

const ConsultationCount = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--text);
`;

const NameContainer = styled.div`
  padding-bottom: 10px;
`;

const NameWrapper = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  ${CONDITION_DESKTOP} {
  }
`;
const IconWrapper = styled.div`
  width: 10%;
  display: flex;
  transform: scale(1.5);

  & > svg {
    width: 100%;
  }
`;

const FastInfoContainer = styled.div`
  padding-bottom: 15px;
`;

const Bold = styled.div`
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.07;
  letter-spacing: normal;
  color: var(--text);
  display: flex;
  padding-bottom: 7px;
  align-items: center;
  gap: 7px;

  & > div {
    margin-right: 7px;
  }
`;

const AchievementContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 2px;
`;

const IconContainer = styled.div`
  margin: 0;
  width: 19px;
  display: flex;
  justify-content: center;
`;

import { useTranslation } from 'next-i18next';
import React, { FC } from 'react';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';
import { unwrapResult } from '@reduxjs/toolkit';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { api, ConsultationOffer, InvoiceStatusEnum, User } from 'api';
import { DislikeSmallFilled, LikeSmallFilled, StarSmallIcon } from 'assets/svg';
import { CONDITION_DESKTOP, DEFAULT_AVATAR } from 'common/constants';
import { routes } from 'common/routes';
import { Button } from 'components/ui/button';
import { buildRoute, checkCanUseFreeToken, chooseEndingWord, coverImageStyle, findFirstNotUsedPromocode } from 'utils';
import { useAppDispatch } from 'hooks/redux';
import { actions, selectors } from 'store/ducks';
import { ChatRouter } from 'components/pages/chat/chat';
import { ImageWithRetina } from 'components/ui/image-with-retina';
import { captureError } from 'utils/captureError';

interface ResponseMessageProps {
  offer: ConsultationOffer;
  user: User;
}

export const ResponseMessage: FC<ResponseMessageProps> = ({ offer, user }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('chat.page');
  const { t: tUtils } = useTranslation('utils');
  const router: ChatRouter = useRouter();
  const chatRooms = useSelector(selectors.chatRooms.selectChatRooms);
  const creatingChatRoom = useSelector(selectors.chatRooms.selectCreatingChatRoom);
  const userPromocodes = useSelector(selectors.profile.selectUserPromocodes);

  const handleGoExpertProfile = () => {
    router.push(buildRoute(routes.expert, { id: offer.expertId }));
  };

  const handleNavigateToFreeChat = async () => {
    try {
      const existRoom = chatRooms.find((room) => room.expertId === offer.expertId);
      if (existRoom) {
        router.push({ query: { activeTab: 'consult', roomId: existRoom.id } });
      } else {
        const tempRoomId: number = Date.now();
        const actionResult = await dispatch(
          actions.chatRooms.createTempChatRoom({ client: user, expert: offer.expert, tempRoomId }),
        );
        unwrapResult(actionResult);
        router.push({ query: { activeTab: 'consult', roomId: tempRoomId } });
      }
    } catch (error: any) {
      captureError(error);
      toast.error(error.message);
    }
  };

  const handlePay = async () => {
    try {
      const chatRoomWithThisExpert = chatRooms?.find((room) => room.expertId === offer.expertId);
      const redirectUrl = chatRoomWithThisExpert
        ? `${window.location.origin}${routes.chat}?activeTab=consult&roomId=${chatRoomWithThisExpert.id}`
        : `${window.location.origin}${routes.chat}?activeTab=consult&expertId=${offer.expertId}`;

      const { data } = await api.V1ConsultationOffersApi.consultationOffersControllerAcceptOne(offer.id, {
        redirectUrl,
      });
      const paymentUrl = data.invoices.find((invoice) => invoice.invoice?.status === InvoiceStatusEnum.Pending)?.invoice
        ?.checkoutUrl;

      if (paymentUrl) {
        window.location.href = paymentUrl;
      }
    } catch (error: any) {
      captureError(error);
      toast.error(error.message || tUtils('somethingWrong'));
    }
  };

  const handlePayWithToken = async () => {
    try {
      const notUsedPromocode = findFirstNotUsedPromocode(userPromocodes);
      if (notUsedPromocode) {
        const { data } = await api.V1ConsultationOffersApi.consultationOffersControllerAcceptOneWithPromocode(
          offer.id,
          { userPromocodeId: notUsedPromocode.id.toString() },
        );
        dispatch(actions.chatRooms.addChatRoom(data.chatRoom));
        dispatch(actions.profile.fetchSelfPromocode());
        await router.push({
          pathname: routes.chat,
          query: {
            activeTab: 'consult',
            roomId: data.chatRoomId,
          },
        });
      }
    } catch (error: any) {
      captureError(error);
      toast.error(error.message || tUtils('somethingWrong'));
    }
  };

  return (
    <OfferMessageContainer>
      <Title>{t('response.body.consultForMoney', { price: offer.price, currency: '\u20bd' })}</Title>
      <OfferMessage>{offer.description}</OfferMessage>

      <ExpertInfo>
        <ExpertAvatar $url={offer.expert?.avatarUrl} />
        <Info>
          <Name>
            {offer.expert?.firstName} {offer.expert?.lastName}
          </Name>
          <ConsultCount>
            {chooseEndingWord(offer.expert?.expertConsultationsCount, [
              t('response.body.countExpertConsult.one', { count: offer.expert?.expertConsultationsCount }),
              t('response.body.countExpertConsult.two', { count: offer.expert?.expertConsultationsCount }),
              t('response.body.countExpertConsult.many', { count: offer.expert?.expertConsultationsCount }),
            ])}
          </ConsultCount>
          <Rate>
            <StarSmallIcon />
            <span>{offer.expert?.expertRating?.toFixed(2).replace('.', ',') || '0,00'}</span>
          </Rate>
        </Info>

        <Reviews>
          <LikeCount>{offer.expert?.expertLikesCount}</LikeCount>
          <LikeSmallFilled />
          <DislikeCount>{offer.expert?.expertDislikesCount}</DislikeCount>
          <DislikeSmallFilled />
        </Reviews>
      </ExpertInfo>

      <ButtonsRow>
        <StyledBorderedButton
          bordered
          fontSize="13px"
          hasPaddingHorizontal={false}
          onClick={handleGoExpertProfile}
          disabled={creatingChatRoom === 'pending'}
        >
          {t('response.body.button.expertProfile')}
        </StyledBorderedButton>
        <StyledBorderedButton
          bordered
          fontSize="13px"
          hasPaddingHorizontal={false}
          onClick={handleNavigateToFreeChat}
          disabled={creatingChatRoom === 'pending' || !offer.expert?.hasFreeChat}
        >
          {t('response.body.button.askExpert')}
        </StyledBorderedButton>
      </ButtonsRow>
      <Button
        fontSize="13px"
        block
        onClick={handlePay}
        disabled={creatingChatRoom === 'pending'}
        loading={creatingChatRoom === 'pending'}
      >
        {t('response.body.button.payConsult', { price: offer.price, currency: '\u20bd' })}
      </Button>
      {checkCanUseFreeToken(userPromocodes, offer.expert) && (
        <FreeButton
          fontSize="13px"
          block
          onClick={handlePayWithToken}
          disabled={creatingChatRoom === 'pending'}
          loading={creatingChatRoom === 'pending'}
          rightIcon={<CoinIcon src="/img/coin.png" alt="coin" />}
        >
          {t('response.body.button.payWithToken')}
        </FreeButton>
      )}
    </OfferMessageContainer>
  );
};

const OfferMessageContainer = styled.div`
  width: 100%;
  max-width: 450px;
  padding: 20px 15px;
  background-color: var(--gray10);
  border-radius: 12px;
  margin-bottom: 30px;
  ${CONDITION_DESKTOP} {
    margin-left: 55px;
    margin-bottom: 40px;
  }
`;
const Title = styled.h3`
  font-size: 17px;
  font-weight: bold;
  ${CONDITION_DESKTOP} {
    font-size: 15px;
  }
`;
const OfferMessage = styled.div`
  font-size: 13px;
  color: var(--gray);
  margin-top: 10px;
  white-space: pre-wrap;
  overflow-wrap: break-word;
`;
const ExpertInfo = styled.div`
  display: grid;
  margin-top: 35px;
  margin-bottom: 26px;
  gap: 0 10px;
  grid-template-columns: 1fr 70px;
  grid-template-rows: auto auto;
  grid-template-areas:
    'info avatar'
    'review review';
  ${CONDITION_DESKTOP} {
    grid-template-columns: 50px 1fr 1fr;
    grid-template-rows: auto;
    grid-template-areas: 'avatar info review';
  }
`;
const ExpertAvatar = styled.div<{ $url?: string }>`
  grid-area: avatar;
  width: 70px;
  height: 70px;
  border-radius: 50%;
  ${({ $url }) => ({ ...coverImageStyle($url || DEFAULT_AVATAR) })}
  ${CONDITION_DESKTOP} {
    width: 50px;
    height: 50px;
  }
`;
const Info = styled.div`
  grid-area: info;
  font-size: 15px;
  ${CONDITION_DESKTOP} {
    font-size: 13px;
  }
`;
const Name = styled.div`
  font-weight: bold;
  line-height: 20px;
  ${CONDITION_DESKTOP} {
    line-height: 13px;
  }
`;
const ConsultCount = styled.div`
  color: var(--gray);
`;
const Rate = styled.div`
  display: flex;
  align-items: center;
  height: 19px;
  & > span {
    align-self: baseline;
    line-height: 19px;
    margin-left: 3px;
  }
`;
const Reviews = styled.div`
  grid-area: review;
  font-size: 26px;
  line-height: 26px;
  display: flex;
  justify-self: flex-start;
  & > svg {
    margin-top: 2px;
  }
  ${CONDITION_DESKTOP} {
    justify-self: flex-end;
  }
`;
const LikeCount = styled.span`
  color: var(--green);
  margin-right: 3px;
`;
const DislikeCount = styled.span`
  color: var(--red);
  margin-right: 3px;
  margin-left: 13px;
`;
const ButtonsRow = styled.div`
  display: grid;
  gap: 10px;
  grid-template-columns: 1fr 1fr;
  margin-bottom: 15px;
`;
const StyledBorderedButton = styled(Button)`
  &:disabled {
    opacity: 0.2;
    border: 1px solid var(--purple);
  }
`;
const FreeButton = styled(Button)`
  margin-top: 15px;
`;
const CoinIcon = styled(ImageWithRetina)`
  position: absolute;
  left: 0;
  top: 0;
  transform: translateY(-50%);
`;

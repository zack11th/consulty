import React, { FC, useState, useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { useSelector } from 'react-redux';
import { selectors } from '../../../../../store/ducks/profile';
import { useForm, Controller } from 'react-hook-form';
import { Textarea } from '../../../../ui/textarea';
import { Button } from '../../../../ui/button';
import styled from 'styled-components';
import { CONDITION_DESKTOP } from '../../../../../common/constants';
import { api } from '../../../../../api';
import { toast } from 'react-toastify';
import { useAppDispatch } from '../../../../../hooks/redux';
import { fetchContentChangeRequest, fetchSelfContentChangeRequests } from '../../../../../store/ducks/profile/actions';
import { selectContentChangeRequest } from '../../../../../store/ducks/profile/selectors';
import { Badge } from '../../../../ui/badge';
import { captureError } from 'utils/captureError';

interface AboutSelfFormValues {
  aboutSelf: string;
}

export const AboutSelfForm: FC = () => {
  const { t } = useTranslation('profile.page');

  const user = useSelector(selectors.selectUser);
  const contentChangeRequest = useSelector(selectContentChangeRequest);

  let initialAboutSelf = '';

  if (contentChangeRequest && contentChangeRequest.aboutSelf) {
    initialAboutSelf = contentChangeRequest.aboutSelf;
  } else {
    initialAboutSelf = user.aboutSelf || '';
  }

  const dispatch = useAppDispatch();

  const { control, handleSubmit, watch } = useForm<AboutSelfFormValues>({
    defaultValues: {
      aboutSelf: initialAboutSelf,
    },
  });

  const aboutSelf = watch('aboutSelf');

  const onSubmit = async ({ aboutSelf }: AboutSelfFormValues) => {
    try {
      await dispatch(fetchContentChangeRequest({ aboutSelf: aboutSelf }));
      toast.success(t('requestHasBeenSend'));
    } catch (e) {
      captureError(e);
      toast.error(t('somethingWrong'));
    }
  };

  return (
    <Container>
      <Title>{t('expertProfileTitle')}</Title>
      <Note>{t('expertProfileNote')}</Note>
      {!!contentChangeRequest && <Badge label={t('contentUnderModeration')} />}
      <TextAreaContainer>
        <Controller
          control={control}
          name={'aboutSelf'}
          render={({ field: { value, onChange } }) => (
            <Textarea
              onChange={(e) => {
                if (e.target.value.length <= 1000) {
                  onChange(e);
                }
              }}
              value={value}
              label={t('aboutYourSelf')}
            />
          )}
        />
      </TextAreaContainer>
      <Button disabled={aboutSelf === initialAboutSelf} onClick={handleSubmit(onSubmit)}>
        {t('save')}
      </Button>
    </Container>
  );
};

const Container = styled.div`
  margin-bottom: 90px;

  & > button {
    margin-top: 40px;
    width: 100%;

    ${CONDITION_DESKTOP} {
      max-width: 280px;
    }
  }
`;

const TextAreaContainer = styled.div`
  padding-top: 15px;
`;

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  max-width: 500px;
  letter-spacing: normal;
  color: var(--gray);
  margin-bottom: 15px;

  & > a {
    color: var(--purple);
    text-decoration: underline;

    :hover {
      text-decoration: none;
    }
  }
`;

const Title = styled.h2`
  margin: 0;
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--black2);
  padding-bottom: 10px;
`;

import React, { FC } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { Controller, useForm } from 'react-hook-form';
import { Input } from '../../../../ui/input';
import { Button } from '../../../../ui/button';
import { CONDITION_DESKTOP } from '../../../../../common/constants';
import { api, CreateCustomRequestDtoTypeEnum } from '../../../../../api';
import { toast } from 'react-toastify';
import { linkPattern } from '../../../../../constants';
import { captureError } from 'utils/captureError';

interface CommissionFormValues {
  link: string;
}

export const CommissionForm: FC = () => {
  const { t } = useTranslation('profile.page');
  const { t: error } = useTranslation('errors.messages');

  const { control, handleSubmit } = useForm<CommissionFormValues>({
    defaultValues: {
      link: '',
    },
  });

  const onSubmit = async ({ link }: CommissionFormValues) => {
    try {
      await api.V1CustomRequestsApi.createOneBaseCustomRequestsControllerCustomRequest({
        type: CreateCustomRequestDtoTypeEnum.SpecialTariff,
        payload: { socialSiteUrl: link },
      });

      toast.success(t('requestHasBeenSend'));
    } catch (e) {
      captureError(e);
      toast.error(t('somethingWrong'));
    }
  };

  return (
    <Container>
      <Label>{t('specialRate')}</Label>
      <Title>{t('commission')}</Title>
      <Note>{t('largeSubsCommission')}</Note>
      <Note>{t('forIt')}</Note>
      <Note>{`1. ${t('getLinksCommission')}`}</Note>
      <Note>{`2. ${t('waitAnswer')}`}</Note>
      <Note>{`3. ${t('getFeedback')}`}</Note>
      <BoldNote>{t('reqBeforeCommission')}</BoldNote>
      <Controller
        name={'link'}
        rules={{
          required: { value: true, message: error('required') },
          pattern: { value: linkPattern, message: error('incorrectLink') },
        }}
        control={control}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <>
            <Input value={value} onChange={onChange} error={error?.message} label={t('linkOnSocMedia')} />
            <Button onClick={handleSubmit(onSubmit)} disabled={value.length === 0}>
              {t('wantSpecialRate')}
            </Button>
          </>
        )}
      />
    </Container>
  );
};

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  max-width: 500px;
  letter-spacing: normal;
  padding-bottom: 20px;
  color: var(--gray);

  & > a {
    color: var(--purple);
    text-decoration: underline;

    :hover {
      text-decoration: none;
    }
  }
`;

const BoldNote = styled(Note)`
  font-weight: bold;
`;

const Container = styled.div`
  margin-top: 90px;

  input {
    width: 100%;
    ${CONDITION_DESKTOP} {
      width: 60%;
    }
  }
  button {
    margin-top: 40px;
    width: 100%;
    ${CONDITION_DESKTOP} {
      width: unset;
    }
  }
`;
const Label = styled.span`
  background-image: linear-gradient(to right, #ae4cfe 5%, #993dfa 51%, #812df6);
  border-radius: 12.5px;
  color: var(--white);
  font-size: 14px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  padding: 3px 9px;
  text-align: center;
`;

const Title = styled.h2`
  margin: 0;
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--black2);
  padding-bottom: 10px;
  padding-top: 17px;
`;

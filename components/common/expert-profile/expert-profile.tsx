import React, { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import Rubrics from '../rubrics';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { Button } from '../../ui/button';
import { useTranslation } from 'next-i18next';
import { api, Category, Rubric, UserCategoryMeta, VerificationRequest } from '../../../api';
import ExpertVerificationForm from '../expert-verification-form';
import CommissionForm from './forms/commission-form';
import PromoCodeForm from './forms/promocode-form';
import ChatSettingsForm from './forms/chat-settings-form';
import FreeConsultForm from './forms/free-consult-form';
import { useAppDispatch } from '../../../hooks/redux';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';
import AboutSelfForm from './forms/about-self-form';
import {
  fetchSelfContentChangeRequests,
  fetchSelfVerificationRequests,
  fetchUpdateIamProfile,
} from '../../../store/ducks/profile/actions';
import { unwrapResult } from '@reduxjs/toolkit';
import StatusVerificationRequest from '../status-verification-request';
import { captureError } from 'utils/captureError';

interface ExpertProfileProps {
  categories: Category[];
  rubrics: Rubric[];
}

export const ExpertProfile: FC<ExpertProfileProps> = ({ categories, rubrics }) => {
  const { t } = useTranslation('profile.page');

  const dispatch = useAppDispatch();

  const user = useSelector(selectors.selectUser);

  const userCategoriesMap = new Map<number, UserCategoryMeta>();
  user.categories?.map((c: UserCategoryMeta) => {
    userCategoriesMap.set(c.id, c);
  });

  const onBecomeExpert = async () => {
    try {
      const res = await dispatch(
        fetchUpdateIamProfile({
          isExpert: true,
        }),
      );
      if (unwrapResult(res)) {
      }
    } catch (e) {
      captureError(e);
    }
  };

  const { isExpert } = user;

  if (isExpert) {
    return (
      <Container>
        <AboutSelfForm />
        <ConsultationTitle>{t('consultationTopics')}</ConsultationTitle>
        <Note>{t('consultationTopicsNote')}</Note>
        <Rubrics categories={categories} rubrics={rubrics} />
        <ExpertVerificationForm />
        <CommissionForm />
        <PromoCodeForm />
        <ChatSettingsForm isActive={!!user.hasFreeChat} />
        <FreeConsultForm isActive={!!user.hasFreeConsultations} stayCount={user.freeConsultationsRemain} />
      </Container>
    );
  }

  return (
    <NoExpertContainer>
      <ContentContainer>
        <Title>{t('noExpertTitle')}</Title>
        <Note>{t('noExpertNote')}</Note>
        <BecomeExpertButton onClick={onBecomeExpert}>{t('noExpertButton')}</BecomeExpertButton>
      </ContentContainer>
    </NoExpertContainer>
  );
};

const Container = styled.div`
  margin-bottom: 90px;
  justify-content: center;
  flex-direction: column;
  padding: 0 2px;
  display: flex;

  ${CONDITION_DESKTOP} {
    padding: 0;
    padding-left: 95px;
    justify-content: left;
  }
`;

const NoExpertContainer = styled.div`
  justify-content: center;
  flex-direction: column;
  padding: 0 2px;
  display: flex;
  button {
    margin-top: 32px;
  }

  ${CONDITION_DESKTOP} {
    padding: 0;
    padding-left: 95px;
    justify-content: left;
  }
`;

const ContentContainer = styled.div`
  @media (max-width: 600px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

const Title = styled.h2`
  margin: 0;
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--black2);
  padding-bottom: 10px;
`;

const ConsultationTitle = styled(Title)`
  margin-top: 90px;
`;

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  max-width: 500px;
  letter-spacing: normal;
  color: var(--gray);

  & > a {
    color: var(--purple);
    text-decoration: underline;
    :hover {
      text-decoration: none;
    }
  }
`;

const BecomeExpertButton = styled(Button)`
  margin-bottom: 20px;
`;

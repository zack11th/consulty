import React, { FC, useCallback, useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { useSelector } from 'react-redux';
import Image from 'next/image';
import styled from 'styled-components';

import Modal from 'components/common/modal';
import { actions, selectors } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';

type ThanksModalProps = {
  isBad?: boolean;
  isReturningMoney?: boolean;
};

export const ThanksModal: FC<ThanksModalProps> = ({ isBad, isReturningMoney }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('utils');
  const isVisible = useSelector(selectors.app.selectIsVisibleThanksModal);

  const onClose = useCallback(() => {
    if (isVisible) {
      dispatch(actions.app.setIsVisibleThanksModal(false));
    }
  }, [dispatch, isVisible]);

  if (!isVisible) {
    return null;
  }

  return (
    <Modal isVisible={isVisible} onClose={onClose}>
      <Container>
        <Title>{isBad ? t('modals.expertReviewBad.title') : t('modals.thanksModal.title')}</Title>
        <Description>
          {isBad
            ? isReturningMoney
              ? t('modals.expertReviewBad.weReturnMoney')
              : t('modals.expertReviewBad.weReadReview')
            : t('modals.thanksModal.description')}
        </Description>
        <Image src="/img/thanks-modal-image.png" width={260} height={194} />
      </Container>
    </Modal>
  );
};

const Container = styled.div`
  text-align: center;
`;
const Title = styled.h3`
  font-size: 17px;
  font-weight: bold;
  line-height: 20px;
  margin-bottom: 15px;
  white-space: pre-line;
`;
const Description = styled.p`
  font-size: 13px;
  line-height: 20px;
  margin-bottom: 25px;
`;

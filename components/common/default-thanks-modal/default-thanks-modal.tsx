import React, { FC, useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import { useSelector } from 'react-redux';
import Image from 'next/image';
import styled from 'styled-components';

import Modal from 'components/common/modal';
import { actions, selectors } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';

export const DefaultThanksModal: FC<unknown> = () => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('utils');
  const isVisible = useSelector(selectors.app.selectIsVisibleDefaultThanksModal);

  const onClose = useCallback(() => {
    if (isVisible) {
      dispatch(actions.app.setIsVisibleDefaultThanksModal(false));
    }
  }, [dispatch, isVisible]);

  if (!isVisible) {
    return null;
  }

  return (
    <Modal isVisible={isVisible} onClose={onClose}>
      <Container>
        <Title>{t('modals.thanksModal.title')}</Title>
        <Description>{t('modals.thanksModal.description')}</Description>
        <Image src="/img/thanks-modal-image.png" width={260} height={194} />
      </Container>
    </Modal>
  );
};

const Container = styled.div`
  text-align: center;
`;
const Title = styled.h3`
  font-size: 17px;
  font-weight: bold;
  line-height: 20px;
  margin-bottom: 15px;
  white-space: pre-line;
`;
const Description = styled.p`
  font-size: 13px;
  line-height: 20px;
  margin-bottom: 25px;
`;

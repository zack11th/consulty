import React, { FC, HTMLAttributes, useMemo } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import SimpleBar from 'simplebar-react';

import { CONDITION_DESKTOP, HEADER_HEIGHT_DESKTOP, HEADER_HEIGHT_MOBILE } from 'common/constants';
import { selectors } from 'store/ducks';
import { ChatRouter, ChatTabs } from 'components/pages/chat/chat';

import { ChatSidebarTabButton } from './components/chat-sidebar-tab-button';
import { ChatSidebarRoomButton } from './components/chat-sidebar-room-button';
import { ChatSidebarResponseButton } from './components/chat-sidebar-response-button';
import { ChatSidebarRequestButton } from './components/chat-sidebar-request-button';

interface ChatSidebarProps extends HTMLAttributes<HTMLDivElement> {}

export const ChatSidebar: FC<ChatSidebarProps> = ({ ...props }) => {
  const { t } = useTranslation('chat.page');
  const router: ChatRouter = useRouter();
  const user = useSelector(selectors.profile.selectUser);
  const chatRooms = useSelector(selectors.chatRooms.selectChatRooms);
  const myRequests = useSelector(selectors.consultationRequests.selectAllMyConsultationRequests);
  const clientRequests = useSelector(selectors.consultationRequests.selectAllClientRequests);
  const unreadChatRoomsCount = useSelector(selectors.chatRooms.selectUnreadRoomsIds).length;
  const responsesUnreadCount = useSelector(selectors.consultationRequests.selectResponseUnreadCount);
  const requestUnreadCount = useSelector(selectors.consultationRequests.selectRequestUnreadCount);

  const handleClickTabButton = (activeTab: ChatTabs) => {
    router.push({ query: { activeTab } });
  };

  const handleClickRoomButton = (activeTab: ChatTabs, roomId: number) => {
    if (roomId.toString() !== router.query.roomId) {
      router.push({ query: { activeTab, roomId } });
    }
  };

  const renderRoomsList = useMemo(() => {
    switch (router.query.activeTab) {
      case 'requests':
        return clientRequests.map((request) => (
          <ChatSidebarRequestButton
            key={request.categoryId}
            request={request}
            unreadCount={requestUnreadCount[request.categoryId]}
            isActive={router.query.roomId === request.categoryId.toString()}
            onClick={() => handleClickRoomButton('requests', request.categoryId)}
          />
        ));
      case 'responses':
        return myRequests.map((request) => (
          <ChatSidebarResponseButton
            key={request.id}
            request={request}
            unreadCount={responsesUnreadCount[request.id]}
            isActive={router.query.roomId === request.id.toString()}
            onClick={() => handleClickRoomButton('responses', request.id)}
          />
        ));
      default:
        return chatRooms
          .filter((room) => !!room.lastMessageId || !!room.consultations?.length)
          .map((room) => (
            <ChatSidebarRoomButton
              key={room.id}
              chatRoom={room}
              isExpertInRoom={room.expertId === user.id}
              isActive={router.query.roomId === room.id.toString()}
              onClick={() => handleClickRoomButton('consult', room.id)}
            />
          ));
    }
  }, [chatRooms, myRequests, clientRequests, router, responsesUnreadCount, requestUnreadCount]);

  return (
    <Container {...props}>
      <SidebarHeader $isExpert={user.isExpert}>
        <ChatSidebarTabButton
          text={t('sidebar.consultations')}
          isActive={!router.query.activeTab || router.query.activeTab === 'consult'}
          unreadCount={unreadChatRoomsCount}
          onClick={() => handleClickTabButton('consult')}
        />
        {user.isExpert && (
          <ChatSidebarTabButton
            text={t('sidebar.requests')}
            isActive={router.query.activeTab === 'requests'}
            unreadCount={Object.keys(requestUnreadCount).length}
            onClick={() => handleClickTabButton('requests')}
          />
        )}
        <ChatSidebarTabButton
          text={t('sidebar.responses')}
          isActive={router.query.activeTab === 'responses'}
          unreadCount={Object.keys(responsesUnreadCount).length}
          onClick={() => handleClickTabButton('responses')}
        />
      </SidebarHeader>

      <ChatRoomsList>{renderRoomsList}</ChatRoomsList>
    </Container>
  );
};

const Container = styled.div`
  border-left: 1px solid var(--gray6);
  border-right: 1px solid var(--gray6);
  height: calc(100vh - ${HEADER_HEIGHT_MOBILE});
  height: calc(var(--v-height) - ${HEADER_HEIGHT_MOBILE});
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 60px calc(100% - 60px);
  ${CONDITION_DESKTOP} {
    height: calc(100vh - ${HEADER_HEIGHT_DESKTOP});
    height: calc(var(--v-height) - ${HEADER_HEIGHT_DESKTOP});
  }
`;
const SidebarHeader = styled.div<{ $isExpert: boolean }>`
  display: grid;
  grid-template-columns: ${({ $isExpert }) => ($isExpert ? '1fr 1fr 1fr' : '1fr 1fr')};
  top: 0;
  height: 60px;
  background-color: var(--white);
  box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.1);
`;
const ChatRoomsList = styled(SimpleBar)`
  padding: 5px;
`;

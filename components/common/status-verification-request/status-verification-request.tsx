import React, { FC } from 'react';
import styled from 'styled-components';
import { VerificationRequest } from '../../../api';
import { useTranslation } from 'next-i18next';
import dayjs from 'dayjs';
import { Badge } from '../../ui/badge';

interface StatusVerificationRequestProps {
  verificationRequest: VerificationRequest;
}

export const StatusVerificationRequest: FC<StatusVerificationRequestProps> = ({ verificationRequest }) => {
  const { t } = useTranslation('profile.page');

  return (
    <Container>
      <Item>
        <Date>
          {t('requestFrom', { date: `${dayjs(verificationRequest.createdAt).locale('ru').format('DD.MM.YYYY')}` })}
        </Date>
        <Badge label={t(verificationRequest.status)} />
      </Item>
    </Container>
  );
};

const Container = styled.div``;

const Item = styled.div`
  display: flex;
  padding: 10px 0;
  align-items: center;
  margin-top: 17px;
`;

const Date = styled.div`
  margin-right: 10px;
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.13;
  max-width: 500px;
  letter-spacing: normal;
  color: var(--gray);

  & > a {
    color: var(--purple);
    text-decoration: underline;

    :hover {
      text-decoration: none;
    }
  }
`;

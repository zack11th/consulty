import { unwrapResult } from '@reduxjs/toolkit';
import { CONDITION_DESKTOP } from 'common/constants';
import Modal from 'components/common/modal';
import { Button } from 'components/ui/button';
import { Input } from 'components/ui/input';
import { TextareaModal } from 'components/ui/textarea-modal';
import { useAppDispatch } from 'hooks/redux';
import { useTranslation } from 'next-i18next';
import React, { FC, HTMLAttributes, useEffect, useMemo, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { actions, selectors } from 'store/ducks';
import styled from 'styled-components';
import { commaNumberFormat, onKeyDownNumber } from 'utils';
import { captureError } from 'utils/captureError';

import { RequestMessage } from '../request-message';

const currencyCode = '\u20bd';

type ModalForm = {
  price: string;
  description: string;
};

interface RequestBodyProps extends HTMLAttributes<HTMLDivElement> {
  categoryId: number | null;
}

export const RequestBody: FC<RequestBodyProps> = ({ categoryId, ...props }) => {
  const { t } = useTranslation('chat.page');
  const dispatch = useAppDispatch();
  const requestCategory = useSelector(selectors.consultationRequests.selectRequestRoomById(categoryId));
  const activeRequestMessageId = useSelector(selectors.consultationRequests.selectActiveRequestMessageId);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [isSendingOffer, setIsSendingOffer] = useState(false);
  const {
    control,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
  } = useForm<ModalForm>({ mode: 'onChange', defaultValues: { price: '', description: '' } });

  const toggleModal = () => {
    setIsVisibleModal(!isVisibleModal);
  };

  const sendResponse = async (values: ModalForm) => {
    try {
      setIsSendingOffer(true);
      const actionResult = await dispatch(
        actions.consultationRequests.createOffer({
          consultationRequestId: activeRequestMessageId!,
          description: values.description,
          price: parseInt(values.price.replace(' ', '')),
        }),
      );
      unwrapResult(actionResult);
      toast.success(t('offerForm.offerSendMessage'));
      reset();
    } catch (error: any) {
      captureError(error);
      toast.error(error.message);
    } finally {
      setIsSendingOffer(false);
      toggleModal();
    }
  };

  const renderMessages = useMemo(() => {
    return requestCategory?.requests.map((request) => (
      <RequestMessage key={request.id} message={request} onClick={toggleModal} />
    ));
  }, [requestCategory]);

  return (
    <>
      <Container {...props}>{renderMessages} </Container>
      {isVisibleModal && (
        <Modal isVisible={isVisibleModal} onClose={toggleModal}>
          <Form onSubmit={handleSubmit(sendResponse)}>
            <FormTitle>{t('offerForm.title')}</FormTitle>
            <FormDescription>{t('offerForm.description')}</FormDescription>
            <ModalInputContainer>
              <Controller
                control={control}
                name="price"
                rules={{
                  validate: {
                    isNumber: (v) => !!parseInt(v) || !v || 'errors.mustBeANumber',
                    maxLength: (v) => v.length < 7 || 'errors.tooLong',
                    minLength: (v) => v.length > 1 || 'errors.tooSmall',
                  },
                }}
                render={({ field: { value, onChange }, fieldState: { error } }) => (
                  <Input
                    value={value}
                    onChange={(e) => onChange(commaNumberFormat(e.currentTarget.value))}
                    placeholder={t('offerForm.pricePlaceholder')}
                    error={error?.message && t(error?.message)}
                    onKeyDown={onKeyDownNumber}
                  />
                )}
              />
              <Currency>{currencyCode}</Currency>
            </ModalInputContainer>
            <Controller
              control={control}
              name="description"
              render={({ field: { value, onChange } }) => (
                <ModalTextareaModal
                  value={value}
                  onChange={onChange}
                  placeholder={t('offerForm.descriptionPlaceholder')}
                />
              )}
            />

            <Button
              disabled={!getValues().price.length || !!errors.price?.message || isSendingOffer}
              loading={isSendingOffer}
              block
              type="submit"
            >
              {t('offerForm.button')}
            </Button>
          </Form>
        </Modal>
      )}
    </>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column-reverse;
  padding: 0 15px 15px;
  ${CONDITION_DESKTOP} {
    padding: 0 35px 35px;
  }
`;

const Form = styled.form``;
const FormTitle = styled.h3`
  text-align: center;
  font-size: 17px;
  font-weight: bold;
  margin-bottom: 15px;
`;
const FormDescription = styled.p`
  text-align: center;
  white-space: pre-wrap;
  font-size: 13px;
  line-height: 1.5;
  margin-bottom: 30px;
`;
const ModalInputContainer = styled.div`
  margin-bottom: 15px;
  position: relative;
`;
const Currency = styled.div`
  position: absolute;
  color: var(--gray);
  right: 15px;
  top: 0;
  height: 50px;
  line-height: 50px;
`;
const ModalTextareaModal = styled(TextareaModal)`
  margin-bottom: 20px;
`;

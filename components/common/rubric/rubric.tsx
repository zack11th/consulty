import { useTranslation } from 'next-i18next';
import React, { FC } from 'react';
import styled from 'styled-components';
import { Rubric as RubricType } from 'api';

import { Badge } from 'components/ui/badge';
import { CONDITION_DESKTOP } from 'common/constants';
import Link from 'next/link';
import { buildRoute } from 'utils/buildRoute';
import { routes } from 'common/routes';

type RubricProps = {
  rubric: RubricType;
};

export const Rubric: FC<RubricProps> = ({ rubric }) => {
  const { t } = useTranslation('utils');

  return (
    <Container>
      <IconContainer>
        <Icon src={rubric.iconUrl} alt={rubric.name} />
      </IconContainer>
      <Content>
        <Title>{rubric.name}</Title>
        <List>
          {rubric.categories.map((item) =>
            rubric.isUpcoming ? (
              <ListItem $isUpcoming key={item.id}>
                {item.name}
              </ListItem>
            ) : (
              <Link key={item.id} href={buildRoute(routes.category, { id: item.id })}>
                <a>
                  <ListItem>{item.name}</ListItem>
                </a>
              </Link>
            ),
          )}
        </List>
        {rubric.isUpcoming && <StyledBadge label={t('soon')} />}
      </Content>
    </Container>
  );
};

const Container = styled.div`
  margin-bottom: 20px;
  ${CONDITION_DESKTOP} {
    margin-bottom: 60px;
    width: 31.5%;
    display: flex;
  }
`;
const IconContainer = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  margin-bottom: 20px;
  margin-right: 20px;
  background-color: var(--gray5);
  flex-shrink: 0;
`;
const Icon = styled.img`
  max-width: 100%;
`;
const Content = styled.div`
  flex-grow: 1;
  width: calc(100% - 80px);
`;
const Title = styled.h3`
  font-weight: bold;
  font-size: 20px;
  margin-bottom: 21px;
  overflow-wrap: break-word;
`;
const List = styled.ul`
  a {
    -webkit-tap-highlight-color: transparent;
  }
`;
const ListItem = styled.li<{ $isUpcoming?: boolean }>`
  font-size: 14px;
  margin-top: 15px;
  overflow-wrap: break-word;
  cursor: ${({ $isUpcoming }) => ($isUpcoming ? 'default' : 'pointer')};
  opacity: ${({ $isUpcoming }) => ($isUpcoming ? 0.3 : undefined)};
  -webkit-tap-highlight-color: transparent;
`;
const StyledBadge = styled(Badge)`
  margin-top: 20px;
`;

import React, { ChangeEvent, FC } from 'react';
import styled from 'styled-components';
import { buildRoute, coverImageStyle } from '../../../utils';
import { Camera } from '../../../assets/svg';
import { useTranslation } from 'next-i18next';
import { routes } from '../../../common/routes';
import Link from 'next/link';
import { CONDITION_DESKTOP } from '../../../common/constants';
import StarRating from '../../ui/star-rating';
import { useRouter } from 'next/router';
import { useAppDispatch } from '../../../hooks/redux';
import { selectors } from '../../../store/ducks/profile';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { selectContentChangeRequest } from '../../../store/ducks/profile/selectors';
import { fetchContentChangeRequest, fetchUpdateIamProfile } from '../../../store/ducks/profile/actions';
import { usePrepareImageToUpload } from 'hooks/usePrepareImageToUpload';
import { captureError } from 'utils/captureError';

interface SidebarProps {}

const links = [
  { href: `${routes.profile}?activeTab=settings`, label: 'Мои настройки', tab: 'settings' },
  { href: `${routes.profile}?activeTab=expertProfile`, label: 'Профиль эксперта', tab: 'expertProfile' },
  { href: `${routes.profile}?activeTab=wallet`, label: 'Кошелек', tab: 'wallet' },
  { href: `${routes.profile}?activeTab=notifications`, label: 'Уведомления', tab: 'notifications' },
  { href: `${routes.profile}?activeTab=support`, label: 'Поддержка', tab: 'support' },
];

export const Sidebar: FC<SidebarProps> = () => {
  const { t } = useTranslation('profile.page');
  const { query } = useRouter();
  const dispatch = useAppDispatch();
  const { firstName, lastName, isExpert, expertRating, reviewsCount, id, ...user } = useSelector(selectors.selectUser);
  const contentChangeRequest = useSelector(selectContentChangeRequest);
  const { selectFile, uploadFileToCloud } = usePrepareImageToUpload();

  const activeTab = query.activeTab || 'settings';

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { files } = e.target;
    selectFile(files, async ({ file }) => {
      try {
        if (file) {
          const avatarUrl = await uploadFileToCloud(file);
          if (isExpert) {
            await dispatch(fetchContentChangeRequest({ avatarUrl }));
          } else {
            await dispatch(fetchUpdateIamProfile({ avatarUrl }));
          }
          toast.success(t('requestHasBeenSend'));
        }
      } catch (error: any) {
        captureError(error);
        toast.error(error.message || t('somethingWrong'));
      }
    });
  };

  let avatarUrl: string | null;

  if (contentChangeRequest && contentChangeRequest.avatarUrl) {
    avatarUrl = contentChangeRequest.avatarUrl;
  } else {
    avatarUrl = user.avatarUrl || null;
  }

  return (
    <Container>
      <Avatar
        htmlFor={'avatar'}
        $url={avatarUrl || null}
        $disable={!!contentChangeRequest && !!contentChangeRequest.avatarUrl}
      >
        {!avatarUrl && <Camera />}
      </Avatar>
      <input id={'avatar'} onChange={onChange} type="file" accept="image/png, image/jpeg" style={{ display: 'none' }} />
      <Name>{`${firstName || ''} ${lastName || ''}`}</Name>
      {isExpert && (
        <Rating>
          <div>{expertRating?.toFixed(1) || 0}</div>
          <StarRating rating={expertRating || 0} />
          <Feedback>
            {reviewsCount === 0 ? (
              <NoReview>{t('noFeedback')}</NoReview>
            ) : (
              <Link href={`${buildRoute(routes.expert, { id })}#reviews`} passHref>
                <ReviewCount>{`${reviewsCount} ${t('reviews')}`}</ReviewCount>
              </Link>
            )}
          </Feedback>
        </Rating>
      )}
      <Links>
        {links.map((val, i) => {
          if (!isExpert && val.tab === 'wallet') {
            return null;
          }

          return (
            <Link key={i} href={val.href}>
              <a className={activeTab === val.tab ? 'active' : ''}>{t(`${val.tab}`)}</a>
            </Link>
          );
        })}
      </Links>
    </Container>
  );
};

const Container = styled.div`
  display: none;
  flex-direction: row;
  max-width: 240px;
  text-align: left;
  margin-right: 10px;
  width: 100%;

  ${CONDITION_DESKTOP} {
    display: block;
  }
`;

const Avatar = styled.label<{ $url: string | null; $disable?: boolean }>`
  margin-bottom: 23px;
  width: 120px;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 120px;
  border-radius: 15px;
  opacity: ${({ $disable }) => ($disable ? '0.5' : '1')};
  background-color: ${({ $url }) => ($url ? 'transparent' : 'var(--purple)')};
  ${({ $url }) => ({ ...coverImageStyle($url) })}
  :hover {
    cursor: pointer;
  }
  & > svg {
    transform: scale(2);
  }
`;

const Name = styled.div`
  font-size: 17px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 10px;
`;

const Rating = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: var(--text);
`;

const Feedback = styled.div`
  font-size: 17px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.18;
  letter-spacing: normal;
`;
const ReviewCount = styled.a`
  color: var(--purple);
`;

const NoReview = styled.div`
  color: var(--gray);
`;

const Links = styled.div`
  margin-top: 40px;

  & > a {
    text-decoration: none;
    color: var(--gray);
    display: block;
    margin: 20px 0;

    &.active {
      color: var(--text);
    }

    :hover {
      color: var(--text);
    }
  }
`;

import { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { useTranslation } from 'next-i18next';

import { chooseEndingWord } from 'utils';
import { Consultation, ConsultationStatusEnum } from 'api';

import { MessageListType, MessageType } from '../room-body';
import { useAppDispatch } from 'hooks/redux';
import { actions } from 'store/ducks';

const WARNING_COUNT_MESSAGES_TO_END = 5;
const WARNING_COUNT_MINUTES_TO_END = 5;

enum TempMessageWhat {
  endByMessage = 'endByMessage',
  endByTime = 'endByTime',
}
interface TempMessageListType extends MessageListType {
  what: TempMessageWhat;
}

interface useTempMessagesProps {
  activeConsultation: Consultation | undefined;
}

export const useTempMessages = ({ activeConsultation }: useTempMessagesProps) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('chat.page');
  const [tempSystemMessages, setTempSystemMessages] = useState<TempMessageListType[]>([]);
  const [intervalTimeNow, setIntervalTimeNow] = useState('');

  useEffect(() => {
    const interval = setInterval(() => setIntervalTimeNow(dayjs().format('YYYY-MM-DD HH:mm:ss.SSS')), 1000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (activeConsultation) {
      const messagesToEnd = activeConsultation.messagesLimit - activeConsultation.expertMessagesCount;
      if (
        messagesToEnd <= WARNING_COUNT_MESSAGES_TO_END &&
        !tempSystemMessages.some((m) => m.what === TempMessageWhat.endByMessage)
      ) {
        const timeNow = dayjs().format('YYYY-MM-DD HH:mm:ss.SSS');
        setTempSystemMessages([
          ...tempSystemMessages,
          {
            message: {
              content: t('chatRoom.systemMessages.approachingMessageLimit', {
                count: messagesToEnd,
                endMessage: chooseEndingWord(messagesToEnd, [
                  t('endMessage.one'),
                  t('endMessage.two'),
                  t('endMessage.many'),
                ]),
              }),
              createdAt: timeNow,
            },
            time: timeNow,
            type: MessageType.systemMessage,
            what: TempMessageWhat.endByMessage,
          },
        ]);
      }
    }
  }, [activeConsultation, tempSystemMessages]);

  useEffect(() => {
    if (activeConsultation) {
      const minutesToEnd = dayjs(activeConsultation.expiresIn).diff(dayjs(intervalTimeNow), 'minute') + 1;
      if (minutesToEnd <= 0) {
        dispatch(
          actions.chatRooms.changeConsultationStatus({
            roomId: activeConsultation.chatRoomId,
            consultationId: activeConsultation.id,
            status: ConsultationStatusEnum.TimeLimitExceeded,
          }),
        );
        dispatch(actions.consultationsChatRoom.updateConsultationByTimeExceeded(activeConsultation));
      }
      if (
        minutesToEnd <= WARNING_COUNT_MINUTES_TO_END &&
        !tempSystemMessages.some((m) => m.what === TempMessageWhat.endByTime)
      ) {
        setTempSystemMessages([
          ...tempSystemMessages,
          {
            message: {
              content: t('chatRoom.systemMessages.approachingTimeLimit', { count: minutesToEnd }),
              createdAt: intervalTimeNow,
            },
            time: intervalTimeNow,
            type: MessageType.systemMessage,
            what: TempMessageWhat.endByTime,
          },
        ]);
      }
    }
  }, [intervalTimeNow, tempSystemMessages, activeConsultation]);

  useEffect(() => {
    if (!activeConsultation) {
      setTempSystemMessages([]);
    }
  }, [activeConsultation]);

  return { tempSystemMessages, setTempSystemMessages };
};

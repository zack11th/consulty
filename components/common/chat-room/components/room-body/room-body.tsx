import React, { FC, HTMLAttributes, useCallback, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { TFunction, useTranslation } from 'next-i18next';
import { unwrapResult } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import InfiniteScroll from 'react-infinite-scroll-component';
import dayjs from 'dayjs';

import { ConsultationWithOffer, MessageExtended } from 'types';
import { actions, selectors } from 'store/ducks';
import { useIsExpertInRoom } from 'hooks/useIsExpertInRoom';
import { CONDITION_DESKTOP, LIMIT_CHAT_MESSAGES } from 'common/constants';
import { Spinner } from 'components/ui/spinner';
import { useAppDispatch } from 'hooks/redux';
import { ChatWidget } from 'components/ui/chat-widget';
import { Button } from 'components/ui/button';
import {
  Consultation,
  ConsultationInvoiceTypeEnum,
  ConsultationStatusEnum,
  ExtraService,
  InvoiceStatusEnum,
} from 'api';

import { ChatMessage } from '../chat-message';
import { useTempMessages } from './hooks/useTempMessages';
import { useChatPagination } from './hooks/useChatPagination';
import { makeConsultationMessages } from './makeSystemMessages/makeConsultationMessages';
import { makeProlongationMessages } from './makeSystemMessages/makeProlongationMessages';
import { makeExtraServiceMessages } from './makeSystemMessages/makeExtraServiceMessages';
import { useWidgetHandlers } from './hooks/useWidgetsHandlers';
import { captureError } from 'utils/captureError';

export enum MessageType {
  userMessage = 'userMessage',
  systemMessage = 'systemMessage',
  pendingStartConsult = 'pendingStarConsult',
  pendingPaymentConsult = 'pendingPaymentConsult',
  pendingPaymentProlongation = 'pendingPaymentProlongation',
  pendingPaymentExtraService = 'pendingPaymentExtraService',
}

export interface MessageListType {
  time: string;
  type: MessageType;
  message?: MessageExtended;
  consultation?: ConsultationWithOffer;
  price?: number;
  extraService?: ExtraService;
}

export interface makeSystemMessagesFuncArgs {
  consultsToBeAdded: Consultation[];
  tChatPage: TFunction;
  isExpertInRoom: boolean;
}

interface RoomBodyProps extends HTMLAttributes<HTMLDivElement> {
  chatRoomId: number | null;
}

export const RoomBody: FC<RoomBodyProps> = ({ chatRoomId, ...props }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('chat.page');
  const { t: tUtils } = useTranslation('utils');

  const user = useSelector(selectors.profile.selectUser);
  const chatRoom = useSelector(selectors.chatRooms.selectChatRoomById(chatRoomId));
  const isExpertInRoom = useIsExpertInRoom(chatRoom);

  const messages = useSelector(selectors.chatMessages.selectChatMessages);
  const consultations = useSelector(selectors.consultationsChatRoom.selectConsultations);
  const activeConsultation = useSelector(selectors.consultationsChatRoom.selectActiveConsultation);
  const { tempSystemMessages, setTempSystemMessages } = useTempMessages({ activeConsultation });
  const {
    handleRejectConsultationOffer,
    handleRejectProlongation,
    handleRejectExtraService,
    handleStartConsultation,
    isSendingRequest,
  } = useWidgetHandlers();

  const fetchingChatMessageStatus = useSelector(selectors.chatMessages.selectFetchingChatMessagesStatus);
  const { page, pageCount, loadMoreMessages } = useChatPagination({ chatRoomId });

  // get chat messages
  useEffect(() => {
    if (chatRoom?.id && !chatRoom.isTempRoom) {
      const fetchChatMessages = async () => {
        try {
          const actionResult = await dispatch(
            actions.chatMessages.getChatMessages({ chatRoomId: chatRoom.id, limit: LIMIT_CHAT_MESSAGES, page: 1 }),
          );
          unwrapResult(actionResult);
        } catch (error: any) {
          captureError(error);
          toast.error(error.message);
        }
      };
      const getOneChatRoom = async () => {
        try {
          const actionResult = await dispatch(actions.chatRooms.getOneChatRoom({ chatRoomId: chatRoom.id }));
          unwrapResult(actionResult);
        } catch (error: any) {
          captureError(error);
          toast.error(error.message);
        }
      };
      fetchChatMessages();
      getOneChatRoom();
    }
  }, [chatRoom?.id, chatRoom?.isTempRoom]);

  useEffect(() => {
    setTempSystemMessages([]);
  }, [chatRoomId, chatRoom?.consultations]);

  const messagesList: MessageListType[] = useMemo(() => {
    let allMessages: MessageListType[] = messages.map((message) => ({
      time: message.createdAt!,
      type: MessageType.userMessage,
      message,
    }));
    let consultsBeAddedToMessages: Consultation[] = [];
    if (allMessages.length) {
      consultsBeAddedToMessages = consultations.filter(
        (c) =>
          ![ConsultationStatusEnum.PendingStart].includes(c.status) &&
          (dayjs(allMessages[allMessages.length - 1].time).diff(dayjs(c.updatedAt)) < 0 ||
            dayjs(allMessages[allMessages.length - 1].time).diff(
              dayjs(
                c.invoices?.find(
                  (invoice) =>
                    invoice.type === ConsultationInvoiceTypeEnum.Initiation &&
                    [
                      InvoiceStatusEnum.Fulfilled,
                      InvoiceStatusEnum.WaitingWithdrawal,
                      InvoiceStatusEnum.Withdrawled,
                    ].includes(invoice.invoice?.status),
                )?.invoice?.updatedAt || 0,
              ),
            ) < 0),
      );
    } else if (fetchingChatMessageStatus === 'fulfilled') {
      consultsBeAddedToMessages = consultations.filter(
        (c) => ![ConsultationStatusEnum.PendingStart].includes(c.status),
      );
    }
    if (consultsBeAddedToMessages.length) {
      const systemConsultationsMessages = makeConsultationMessages({
        consultsToBeAdded: consultsBeAddedToMessages,
        messagesChatRoom: allMessages,
        isExpertInRoom,
        tChatPage: t,
        tUtils,
      });
      const prolongationSystemMessages = makeProlongationMessages({
        consultsToBeAdded: consultsBeAddedToMessages,
        isExpertInRoom,
        tChatPage: t,
      });
      const extraServiceMessages = makeExtraServiceMessages({
        consultsToBeAdded: consultsBeAddedToMessages,
        activeConsultation,
        isExpertInRoom,
        tChatPage: t,
      });

      allMessages = [
        ...allMessages,
        ...(systemConsultationsMessages as MessageListType[]),
        ...tempSystemMessages,
        ...(prolongationSystemMessages as MessageListType[]),
        ...(extraServiceMessages as MessageListType[]),
      ].sort((a, b) => (dayjs(a.time).diff(dayjs(b.time)) > 0 ? -1 : 1));
    }

    // pendingStartConsultations always to first message
    const pendingStartConsultations: MessageListType[] = consultations
      .filter((c) => c.status === ConsultationStatusEnum.PendingStart)
      .map((c) => ({ time: '', type: MessageType.pendingStartConsult, consultation: c }));

    allMessages.unshift(...pendingStartConsultations);
    return allMessages;
  }, [messages, consultations, tempSystemMessages, isExpertInRoom, activeConsultation]);

  // render
  const renderLastMessageDivider = useCallback(
    (message: MessageExtended) => {
      if (
        user.id !== message.authorId &&
        message.id !== chatRoom?.lastMessageId &&
        message.id ===
          (user.id === chatRoom?.expertId ? chatRoom.expertLastReadMessageId : chatRoom?.clientLastReadMessageId)
      ) {
        return (
          <LastMessageDivider>
            <span>{t('unreadMessages')}</span>
          </LastMessageDivider>
        );
      }
    },
    [user, chatRoom, t],
  );

  const renderMessagesList: React.ReactNode[] = useMemo(() => {
    return messagesList.map((m) => {
      switch (m.type) {
        case MessageType.userMessage:
          return (
            <React.Fragment key={m.message!.id || m.message!.createdAt}>
              {renderLastMessageDivider(m.message!)}
              <ChatMessage
                message={m.message!}
                avatar={isExpertInRoom ? chatRoom?.client.avatarUrl : chatRoom?.expert.avatarUrl}
                variant={user.id === m.message!.authorId ? 'me' : 'companion'}
              />
            </React.Fragment>
          );
        case MessageType.systemMessage:
          return <ChatMessage key={m.time} message={m.message!} variant="system" />;
        case MessageType.pendingPaymentConsult:
          const pendingInvoice = m.consultation!.invoices?.find(
            (invoice) => invoice.invoice?.status === InvoiceStatusEnum.Pending,
          )?.invoice;
          const pendingPrice = pendingInvoice?.isFree ? tUtils('byToken') : pendingInvoice?.amount;
          if (!pendingInvoice) {
            return null;
          } else {
            return (
              <ChatWidget
                key={m.consultation!.createdAt}
                title={t('chatRoom.widgets.pendingPayment.title')}
                theme={m.consultation!.category?.name || ''}
                price={pendingPrice}
                description={t('chatRoom.widgets.pendingPayment.description')}
                duration={t('defaultDurationConsultation')}
                expiresIn={t('chatRoom.widgets.pendingPayment.expiresIn', {
                  time: dayjs(pendingInvoice?.expiresIn).format('HH:mm'),
                })}
                variant="companion"
                actionBlock={
                  <>
                    <a href={pendingInvoice?.checkoutUrl}>
                      <WidgetButton block disabled={isSendingRequest} loading={isSendingRequest}>
                        {t('chatRoom.widgets.pendingPayment.buttons.pay', {
                          price: pendingPrice,
                          currency: typeof pendingPrice === 'number' ? '₽' : '',
                        })}
                      </WidgetButton>
                    </a>
                    {!!m.consultation!.consultationOffer?.id && (
                      <WidgetButton
                        block
                        bordered
                        disabled={isSendingRequest}
                        loading={isSendingRequest}
                        onClick={() =>
                          handleRejectConsultationOffer(chatRoomId!, m.consultation!.consultationOffer!.id)
                        }
                      >
                        {t('chatRoom.widgets.pendingPayment.buttons.cancel')}
                      </WidgetButton>
                    )}
                  </>
                }
              />
            );
          }
        case MessageType.pendingPaymentProlongation:
          const forFree = tUtils('forFree');
          const parentInvoice = m.consultation!.invoices.find(
            (invoice) => invoice.invoice?.status === InvoiceStatusEnum.Pending,
          );
          const prolongationInvoice = parentInvoice?.invoice;
          if (!activeConsultation || activeConsultation?.id !== m.consultation!.id || !prolongationInvoice) {
            return false;
          } else {
            return (
              <ChatWidget
                key={m.time}
                title={t('chatRoom.widgets.pendingProlongation.title')}
                theme={m.consultation!.category?.name || ''}
                price={m.price! > 0 ? m.price : forFree}
                description={t('chatRoom.widgets.pendingProlongation.description')}
                duration={t('defaultDurationConsultation')}
                expiresIn={t('chatRoom.widgets.pendingProlongation.expiresIn', {
                  time: dayjs(prolongationInvoice.expiresIn).format('HH:mm'),
                })}
                variant="companion"
                actionBlock={
                  <>
                    <a href={prolongationInvoice.checkoutUrl}>
                      <WidgetButton block disabled={isSendingRequest} loading={isSendingRequest}>
                        {m.price! > 0
                          ? t('chatRoom.widgets.pendingProlongation.buttons.pay', {
                              price: m.price,
                              currency: '₽',
                            })
                          : t('chatRoom.widgets.pendingProlongation.buttons.payFree')}
                      </WidgetButton>
                    </a>
                    <WidgetButton
                      block
                      bordered
                      disabled={isSendingRequest}
                      loading={isSendingRequest}
                      onClick={() => handleRejectProlongation(parentInvoice!.id)}
                    >
                      {t('chatRoom.widgets.pendingProlongation.buttons.cancel')}
                    </WidgetButton>
                  </>
                }
              />
            );
          }
        case MessageType.pendingStartConsult:
          const initialInvoice = m.consultation!.invoices?.find(
            (invoice) => invoice.type === ConsultationInvoiceTypeEnum.Initiation,
          )?.invoice;
          const price = initialInvoice?.isFree ? tUtils('byToken') : initialInvoice?.amount;
          return isExpertInRoom ? (
            <ChatWidget
              key={m.consultation!.createdAt}
              title={t('chatRoom.widgets.pendingStart.title')}
              theme={m.consultation!.category?.name || ''}
              price={price}
              duration={t('defaultDurationConsultation')}
              actionBlock={
                <Button
                  block
                  onClick={() => handleStartConsultation(m.consultation!.id)}
                  disabled={isSendingRequest}
                  loading={isSendingRequest}
                >
                  {t('chatRoom.widgets.pendingStart.button')}
                </Button>
              }
            />
          ) : (
            <ChatMessage
              key={m.consultation!.createdAt}
              message={{
                content: t('chatRoom.systemMessages.waitStartConsultation', {
                  price,
                  currency: typeof price === 'number' ? '₽' : '',
                }),
                createdAt: m.consultation?.updatedAt,
              }}
              variant="system"
            />
          );
        case MessageType.pendingPaymentExtraService:
          return (
            <ChatWidget
              key={m.extraService!.createdAt}
              variant="companion"
              title={t('chatRoom.widgets.pendingExtraService.title')}
              description={m.extraService!.description}
              price={m.price}
              expiresIn={t('chatRoom.widgets.pendingExtraService.expiresIn', {
                time: dayjs(m.extraService!.invoice.expiresIn).format('HH:mm'),
              })}
              actionBlock={
                <>
                  <a href={m.extraService!.invoice.checkoutUrl || ''}>
                    <WidgetButton
                      block
                      disabled={isSendingRequest || !m.extraService!.invoice.checkoutUrl}
                      loading={isSendingRequest}
                    >
                      {t('chatRoom.widgets.pendingExtraService.buttons.pay', {
                        price: m.price,
                        currency: '₽',
                      })}
                    </WidgetButton>
                  </a>
                  <WidgetButton
                    block
                    bordered
                    disabled={isSendingRequest}
                    loading={isSendingRequest}
                    onClick={() => handleRejectExtraService(m.extraService!.consultationId, m.extraService!.id)}
                  >
                    {t('chatRoom.widgets.pendingExtraService.buttons.cancel')}
                  </WidgetButton>
                </>
              }
            />
          );
        default:
          throw new Error(`MessageType '${m.type}' не предусмотрено рендером."`);
      }
    });
  }, [messagesList, isExpertInRoom, chatRoom, user, isSendingRequest]);

  return (
    <Container {...props} id="chat-room-body">
      <InfiniteScroll
        dataLength={renderMessagesList.length}
        hasMore={!!pageCount && pageCount > page}
        loader={<Spinner />}
        next={loadMoreMessages}
        style={{ display: 'flex', flexDirection: 'column-reverse' }}
        inverse={true}
        scrollableTarget="chat-room-body"
      >
        {renderMessagesList}
      </InfiniteScroll>
      {fetchingChatMessageStatus === 'pending' && <Spinner />}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column-reverse;
  padding: 0 15px 15px;
  ${CONDITION_DESKTOP} {
    padding: 0 35px 35px;
  }
`;
const LastMessageDivider = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  flex-shrink: 0;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  line-height: 1.25;
  color: var(--gray);
  position: relative;
  overflow: hidden;
  ${CONDITION_DESKTOP} {
    font-style: italic;
  }
  & > span {
    flex-shrink: 0;
    margin: 0 10px;
  }
  &::before,
  &::after {
    content: '';
    position: relative;
    display: inline-block;
    width: 50%;
    height: 1px;
    vertical-align: middle;
    background: var(--gray2);
  }
`;
const WidgetButton = styled(Button)`
  &:not(:first-child) {
    margin-top: 10px;
  }
`;

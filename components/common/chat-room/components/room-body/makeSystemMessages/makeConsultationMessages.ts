import { ConsultationInvoiceTypeEnum, ConsultationStatusEnum, InvoiceStatusEnum } from 'api';
import dayjs from 'dayjs';
import { TFunction } from 'next-i18next';
import { chooseEndingWord } from 'utils';
import { makeSystemMessagesFuncArgs, MessageListType, MessageType } from '../room-body';

interface makeConsultationMessagesArgs extends makeSystemMessagesFuncArgs {
  tUtils: TFunction;
  messagesChatRoom: MessageListType[];
}

export const makeConsultationMessages = ({
  consultsToBeAdded,
  tChatPage,
  tUtils,
  messagesChatRoom,
  isExpertInRoom,
}: makeConsultationMessagesArgs): (MessageListType | false)[] => {
  return consultsToBeAdded
    .flatMap((c) => {
      const startInvoice = c.invoices?.find(
        (invoice) =>
          invoice.type === ConsultationInvoiceTypeEnum.Initiation &&
          [InvoiceStatusEnum.Fulfilled, InvoiceStatusEnum.WaitingWithdrawal, InvoiceStatusEnum.Withdrawled].includes(
            invoice.invoice?.status,
          ),
      )?.invoice;

      const startTime = startInvoice?.status === InvoiceStatusEnum.Withdrawled ? c.createdAt : startInvoice?.updatedAt;
      const initInvoice = c.invoices?.find(
        (invoice) => invoice.type === ConsultationInvoiceTypeEnum.Initiation,
      )?.invoice;
      const price = initInvoice?.isFree ? tUtils('byToken') : initInvoice?.amount;

      const systemMessageStartConsult: MessageListType | false = startTime
        ? {
            message: {
              content: tChatPage('chatRoom.systemMessages.startConsultation', {
                price,
                currency: typeof price === 'number' ? '₽' : '',
              }),
              createdAt: startTime,
            },
            time: startTime,
            type: MessageType.systemMessage,
          }
        : false;

      switch (c.status) {
        case ConsultationStatusEnum.Active:
          return systemMessageStartConsult;
        case ConsultationStatusEnum.TimeLimitExceeded:
          return [
            {
              message: { content: tChatPage('chatRoom.systemMessages.endTimeExceeded'), createdAt: c.updatedAt },
              time: c.updatedAt,
              type: MessageType.systemMessage,
            },
            dayjs(startTime).diff(dayjs(messagesChatRoom[messagesChatRoom.length - 1]?.time || 0)) > 0 &&
              systemMessageStartConsult,
          ];
        case ConsultationStatusEnum.MessagesLimitExceeded:
          return [
            {
              message: {
                content: tChatPage('chatRoom.systemMessages.endMessageLimitExceeded', {
                  countMessages: c.messagesLimit,
                  endMessages: chooseEndingWord(c.messagesLimit, [
                    tChatPage('endMessage.one'),
                    tChatPage('endMessage.two'),
                    tChatPage('endMessage.many'),
                  ]),
                }),
                createdAt: c.updatedAt,
              },
              time: c.updatedAt,
              type: MessageType.systemMessage,
            },
            dayjs(startTime).diff(dayjs(messagesChatRoom[messagesChatRoom.length - 1]?.time || 0)) > 0 &&
              systemMessageStartConsult,
          ];
        case ConsultationStatusEnum.EndedByClient:
          return [
            {
              message: { content: tChatPage('chatRoom.systemMessages.endByClient'), createdAt: c.updatedAt },
              time: c.updatedAt,
              type: MessageType.systemMessage,
            },
            dayjs(startTime).diff(dayjs(messagesChatRoom[messagesChatRoom.length - 1]?.time || 0)) > 0 &&
              systemMessageStartConsult,
          ];
        case ConsultationStatusEnum.EndedByExpert:
          return [
            {
              message: { content: tChatPage('chatRoom.systemMessages.endByExpert'), createdAt: c.updatedAt },
              time: c.updatedAt,
              type: MessageType.systemMessage,
            },
            dayjs(startTime).diff(dayjs(messagesChatRoom[messagesChatRoom.length - 1]?.time || 0)) > 0 &&
              systemMessageStartConsult,
          ];
        case ConsultationStatusEnum.Ended:
          return [
            {
              message: { content: tChatPage('chatRoom.systemMessages.end'), createdAt: c.updatedAt },
              time: c.updatedAt,
              type: MessageType.systemMessage,
            },
            dayjs(startTime).diff(dayjs(messagesChatRoom[messagesChatRoom.length - 1]?.time || 0)) > 0 &&
              systemMessageStartConsult,
          ];
        case ConsultationStatusEnum.PendingPayment:
          return isExpertInRoom
            ? {
                message: { content: tChatPage('chatRoom.systemMessages.waitPayment'), createdAt: c.updatedAt },
                time: c.updatedAt,
                type: MessageType.systemMessage,
              }
            : {
                consultation: c,
                time: c.updatedAt,
                type: MessageType.pendingPaymentConsult,
              };
        case ConsultationStatusEnum.ClientRejectChatOffer:
          return {
            message: {
              content: isExpertInRoom
                ? tChatPage('chatRoom.systemMessages.rejectChatOffer.expert')
                : tChatPage('chatRoom.systemMessages.rejectChatOffer.client'),
              createdAt: c.updatedAt,
            },
            time: c.updatedAt,
            type: MessageType.systemMessage,
          };
        case ConsultationStatusEnum.PaymentExpired:
          return {
            type: MessageType.systemMessage,
            time: c.expiresIn!,
            message: {
              content: isExpertInRoom
                ? tChatPage('chatRoom.systemMessages.initInvoiceExpired.expert')
                : tChatPage('chatRoom.systemMessages.initInvoiceExpired.client'),
              createdAt: c.expiresIn,
            },
          };
        default:
          throw new Error(`ConsultationStatusEnum '${c.status}' не предусмотрено."`);
      }
    })
    .filter(Boolean);
};

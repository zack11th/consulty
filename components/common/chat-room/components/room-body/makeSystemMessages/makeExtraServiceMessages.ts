import { InvoiceStatusEnum } from 'api';
import { ConsultationWithOffer } from 'types';
import { makeSystemMessagesFuncArgs, MessageListType, MessageType } from '../room-body';

interface makeExtraServiceMessagesArgs extends makeSystemMessagesFuncArgs {
  activeConsultation: ConsultationWithOffer | undefined;
}

export const makeExtraServiceMessages = ({
  consultsToBeAdded,
  tChatPage,
  isExpertInRoom,
  activeConsultation,
}: makeExtraServiceMessagesArgs): (MessageListType | false)[] => {
  return consultsToBeAdded
    .filter((c) => !!c.extraServices?.length)
    .flatMap((c) =>
      c.extraServices
        .map((service) => {
          switch (service.invoice.status) {
            case InvoiceStatusEnum.Pending:
              return c.id === activeConsultation?.id
                ? isExpertInRoom
                  ? {
                      time: service.invoice.updatedAt,
                      type: MessageType.systemMessage,
                      message: {
                        content: tChatPage('chatRoom.systemMessages.extraService.pendingPayment', {
                          description: service.description,
                        }),
                        createdAt: service.invoice.updatedAt,
                      },
                    }
                  : {
                      time: service.invoice.updatedAt,
                      type: MessageType.pendingPaymentExtraService,
                      extraService: service,
                      price: service.invoice.amount,
                    }
                : false;
            case InvoiceStatusEnum.Fulfilled:
              return {
                time: service.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.extraService.fulfilled.expert', {
                        description: service.description,
                        price: service.invoice.amount,
                      })
                    : tChatPage('chatRoom.systemMessages.extraService.fulfilled.client', {
                        description: service.description,
                        price: service.invoice.amount,
                      }),
                  createdAt: service.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.WaitingWithdrawal:
              return {
                time: service.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.extraService.fulfilled.expert', {
                        description: service.description,
                        price: service.invoice.amount,
                      })
                    : tChatPage('chatRoom.systemMessages.extraService.fulfilled.client', {
                        description: service.description,
                        price: service.invoice.amount,
                      }),
                  createdAt: service.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Withdrawled:
              return {
                time: service.invoice.createdAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.extraService.fulfilled.expert', {
                        description: service.description,
                        price: service.invoice.amount,
                      })
                    : tChatPage('chatRoom.systemMessages.extraService.fulfilled.client', {
                        description: service.description,
                        price: service.invoice.amount,
                      }),
                  createdAt: service.invoice.createdAt,
                },
              };
            case InvoiceStatusEnum.Rejected:
              return {
                time: service.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.extraService.rejected.expert')
                    : tChatPage('chatRoom.systemMessages.extraService.rejected.client'),
                  createdAt: service.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Reversed:
              return {
                time: service.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.extraService.reversed.expert')
                    : tChatPage('chatRoom.systemMessages.extraService.reversed.client'),
                  createdAt: service.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Expired:
              return c.id === activeConsultation?.id
                ? {
                    time: service.invoice.updatedAt,
                    type: MessageType.systemMessage,
                    message: {
                      content: tChatPage('chatRoom.systemMessages.extraService.expired'),
                      createdAt: service.invoice.updatedAt,
                    },
                  }
                : false;
            default:
              throw new Error(`service.invoice?.status ${service.invoice?.status} не предусмотрен.`);
          }
        })
        .filter(Boolean),
    );
};

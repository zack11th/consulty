import { ConsultationInvoiceTypeEnum, InvoiceStatusEnum } from 'api';
import { makeSystemMessagesFuncArgs, MessageListType, MessageType } from '../room-body';

export const makeProlongationMessages = ({
  consultsToBeAdded,
  isExpertInRoom,
  tChatPage,
}: makeSystemMessagesFuncArgs): (MessageListType | false)[] => {
  return consultsToBeAdded
    .filter((c) => c.invoices?.some((invoice) => invoice.type === ConsultationInvoiceTypeEnum.Prolongation))
    .flatMap((c) => {
      const invoicesToMessages: (MessageListType | false)[] = c.invoices
        .filter((invoice) => invoice.type === ConsultationInvoiceTypeEnum.Prolongation)
        .map((invoice) => {
          switch (invoice.invoice?.status) {
            case InvoiceStatusEnum.WaitingWithdrawal:
              return {
                time: invoice.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content:
                    invoice.invoice.amount && invoice.invoice.amount > 0
                      ? isExpertInRoom
                        ? tChatPage('chatRoom.systemMessages.prolongationSuccess.expert')
                        : tChatPage('chatRoom.systemMessages.prolongationSuccess.client', {
                            price: invoice.invoice.amount,
                            currency: '₽',
                          })
                      : tChatPage('chatRoom.systemMessages.prolongationSuccess.free'),
                  createdAt: invoice.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Withdrawled:
              return {
                time: invoice.invoice.createdAt,
                type: MessageType.systemMessage,
                message: {
                  content:
                    invoice.invoice.amount && invoice.invoice.amount > 0
                      ? isExpertInRoom
                        ? tChatPage('chatRoom.systemMessages.prolongationSuccess.expert')
                        : tChatPage('chatRoom.systemMessages.prolongationSuccess.client', {
                            price: invoice.invoice.amount,
                            currency: '₽',
                          })
                      : tChatPage('chatRoom.systemMessages.prolongationSuccess.free'),
                  createdAt: invoice.invoice.createdAt,
                },
              };
            case InvoiceStatusEnum.Fulfilled:
              return {
                time: invoice.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content:
                    invoice.invoice.amount && invoice.invoice.amount > 0
                      ? isExpertInRoom
                        ? tChatPage('chatRoom.systemMessages.prolongationSuccess.expert')
                        : tChatPage('chatRoom.systemMessages.prolongationSuccess.client', {
                            price: invoice.invoice.amount,
                            currency: '₽',
                          })
                      : tChatPage('chatRoom.systemMessages.prolongationSuccess.free'),
                  createdAt: invoice.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Pending:
              return isExpertInRoom
                ? {
                    time: invoice.invoice.updatedAt,
                    type: MessageType.systemMessage,
                    message: {
                      content: tChatPage('chatRoom.systemMessages.pendingProlongation'),
                      createdAt: invoice.invoice.updatedAt,
                    },
                  }
                : {
                    time: invoice.invoice.updatedAt,
                    type: MessageType.pendingPaymentProlongation,
                    consultation: c,
                    price: invoice.invoice.amount,
                  };
            case InvoiceStatusEnum.Rejected:
              return {
                time: invoice.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.prolongationReject.expert')
                    : tChatPage('chatRoom.systemMessages.prolongationReject.client'),
                  createdAt: invoice.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Reversed:
              return {
                time: invoice.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: isExpertInRoom
                    ? tChatPage('chatRoom.systemMessages.prolongationReversed.expert')
                    : tChatPage('chatRoom.systemMessages.prolongationReversed.client'),
                  createdAt: invoice.invoice.updatedAt,
                },
              };
            case InvoiceStatusEnum.Expired:
              return {
                time: invoice.invoice.updatedAt,
                type: MessageType.systemMessage,
                message: {
                  content: tChatPage('chatRoom.systemMessages.expiresInvoice'),
                  createdAt: invoice.invoice.updatedAt,
                },
              };
            case undefined:
              return false;
            default:
              throw new Error(`invoice.invoice?.status ${invoice.invoice?.status} не предусмотрен.`);
          }
        })
        .filter(Boolean);
      return invoicesToMessages;
    });
};

import { useTranslation } from 'next-i18next';
import React, { FC, HTMLAttributes, useCallback, useMemo, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import styled from 'styled-components';

import {
  StarSmallIcon,
  LikeSmallIcon,
  UnLikeSmallIcon,
  WarningGrayIcon,
  ChatSmallIcon,
  ClockIcon,
  ServiceIcon,
} from 'assets/svg';
import { buildRoute, chooseEndingWord, coverImageStyle } from 'utils';
import { actions, selectors } from 'store/ducks';
import { useIsExpertInRoom } from 'hooks/useIsExpertInRoom';
import { CONDITION_DESKTOP, DEFAULT_AVATAR } from 'common/constants';
import { routes } from 'common/routes';
import { Button } from 'components/ui/button';
import Modal from 'components/common/modal';
import { ComplainModal } from 'components/common/complain-modal';
import { CreateConsultationModal } from 'components/common/create-consultation-modal';
import { useAppDispatch } from 'hooks/redux';
import { api, Consultation, ConsultationStatusEnum, InvoiceStatusEnum, UserCategoryMeta } from 'api';

import { LinkButton } from './components/link-button';
import { captureError } from 'utils/captureError';

interface RoomAsideProps extends HTMLAttributes<HTMLDivElement> {
  chatRoomId: number | null;
  activeConsultation?: Consultation;
}

export const RoomAside: FC<RoomAsideProps> = ({ chatRoomId, activeConsultation, ...props }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('chat.page');
  const { t: tUtils } = useTranslation('utils');
  const router = useRouter();
  const user = useSelector(selectors.profile.selectUser);
  const chatRoom = useSelector(selectors.chatRooms.selectChatRoomById(chatRoomId));
  const consultations = useSelector(selectors.consultationsChatRoom.selectConsultations);
  const isExpertInRoom = useIsExpertInRoom(chatRoom);
  const [isCreateConsultModalOpen, setIsCreateConsultModalOpen] = useState(false);
  const [isEndConsultModalOpen, setIsEndConsultModalOpen] = useState(false);
  const isComplainModalOpen = useSelector(selectors.app.selectIsVisibleComplainModal);
  const [expertCategories, setExpertCategories] = useState<UserCategoryMeta[]>([]);

  const isPendingConsultationStatus = useMemo(() => {
    return consultations.some((c) =>
      [ConsultationStatusEnum.PendingPayment, ConsultationStatusEnum.PendingStart].includes(c.status),
    );
  }, [consultations]);

  const isDeletedCompanion = useMemo(() => {
    if (chatRoom) {
      return Boolean(chatRoom.client?.deletedAt) || Boolean(chatRoom.expert?.deletedAt);
    }
  }, [chatRoom]);

  const consultationDuration = useMemo(() => {
    if (!!activeConsultation) {
      return t('aside.howLong', {
        count: activeConsultation.messagesLimit,
        endMessage: chooseEndingWord(activeConsultation.messagesLimit, [
          t('endMessage.one'),
          t('endMessage.two'),
          t('endMessage.many'),
        ]),
        time: activeConsultation.messagesLimit / 20,
        endHours: chooseEndingWord(activeConsultation.messagesLimit / 20, [
          t('endHours.one'),
          t('endHours.two'),
          t('endHours.many'),
        ]),
      });
    }
  }, [t, activeConsultation]);

  const renderRequestButtonText = useCallback(() => {
    if (isPendingConsultationStatus) {
      return isExpertInRoom ? t('aside.buttons.requestSends.expert') : t('aside.buttons.requestSends.user');
    } else {
      return isExpertInRoom ? t('aside.buttons.request.expert') : t('aside.buttons.request.user');
    }
  }, [isExpertInRoom, isPendingConsultationStatus]);

  // end consultation handlers
  const handleEndConsultation = () => {
    setIsEndConsultModalOpen(true);
  };

  const confirmEndConsult = async () => {
    try {
      setIsEndConsultModalOpen(false);
      if (activeConsultation) {
        await api.V1ConsultationsApi.consultationsControllerStopConsultation(activeConsultation.id);
      }
    } catch (error: any) {
      captureError(error);
      toast.error(error.message || tUtils('somethingWrong'));
    }
  };

  // request consultation handlers
  const handleRequestButtonClick = async () => {
    setIsCreateConsultModalOpen(true);
    if (!isExpertInRoom) {
      const { data } = await api.V1UsersApi.usersControllerGetUserOne(chatRoom!.expertId);
      setExpertCategories(data.categories || []);
    }
  };

  const createConsultationChatOffer = async (userCategoryMetaId: number) => {
    try {
      const { data } = await api.V1ChatsApi.chatRoomsControllerCreateChatConsultationOffer(chatRoomId!, {
        userCategoryMetaId,
        redirectUrl: `${window.location.origin}${routes.chat}?activeTab=consult&roomId=${chatRoom?.id}`,
      });
      dispatch(actions.consultationsChatRoom.changeConsultationByIdOrAdd(data.consultation));
      dispatch(actions.chatRooms.addUnreadMark({ roomId: data.chatRoomId }));
      dispatch(
        actions.chatRooms.changeConsultationStatus({
          roomId: data.chatRoomId,
          consultationId: data.consultationId,
          status: ConsultationStatusEnum.PendingPayment,
        }),
      );
    } catch (error: any) {
      captureError(error);
      toast.error(error.message || tUtils('somethingWrong'));
    }
  };

  const requestingConsultation = async (userCategoryMetaId: number) => {
    try {
      const { data } = await api.V1ConsultationsApi.consultationsControllerCreateOneFromUserCategoryMeta({
        userCategoryMetaId,
        redirectUrl: `${window.location.origin}${routes.chat}?activeTab=consult&roomId=${chatRoom?.id}`,
      });
      const redirectUrl = data.invoices.find((invoice) => invoice.invoice?.status === InvoiceStatusEnum.Pending)
        ?.invoice?.checkoutUrl;

      if (redirectUrl) {
        window.location.href = redirectUrl;
      }
    } catch (error: any) {
      captureError(error);
      toast.error(error.message || tUtils('somethingWrong'));
    }
  };

  const handleClickProlongation = () => {
    dispatch(actions.app.setIsVisibleProlongationModal(true));
  };

  const handleClickExtraService = () => {
    dispatch(actions.app.setIsVisibleExtraServiceModal(true));
  };

  return (
    <>
      <div {...props}>
        <DesktopContainer>
          <TopBlock>
            <Avatar $url={isExpertInRoom ? chatRoom?.client?.avatarUrl : chatRoom?.expert?.avatarUrl}>
              {!isExpertInRoom && (
                <Rate>
                  <StarSmallIcon />
                  <span>{chatRoom?.expert?.expertRating?.toFixed(2).replace('.', ',') || '0,00'}</span>
                </Rate>
              )}
            </Avatar>
            <SubTitle>
              {isDeletedCompanion
                ? tUtils('userIsDeleted')
                : isExpertInRoom
                ? t('aside.youConsult')
                : t('aside.expertConsultYou')}
            </SubTitle>
            <Name>
              {isExpertInRoom ? (
                `${chatRoom?.client?.firstName || ''} ${chatRoom?.client?.lastName || ''}`
              ) : (
                <Link href={buildRoute(routes.expert, { id: chatRoom?.expertId || '' })} passHref>
                  <a>
                    {chatRoom?.expert?.firstName || ''} {chatRoom?.expert?.lastName || ''}
                  </a>
                </Link>
              )}
            </Name>

            {!isExpertInRoom ? (
              <>
                <TableRow>
                  <TableName>{t('aside.allConsultation')}</TableName>
                  <TableValue>{chatRoom?.expert?.expertConsultationsCount}</TableValue>
                </TableRow>
                <TableRow>
                  <TableName>{t('aside.reviews')}</TableName>
                  <TableValue>
                    <span>{chatRoom?.expert?.expertLikesCount}</span>
                    <LikeSmallIcon />
                    <span>{chatRoom?.expert?.expertDislikesCount}</span>
                    <UnLikeSmallIcon />
                  </TableValue>
                </TableRow>
              </>
            ) : (
              <TableRow>
                <TableName>{t('aside.reviews')}</TableName>
                <TableValue>
                  <span>{chatRoom?.client?.clientLikesCount}</span>
                  <LikeSmallIcon />
                  <span>{chatRoom?.client?.clientDislikesCount}</span>
                  <UnLikeSmallIcon />
                </TableValue>
              </TableRow>
            )}
          </TopBlock>

          {!isDeletedCompanion &&
            (!!activeConsultation ? (
              <>
                <Separator></Separator>
                <StyledConsultation>
                  <BlockTitle>{t('aside.thisConsult')}</BlockTitle>
                  <ConsultInfo>
                    <Label>{t('aside.theme')}</Label>
                    <ConsultValue>{activeConsultation.category?.name}</ConsultValue>
                  </ConsultInfo>
                  <ConsultInfo>
                    <Label>{t('aside.duration')}</Label>
                    <ConsultValue>{consultationDuration}</ConsultValue>
                  </ConsultInfo>
                  <ConsultButton>
                    <Button bordered block onClick={handleEndConsultation}>
                      {t('aside.buttons.endConsult')}
                    </Button>
                  </ConsultButton>
                </StyledConsultation>
                <Separator></Separator>

                {isExpertInRoom && (
                  <>
                    <Additional>
                      <BlockTitle>{t('aside.additionally')}</BlockTitle>
                      <LinkButton
                        icon={<ClockIcon />}
                        text={t('aside.extendConsult')}
                        onClick={handleClickProlongation}
                      />
                      <LinkButton
                        icon={<ServiceIcon />}
                        text={t('aside.extraService')}
                        onClick={handleClickExtraService}
                      />
                    </Additional>
                    <Separator></Separator>
                  </>
                )}
              </>
            ) : (
              <RequestButtonContainer>
                <Button
                  bordered
                  block
                  disabled={isPendingConsultationStatus}
                  fontSize="14px"
                  hasPaddingHorizontal={false}
                  onClick={handleRequestButtonClick}
                >
                  {renderRequestButtonText()}
                </Button>
              </RequestButtonContainer>
            ))}

          <Support>
            <BlockTitle>{t('aside.support')}</BlockTitle>
            <LinkButton
              icon={<WarningGrayIcon />}
              text={t('aside.problems')}
              onClick={() => dispatch(actions.app.setIsVisibleComplainModal(true))}
            />
            <LinkButton
              icon={<ChatSmallIcon />}
              text={t('aside.answers')}
              onClick={() => router.push({ pathname: routes.profile, query: { activeTab: 'support' } })}
            />
          </Support>
        </DesktopContainer>

        <MobileContainer>
          <PaymentConsult>
            {isDeletedCompanion ? tUtils('userIsDeleted') : t('aside.mobilePaymentConsult')}
          </PaymentConsult>
          {!isDeletedCompanion && (
            <Button
              size="small"
              fontSize="12px"
              disabled={isPendingConsultationStatus}
              onClick={!!activeConsultation ? handleEndConsultation : handleRequestButtonClick}
            >
              {!!activeConsultation
                ? t('aside.buttons.mobile.endConsult')
                : isExpertInRoom
                ? t('aside.buttons.mobile.request.expert')
                : t('aside.buttons.mobile.request.client')}
            </Button>
          )}
        </MobileContainer>
      </div>

      {isEndConsultModalOpen && (
        <Modal isVisible={isEndConsultModalOpen} onClose={() => setIsEndConsultModalOpen(false)}>
          <ConfirmEndTitle>{t('modals.confirmEndConsultation.title')}</ConfirmEndTitle>
          <ConfirmEndAction>
            <Button hasPaddingHorizontal={false} width="120px" onClick={confirmEndConsult}>
              {t('modals.confirmEndConsultation.yes')}
            </Button>
            <Button bordered hasPaddingHorizontal={false} width="120px" onClick={() => setIsEndConsultModalOpen(false)}>
              {t('modals.confirmEndConsultation.no')}
            </Button>
          </ConfirmEndAction>
        </Modal>
      )}

      <ComplainModal
        isVisible={isComplainModalOpen}
        onClose={() => dispatch(actions.app.setIsVisibleComplainModal(false))}
      />
      <CreateConsultationModal
        isVisible={isCreateConsultModalOpen}
        onClose={() => setIsCreateConsultModalOpen(false)}
        IsExpertInRoom={isExpertInRoom}
        expertCategories={isExpertInRoom ? user.categories : expertCategories}
        handleSubmitModal={isExpertInRoom ? createConsultationChatOffer : requestingConsultation}
      />
    </>
  );
};

// container
const DesktopContainer = styled.div`
  display: none;
  ${CONDITION_DESKTOP} {
    display: flex;
    width: 100%;
    flex-direction: column;
    min-height: 100%;
    border-left: 1px solid var(--gray6);
    border-right: 1px solid var(--gray6);
  }
`;
const TopBlock = styled.div`
  padding: 31px 20px 26px;
  text-align: center;
`;
const StyledConsultation = styled.div`
  padding: 20px;
`;
const Additional = styled.div`
  padding: 20px;
`;
const Support = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: 20px;
`;
const Separator = styled.div`
  width: 100%;
  height: 3px;
  flex-shrink: 0;
  background-color: #ebebeb;
`;
const BlockTitle = styled.h3`
  font-size: 17px;
  font-weight: bold;
  margin-bottom: 17px;
`;

// top-block
const Avatar = styled.div<{ $url?: string }>`
  position: relative;
  width: 120px;
  height: 120px;
  border-radius: 15px;
  margin: 0 auto 20px;
  ${({ $url }) => ({ ...coverImageStyle($url || DEFAULT_AVATAR) })}
`;
const Rate = styled.div`
  position: absolute;
  height: 22px;
  width: 65px;
  display: flex;
  justify-content: center;
  align-items: center;
  bottom: 0;
  left: 50%;
  transform: translate(-50%, 50%);
  padding: 0 13px 0 6px;
  border-radius: 11px;
  box-shadow: 0 0 15px 0 rgba(0, 0, 0, 0.1);
  background-color: var(--white);
  font-size: 13px;
  color: #2a0065;
  & > span {
    margin-left: 5px;
  }
`;
const SubTitle = styled.p`
  font-size: 13px;
  color: rgba(60, 60, 67, 0.6);
`;
const Name = styled.p`
  margin: 10px 0 30px;
  font-size: 17px;
  font-weight: bold;
  & > a {
    &:hover {
      text-decoration: underline;
    }
  }
`;
const TableRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 10px;
  line-height: 20px;
  align-items: center;
  color: var(--text);
`;
const TableName = styled.p`
  font-size: 14px;
`;
const TableValue = styled.div`
  font-size: 15px;
  display: flex;
  align-items: center;
  & > span {
    margin-right: 4px;
    margin-left: 10px;
  }
  & > svg {
    margin-top: -2px;
  }
`;

// consultation
const ConsultInfo = styled.div`
  margin-bottom: 20px;
`;
const Label = styled.div`
  font-size: 13px;
  line-height: 20px;
  color: var(--gray2);
`;
const ConsultValue = styled.div`
  font-size: 13px;
  line-height: 20px;
`;
const ConsultButton = styled.div`
  margin-top: 30px;
  margin-bottom: 10px;
`;
const RequestButtonContainer = styled.div`
  margin: 0 20px;
`;

// mobile container
const MobileContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 11px 15px;
  width: 100%;
  height: 50px;
  background-color: var(--white);
  border-bottom: 1px solid #e0e0e0;
  border-top: 1px solid #e0e0e0;
  ${CONDITION_DESKTOP} {
    display: none;
  }
`;
const PaymentConsult = styled.p`
  font-size: 14px;
  line-height: 20px;
`;

// modals
const ConfirmEndTitle = styled.h3`
  font-size: 17px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 30px;
`;
const ConfirmEndAction = styled.div`
  display: flex;
  justify-content: space-between;
`;

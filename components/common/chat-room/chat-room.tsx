import React, { FC, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import router from 'next/router';
import dayjs from 'dayjs';
import { toast } from 'react-toastify';
import { useTranslation } from 'next-i18next';

import { useChatPrivateCannel } from 'hooks/useChatPrivateChannel';
import { useIsExpertInRoom } from 'hooks/useIsExpertInRoom';
import { CONDITION_DESKTOP, HEADER_HEIGHT_DESKTOP } from 'common/constants';
import { selectors, actions } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';
import { MessageExtended } from 'types';
import { routes } from 'common/routes';
import { api } from 'api';

import { RoomHeader } from './components/room-header';
import { ChatInputMessage } from './components/chat-input-message';
import { RoomAside } from './components/room-aside';
import { RoomBody } from './components/room-body';
import { ReviewClientModal } from './components/review-client-modal';
import { ReviewExpertModal } from './components/review-expert-modal';
import { ProlongationModal } from '../prolongation-modal';
import { ExtraServiceModal } from '../extra-service-modal/extra-service-modal';
import { captureError } from 'utils/captureError';

export const ChatRoom: FC = () => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('utils');
  const user = useSelector(selectors.profile.selectUser);
  const chatRoomId = useSelector(selectors.chatRooms.selectCurrentChatRoomId);
  const chatRoom = useSelector(selectors.chatRooms.selectChatRoomById(chatRoomId));
  const activeConsultation = useSelector(selectors.consultationsChatRoom.selectActiveConsultation);
  const [consultationIdForReview, setConsultationIdForReview] = useState(0);
  const isProlongationModalOpen = useSelector(selectors.app.selectIsVisibleProlongationModal);
  const isExtraServiceModalOpen = useSelector(selectors.app.selectIsVisibleExtraServiceModal);
  const isExpertInRoom = useIsExpertInRoom(chatRoom);
  const lastMessageId = useRef<number | null>(null);
  useChatPrivateCannel();

  useEffect(() => {
    return () => {
      dispatch(actions.consultationsChatRoom.setIsReviewModalOpen(false));
    };
  }, [chatRoomId, dispatch]);

  useEffect(() => {
    if (chatRoom) {
      dispatch(actions.consultationsChatRoom.setConsultationsChatRoom(chatRoom));
    }
  }, [chatRoom?.id, chatRoom?.consultations]);

  useEffect(() => {
    if (chatRoom?.lastMessageId) {
      lastMessageId.current = chatRoom?.lastMessageId;
    }
  }, [chatRoom?.lastMessageId]);

  useEffect(() => {
    if (chatRoom && !chatRoom.isTempRoom) {
      dispatch(actions.chatRooms.resetUnreadMark({ chatRoomId: chatRoom.id }));
    }
    return () => {
      if (chatRoom?.id && !chatRoom.isTempRoom && lastMessageId.current) {
        api.V1ChatsApi.chatRoomsControllerUpdateLastReadMessage(chatRoom.id, { messageId: lastMessageId.current });
        dispatch(actions.chatRooms.resetUnreadMark({ chatRoomId: chatRoom.id }));
        dispatch(
          actions.chatRooms.changeLastReadMessage({
            chatRoomId: chatRoom.id,
            isExpertInRoom: user.id === chatRoom.expertId,
            lastMessageId: lastMessageId.current,
          }),
        );
      }
    };
  }, [chatRoom?.id, chatRoom?.isTempRoom]);

  useEffect(() => {
    if (activeConsultation) {
      setConsultationIdForReview(activeConsultation.id);
    }
  }, [activeConsultation]);

  const onSendMessage = async ({ message = '', attachments = [] }: { message?: string; attachments?: string[] }) => {
    const content = message.trim();
    if (content || attachments.length) {
      const message: MessageExtended = {
        content: content || '',
        ...(!!attachments.length && { attachments }),
        status: 'pending',
        authorId: user.id,
        createdAt: dayjs().toISOString(),
        author: { ...user },
      };
      if (!chatRoom?.isTempRoom) {
        dispatch(actions.chatMessages.addChatMessage(message));
        const actionResult = await dispatch(
          actions.chatMessages.sendChatMessage({
            chatRoomId: chatRoomId!,
            content: content || '',
            ...(!!attachments.length && { attachments }),
            consultationId: activeConsultation?.id,
          }),
        );
        const newMessage = unwrapResult(actionResult);
        newMessage && dispatch(actions.chatRooms.updateLastMessage({ message: newMessage, user }));
      } else {
        dispatch(actions.chatMessages.addChatMessage(message));
        const actionResult = await dispatch(
          actions.chatRooms.replaceTempToRealChatRoom({
            client: user,
            expert: chatRoom.expert,
            initialMessage: { content: content || '', ...(!!attachments.length && { attachments }) },
            tempRoomId: chatRoom.id,
          }),
        );
        const newRoom = unwrapResult(actionResult);
        newRoom && router.replace({ query: { activeTab: 'consult', roomId: newRoom.room.id } });
      }
    }
  };

  const onProlongation = async ({ price }: { price: number }) => {
    try {
      if (activeConsultation) {
        await api.V1ConsultationsApi.consultationsControllerCreateProlongationRequest(activeConsultation.id, {
          price,
          redirectUrl: `${window.location.origin}${routes.chat}?activeTab=consult&roomId=${chatRoom?.id}`,
        });
      }
    } catch (error: any) {
      captureError(error);
      toast.error(error.message || t('somethingWrong'));
    }
  };

  return !chatRoom ? null : (
    <>
      <Container>
        <Wrapper>
          <StyledRoomHeader chatRoomId={chatRoomId} />
          <StyledRoomBody chatRoomId={chatRoomId} />
          <StyledRoomAside chatRoomId={chatRoomId} activeConsultation={activeConsultation} />
          <RoomFooter>
            <ChatInputMessage
              onSendMessage={onSendMessage}
              disabled={!chatRoom?.expert?.hasFreeChat && !activeConsultation}
            />
          </RoomFooter>
        </Wrapper>
      </Container>

      {chatRoom &&
        (isExpertInRoom ? (
          <ReviewClientModal userId={user.id} clientId={chatRoom.clientId} consultationId={consultationIdForReview} />
        ) : (
          <ReviewExpertModal expertId={chatRoom.expertId} userId={user.id} consultationId={consultationIdForReview} />
        ))}

      <ProlongationModal
        isVisible={isProlongationModalOpen}
        onClose={() => dispatch(actions.app.setIsVisibleProlongationModal(false))}
        handleProlongation={onProlongation}
      />

      <ExtraServiceModal
        isVisible={isExtraServiceModalOpen}
        onClose={() => dispatch(actions.app.setIsVisibleExtraServiceModal(false))}
      />
    </>
  );
};

const Container = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1100;
  overflow: hidden;
  background-color: var(--white);
  ${CONDITION_DESKTOP} {
    z-index: 0;
  }
`;
const Wrapper = styled.div`
  height: 100%;
  display: grid;
  align-content: space-around;
  grid-template-columns: 1fr;
  grid-template-rows: 60px 1fr minmax(60px, auto);
  grid-template-areas:
    'header'
    'aside'
    'body'
    'footer';
  ${CONDITION_DESKTOP} {
    grid-template-columns: 1fr 320px;
    grid-template-rows: 60px 1fr minmax(75px, auto);
    grid-template-areas:
      'header header'
      'body aside'
      'footer aside';
  }
`;
const StyledRoomHeader = styled(RoomHeader)`
  grid-area: header;
`;
const StyledRoomBody = styled(RoomBody)`
  grid-area: body;
  overflow-y: auto;
  /* for Safari browser */
  /* max-height: calc(100vh - 60px - 60px); */
  ${CONDITION_DESKTOP} {
    /* max-height: calc(100vh - ${HEADER_HEIGHT_DESKTOP} - 60px - 75px); */
  }
`;
const StyledRoomAside = styled(RoomAside)`
  grid-area: aside;
  display: flex;
  ${CONDITION_DESKTOP} {
    overflow-y: auto;
    /* for Safari browser */
    /* max-height: calc(100vh - ${HEADER_HEIGHT_DESKTOP} - 60px); */
  }
`;
const RoomFooter = styled.div`
  grid-area: footer;
  padding: 12px 15px;
  ${CONDITION_DESKTOP} {
    padding: 10px 30px 20px;
  }
`;

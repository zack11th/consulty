import React, { Dispatch, FC, SetStateAction } from 'react';
import { toast } from 'react-toastify';
import { Controller, useFormContext } from 'react-hook-form';
import { AuthFormSteps, AuthFormValues } from '../../auth-modal';
import { InputPhone } from '../../../../ui/input-phone';
import { Button } from '../../../../ui/button';
import { Checkbox } from '../../../../ui/checkbox';
import Link from 'next/link';
import { routes } from '../../../../../common/routes';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { api } from '../../../../../api';
import { useSelector } from 'react-redux';
import { actions, selectors } from '../../../../../store/ducks/app';
import { clearTel } from '../../../../../utils';
import { useAppDispatch } from '../../../../../hooks/redux';
import { useAutofocus } from 'hooks/useAutofocus';
import ym from 'react-yandex-metrika';
import { ENVIRONMENT_TYPE } from 'common/constants';


interface AuthNumberFormProps {
  setStep: Dispatch<SetStateAction<AuthFormSteps>>;
  currentPhone: string;
  setCurrentPhone: Dispatch<SetStateAction<string>>;
}

const isNotFilledTel = (t: any) => {
  return (v: string) => {
    const clearedTel = clearTel(v);
    if (clearedTel.length === 1) {
      return t('authModal.numberRequired');
    }
    return clearedTel.length < 8 ? t('authModal.incorrectNumber') : undefined;
  };
};

const sendDataToMetrics = () => {
  if (ENVIRONMENT_TYPE === 'production') {
    ym('reachGoal', ['Landing_ex-Modal_window-get_sms']);
    window.gtag('event', 'Landing_ex-Modal_window-get_sms');
    window.fbq('trackCustom', 'Landing_ex-Modal_window-get_sms');
  }
};

export const AuthNumberForm: FC<AuthNumberFormProps> = ({ setStep, currentPhone, setCurrentPhone }) => {
  const { t } = useTranslation('header.component');
  const { control, handleSubmit, watch, setError, setFocus, getValues } = useFormContext<AuthFormValues>();
  const isWait = useSelector(selectors.selectAuthModalIsWait);
  const dispatch = useAppDispatch();

  useAutofocus<AuthFormValues>('number', setFocus);

  const onGetSms = async ({ number, agreement }: AuthFormValues) => {
    sendDataToMetrics();
    setCurrentPhone(clearTel(number));
    if(localStorage.getItem('phone') !== getValues('number')){
      dispatch(actions.setCounter(60));
    }
    localStorage.setItem('phone',number);
    if (currentPhone === clearTel(number)) {
      if (isWait) {
        setStep(AuthFormSteps.smsCode);
        return;
      }
    } else {
      if (isWait) {
        dispatch(actions.setWait(false));
      }
    }
    if (agreement) {
      try {
        const result = await api.V1UsersApi.usersControllerSendSms({ phone: clearTel(number) });
        if (result.status === 201) {
          setStep(AuthFormSteps.smsCode);
          process.env.NEXT_PUBLIC_ENVIRONMENT !== 'production' && toast.info(result.data, { autoClose: false });
        }
      } catch (error: any) {
        if (error.statusCode === 429) {
          setError('code', { message: t('authModal.notSendCode') });
          setStep(AuthFormSteps.smsCode);
        } else {
          toast.error(error.message || t('authModal.somethingWrong'));
        }
      }
    }
  };

  return (
    <>
      <Controller
        name={'number'}
        control={control}
        rules={{
          validate: isNotFilledTel(t),
        }}
        render={({ field, fieldState: { error } }) => (
          <InputPhone onChange={field.onChange} value={field.value} error={error?.message} ref={field.ref} />
        )}
      />
      <Button disabled={!watch('agreement')} onClick={handleSubmit(onGetSms)}>
        {t('authModal.getSms')}
      </Button>

      <AgreementContainer>
        <Controller
          control={control}
          name={'agreement'}
          render={({ field }) => <Checkbox checked={field.value} onChange={field.onChange} />}
        />
        <Agreement>
          {t('authModal.agreementFirstPart')}
          <Link href={routes.home}>{t('authModal.termsOfOffer')}</Link>
          {t('authModal.agreementSecondPart')}
        </Agreement>
      </AgreementContainer>
    </>
  );
};

const Agreement = styled.div`
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.25;
  letter-spacing: normal;
  color: var(--paleText);
  max-width: 230px;

  & > a {
    font-weight: bold;
    text-decoration: none;
    color: var(--paleText);

    :hover {
      text-decoration: underline;
    }
  }
`;
const AgreementContainer = styled.div`
  display: flex;
  align-items: flex-start;
`;

import React, { ChangeEvent, Dispatch, FC, SetStateAction, useState, useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { coverImageStyle } from '../../../../../utils';
import { useDebounce } from 'use-debounce';
import styled from 'styled-components';
import { Camera } from '../../../../../assets/svg';
import { useTranslation } from 'next-i18next';
import { Input } from '../../../../ui/input';
import { Button } from '../../../../ui/button';
import { useAppDispatch } from '../../../../../hooks/redux';
import { fetchUpdateIamProfile } from '../../../../../store/ducks/profile/actions';
import { unwrapResult } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import Tip from '../../../../ui/tip';
import { emailPattern, namePattern } from '../../../../../constants';
import { AuthFormSteps } from '../../auth-modal';
import { usePrepareImageToUpload } from 'hooks/usePrepareImageToUpload';
import { api } from 'api';
import { useAutofocus } from 'hooks/useAutofocus';
import { captureError } from 'utils/captureError';

interface AuthFinalStepFormProps {
  closeModal: () => void;
  setStep: Dispatch<SetStateAction<AuthFormSteps>>;
}

interface FinalStepFormValues {
  firstName: string;
  lastName: string;
  email: string;
  avatar: string;
}

export const AuthFinalStepForm: FC<AuthFinalStepFormProps> = ({ closeModal, setStep }) => {
  const { control, watch, handleSubmit, setError, clearErrors, setFocus } = useForm<FinalStepFormValues>({
    defaultValues: {
      firstName: '',
      lastName: '',
      email: '',
      avatar: '',
    },
  });
  const [firstName, lastName, email] = watch(['firstName', 'lastName', 'email']);
  const [emailForCheck] = useDebounce(email, 500);

  const dispatch = useAppDispatch();
  const { t } = useTranslation('header.component');
  const { t: u } = useTranslation('utils');

  const [imgSrc, setImgSrc] = useState<string>();
  const [file, setFile] = useState<File>();
  const { selectFile, uploadFileToCloud } = usePrepareImageToUpload();

  useAutofocus<FinalStepFormValues>('firstName', setFocus);

  useEffect(() => {
    const checkEmailIsUniq = async () => {
      try {
        const { data } = await api.V1UsersApi.usersControllerEmailIsUniq(emailForCheck);
        if (!data) {
          setError('email', { message: u('emailIsNoUniq') });
        } else {
          clearErrors('email');
        }
      } catch (error: any) {}
    };
    if (Boolean(emailForCheck)) {
      checkEmailIsUniq();
    }
  }, [emailForCheck]);

  const onSubmit = async ({ firstName, lastName, email }: FinalStepFormValues) => {
    try {
      let avatarUrl: string | undefined;
      if (file) {
        avatarUrl = await uploadFileToCloud(file);
      }
      const res = await dispatch(
        fetchUpdateIamProfile({
          firstName,
          lastName,
          email,
          ...(avatarUrl && { avatarUrl }),
        }),
      );
      if (unwrapResult(res)) {
        toast.success(t('saveInfo'));
        api.V1UsersApi.usersControllerSendVerificationEmail();
        closeModal();
        setStep(AuthFormSteps.number);
      }
    } catch (e: any) {
      captureError(e);
      toast.error(e.message || u('somethingWrong'));
    }
  };

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { files } = e.target;
    try {
      selectFile(files, ({ readerResult, file }) => {
        setFile(file);
        setImgSrc(readerResult);
      });
    } catch (error: any) {
      captureError(e);
      toast.error(error.message);
    }
  };

  return (
    <Container>
      <Tip text={u('imageSize')}>
        <Avatar htmlFor={'avatar'} $url={imgSrc}>
          <CameraContainer isVisible={!imgSrc}>
            <Camera />
          </CameraContainer>
        </Avatar>
      </Tip>

      <Title>
        {firstName.length > 0 || lastName.length > 0
          ? `${firstName.trim()} ${lastName.trim()}`
          : t('authModal.yourName')}
      </Title>
      <Note>{t('authModal.enterName')}</Note>
      <Controller
        name={'firstName'}
        control={control}
        rules={{
          pattern: { value: namePattern, message: u('nameRules') },
          required: { value: true, message: u('required') },
          maxLength: { value: 45, message: u('maxLength') },
        }}
        render={({ field: { value, onChange, ref }, fieldState: { error } }) => (
          <Input
            value={value}
            onChange={onChange}
            error={error?.message}
            placeholder={t('authModal.firstName')}
            ref={ref}
          />
        )}
      />
      <Controller
        name={'lastName'}
        control={control}
        rules={{
          pattern: { value: namePattern, message: u('nameRules') },
          required: { value: true, message: u('required') },
          maxLength: { value: 45, message: u('maxLength') },
        }}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input value={value} onChange={onChange} error={error?.message} placeholder={t('authModal.lastName')} />
        )}
      />
      <Controller
        name={'email'}
        control={control}
        rules={{
          pattern: { value: emailPattern, message: u('incorrectEmail') },
          required: { value: true, message: u('required') },
        }}
        render={({ field: { value, onChange }, fieldState: { error } }) => (
          <Input value={value} onChange={onChange} error={error?.message} placeholder={t('authModal.email')} />
        )}
      />
      <Button
        disabled={firstName.length === 0 || lastName.length === 0 || email.length === 0}
        onClick={handleSubmit(onSubmit)}
      >
        {t('authModal.enter')}
      </Button>
      <input id={'avatar'} onChange={onChange} type="file" accept="image/png, image/jpeg" style={{ display: 'none' }} />
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;

  & > button {
    width: 100%;
    margin-top: 15px;
  }

  & > div > input {
    margin: 5px 0;
  }
`;

const Avatar = styled.label<{ $url?: string }>`
  width: 70px;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 70px;
  border-radius: 15px;
  background-color: ${({ $url }) => ($url ? 'transparent' : 'var(--purple)')};

  ${({ $url }) => ({ ...coverImageStyle($url) })}
  :hover {
    cursor: pointer;
  }
`;

const Title = styled.div`
  font-size: 19px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.05;
  letter-spacing: normal;
  text-align: center;
  color: var(--text);
  padding-bottom: 15px;
  margin-top: 20px;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Note = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.54;
  letter-spacing: normal;
  text-align: center;
  color: var(--text);
  padding-bottom: 30px;
  max-width: 260px;
`;

const CameraContainer = styled.div<{ isVisible: boolean }>`
  opacity: ${(props) => (props.isVisible ? '1' : '0')};
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 15px;
  transition: opacity 0.5s ease, background-color 0.5s ease;

  :hover {
    opacity: 1;
    background-color: rgb(139 51 255 / 50%);
  }

  & > svg {
    transform: scale(1.5);
  }
`;

import React, { Dispatch, FC, SetStateAction } from 'react';
import { useTranslation } from 'next-i18next';
import { Controller, useFormContext } from 'react-hook-form';
import { AuthFormSteps, AuthFormValues } from '../../auth-modal';
import { Input } from '../../../../ui/input';
import styled from 'styled-components';
import { api } from '../../../../../api';
import { toast } from 'react-toastify';
import { useAppDispatch } from '../../../../../hooks/redux';
import { fetchSelfPromocode } from '../../../../../store/ducks/profile/actions';
import { unwrapResult } from '@reduxjs/toolkit';
import { useAutofocus } from 'hooks/useAutofocus';
import ym from 'react-yandex-metrika';
import { ENVIRONMENT_TYPE } from 'common/constants';

interface AuthPromoCodeFormProps {
  setStep: Dispatch<SetStateAction<AuthFormSteps>>;
}

const PROMOCODE_LENGTH = 4;

const sendDataToMetrics = () => {
  if (ENVIRONMENT_TYPE === 'production') {
    ym('reachGoal', ['Landing_ex-modal_window-promo_code']);
    window.gtag('event', 'Landing_ex-modal_window-promo_code');
    window.fbq('trackCustom', 'Landing_ex-modal_window-promo_code');
  }
};

export const AuthPromoCodeForm: FC<AuthPromoCodeFormProps> = ({ setStep }) => {
  const { t } = useTranslation('header.component');
  const { control, setError, setFocus } = useFormContext<AuthFormValues>();
  const dispatch = useAppDispatch();

  useAutofocus<AuthFormValues>('promoCode', setFocus);

  const onNoHave = () => {
    setStep(AuthFormSteps.final);
  };

  const validatePromoCode = async (code: string) => {
    try {
      const { data: isAccepted } = await api.V1PromocodesApi.promocodesControllerApplyPromocode({
        code: code,
      });
      if (isAccepted) {
        sendDataToMetrics();
        toast.success(t('authModal.promoCodeIsAccepted'));
        try {
          const res = await dispatch(fetchSelfPromocode());
          unwrapResult(res);
        } catch (e) {
          toast.error(t('dontSuccessFetchPromocodes'));
        }
        setStep(AuthFormSteps.final);
      } else {
        setError('promoCode', t('authModal.incorrectPromoCode'));
      }
    } catch (e) {
      toast.error(t('authModal.somethingWrong'));
    }
  };

  return (
    <>
      <Controller
        name={'promoCode'}
        control={control}
        render={({ field: { onChange, value, ref }, fieldState: { error } }) => (
          <Input
            onChange={(e) => {
              const { value } = e.target;
              if (value.length > PROMOCODE_LENGTH) {
                return;
              }
              if (value.length === PROMOCODE_LENGTH) {
                validatePromoCode(value);
              }
              onChange(e);
            }}
            centered
            value={value}
            ref={ref}
            error={error?.message}
            placeholder={t('authModal.promoCodePlaceholder')}
          />
        )}
      />
      <ButtonsContainer>
        <Button onClick={onNoHave}>{t('authModal.noHavePromoCode')}</Button>
      </ButtonsContainer>
    </>
  );
};

const ButtonsContainer = styled.div`
  margin-top: 65px;
`;

const Button = styled.div`
  color: var(--purple);
  padding: 7.5px 0;
  text-align: center;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

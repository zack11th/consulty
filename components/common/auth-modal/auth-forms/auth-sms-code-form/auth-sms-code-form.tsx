import React, { Dispatch, FC, SetStateAction, useEffect, useRef, useState } from 'react';
import { AuthFormSteps, AuthFormValues } from '../../auth-modal';
import { Controller, useFormContext } from 'react-hook-form';
import { Input } from '../../../../ui/input';
import { useTranslation } from 'next-i18next';
import styled from 'styled-components';
import { Timer } from '../../timer';
import { api } from '../../../../../api';
import { useAppDispatch } from '../../../../../hooks/redux';
import { actions as appActions, selectors } from '../../../../../store/ducks/app';
import { useSelector } from 'react-redux';
import { clearTel } from '../../../../../utils';
import {
  fetchConsultationsWithoutReview,
  fetchSelfPromocode,
  fetchSignInSms,
} from '../../../../../store/ducks/profile/actions';
import { unwrapResult } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import { actions } from 'store/ducks';
import { useAutofocus } from 'hooks/useAutofocus';
import { captureError } from 'utils/captureError';

interface AuthSmsCodeFormProps {
  closeModal: () => void;
  setStep: Dispatch<SetStateAction<AuthFormSteps>>;
}

export const AuthSmsCodeForm: FC<AuthSmsCodeFormProps> = ({ setStep, closeModal }) => {
  const { t } = useTranslation('header.component');
  const dispatch = useAppDispatch();
  const { control, watch, getValues, clearErrors, setError, setFocus } = useFormContext<AuthFormValues>();
  const code = watch('code');

  const isWait = useSelector(selectors.selectAuthModalIsWait);
  const counter = useSelector(selectors.selectAuthModalCounter);

  const setWait = (value: boolean) => {
    if(counter !== 60) return
    if (value) {
      dispatch(appActions.setWait(value));
      dispatch(appActions.setCounter(59));
    } else {
      dispatch(appActions.setWait(value));
    }
  };  

  useEffect(() => {
    setWait(true);
  }, []);  

  useEffect(() => {
    if (code.length === 4) {
      validateCode(code).then();
    }
  }, [code]);

  const onRepeatMessage = async () => {
    try {
      const { number } = getValues();
      const { data } = await api.V1UsersApi.usersControllerSendSms({ phone: clearTel(number) }).then();
      setWait(true);
      process.env.NEXT_PUBLIC_ENVIRONMENT !== 'production' && toast.info(data, { autoClose: false });
    } catch (e: any) {
      if (e.statusCode === 429) {
        setError('code', { message: t('authModal.notSendCode') });
        setWait(true);
      }
    }
  };

  const validateCode = async (code: string) => {
    const { number } = getValues();
    try {
      const res = await dispatch(
        fetchSignInSms({
          smscode: code,
          phone: clearTel(number),
        }),
      );
      const { firstName, lastName } = unwrapResult(res)!;
      if (firstName !== null && lastName !== null) {
        clearErrors();
        try {
          const promocodesRes = await dispatch(fetchSelfPromocode());
          dispatch(fetchConsultationsWithoutReview());
          dispatch(actions.profile.fetchWallet());
          unwrapResult(promocodesRes);
        } catch (e) {
          captureError(e);
          toast.error(t('authModal.dontSuccessFetchPromocodes'));
        }
        closeModal();
        setStep(AuthFormSteps.number);
        return;
      }
      localStorage.removeItem('phone')
      setStep(AuthFormSteps.promoCode);
    } catch (e: any) {
      captureError(e);
      clearErrors();
      setError('code', { message: e.message });
    }
  };

  const onReturnPrevStep = () => {
    setStep(AuthFormSteps.number);
  };

  return (
    <>
      <Controller
        name={'code'}
        control={control}
        render={({ field: { value, onChange, ref }, fieldState: { error } }) => (
          <Input
            type={'number'}
            value={value}
            ref={ref}
            centered
            onChange={(e) => {
              const { value } = e.target;
              if (value.length > 4) {
                return;
              }
              onChange(e);
            }}
            error={error?.message}
            placeholder={t('authModal.smsCodePlaceholder')}
          />
        )}
      />
      <ButtonsContainer>
        {isWait ? <Timer setWait={setWait} /> : <Button onClick={onRepeatMessage}>{t('authModal.sendRepeat')}</Button>}
        <Button onClick={onReturnPrevStep}>{t('authModal.return')}</Button>
      </ButtonsContainer>
    </>
  );
};

const ButtonsContainer = styled.div`
  margin-top: 65px;
`;

const Button = styled.div`
  color: var(--purple);
  padding: 7.5px 0;
  text-align: center;

  :hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

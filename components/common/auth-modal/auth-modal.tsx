import React, { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { useForm, FormProvider } from 'react-hook-form';
import { renderAuthForm } from './renderAuthForm';
import { renderAuthTitle } from './renderAuthTitle';
import { renderAuthNote } from './renderAuthNote';
import { useSelector } from 'react-redux';
import { actions, selectors } from '../../../store/ducks/app';
import { useAppDispatch } from '../../../hooks/redux';
import Modal from '../modal';

export interface AuthFormValues {
  number: string;
  agreement: boolean;
  code: string;
  promoCode: string;
  wait: boolean;
  counter: number;
}

export enum AuthFormSteps {
  number,
  smsCode,
  promoCode,
  final,
}

type AuthModalProps = {
  initStep: AuthFormSteps;
};

export const AuthModal: FC<AuthModalProps> = ({ initStep }) => {
  const { t } = useTranslation('header.component');
  const [step, setStep] = useState<AuthFormSteps>(initStep);
  const [currentPhone, setCurrentPhone] = useState<string>('');
  const authForm = useForm<AuthFormValues>({
    defaultValues: {
      number: '',
      agreement: false,
      code: '',
      promoCode: '',
    },
  });
  const dispatch = useAppDispatch();

  const isVisible = useSelector(selectors.selectAuthModalIsVisible);

  const onCloseModal = () => {
    authForm.reset({
      number: '',
      agreement: false,
      code: '',
      promoCode: '',
    });
    setStep(AuthFormSteps.number);
    dispatch(actions.hideAuthModal());
  };

  const isCanCloseModal = [AuthFormSteps.number, AuthFormSteps.smsCode].includes(step) || undefined;

  const { getValues } = authForm;

  const isWait = useSelector(selectors.selectAuthModalIsWait);
  const counter = useSelector(selectors.selectAuthModalCounter);

  useEffect(() => {
    setStep(initStep);
  }, [initStep]);

  useEffect(() => {
    let timer: NodeJS.Timer;
    if (counter > 0) {
      if (isWait) {
        timer = setTimeout(() => {
          dispatch(actions.setCounter(counter - 1));
        }, 1000);
      }
    } else {
      dispatch(actions.setWait(false));
      dispatch(actions.setCounter(60));
    }
    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [counter]);

  return (
    <Modal isVisible={isVisible} onClose={isCanCloseModal && onCloseModal}>
      {step !== AuthFormSteps.final && (
        <>
          <Title>{renderAuthTitle(step, t)}</Title>
          <Note>{renderAuthNote(step, getValues().number, t)}</Note>
        </>
      )}
      <FormProvider {...authForm}>
        {renderAuthForm(step, setStep, onCloseModal, currentPhone, setCurrentPhone)}
      </FormProvider>
    </Modal>
  );
};

const Title = styled.div`
  font-size: 19px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.05;
  letter-spacing: normal;
  text-align: center;
  color: var(--text);
  padding-bottom: 15px;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Note = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.54;
  letter-spacing: normal;
  text-align: center;
  color: var(--text);
  padding-bottom: 30px;
  max-width: 260px;
`;
const CrossContainer = styled.div`
  position: absolute;
  right: -15px;
  width: 50px;
  top: -50px;
  height: 50px;
  justify-content: center;
  align-items: center;
  display: flex;
  :hover {
    cursor: pointer;
    background-color: #8b33ffc9;
  }
`;

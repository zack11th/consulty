import React, { Dispatch, SetStateAction } from 'react';
import { AuthFormSteps } from './auth-modal';
import AuthNumberForm from './auth-forms/auth-number-form';
import AuthSmsCodeForm from './auth-forms/auth-sms-code-form';
import AuthPromoCodeForm from './auth-forms/auth-promo-code-form';
import AuthFinalStepForm from './auth-forms/auth-final-step-form';

export function renderAuthForm(
  step: AuthFormSteps,
  setStep: Dispatch<SetStateAction<AuthFormSteps>>,
  closeModal: () => void,
  currentPhone: string,
  setCurrentPhone: Dispatch<SetStateAction<string>>,
) {
  switch (step) {
    case AuthFormSteps.number:
      return <AuthNumberForm setStep={setStep} currentPhone={currentPhone} setCurrentPhone={setCurrentPhone} />;
    case AuthFormSteps.smsCode:
      return <AuthSmsCodeForm setStep={setStep} closeModal={closeModal} />;
    case AuthFormSteps.promoCode:
      return <AuthPromoCodeForm setStep={setStep} />;
    case AuthFormSteps.final:
      return <AuthFinalStepForm closeModal={closeModal} setStep={setStep} />;
  }
}

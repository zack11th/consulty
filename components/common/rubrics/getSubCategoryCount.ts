import { Category, UserCategoryMeta } from '../../../api';

export const getSubCategoryCount = (
  subCategory: Category[] | undefined,
  subCategoryMap: Map<number, Category[]>,
  userCategoryMap: Map<number, UserCategoryMeta>,
) => {
  let count = 0;
  if (subCategory) {
    for (let i = 0; i < subCategory.length; i++) {
      const category = subCategory[i];
      if (userCategoryMap.has(category.id)) {
        count++;
        continue;
      }
      const underSubCategories = subCategoryMap.get(category.id);
      if (underSubCategories) {
        const underCount = getSubCategoryCount(underSubCategories, subCategoryMap, userCategoryMap);
        if (underCount !== 0) {
          count++;
        }
      }
    }
  }
  return count;
};

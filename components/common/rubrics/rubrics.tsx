import React, { FC, useState } from 'react';
import styled from 'styled-components';
import { CONDITION_DESKTOP } from '../../../common/constants';
import RubricOption from '../rubric-option';
import { Category, Rubric, UserCategoryMeta } from '../../../api';
import { mappingCategory } from './mappingCategory';
import Modal from '../modal';
import ConsultationPriceForm from '../expert-profile/forms/consultation-price-form';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';
import CheckMark from '../../../assets/svg/CheckMark';
import { useTranslation } from 'next-i18next';

interface RubricsProps {
  categories: Category[];
  rubrics: Rubric[];
  isUpcomingRubrics?: boolean;
}

export enum CategoryActions {
  add,
  delete,
  update,
}

export const Rubrics: FC<RubricsProps> = ({ categories, rubrics, isUpcomingRubrics }) => {
  const subCategoryMap = mappingCategory(categories);
  const userCategoryMap = new Map<number, UserCategoryMeta>();
  const { categories: defaultCategories } = useSelector(selectors.selectUser);

  const { t } = useTranslation('profile.page');

  const userCategories = defaultCategories ? defaultCategories : [];

  userCategories.forEach((c: UserCategoryMeta) => {
    userCategoryMap.set(c.categoryId, c);
  });

  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [categoryId, setCategoryId] = useState<number>(0);
  const [category, setCategory] = useState<Category | null>(null);
  const [categoryAction, setCategoryAction] = useState<CategoryActions>(CategoryActions.add);
  const [isOpen, setIsOpen] = useState<boolean>(true);

  const onOpenModal = (categoryId: number, category: Category, action: CategoryActions) => {
    return () => {
      setCategoryAction(action);
      setCategoryId(categoryId);
      setCategory(category);
      setIsModalVisible(true);
    };
  };

  const onCloseModal = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Modal isVisible={isModalVisible} onClose={onCloseModal}>
        <ConsultationPriceForm
          onClose={onCloseModal}
          actionType={categoryAction}
          categoryId={categoryId}
          category={category}
        />
      </Modal>
      <Container>
        {rubrics.map(({ id, isUpcoming, categories, name }) => (
          <React.Fragment key={id}>
            {isUpcoming === !!isUpcomingRubrics && (
              <div>
                <Header $isOpen={isOpen} onClick={() => setIsOpen((s) => !s)}>
                  <Title>{name}</Title>
                  <InfoContainer>
                    {/* <CountContainer>
                      <Count>
                        <span>
                          {t('rubricsCount', {
                            count: userCategories.length,
                            total: subCategoryCount + categories.length,
                          })}
                        </span>
                      </Count>
                    </CountContainer> */}
                    <PriceContainer />
                    <OpenHandlerContainer isOpen={isOpen}>
                      <CheckMark />
                    </OpenHandlerContainer>
                  </InfoContainer>
                </Header>
                {isOpen && (
                  <Table>
                    {categories.map((cat) => (
                      <RubricOption
                        onOpenModal={onOpenModal}
                        key={cat.id}
                        category={cat}
                        subCategoryMap={subCategoryMap}
                        userCategoryMap={userCategoryMap}
                      />
                    ))}
                  </Table>
                )}
              </div>
            )}
          </React.Fragment>
        ))}
      </Container>
    </>
  );
};

const Container = styled.div`
  margin-top: 90px;
  justify-content: center;
  flex-direction: column;
  padding: 0 8px;
  margin-bottom: 90px;

  display: flex;

  ${CONDITION_DESKTOP} {
    padding: 0 95px 0 0;
    justify-content: left;

    & > div {
      width: 500px;
    }
  }
`;
const Title = styled.h3`
  margin: 0;
  font-size: 17px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--black2);
  padding-bottom: 20px;
`;

const Table = styled.div``;
const Header = styled.div<{ $isOpen: boolean }>`
  display: flex;
  justify-content: space-between;
  border-bottom: solid 1px #e6e9ea;

  & > svg {
    transition: 0.3s ease;
    transform: rotate(${({ $isOpen }) => ($isOpen ? 90 : 0)}deg);
  }

  :hover {
    cursor: pointer;
  }
`;
const Count = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.54;
  letter-spacing: normal;
  color: var(--gray);
  margin-right: 38px;
  display: flex;

  ${CONDITION_DESKTOP} {
    margin: 0;
  }
`;
const CountContainer = styled.div`
  display: flex;

  ${CONDITION_DESKTOP} {
    min-width: 70px;
  }
`;
const OpenHandlerContainer = styled.div<{ isOpen: boolean }>`
  display: flex;
  justify-content: flex-end;

  & > svg {
    transition: 0.3s ease;
    transform: rotate(${({ isOpen }) => (isOpen ? 90 : 0)}deg);
  }

  ${CONDITION_DESKTOP} {
    min-width: 30px;
  }
`;
const InfoContainer = styled.div`
  display: flex;
`;
const PriceContainer = styled.div`
  display: flex;
  justify-content: flex-end;

  ${CONDITION_DESKTOP} {
    min-width: 70px;
  }
`;

import React, { FC, useState } from 'react';
import styled from 'styled-components';
import { Checkbox } from '../../ui/checkbox';
import Pen from '../../../assets/svg/Pen';
import CheckMark from '../../../assets/svg/CheckMark';
import { api, Category, UserCategoryMeta } from '../../../api';
import { useTranslation } from 'next-i18next';
import { CategoryActions } from '../rubrics/rubrics';
import { getSubCategoryCount } from '../rubrics/getSubCategoryCount';
import { toast } from 'react-toastify';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { useAppDispatch } from '../../../hooks/redux';
import { fetchDeleteUserFromCategory } from '../../../store/ducks/profile/actions';
import { unwrapResult } from '@reduxjs/toolkit';
import { captureError } from 'utils/captureError';

interface RubricOptionProps {
  category: Category;
  subCategoryMap: Map<number, Category[]>;
  onOpenModal: (i: number, n: Category, a: CategoryActions) => () => void;
  userCategoryMap: Map<number, UserCategoryMeta>;
}

export const RubricOption: FC<RubricOptionProps> = ({ category, subCategoryMap, onOpenModal, userCategoryMap }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  const subCategory = subCategoryMap.get(category.id);
  let count = getSubCategoryCount(subCategory, subCategoryMap, userCategoryMap);

  const { t } = useTranslation('profile.page');

  const onDeleteCategory = (id: number) => {
    return async () => {
      try {
        const res = await dispatch(fetchDeleteUserFromCategory(id));
        if (unwrapResult(res)) {
          toast.success(t('saveInfo'));
        }
      } catch (e) {
        captureError(e);
        toast.error(t('notSuccessSaveInfo'));
      }
    };
  };

  return (
    <Container>
      <Header>
        <TitleContainer onClick={() => setIsOpen((state) => !state)}>
          <Checkbox
            checked={userCategoryMap.has(category.id)}
            onChange={
              userCategoryMap.has(category.id)
                ? onDeleteCategory(userCategoryMap.get(category.id)!.id)
                : onOpenModal(category.id, category, CategoryActions.add)
            }
          />
          <Title>
            <span>{category.name}</span>
          </Title>
        </TitleContainer>
        <InfoContainer>
          {/* <CountContainer>
            {subCategory && (
              <Count>
                <span>{t('rubricsCount', { count: count, total: subCategory.length })}</span>
              </Count>
            )}
          </CountContainer> */}
          <PriceContainer>
            {userCategoryMap.has(category.id) && (
              <Price onClick={onOpenModal(category.id, category, CategoryActions.update)}>
                <span>
                  {userCategoryMap.get(category.id)?.price}
                  {' \u20bd'}
                </span>
                <Pen />
              </Price>
            )}
          </PriceContainer>
          <OpenHandlerContainer>
            {subCategory && (
              <OpenHandler $isOpen={isOpen} onClick={() => setIsOpen((state) => !state)}>
                <CheckMark width={13} height={15} />
              </OpenHandler>
            )}
          </OpenHandlerContainer>
        </InfoContainer>
      </Header>
      {isOpen && (
        <ItemsContainer>
          {subCategory &&
            subCategory.map((cat) => (
              <React.Fragment key={cat.id}>
                {subCategoryMap.has(cat.id) ? (
                  <RubricOption
                    key={cat.id}
                    category={cat}
                    onOpenModal={onOpenModal}
                    subCategoryMap={subCategoryMap}
                    userCategoryMap={userCategoryMap}
                  />
                ) : (
                  <Item key={cat.id}>
                    <TitleContainer>
                      <Checkbox
                        checked={userCategoryMap.has(cat.id)}
                        onChange={
                          userCategoryMap.has(cat.id)
                            ? onDeleteCategory(userCategoryMap.get(cat.id)!.id)
                            : onOpenModal(cat.id, cat, CategoryActions.add)
                        }
                      />
                      <Title>{cat.name}</Title>
                    </TitleContainer>
                    <InfoContainer>
                      <CountContainer />
                      <PriceContainer>
                        {userCategoryMap.has(cat.id) && (
                          <Price onClick={onOpenModal(cat.id, cat, CategoryActions.update)}>
                            <span>
                              {userCategoryMap.get(cat.id)?.price}
                              {' \u20bd'}
                            </span>
                            <Pen />
                          </Price>
                        )}
                      </PriceContainer>
                      <OpenHandlerContainer />
                    </InfoContainer>
                  </Item>
                )}
              </React.Fragment>
            ))}
        </ItemsContainer>
      )}
    </Container>
  );
};

const Container = styled.div``;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  transition: 0.3s ease;
  border-radius: 4px;
  padding: 10px;
  gap: 10px;
  ${CONDITION_DESKTOP} {
    padding: 10px 24px;
  }
  :hover {
    box-shadow: 0 0 15px 0 rgb(0 0 0 / 10%);
  }
`;
const Item = styled.div`
  display: flex;
  justify-content: space-between;
  transition: 0.3s ease;
  border-radius: 4px;
  padding: 10px;
  gap: 10px;
  ${CONDITION_DESKTOP} {
    padding: 10px 24px;
  }
  :hover {
    box-shadow: 0 0 15px 0 rgb(0 0 0 / 10%);
  }
`;
const TitleContainer = styled.div`
  display: flex;

  :hover {
    cursor: pointer;
  }
`;

const InfoContainer = styled.div`
  display: flex;
  gap: 5px;
`;

const Count = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.54;
  letter-spacing: normal;
  color: var(--gray);
  display: flex;
  align-items: center;
  & > span {
    white-space: nowrap;
  }
  ${CONDITION_DESKTOP} {
    margin: 0;
  }
`;

const Price = styled.div`
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.67;
  letter-spacing: normal;
  color: var(--purple);
  display: flex;
  align-items: center;
  white-space: nowrap;
  ${CONDITION_DESKTOP} {
    min-width: 70px;
  }

  :hover {
    cursor: pointer;
  }
`;

const Title = styled.div`
  font-size: 15px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: var(--text);
  margin-left: 0;
  display: flex;
  align-items: center;
  ${CONDITION_DESKTOP} {
    margin-left: 5px;
  }
`;

const OpenHandler = styled.div<{ $isOpen: boolean }>`
  display: flex;
  align-items: center;
  transition: 0.3s ease;

  ${({ $isOpen }) => ($isOpen ? `transform: rotate(90deg);` : `transform: rotate(0deg);`)}
  :hover {
    cursor: pointer;
  }
`;

const ItemsContainer = styled.div`
  padding-left: 10px;
  ${CONDITION_DESKTOP} {
    padding-left: 30px;
  }
`;

const PriceContainer = styled.div`
  display: flex;
  justify-content: flex-end;

  ${CONDITION_DESKTOP} {
    min-width: 70px;
  }
`;
const CountContainer = styled.div`
  display: flex;
  justify-content: center;

  ${CONDITION_DESKTOP} {
    min-width: 70px;
  }
`;
const OpenHandlerContainer = styled.div`
  display: flex;
  justify-content: flex-end;

  ${CONDITION_DESKTOP} {
    min-width: 30px;
  }
`;

import React, { FC, useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';
import { useTranslation } from 'next-i18next';
import { Select } from '../../ui/select';
import { Pagination } from './pagination';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { useAppDispatch } from '../../../hooks/redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { ExpertContract } from '../expert-contract/expert-contract';
import {
  fetchSelfPaymentMethods,
  fetchSelfPayments,
  fetchWallet,
  fetchSelfContract,
} from '../../../store/ducks/profile/actions';
import { getEventDateTimeFromString } from '../../../utils/time';
import { PaymentOperationTypeEnum } from '../../../api';
import CardsForWithdrawal from '../cards-for-withdrawal';
import { useRouter } from 'next/router';
import { routes } from '../../../common/routes';
import { captureError } from 'utils/captureError';

interface ExpertWalletProps {}

export const ExpertWallet: FC<ExpertWalletProps> = ({}) => {
  const user = useSelector(selectors.selectUser);
  const wallet = useSelector(selectors.selectWallet);
  const payments = useSelector(selectors.selectPayments);
  const contract = useSelector(selectors.selectContract);

  const router = useRouter();

  const dispatch = useAppDispatch();

  const { t, i18n } = useTranslation('profile.page');

  const [page, setPage] = useState<number>(1);
  const [operationType, setOperationType] = useState({ label: 'Все переводы', value: '' });

  const selectOptions = useMemo(() => {
    return [
      { label: t('walletTab.allOperations'), value: '' },
      { label: t('walletTab.transfer'), value: PaymentOperationTypeEnum.Transfer },
      { label: t('walletTab.refill'), value: PaymentOperationTypeEnum.Refill },
      { label: t('walletTab.revert'), value: PaymentOperationTypeEnum.Revert },
      { label: t('walletTab.withdrawal'), value: PaymentOperationTypeEnum.Withdrawal },
    ];
  }, [t]);

  useEffect(() => {
    if (!user.isExpert) {
      router.push(routes.profile);
    }

    getWallet();
    getContract();
  }, []);

  useEffect(() => {
    getPayments();
  }, [page, operationType]);

  useEffect(() => {
    onPage(1);
  }, [operationType]);

  const onPage = (page: number) => {
    if (payments.pageCount && page <= payments.pageCount) {
      setPage(page);
    }
  };

  const getContract = async () => {
    try {
      const res = await dispatch(fetchSelfContract());
      unwrapResult(res);
    } catch (e) {
      captureError(e);
      return;
    }
  };

  const getWallet = async () => {
    try {
      const res = await dispatch(fetchWallet());
      unwrapResult(res);
    } catch (e) {
      captureError(e);
      return;
    }
  };

  const getPaymentMethods = async () => {
    try {
      await dispatch(fetchSelfPaymentMethods());
    } catch (e) {
      captureError(e);
      return;
    }
  };

  const getPayments = async () => {
    try {
      const type = operationType.value.length === 0 ? undefined : operationType.value;
      await dispatch(fetchSelfPayments({ page: page, type: type as PaymentOperationTypeEnum }));
    } catch (e) {
      captureError(e);
      return;
    }
  };

  const onChangeOperationType = (value: { label: string; value: string } | null) => {
    if (value) {
      setOperationType(value);
    }
  };

  if (!user.isExpert) {
    return null;
  }
  
  return (
    <Container>
      <Title>{t('walletTab.title')}</Title>

      <Block>
        <SmallTitle>{t('walletTab.myBill')}</SmallTitle>
        <Note>{t('walletTab.myBillNote')}</Note>
        <WalletValue>{t('walletTab.count', { count: parseFloat(wallet.balance?.toFixed(2)) })}</WalletValue>
      </Block>

      <Block>
        <CardsForWithdrawal wallet={wallet} />
      </Block>

      {/* <Block>
        <SmallTitle>{t('walletTab.withdrawnTitle')}</SmallTitle>
        <Note>{t('walletTab.withdrawnNote')}</Note>
        <Button bordered={true}>{t('walletTab.withdrawn')}</Button>
      </Block> */}

      <ExpertContract />

      <Block>
        <SmallTitle>{t('walletTab.operationsHistory')}</SmallTitle>
        <SelectContainer>
          <Select
            instanceId="wallet-select"
            placeholder={t('walletTab.allOperations')}
            onChange={onChangeOperationType}
            options={selectOptions}
            value={operationType}
          />
        </SelectContainer>
        <Table>
          <Row>
            <ColumnName>{t('walletTab.date')}</ColumnName>
            <ColumnName>{t('walletTab.operation')}</ColumnName>
            <ColumnName>{t('walletTab.status')}</ColumnName>
            <ColumnName>{t('walletTab.amount')}</ColumnName>
          </Row>

          {payments.data?.map((payment) => (
            <Row key={payment.id}>
              <Column>{getEventDateTimeFromString(payment.createdAt, i18n.language)}</Column>
              <Column>{t(`walletTab.${payment.operationType}`)}</Column>
              <Column>{t(`walletTab.${payment.status}`)}</Column>
              <Column>{t('walletTab.count', { count: parseFloat(payment.amount.toFixed(2)) })}</Column>
            </Row>
          ))}
        </Table>
      </Block>

      {payments.total > 10 && <Pagination total={payments.total} page={page} setPage={onPage} pageCount={payments.pageCount || 0} />}
    </Container>
  );
};

const Container = styled.div`
  margin-bottom: 90px;
  padding-left: 0;

  ${CONDITION_DESKTOP} {
    padding-left: 95px;
  }
`;

const Title = styled.div`
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 40px;
`;

const SmallTitle = styled(Title)`
  font-size: 17px;
  margin-bottom: 10px;
`;

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  max-width: 500px;
  color: var(--gray);
  margin-bottom: 10px;
`;

const WalletValue = styled.div`
  font-size: 42px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--purple);
`;

const Block = styled.div`
  margin-bottom: 60px;
`;

const SelectContainer = styled.div`
  margin-top: 24px;
  & > div > div {
    width: 180px;
  }
`;

const Table = styled.div`
  overflow-x: scroll;
  & > div:first-child {
    border-bottom: none;
  }
`;
const Row = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 0.5fr;
  padding-bottom: 5px;
  width: 600px;
  border-bottom: 1px solid var(--gray6);
`;
const Column = styled.div`
  font-size: 13px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 3.85;
  letter-spacing: normal;
  color: var(--text);
`;

const ColumnName = styled(Column)`
  text-transform: uppercase;
  color: var(--gray);
`;

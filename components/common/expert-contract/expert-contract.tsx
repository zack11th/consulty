import React, { FC } from 'react';
import { getEventDateFromString } from '../../../utils';
import { Badge } from '../../ui/badge';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';
import { useTranslation } from 'next-i18next';
import { Button } from '../../ui/button';
import { useRouter } from 'next/router';
import { routes } from '../../../common/routes';
import { ContractStatusEnum } from '../../../api';

export const ExpertContract: FC = () => {
  const user = useSelector(selectors.selectUser);
  const contract = useSelector(selectors.selectContract);
  const { t, i18n } = useTranslation('profile.page');

  const router = useRouter();

  const onFormalizeContract = () => {
    if (contract.id) {
      router.push(routes.contractCorrect);
    } else {
      router.push(routes.contract);
    }
  };

  return (
    <Block>
      <SmallTitle>{t('walletTab.yourContract')}</SmallTitle>

      {contract.id && (
        <>
          <Note>
            {t('walletTab.yourContractNumber', {
              number: contract.id,
              date: getEventDateFromString(contract.createdAt, i18n.language),
            })}
          </Note>
          {user.tariffPlan && (
            <Note>{t('walletTab.yourContractCommission', { count: user.tariffPlan.commissionPercent })}</Note>
          )}
          {contract.adminComments && (
            <>
              <BoldNote>{t('walletTab.comments')}</BoldNote>
              {contract.adminComments.map((comment) => (
                <Note>{comment}</Note>
              ))}
            </>
          )}
          <Badge label={t(`walletTab.${contract.status}`)} />
        </>
      )}
      {contract.status !== ContractStatusEnum.Accepted && (
        <Button bordered={true} onClick={onFormalizeContract}>
          {contract.id ? t('walletTab.correctContract') : t('walletTab.formalizeContract')}
        </Button>
      )}
    </Block>
  );
};

const Title = styled.div`
  font-size: 25px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 40px;
`;

const SmallTitle = styled(Title)`
  font-size: 17px;
  margin-bottom: 10px;
`;

const Block = styled.div`
  margin-bottom: 60px;

  & > button {
    margin-top: 10px;
  }
`;

const Note = styled.div`
  font-size: 14px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  max-width: 500px;
  color: var(--gray);
  margin-bottom: 10px;
`;

const BoldNote = styled(Note)`
  font-weight: bold;
  color: var(--text);
`;

import React, { FC, useMemo } from 'react';
import { MainLayout } from '../main-layout';
import styled from 'styled-components';
import { CONDITION_DESKTOP, DESKTOP } from '../../../common/constants';
import Sidebar from '../../common/sidebar';
import { User, UserBlockEnum } from '../../../api';
import { useTranslation } from 'react-i18next';
import { EmailVerifyBanner } from 'components/common/email-verify-banner';
import { Spinner } from 'components/ui/spinner';

interface ProfileLayoutProps {
  head: {
    title: string;
  };
  profile: User;
}

export const ProfileLayout: FC<ProfileLayoutProps> = ({ children, head, profile }) => {
  const { t } = useTranslation('utils');

  const renderBlockUser = useMemo(() => {
    return [UserBlockEnum.Partial, UserBlockEnum.Topic].includes(profile?.block) ? (
      <BlockUserContainer>
        <BlockUserTitle>{t('block.blockTitle')}</BlockUserTitle>
        <BlockUserContent>
          {profile?.block === UserBlockEnum.Topic
            ? t('block.blockTopics', { reason: profile.reasonBlock })
            : t('block.blockPartial', { reason: profile.reasonBlock })}
        </BlockUserContent>
      </BlockUserContainer>
    ) : null;
  }, [profile?.block, profile?.reasonBlock]);

  return (
    <MainLayout head={head} hasFooter>
      {Boolean(profile.id) ? (
        <Wrapper>
          {!profile.isEmailVerified && <EmailVerifyBanner />}
          <Container className={'container'}>
            <Sidebar />
            <div>{children}</div>
            {renderBlockUser}
          </Container>
        </Wrapper>
      ) : (
        <StyledSpinner />
      )}
    </MainLayout>
  );
};

const Wrapper = styled.div`
  width: 100%;
`;

const Container = styled.div`
  &&&{
    flex-direction: column-reverse;
    width: 100vw;
    margin: 0 auto;
    display: flex;
    margin-top: 30px;

    ${CONDITION_DESKTOP} {
      max-width: ${DESKTOP}px;
      margin-top: 90px;
      flex-direction: row;
      width: 100%;
      align-items: flex-start;
    }
  }
`;

const BlockUserContainer = styled.div`
  padding: 10px;
  background-color: var(--gray6);
  border: 3px solid var(--red);
  border-radius: 10px;
  margin-bottom: 10px;
  ${CONDITION_DESKTOP} {
    margin-left: 20px;
    max-width: 310px;
  }
`;
const BlockUserTitle = styled.p`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 5px;
  text-align: center;
  ${CONDITION_DESKTOP} {
    font-size: 17px;
  }
`;
const BlockUserContent = styled.p`
  font-size: 17px;
  white-space: pre-line;
  ${CONDITION_DESKTOP} {
    font-size: 14px;
  }
`;
const StyledSpinner = styled(Spinner)`
  margin-top: 30px;
  margin-bottom: 30px;
`;

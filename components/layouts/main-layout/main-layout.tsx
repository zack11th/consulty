import Head from 'next/head';
import React, { FC, useEffect } from 'react';
import styled from 'styled-components';

import { Header } from 'components/common/header';
import { LinkType } from 'types';
import Footer from 'components/common/footer';

interface Props {
  head: {
    title: string;
    ogImage?: string;
  };
  navbar?: LinkType[];

  isLoginButtonVisible?: boolean;
  hasFooter?: boolean;
}

const MainLayout: FC<Props> = ({ head, navbar, children, isLoginButtonVisible = true, hasFooter }) => {
  useEffect(() => {
    const calcHeight = () => {
      const vHeight = process.browser && window.innerHeight;
      process.browser && document.documentElement.style.setProperty('--v-height', `${vHeight}px`);
    };
    calcHeight();
    window.addEventListener('resize', calcHeight);
    return () => {
      window.removeEventListener('resize', calcHeight);
    };
  }, []);

  return (
    <MainContainer>
      <Head>
        <title>{head.title}</title>
        <meta
          name="description"
          content="Cервис платных онлайн-консультаций, где можно найти эксперта в любой теме и заказать у него консультацию."
        />
        <meta name="keywords" content="консультация,получить консультацию,эксперт,экспертиза,лучшие эксперты" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png" />
        <link rel="icon" type="image/svg+xml" sizes="any" href="/favicons/favicon.svg" />
        <link rel="apple-touch-icon" href="/favicons/apple-touch-icon-180.png" />
        <link rel="mask-icon" href="/favicons/favicon.svg" />
        <meta property="og:title" content={head.title || 'Consulty - сервис онлайн-консультаций'} />
        <meta property="og:type" content="website" />
        {process.browser && window && <meta property="og:url" content={window.origin} />}
        <meta
          property="og:image"
          content={head.ogImage || 'https://storage.yandexcloud.net/consulty-stage/logo3x.png'}
        />
      </Head>
      <Header links={navbar} isLoginButtonVisible />
      <Main>{children}</Main>
      {hasFooter && <Footer />}
    </MainContainer>
  );
};

export { MainLayout };

const MainContainer = styled.div`
  min-height: 100vh;
  min-height: var(--v-height);
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: auto 1fr;
`;

const Main = styled.div`
  background-color: var(--white);
`;

import { FC, useState } from 'react';
import Head from 'next/head';
import styled from 'styled-components';
import { Button } from 'components/ui/button';
import { ButtonFillable } from 'components/ui/button-fillable';
import { TabButton } from 'components/ui/tab-button';
import { Checkbox } from 'components/ui/checkbox';
import { SwitchButton } from 'components/ui/switch-button';
import { Achievement } from 'components/ui/achievement';
import { CategoryButton } from 'components/ui/category-button';
import { Badge } from 'components/ui/badge';
import { Input } from 'components/ui/input';
import { Select } from 'components/ui/select';
import { InputPhone } from 'components/ui/input-phone';
import { Textarea } from 'components/ui/textarea';
import { TextareaModal } from 'components/ui/textarea-modal';
import { PlaneIcon, RefreshIcon, FastIcon } from 'assets/svg';
import { ImageWithRetina } from 'components/ui/image-with-retina/image-with-retina';

const optionsSelect = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
];

const UI: FC = () => {
  const [activeIndex, setActiveIndex] = useState<number>(0);
  const [activeIndexBig, setActiveIndexBig] = useState<number>(0);
  const [checkbox, setCheckbox] = useState<boolean>(false);
  const [switched, setSwitched] = useState<boolean>(true);
  const [phone, setPhone] = useState<string>('');
  const [textareaValue, setTextareaValue] = useState<string>('');
  return (
    <div>
      <Head>
        <title>UI kit</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>
        <p>image with retina</p>
        <Row>
          <Column>
            <ImageWithRetina src="/img/coin.png" alt="coin" />
          </Column>
        </Row>
        <p>Buttons</p>
        <Row>
          <Column>
            <Container>
              <Button size="big" disabled>
                Button 1
              </Button>
            </Container>
            <Container>
              <Button bordered size="big">
                Button 2
              </Button>
            </Container>
            <Container>
              <Button>Button 3</Button>
            </Container>
            <Container>
              <Button color="red">Button 4</Button>
            </Container>
          </Column>
          <Column>
            <Container>
              <Button bordered>Button 5</Button>
            </Container>
            <Container>
              <Button rightIcon={<PlaneIcon />}>Button 6</Button>
            </Container>
            <Container>
              <Button color="gray">Button 7</Button>
            </Container>
            <Container>
              <Button size="small" bordered>
                Button 8
              </Button>
            </Container>
            <Container>
              <Button size="small" bordered hasBottomRadius={false}>
                Button 9
              </Button>
            </Container>
            <Container>
              <Button color="purple-gradient" hasShadowHover disabled>
                Стать экспертом в Consulty
              </Button>
            </Container>
          </Column>
          <Column $bg="var(--purple)">
            <Container>
              <Button size="big" bordered color="white-text">
                Button 10
              </Button>
            </Container>
            <Container>
              <Button bordered color="white-text">
                Button 11
              </Button>
            </Container>
            <Container>
              <Button size="small" bordered color="white-text">
                Button 12
              </Button>
            </Container>
            <Container>
              <Button color="white" hasShadowHover>
                Button 13
              </Button>
            </Container>
          </Column>
          <Column $bg="var(--purple)">
            <Container>
              <Button color="purple-opacity" hasShadowHover leftIcon={<RefreshIcon />}>
                Button 14
              </Button>
            </Container>
            <Container>
              <Button color="purple-opacity" leftIcon={<RefreshIcon />}>
                Button 15
              </Button>
            </Container>
            <Container>
              <ButtonFillable leftIcon={<RefreshIcon />} fill={40}>
                Button 16
              </ButtonFillable>
            </Container>
            <Row>
              <CategoryButton label="Кафе" quantity={12} href={'/ui'} />
              <CategoryButton label="Рестораны" quantity={5} href={'/ui'} />
            </Row>
          </Column>
        </Row>
        <p>Tabs</p>
        <Row>
          <Column>
            <Container>
              <TabButton
                icon="/img/ball-small.png"
                iconRetina="/img/ball-small@2x.png"
                index={0}
                activeIndex={activeIndex}
                setActiveIndex={setActiveIndex}
              >
                Спорт
              </TabButton>
              <TabButton
                icon="/img/ball-small.png"
                iconRetina="/img/ball-small@2x.png"
                index={1}
                activeIndex={activeIndex}
                setActiveIndex={setActiveIndex}
              >
                Спорт
              </TabButton>
            </Container>
            <Container>
              <TabButton $size="big" index={0} activeIndex={activeIndexBig} setActiveIndex={setActiveIndexBig}>
                Спорт
              </TabButton>
              <TabButton $size="big" index={1} activeIndex={activeIndexBig} setActiveIndex={setActiveIndexBig}>
                Спорт
              </TabButton>
            </Container>
          </Column>
        </Row>
        <p>inputs</p>
        <Row>
          <Container>
            <Checkbox checked={checkbox} onChange={setCheckbox}>
              Checkbox
            </Checkbox>
          </Container>
          <Container>
            <SwitchButton checked={switched} onChange={setSwitched} />
          </Container>
        </Row>
        <p>inputs</p>
        <Row>
          <Container>
            <Input label="Label text" placeholder="Some placeholder" required />
          </Container>
          <Container>
            <Input label="Label text" error="Something went wrong" placeholder="Some" centered />
          </Container>
          <Container>
            <Input label="Label text" successMessage="Something success" placeholder="Some placeholder" disabled />
          </Container>
          <Container $width="25%">
            <Select instanceId="select" label="Label text" options={optionsSelect} placeholder="Some select" />
          </Container>
          <Container>
            <InputPhone
              label="Phone input"
              required
              value={phone}
              onChange={(val) => {
                setPhone(val);
              }}
            />
          </Container>
        </Row>
        <Row>
          <Container $width="50%">
            <Textarea
              label="Label text"
              required
              value={textareaValue}
              onChange={(e) => setTextareaValue(e.target.value)}
            />
          </Container>
          <Container $width="30%">
            <TextareaModal placeholder="Some placeholder" required />
          </Container>
        </Row>
        <p>other</p>
        <Row>
          <Container>
            <Achievement icon={<FastIcon />} label="Быстро отвечает" />
          </Container>
          <Container>
            <Badge label="Скоро" />
          </Container>
        </Row>
      </Main>
    </div>
  );
};

export default UI;

const Main = styled.main``;

const Row = styled.div`
  display: flex;
`;

const Column = styled.div<{ $bg?: string }>`
  display: flex;
  flex-direction: column;
  background-color: ${({ $bg }) => $bg || undefined};
`;

const Container = styled.div<{ $width?: string }>`
  width: ${({ $width }) => ($width ? $width : undefined)};
  margin: 5px;
`;

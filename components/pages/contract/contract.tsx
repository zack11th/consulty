import React, { FC, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { MainLayout } from '../../layouts/main-layout';
import ContractForm from '../../common/contract-form';
import ContractFormProgressBar from '../../common/contract-form-progress-bar';
import ContractFormFinalStep from '../../common/contract-form-final-step';
import { CONDITION_DESKTOP } from '../../../common/constants';

export enum ContractFormSteps {
  variant,
  offer,
  props,
  final,
}

export interface StepsState {
  variant: boolean;
  offer: boolean;
  props: boolean;
}

export const Contract: FC = () => {
  const { t } = useTranslation('contract.page');

  const head = {
    title: t('head.title'),
  };

  const [stepsState, setStepsState] = useState<StepsState>({
    variant: false,
    offer: false,
    props: false,
  });
  const [step, setStep] = useState<ContractFormSteps>(ContractFormSteps.variant);

  const changeStepHandle = (step: ContractFormSteps) => {
    setStep(step);
  };

  const changeProgressBarStateHandle = (data: Partial<StepsState>) => {
    setStepsState((s) => {
      return { ...s, ...data };
    });
  };

  return (
    <MainLayout head={head} hasFooter>
      <Container className={'container'}>
        {step !== ContractFormSteps.final && (
          <>
            <ContractFormProgressBar
              step={step}
              stepsState={stepsState}
              setStepsState={changeProgressBarStateHandle}
              setStep={changeStepHandle}
            />

            <ContractForm
              step={step}
              changeStepHandle={changeStepHandle}
              changeProgressBarStateHandle={changeProgressBarStateHandle}
            />
          </>
        )}

        {step === ContractFormSteps.final && <ContractFormFinalStep />}
      </Container>
    </MainLayout>
  );
};

const Container = styled.div`
  &&&{
    padding: 0;
    margin-bottom: 60px;

    ${CONDITION_DESKTOP} {
      padding: 60px;
      margin: 0;
    }
  }
`;

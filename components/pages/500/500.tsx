import React, { FC } from 'react';
import styled from 'styled-components';

import { withUserOnStaticPage } from 'hocs/withUserOnStaticPage';
import { useTranslation } from 'next-i18next';
import { MainLayout } from 'components/layouts/main-layout';

const Error500: FC = () => {
  const { t } = useTranslation('500.page');

  const head = {
    title: t('head.title'),
  };

  return (
    <MainLayout head={head} hasFooter>
      <Container>
        <Oops>{t('Oops')}</Oops>
        <SomethingWrongText>{t('somethingWrong')}</SomethingWrongText>
      </Container>
      ;
    </MainLayout>
  );
};

export default withUserOnStaticPage(Error500);

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
const Oops = styled.p`
  font-size: 60px;
  font-weight: bold;
  color: var(--purple);
`;
const SomethingWrongText = styled.h1`
  font-size: 24px;
`;

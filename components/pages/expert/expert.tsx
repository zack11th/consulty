import React, { FC } from 'react';
import { Achievement, Category, Rubric, User, UserCategoryMeta } from '../../../api';
import { MainLayout } from '../../layouts/main-layout';
import styled from 'styled-components';
import ExpertPublicCard from '../../common/expert-public-card';
import { CONDITION_DESKTOP } from '../../../common/constants';
import PublicExpertConsultations from '../../common/public-expert-consultations';
import ReviewsList from '../../common/reviews-list';
import { useTranslation } from 'next-i18next';
import StartConsultationsWidget from '../../common/start-consultation-widget';
import { useSelector } from 'react-redux';
import { selectors } from '../../../store/ducks/profile';

export interface ExpertProps {
  profile: User;
  achievements: Achievement[];
  rubrics: Rubric[];
  categories: Category[];
}

const getTitleCategoryName = (userCategories?: UserCategoryMeta[]): string => {
  if (!userCategories || !userCategories.length) {
    return 'эксперт на сервисе Consulty';
  }

  const expensiveCategory = userCategories.reduce(
    (expensive, cur) => (cur.price > expensive.price ? cur : expensive),
    userCategories[0],
  );
  return `эксперт по теме ${expensiveCategory.category.name.toLowerCase()}`;
};

export const Expert: FC<ExpertProps> = ({ profile, achievements, rubrics, categories }) => {
  const { t } = useTranslation('expert.page');

  const titleCategoryName = getTitleCategoryName(profile.categories);
  const head = {
    title: t('head.title', {
      firstName: profile.firstName,
      lastName: profile.lastName,
      categoryName: titleCategoryName,
    }),
    ogImage: profile.avatarUrl,
  };

  const achievementsMap = new Map<number, Achievement>();
  achievements.forEach((a) => achievementsMap.set(a.id, a));

  const user = useSelector(selectors.selectUser);

  return (
    <MainLayout head={head} hasFooter>
      <Container className={'container'}>
        <TopContentContainer>
          <ExpertPublicCard profile={profile} achievementsMap={achievementsMap} />
          {user.id !== profile.id ? <StartConsultationsWidget expert={profile} user={user} /> : <div />}
        </TopContentContainer>

        <PublicExpertConsultations profile={profile} rubrics={rubrics} categories={categories} />
        {!!profile.aboutSelf && (
          <AboutSelf>
            <Title>{t('aboutSelf')}</Title>
            <Note>
              <span>{profile.aboutSelf}</span>
            </Note>
          </AboutSelf>
        )}
        <ReviewsList userId={profile.id} />
      </Container>
    </MainLayout>
  );
};

const Container = styled.div`
  &&&{
    padding: 20px;
    overflow: hidden;
    ${CONDITION_DESKTOP} {
      padding: 70px 20px;
      overflow: unset;
    }
  }
`;
const Title = styled.div`
  font-size: 22px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.14;
  letter-spacing: normal;
  color: var(--paleText);
  margin-bottom: 20px;
`;

const Note = styled.div`
  font-size: 15px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.67;
  letter-spacing: normal;
  color: var(--black2);
  max-width: 250px;
  text-overflow: ellipsis;
  overflow: hidden;
  ${CONDITION_DESKTOP} {
    max-width: 600px;
  }
`;
const AboutSelf = styled.div`
  margin-bottom: 90px;
`;

const TopContentContainer = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  margin-bottom: 60px;

  & > div:last-child {
    margin-left: 0;

    ${CONDITION_DESKTOP} {
      margin-left: 60px;
    }
  }

  ${CONDITION_DESKTOP} {
    flex-direction: row;
    margin-bottom: 0;
  }

  @media (max-width: 600px) {
    justify-content: center;
    align-items: center;
  }
`;

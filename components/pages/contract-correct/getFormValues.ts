import { SelfEmployedContractPayloadDto } from '../../../api';
import { getDigitsEventDateFromString } from '../../../utils';

export const getFormValues = (locale: string, payload?: SelfEmployedContractPayloadDto) => {
  return {
    INN: payload?.inn || '',
    email: payload?.email || '',
    fullName: payload?.fullName || '',
    physicalAddress: payload?.physicalAddress || '',
    passportSeries: payload?.passportSeries || '',
    passportNumber: payload?.passportNumber || '',
    whenPassportIssued: payload ? getDigitsEventDateFromString(payload.passportWhenGiven, locale) : '',
    addressOfRegistration: payload?.residencePlaceRegistrationAddress || '',
    whoPassportIssued: payload?.passportIssuedBy || '',
    phone: payload?.contactPhone || '',
    birthday: payload ? getDigitsEventDateFromString(payload.birthDate, locale) : '',
  };
};

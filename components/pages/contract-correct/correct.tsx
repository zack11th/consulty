import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { MainLayout } from '../../layouts/main-layout';
import { CONDITION_DESKTOP } from '../../../common/constants';
import { Controller, useForm } from 'react-hook-form';
import { ContractFormValues } from '../../common/contract-form/contract-form';
import { useAppDispatch } from '../../../hooks/redux';
import { uploadFile } from '../../../utils/uploadFile';
import { fetchSelfContract, updateSelfEmployedContract } from '../../../store/ducks/profile/actions';
import dayjs from 'dayjs';
import { unwrapResult } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import { Input } from '../../ui/input';
import {
  datePattern,
  emailPattern,
  innPathPattern,
  passportNumberPattern,
  passportSeriesPattern,
} from '../../../constants';
import PhotoInput from '../../ui/photo-input';
import { Button } from '../../ui/button';
import { selectors } from '../../../store/ducks/profile';
import { useSelector } from 'react-redux';
import { SelfEmployedContractPayloadDto } from '../../../api';
import { getFormValues } from './getFormValues';
import { useRouter } from 'next/router';
import { routes } from '../../../common/routes';
import { InputPhone } from 'components/ui/input-phone';
import { InputDate } from 'components/ui/input-date';
import { captureError } from 'utils/captureError';

export const Correct: FC = () => {
  const { t, i18n } = useTranslation('contract.page');

  const contract = useSelector(selectors.selectContract);
  const payload = contract.payload as SelfEmployedContractPayloadDto;
  const dispatch = useAppDispatch();

  const router = useRouter();

  const head = {
    title: t('head.title'),
  };

  const { handleSubmit, setValue, control, clearErrors, reset } = useForm<ContractFormValues>({
    defaultValues: getFormValues(i18n.language, payload),
  });

  const { t: u } = useTranslation('utils');

  type FileInputsName = 'registrationInPassportPhoto' | 'selfWithPassport' | 'passportPhoto';

  const getContract = async () => {
    try {
      const res = await dispatch(fetchSelfContract());
      unwrapResult(res);
    } catch (e) {
      captureError(e);
      return;
    }
  };

  useEffect(() => {
    getContract();
  }, []);

  useEffect(() => {
    reset(getFormValues(i18n.language, payload));
  }, [contract]);

  const onChangePhoto = (name: FileInputsName) => (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (files) {
      const file = files.item(0);
      if (file) {
        setValue(name, file);
        clearErrors(name);
      }
    }
  };

  const onSubmit = async (values: ContractFormValues) => {
    try {
      const selfWithPassport = values.selfWithPassport
        ? await uploadFile(values.selfWithPassport, u)
        : payload.selfieWithPassportSecondAndThirdPageUrl;

      const res = await dispatch(
        updateSelfEmployedContract({
          id: contract.id,
          payload: {
            payload: {
              birthDate: dayjs(values.birthday, 'DD.MM.YYYY').toISOString(),
              birthPlace: values.birthPlace,
              contactPhone: values.phone,
              email: values.email,
              fullName: values.fullName,
              inn: values.INN,
              passportIssuedBy: values.whoPassportIssued,
              passportNumber: values.passportNumber,
              passportSeries: values.passportSeries,
              passportWhenGiven: dayjs(values.whenPassportIssued, 'DD.MM.YYYY').toISOString(),
              physicalAddress: values.physicalAddress,
              residencePlaceRegistrationAddress: values.addressOfRegistration,
              selfieWithPassportSecondAndThirdPageUrl: selfWithPassport,
            },
          },
        }),
      );
      unwrapResult(res);

      router.push(`${routes.profile}?activeTab=wallet`);
    } catch (e) {
      captureError(e);
      toast.error(u('failedUpdateContract'));
      return;
    }
  };

  return (
    <MainLayout head={head} hasFooter>
      <Container className={'container'}>
        <FormWrapper>
          <FormContainer>
            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
              }}
              name={'fullName'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'ФИО'}
                  placeholder="Иванов Иван Иванович"
                  onChange={onChange}
                  required
                  error={error?.message}
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
                pattern: { value: datePattern, message: 'Дата должна быть в формате 31.01.2000' },
              }}
              name={'birthday'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <InputDate
                  value={value}
                  label={'Дата рождения'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder={'01.05.1992'}
                />
              )}
            />

            <Controller
              control={control}
              name={'birthPlace'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'Место рождения'}
                  placeholder={'г. Москва'}
                  onChange={onChange}
                  error={error?.message}
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
                pattern: { value: passportSeriesPattern, message: 'Серия паспорта должна быть из 4 цифр' },
              }}
              name={'passportSeries'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  type={'text'}
                  label={'Серия паспорта'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder="4444"
                  maxLength={4}
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
                pattern: { value: passportNumberPattern, message: 'Номер паспорта должен быть из 6 цифр' },
              }}
              name={'passportNumber'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'Номер паспорта'}
                  type={'text'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder="555555"
                  maxLength={6}
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
                pattern: { value: datePattern, message: 'Дата должна быть в формате 31.01.2000' },
              }}
              name={'whenPassportIssued'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <InputDate
                  value={value}
                  label={'Когда выдан'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder={'01.01.2021'}
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
              }}
              name={'whoPassportIssued'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'Кем выдан'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder="Отделом внутренних дел Октябрьского округа города Архангельска"
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
              }}
              name={'addressOfRegistration'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'Адрес регистрации по месту жительства'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder="Кемеровская обл., гор. Ленинск-Кузнецкий, ул. Суворова, д. 9, кв. 1"
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
                pattern: { value: innPathPattern, message: 'ИНН должен быть из 12 цифр' },
              }}
              name={'INN'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  type={'text'}
                  label={'ИНН'}
                  required
                  onChange={onChange}
                  error={error?.message}
                  placeholder="123456789012"
                  maxLength={12}
                />
              )}
            />

            <Controller
              control={control}
              name={'physicalAddress'}
              rules={{
                required: { value: true, message: 'Обязательно' },
              }}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'Физический адрес'}
                  required
                  onChange={onChange}
                  error={error?.message}
                  placeholder="Кемеровская обл., гор. Ленинск-Кузнецкий, ул. Суворова, д. 9, кв. 1"
                />
              )}
            />

            <Controller
              control={control}
              rules={{
                required: { value: true, message: 'Обязательно' },
              }}
              name={'phone'}
              defaultValue={''}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <InputPhone
                  value={value}
                  label={'Контактный номер телефона'}
                  type={'text'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder="+79088034444"
                />
              )}
            />

            <Controller
              control={control}
              name={'email'}
              defaultValue={''}
              rules={{
                required: { value: true, message: 'Обязательно' },
                pattern: { value: emailPattern, message: 'Некорректный Email' },
              }}
              render={({ field: { value, onChange }, fieldState: { error } }) => (
                <Input
                  value={value}
                  label={'Электронная почта для уведомлений'}
                  onChange={onChange}
                  required
                  error={error?.message}
                  placeholder="example@gmail.com"
                />
              )}
            />

            <Title>
              <span>Селфи с второй и третьей страницей паспорта (должно быть видно паспорт и лицо целиком)</span>
              <Required>*</Required>
            </Title>
            <Controller
              control={control}
              name={'selfWithPassport'}
              rules={{
                required: { value: true, message: 'Обязательно' },
              }}
              render={({ fieldState: { error } }) => (
                <PhotoInput
                  prefix={'selfWithPassport'}
                  label={'Выбрать'}
                  width={'50%'}
                  error={error?.message}
                  onChange={onChangePhoto('selfWithPassport')}
                  sizeLimitMb={15}
                  largeSizeError={'Файл больше 15 МБ'}
                />
              )}
            />

            <Button onClick={handleSubmit(onSubmit)}>Продолжить</Button>
          </FormContainer>
        </FormWrapper>
      </Container>
    </MainLayout>
  );
};

const Container = styled.div`
  &&&{
    padding: 0;
    margin-bottom: 60px;

    ${CONDITION_DESKTOP} {
      padding: 60px;
      margin: 0;
    }
  }
`;

const Required = styled.span`
  color: var(--purple);
`;

const Title = styled.span`
  font-size: 15px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.33;
  letter-spacing: normal;
  color: var(--text);
  margin-bottom: 22px;
  display: block;
`;

const FormWrapper = styled.div`
  width: 100%;
  margin: 0 auto;
  input {
    max-width: 420px;
  }

  ${CONDITION_DESKTOP} {
    width: 50%;
  }
`;

const FormContainer = styled.div`
  margin-bottom: 60px;

  & > div {
    margin-bottom: 30px;
  }
`;

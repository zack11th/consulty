import React, { FC, useState, useEffect } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import { NextRouter, useRouter } from 'next/router';

import { Category as CategoryType, GetCategoryDto } from 'api';
import { LinkType } from 'types';
import { routes } from 'common/routes';
import { CONDITION_DESKTOP } from 'common/constants';
import { ScrollDown } from 'assets/svg';
import { buildRoute } from 'utils/buildRoute';
import { MainLayout } from 'components/layouts/main-layout';
import { AskExpertForm } from 'components/common/ask-expert-form';
import { CategoryButton } from 'components/ui/category-button';
import { Breadcrumbs, BreadcrumbType } from 'components/ui/breadcrumbs';
import ExpertsInCategory from '../../common/experts-in-category';

interface CategoryRouter extends NextRouter {
  query: {
    id?: string;
  };
}

type CategoryProps = {
  category: GetCategoryDto;
};

const Category: FC<CategoryProps> = ({ category }) => {
  const { t } = useTranslation('category.page');
  const { query }: CategoryRouter = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState<BreadcrumbType[]>([]);

  const head = {
    title: category.rootCategory
      ? t('head.title--withRoot', {
          categoryName: category.category.name.toLowerCase(),
          rootCategoryName: category.rootCategory.name.toLowerCase(),
        })
      : t('head.title--noRoot', { categoryName: category.category.name.toLowerCase() }),
  };
  const navbar: LinkType[] = [
    {
      text: t('navbar.allTopics'),
      href: routes.topics,
    },
    {
      text: t('navbar.becomeExpert'),
      href: routes.expertLanding,
    },
  ];

  useEffect(() => {
    setBreadcrumbs([]);
    if (category.rootCategory?.id) {
      makeBreadCrumbs(category.rootCategory);
    } else {
      setBreadcrumbs([{ name: t('topContent.rootRubric') }]);
    }
  }, [category]);

  const makeBreadCrumbs = (rootCategory: CategoryType) => {
    setBreadcrumbs((prev) => [{ name: rootCategory.name, id: rootCategory.id }, ...prev]);
    if (rootCategory.rootCategory) {
      makeBreadCrumbs(rootCategory.rootCategory);
    } else {
      setBreadcrumbs((prev) => [{ name: t('topContent.rootRubric') }, ...prev]);
      return;
    }
  };

  return (
    <MainLayout head={head} navbar={navbar} hasFooter>
      <TopContent>
        <div className="container">
          <Breadcrumbs breadcrumbs={breadcrumbs} routeTemplate={routes.category} rootRoute={routes.topics} />
          <Title>{t('title', { categoryName: category.category.name })}</Title>

          {!!category.subCategories.length && (
            <Subcategory>
              <SubcategoryText>{t('topContent.subRubrics')}</SubcategoryText>
              <SubcategoryList>
                {category.subCategories.map((item) => (
                  <CategoryButton
                    key={item.id}
                    label={item.name}
                    href={buildRoute(routes.category, { id: item.id })}
                    quantity={item.expertsCount}
                  />
                ))}
              </SubcategoryList>
            </Subcategory>
          )}

          <AskExpertForm
            expertsCount={category.category.expertsCount}
            threeBestExpert={category.threeBestExpert}
            categoryId={category.category.id}
          />

          <FindExpertYourself>{t('topContent.findYourself')}</FindExpertYourself>
        </div>
        <ScrollButton>
          <ScrollDown />
        </ScrollButton>
      </TopContent>

      <ExpertsInCategory
        sortCriteria={'countOfConsultationExpert'}
        title={t('experts.mostPopular')}
        note={t('experts.mostPopularNote', { categoryName: category.category.name })}
      />
      <ExpertsInCategory
        sortCriteria={'rating'}
        title={t('experts.bestReviews')}
        note={t('experts.bestReviewsNote', { categoryName: category.category.name })}
      />
    </MainLayout>
  );
};

export default Category;

const TopContent = styled.div`
  position: relative;
  background-image: var(--gradient2);
  color: var(--white);
  padding-top: 30px;
  padding-bottom: 34px;
  ${CONDITION_DESKTOP} {
    min-width: 1180px;
  }
`;
const BreadCrumbs = styled.div``;
const BreadCrumbItem = styled.a`
  font-size: 14px;
  color: var(--white);
  text-decoration: underline;
  opacity: 0.7;
  &:hover {
    opacity: 0.9;
  }
`;
const Title = styled.h1`
  font-weight: bold;
  font-size: 21px;
  margin: 30px 0 20px;
`;
const Subcategory = styled.div``;
const SubcategoryText = styled.p`
  font-size: 15px;
  opacity: 0.7;
  margin-bottom: 15px;
`;
const SubcategoryList = styled.div``;
const FindExpertYourself = styled.p`
  text-align: center;
  font-weight: bold;
  font-size: 15px;
`;
const ScrollButton = styled.div`
  position: absolute;
  left: 50%;
  top: 100%;
  transform: translate(-50%, -50%);
  width: 42px;
  height: 42px;
  border: 1px solid var(--white);
  border-radius: 50%;
  cursor: pointer;
  -webkit-tap-highlight-color: transparent;
  overflow: hidden;
  &:hover {
    svg {
      transform: scale(1.1);
    }
  }
`;

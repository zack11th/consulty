import { FC, useCallback, useMemo, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';

import { MainLayout } from 'components/layouts/main-layout';
import { Rubric } from 'components/common/rubric';
import { Button } from 'components/ui/button';
import { Rubric as RubricType } from 'api';
import { CONDITION_DESKTOP, ENVIRONMENT_TYPE, SUPPORT_EMAIL } from 'common/constants';
import { CheckPurpleIcon, RubricsPageMessage } from 'assets/svg';
import { useRouter } from 'next/router';
import { routes } from 'common/routes';
import ym from 'react-yandex-metrika';
import Link from 'next/link';
import Modal from 'components/common/modal';
import { OfferRubric } from 'components/common/offer-rubric-form/offer-rubric-form';

type TopicsProps = {
  upRubrics: RubricType[];
  downRubrics: RubricType[];
};

const sendDataToMetrics = () => {
  if (ENVIRONMENT_TYPE === 'production') {
    ym('reachGoal', [`Landing_ex-did'n't_find-offer_topic`]);
    window.gtag('event', `Landing_ex-did'n't_find-offer_topic`);
    window.fbq('trackCustom', `Landing_ex-did'n't_find-offer_topic`);
    // TODO: add proper mail to with subject, see code below with Link variant and delete if necessary
    window.open(`mailto:${SUPPORT_EMAIL}?subject='Предложить тему'`);
  }
};

const Topics: FC<TopicsProps> = ({ upRubrics, downRubrics }) => {
  const { t } = useTranslation('topics.page');
  const router = useRouter();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const openModal = useCallback(() => {
    setIsModalOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, []);
  const head = {
    title: t('head.title'),
  };

  const renderUpRubrics = useMemo(() => upRubrics.map((item) => <Rubric rubric={item} key={item.id} />), [upRubrics]);
  const renderDownRubrics = useMemo(
    () => downRubrics.map((item) => <Rubric rubric={item} key={item.id} />),
    [downRubrics],
  );

  return (
    <MainLayout head={head} hasFooter>
      <h1 className="hidden">{t('head.title')}</h1>
      <Content className="container">
        <Title>{t('title')}</Title>
        <Subtitle>{t('subtitle')}</Subtitle>

        <RubricsList>{renderUpRubrics}</RubricsList>

        <BecomeExpert>
          <BecomeExpertText>
            <BecomeTitle>{t('becomeExpert.title')}</BecomeTitle>
            <BecomeList>
              <BecomeItem>
                <CheckPurpleIcon /> <span>{t('becomeExpert.list.one')}</span>
              </BecomeItem>
              <BecomeItem>
                <CheckPurpleIcon /> <span>{t('becomeExpert.list.two')}</span>
              </BecomeItem>
              <BecomeItem>
                <CheckPurpleIcon /> <span>{t('becomeExpert.list.three')}</span>
              </BecomeItem>
            </BecomeList>
            <Button width="182px" onClick={() => router.push(routes.expertLanding)}>
              {t('becomeExpert.button')}
            </Button>
          </BecomeExpertText>
          <BecomeExpertImages>
            <GirlImage
              src={'/img/rubrics/girl.png'}
              srcSet="/img/rubrics/girl@2x.png 2x, /img/rubrics/girl@3x.png 3x"
            />
            <MessageImageCont>
              <RubricsPageMessage />
            </MessageImageCont>
          </BecomeExpertImages>
        </BecomeExpert>

        <RubricsList>{renderDownRubrics}</RubricsList>

        <ProposeRubric>
          <ProposeIcon
            src="/img/think-for-banner.png"
            srcSet="/img/think-for-banner@2x.png 2x, /img/think-for-banner@3x.png 3x"
          />
          <ProposeText>
            <ProposeTitle>{t('propose.title')}</ProposeTitle>
            <ProposeDescr>{t('propose.description')}</ProposeDescr>
          </ProposeText>
          {/* <Link href={`mailto:${SUPPORT_EMAIL}?cc=?subject=${t('propose.button')}`}> */}
          <Button bordered color="white" onClick={openModal}>
            {t('propose.button')}
          </Button>
          {/* </Link> */}
          <Modal isVisible={isModalOpen} onClose={closeModal}>
            <OfferRubric/>
          </Modal>
        </ProposeRubric>
      </Content>
    </MainLayout>
  );
};

export default Topics;

const Content = styled.main``;
const Title = styled.h2`
  font-size: 24px;
  color: var(--black2);
  padding: 30px 0 10px;
  ${CONDITION_DESKTOP} {
    padding-top: 60px;
    text-align: center;
    font-size: 42px;
  }
`;
const Subtitle = styled.p`
  font-size: 15px;
  ${CONDITION_DESKTOP} {
    text-align: center;
    font-size: 17px;
  }
`;
const RubricsList = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  margin: 60px 0 0;
  ${CONDITION_DESKTOP} {
    flex-direction: row;
    justify-content: space-between;
    margin: 90px 0 0;
  }
`;
const BecomeExpert = styled.div`
  display: flex;
  flex-direction: column;
  background-color: var(--yellow);
  margin: 0 -10px;
  border-radius: 12px;
  overflow: hidden;
  ${CONDITION_DESKTOP} {
    margin: 0;
    flex-direction: row;
    justify-content: space-between;
  }
`;
const BecomeExpertText = styled.div`
  padding: 30px 30px 53px;
  ${CONDITION_DESKTOP} {
    padding: 40px 40px 66px;
  }
`;
const BecomeTitle = styled.h3`
  font-weight: bold;
  font-size: 20px;
  margin-bottom: 20px;
  ${CONDITION_DESKTOP} {
    font-size: 26px;
  }
`;
const BecomeList = styled.ul`
  margin-bottom: 40px;
`;
const BecomeItem = styled.li`
  font-size: 13px;
  display: flex;
  margin-bottom: 15px;
  ${CONDITION_DESKTOP} {
    margin-left: 10px;
    font-size: 15px;
  }

  span {
    margin-left: 10px;
  }
  svg {
    flex-shrink: 0;
  }
`;
const BecomeExpertImages = styled.div`
  position: relative;
  ${CONDITION_DESKTOP} {
    width: 477px;
  }
`;
const GirlImage = styled.img`
  width: 100%;
`;
const MessageImageCont = styled.div`
  position: absolute;
  width: 172px;
  top: -27px;
  left: 22px;
  ${CONDITION_DESKTOP} {
    width: 250px;
    top: 35px;
    left: -88px;
  }
`;
const ProposeRubric = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 46px;
  padding: 30px 20px 40px;
  border-radius: 12px;
  color: var(--white);
  background-image: var(--gradient2);
  ${CONDITION_DESKTOP} {
    flex-direction: row;
    padding: 25px 60px 25px 35px;
  }
`;
const ProposeIcon = styled.img`
  width: 70px;
`;
const ProposeText = styled.div`
  text-align: center;
  ${CONDITION_DESKTOP} {
    text-align: left;
    flex-grow: 1;
    margin-left: 25px;
  }
`;
const ProposeTitle = styled.h3`
  font-weight: bold;
  font-size: 21px;
  margin: 20px 0 15px;
  ${CONDITION_DESKTOP} {
    margin: 0 0 9px;
  }
`;
const ProposeDescr = styled.p`
  font-weight: 300;
  font-size: 16px;
  margin-bottom: 30px;
  ${CONDITION_DESKTOP} {
    margin: 0;
  }
`;

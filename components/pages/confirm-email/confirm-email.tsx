import React, { FC, useEffect, useMemo, useState } from 'react';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import styled from 'styled-components';

import { withUserOnStaticPage } from 'hocs/withUserOnStaticPage';
import { MainLayout } from 'components/layouts/main-layout';
import { routes } from 'common/routes';
import { Spinner } from 'components/ui/spinner';
import { Button } from 'components/ui/button';
import { NextRouter, useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { actions, selectors } from 'store/ducks';
import { api } from 'api';
import { toast } from 'react-toastify';
import { useAppDispatch } from 'hooks/redux';
import { captureError } from 'utils/captureError';

type Router = NextRouter & { query: { token?: string } };
type ErrorPageState = 'sendingRequest' | 'hasError' | 'repeatMailSend' | 'confirmTrue';

const ConfirmEmail: FC = () => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('confirm-email.page');
  const router: Router = useRouter();
  const isAuthentication = useSelector(selectors.profile.selectIsAuthentication);
  const [state, setState] = useState<ErrorPageState>('sendingRequest');

  const head = {
    title: t('head.title'),
  };

  useEffect(() => {
    const sendConfirm = async () => {
      try {
        await api.V1UsersApi.usersControllerConfirmVerificationEmail({ token: router.query.token! });
        dispatch(actions.profile.fetchMe());
        setState('confirmTrue');
      } catch (error: any) {
        captureError(error);
        setState('hasError');
      }
    };
    if (router.query.token) {
      sendConfirm();
    }
  }, [router]);

  const sendRepeatEmail = async () => {
    try {
      await api.V1UsersApi.usersControllerSendVerificationEmail();
      setState('repeatMailSend');
    } catch (error: any) {
      captureError(error);
      toast.error(error.message);
    }
  };

  const renderContent = useMemo(() => {
    switch (state) {
      case 'sendingRequest':
        return (
          <>
            <StyledSpinner />
            <Description>{t('requestSending')}</Description>
          </>
        );
      case 'hasError':
        return (
          <>
            <Title>{t('errorTitle')}</Title>
            <Description>{t('error')}</Description>
            {isAuthentication && <StyledButton onClick={sendRepeatEmail}>{t('buttonSendAgain')}</StyledButton>}
          </>
        );
      case 'repeatMailSend':
        return (
          <>
            <Description>{t('repeatConfirmEmailSend')}</Description>
            <Link href={routes.topics}>
              <StyledLink href={routes.topics}>{t('goToApp')}</StyledLink>
            </Link>
          </>
        );
      case 'confirmTrue':
      default:
        return (
          <>
            <Title>{t('thanks')}</Title>
            <Description>{t('emailIsVerified')}</Description>
            <Link href={routes.topics}>
              <StyledLink href={routes.topics}>{t('goToApp')}</StyledLink>
            </Link>
          </>
        );
    }
  }, [t, state, isAuthentication]);

  return (
    <MainLayout head={head} hasFooter>
      <Container className="container">{renderContent}</Container>;
    </MainLayout>
  );
};

export default withUserOnStaticPage(ConfirmEmail);

const Container = styled.div`
  &&&{
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    }
`;
const Title = styled.p`
  font-size: 30px;
  font-weight: bold;
  color: var(--purple);
  margin-bottom: 10px;
  text-align: center;
`;
const Description = styled.h1`
  font-size: 20px;
  text-align: center;
`;
const StyledLink = styled.a`
  margin-top: 10px;
  color: var(--purple);
  text-decoration: underline;
  &:hover {
    color: var(--purple3);
  }
`;
const StyledSpinner = styled(Spinner)`
  margin-bottom: 20px;
`;
const StyledButton = styled(Button)`
  margin-top: 20px;
  margin-bottom: 20px;
`;

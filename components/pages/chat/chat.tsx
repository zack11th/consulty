import React, { FC, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'next-i18next';
import { NextRouter, useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';

import { MainLayout } from 'components/layouts/main-layout';
import { ChatSidebar } from 'components/common/chat-sidebar';
import { ChatRoom } from 'components/common/chat-room';
import { RequestRoom } from 'components/common/request-room';
import { Button } from 'components/ui/button';
import { CONDITION_DESKTOP, HEADER_HEIGHT_DESKTOP, HEADER_HEIGHT_MOBILE } from 'common/constants';
import { useAppDispatch } from 'hooks/redux';
import { actions, selectors } from 'store/ducks';
import { routes } from 'common/routes';
import { ResponseRoom } from 'components/common/response-room';
import { captureError } from 'utils/captureError';

export type ChatTabs = 'consult' | 'requests' | 'responses';
export interface ChatRouter extends NextRouter {
  query: {
    activeTab?: ChatTabs;
    roomId?: string;
    expertId?: string;
  };
}

const Chat: FC = () => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('chat.page');
  const user = useSelector(selectors.profile.selectUser);
  const chatRooms = useSelector(selectors.chatRooms.selectChatRooms);
  const myRequests = useSelector(selectors.consultationRequests.selectAllMyConsultationRequests);
  const clientRequests = useSelector(selectors.consultationRequests.selectAllClientRequests);
  const router: ChatRouter = useRouter();

  const head = {
    title: t('head.title'),
  };

  useEffect(() => {
    if (router.query.expertId) {
      const goToChatRoom = async () => {
        try {
          const actionResult = await dispatch(actions.chatRooms.getChatRooms({ user }));
          const result = await unwrapResult(actionResult);
          const chatRoomWithThisExpert = result?.rooms.find(
            (room) => room.expertId.toString() === router.query.expertId,
          );
          if (chatRoomWithThisExpert) {
            router.replace({ query: { activeTab: 'consult', roomId: chatRoomWithThisExpert.id } });
          } else {
            router.replace({ query: { activeTab: 'consult' } });
          }
        } catch (e) {
          captureError(e);
          router.replace({ query: { activeTab: 'consult' } });
        }
      };

      goToChatRoom();
    }
  }, [router.query]);

  useEffect(() => {
    switch (router.query.activeTab) {
      case 'requests':
        router.query.roomId &&
          dispatch(actions.consultationRequests.setCurrentRequestRoom({ roomId: Number(router.query.roomId) }));
        break;
      case 'responses':
        router.query.roomId &&
          dispatch(actions.consultationRequests.setCurrentResponseRoom({ roomId: Number(router.query.roomId) }));
        break;
      default:
        dispatch(actions.chatMessages.clearChatMessages());
        router.query.roomId && dispatch(actions.chatRooms.setActiveRoom({ roomId: Number(router.query.roomId) }));
    }
    return () => {
      dispatch(actions.chatRooms.setActiveRoom({ roomId: null }));
      dispatch(actions.consultationRequests.setCurrentRequestRoom({ roomId: null }));
      dispatch(actions.consultationRequests.setCurrentResponseRoom({ roomId: null }));
    };
  }, [router.query]);

  useEffect(() => {
    if (user.id) {
      switch (router.query.activeTab) {
        case 'responses':
          dispatch(
            actions.consultationRequests.fetchManyConsultationsRequests({
              filter: [`clientId||$eq||${user.id}`],
            }),
          );
          break;
        default:
          break;
      }
    }
  }, [router.query.activeTab, user]);

  useEffect(() => {
    router.query.activeTab === 'responses' && !myRequests.length && router.replace(routes.chat);
  }, []);

  const renderEmptyTextByActiveTab = useMemo(() => {
    switch (router.query.activeTab) {
      case 'requests':
        return !clientRequests.length ? (
          <EmptyChatDescription>{t('emptyRequests.description')}</EmptyChatDescription>
        ) : (
          <EmptyChatDescription>{t('notSelectedRequest')}</EmptyChatDescription>
        );
      case 'responses':
        return !myRequests.length ? (
          <>
            <EmptyChatTitle>{t('emptyResponses.title')}</EmptyChatTitle>
            <EmptyChatDescription>{t('emptyResponses.description')}</EmptyChatDescription>
            <Button bordered onClick={() => router.push(routes.topics)} width="240px">
              {t('emptyResponses.button')}
            </Button>
          </>
        ) : (
          <EmptyChatDescription>{t('notSelectedResponse')}</EmptyChatDescription>
        );
      default:
        return !chatRooms.length ? (
          <>
            <EmptyChatTitle>{t('emptyChat.title')}</EmptyChatTitle>
            <EmptyChatDescription>{t('emptyChat.description')}</EmptyChatDescription>
            <Button bordered onClick={() => router.push(routes.topics)} width="240px">
              {t('emptyChat.button')}
            </Button>
          </>
        ) : (
          <EmptyChatDescription>{t('notSelectedChat')}</EmptyChatDescription>
        );
    }
  }, [router, chatRooms.length, myRequests.length, clientRequests.length]);

  const renderChatRoom = useMemo(() => {
    switch (router.query.activeTab) {
      case 'requests':
        return router.query.roomId && <RequestRoom />;
      case 'responses':
        return router.query.roomId && <ResponseRoom />;
      default:
        return router.query.roomId && <ChatRoom />;
    }
  }, [router.query]);

  return (
    <MainLayout head={head}>
      <Container>
        <ChatSidebar />
        <ChatContainer>
          {renderEmptyTextByActiveTab}
          {renderChatRoom}
        </ChatContainer>
      </Container>
    </MainLayout>
  );
};

export default Chat;

const Container = styled.div`
  display: grid;
  height: calc(100vh - ${HEADER_HEIGHT_MOBILE} - 1px);
  height: calc(var(--v-height) - ${HEADER_HEIGHT_MOBILE} - 1px);
  overflow-y: hidden;
  grid-auto-columns: 1fr;
  ${CONDITION_DESKTOP} {
    position: relative;
    height: calc(100vh - ${HEADER_HEIGHT_DESKTOP} - 1px);
    height: calc(var(--v-height) - ${HEADER_HEIGHT_DESKTOP} - 1px);
    grid-template-columns: 320px 1fr;
  }
`;
const ChatContainer = styled.div`
  display: block;
  width: 0;
  height: 0;
  ${CONDITION_DESKTOP} {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
  }
`;
const EmptyChatTitle = styled.p`
  font-size: 17px;
  font-weight: bold;
  color: var(--paleText);
  margin-bottom: 10px;
`;
const EmptyChatDescription = styled.p`
  font-size: 14px;
  color: var(--gray9);
  margin-bottom: 30px;
`;

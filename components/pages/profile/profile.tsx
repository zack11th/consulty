import { FC, useEffect } from 'react';
import { useTranslation } from 'next-i18next';
import { SettingForm } from '../../common/setting-form/setting-form';
import { useRouter } from 'next/router';
import ExpertProfile from '../../common/expert-profile';
import ProfileLayout from '../../layouts/profile-layout';
import { Category, Rubric, User } from '../../../api';
import ExpertWallet from '../../common/expert-wallet';
import { Support } from '../../common/support';
import NotificationSettings from '../../common/notification-settings';
import { useSelector } from 'react-redux';
import { selectors } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';
import { fetchSelfContentChangeRequests, fetchSelfVerificationRequests } from 'store/ducks/profile/actions';
import { captureError } from 'utils/captureError';

export enum Tabs {
  settings = 'settings',
  expertProfile = 'expertProfile',
  wallet = 'wallet',
  support = 'support',
  notifications = 'notifications',
}

export interface ProfileProps {
  profile: User;
  categories: Category[];
  rubrics: Rubric[];
}

export const Profile: FC<ProfileProps> = ({ categories, rubrics }) => {
  const { t } = useTranslation('profile.page');
  const user = useSelector(selectors.profile.selectUser);

  const dispatch = useAppDispatch();

  const { query } = useRouter();

  let { activeTab } = query;

  if (!activeTab) {
    activeTab = Tabs.settings;
  }

  const getVerificationRequests = async () => {
    try {
      await dispatch(fetchSelfVerificationRequests());
    } catch (e) {
      captureError(e);
      return;
    }
  };

  const getContentChangeRequests = async () => {
    try {
      await dispatch(fetchSelfContentChangeRequests());
    } catch (e) {
      captureError(e);

      return;
    }
  };

  useEffect(() => {
    getVerificationRequests();
    getContentChangeRequests();
  }, []);

  const head = { title: t('head.title') };

  return (
    <ProfileLayout profile={user} head={head}>
      {activeTab === Tabs.settings && <SettingForm user={user} />}
      {activeTab === Tabs.expertProfile && <ExpertProfile categories={categories} rubrics={rubrics} />}
      {activeTab === Tabs.wallet && <ExpertWallet />}
      {activeTab === Tabs.notifications && <NotificationSettings />}
      {activeTab === Tabs.support && <Support />}
    </ProfileLayout>
  );
};

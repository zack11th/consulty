import React, { FC, ImgHTMLAttributes } from 'react';

interface ImageWithRetinaProps extends ImgHTMLAttributes<HTMLImageElement> {
  src: string;
  alt: string;
}

const ImageWithRetina: FC<ImageWithRetinaProps> = ({ src, alt, ...props }) => {
  const fullPath = src.split('.');
  const fileExt = fullPath.pop();
  const filePathWithName = fullPath.join('.');

  return (
    <img
      {...props}
      src={src}
      srcSet={`${filePathWithName}@2x.${fileExt} 2x, ${filePathWithName}@3x.${fileExt} 3x`}
      alt={alt}
    />
  );
};

export { ImageWithRetina };

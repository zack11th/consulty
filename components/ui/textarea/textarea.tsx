import React, { FC, TextareaHTMLAttributes } from 'react';
import styled from 'styled-components';

const DEFAULT_MAX_COUNT = 1000;

interface TextareaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  label: string;
  max?: number;
  $width?: string;
}

export const Textarea: FC<TextareaProps> = ({ label, max, $width, ...props }) => {
  return (
    <Container $width={$width}>
      <HeadRow>
        <Label>{label}</Label>
        <Counter>
          {props.value?.toString().length} / {max ? max : DEFAULT_MAX_COUNT}
        </Counter>
      </HeadRow>
      <StyledTextarea {...props} />
    </Container>
  );
};

const Container = styled.div<{ $width?: string }>`
  display: flex;
  flex-direction: column;
  width: ${({ $width }) => ($width ? $width : '100%')};
`;

const HeadRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-right: -7px;
`;

const Label = styled.label`
  font-weight: bold;
  font-size: 17px;
  margin-bottom: 14px;
`;

const Counter = styled.div`
  font-size: 15px;
  color: var(--gray9);
`;

const StyledTextarea = styled.textarea`
  width: 100%;
  height: 146px;
  outline: none;
  border: 1px solid var(--gray3);
  border-radius: 5px;
  background-color: var(--white);
  padding: 15px;
  font-size: 15px;
  line-height: 1.33;
  resize: none;
  color: var(--text);
  
  &:focus{
    color: var(--text);
  }

  &::placeholder {
    color: var(--gray);
  }

  &:disabled {
    background-color: #f9f9f9;
  }
`;

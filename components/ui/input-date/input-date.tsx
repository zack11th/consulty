import React, { ForwardedRef, forwardRef } from 'react';
import { PhoneInputProps } from 'react-phone-number-input';
import styled from 'styled-components';
import { FieldWrapper } from '../field-wrapper';
import { FieldWrapperProps } from '../field-wrapper/field-wrapper';
import NumberFormat from 'react-number-format';
import { ChangeEvent } from 'hoist-non-react-statics/node_modules/@types/react';

type InputDateProps = PhoneInputProps & FieldWrapperProps;

export const InputDate = forwardRef(
  (
    {
      label,
      error,
      successMessage,
      width,
      centered,
      required,
      onChange,
      disabled,
      name,
      value,
      ...props
    }: InputDateProps,
    ref: ForwardedRef<HTMLInputElement>,
  ) => {
    return (
      <FieldWrapper
        label={label}
        error={error}
        successMessage={successMessage}
        width={width}
        centered={centered}
        required={required}
      >
        <StyledDate
          format="##.##.####"
          placeholder="01.01.2021"
          mask={['_', '_', '_', '_', '_', '_', '_', '_']}
          name={name}
          value={value}
          getInputRef={ref}
          disabled={disabled || false}
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            e.persist();
            onChange(e.target.value);
          }}
        />
      </FieldWrapper>
    );
  },
);

const StyledDate = styled(NumberFormat)`
  width: 100%;
  height: 50px;
  outline: none;
  border: 1px solid var(--gray3);
  border-radius: 12px;
  background-color: var(--white);
  padding: 0 15px;
  font-weight: 500;
  font-size: 15px;
  line-height: 1.67;

  color: var(--text);

  &:disabled {
    background-color: #f9f9f9;
  }
`;

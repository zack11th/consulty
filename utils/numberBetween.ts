export const numberBetween = (num: number, max: number, min: number) => {
  return num > min && num < max;
};
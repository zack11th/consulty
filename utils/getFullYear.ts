export function getFullYear(date: any){
  const [fullYear] = date.split(' ');
  const [year, mouth, day] = fullYear.split('-');
  return {
    year,
    mouth: mouth[0] === '0' ? mouth[1] : mouth,
    day: day[0] === '0' ? day[1] : day,
  }
}
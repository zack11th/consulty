import { TFunction } from 'next-i18next';
import { Message } from 'api';
import { AUDIO_MESSAGE_EXT } from 'common/constants';

export const makeMessageContent = (message: Message, t: TFunction): string => {
  if (message.attachments && message.attachments[0].split('.').pop() === AUDIO_MESSAGE_EXT) {
    return t('messageContent.audio');
  }
  return message.content;
};

export const clearTel = (tel: string) => tel.replace(/[^0-9]/g, "");

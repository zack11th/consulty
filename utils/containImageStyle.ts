import type { CSSObject } from 'styled-components';

export const containImageStyle = (url: string | null): CSSObject => ({
  backgroundSize: 'contain',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  backgroundImage: url ? `url(${url})` : undefined,
});

import type { CSSObject } from 'styled-components';

export const coverImageStyle = (url: string | null | undefined): CSSObject => ({
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  backgroundImage: url ? `url(${url})` : undefined,
});

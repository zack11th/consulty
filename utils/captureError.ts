import * as Sentry from '@sentry/nextjs';

export const captureError = (e: any) => Sentry.captureException(e);

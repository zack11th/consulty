export const ellipsizeText = (text: string | undefined, count: number, isDots: boolean = true): string | undefined => {
  if (!text) {
    return undefined;
  }
  const dots = isDots ? '...' : '';
  return text.length > count ? `${text.slice(0, count)}${dots}` : text;
};

export const commaNumberFormat = (value: string) => {
  return value.replace(' ', '').replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
};

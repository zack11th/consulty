import { AppContext } from 'next/app';

export default function getUserToken(appContext: AppContext): string | undefined | null {
  const cookie = appContext.ctx.req?.headers.cookie;
  const match = cookie?.match(new RegExp('(^| )token=([^;]+)'));
  const cookieToken = match && match[2];

  return process.browser ? localStorage.getItem('TOKEN') : cookieToken;
}

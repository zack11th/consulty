#!/bin/bash
echo "Download swagger schema:"
curl "https://api-stage.consulty.online/api-json" -o swagger.json
echo "Generate types"
npx @manifoldco/swagger-to-ts swagger.json --output ./types/global-types.ts
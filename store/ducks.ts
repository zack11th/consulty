import { combineReducers } from '@reduxjs/toolkit';

import * as chatRooms from './ducks/chatRooms';
import * as chatMessages from './ducks/chatMessages';
import * as profile from './ducks/profile';
import * as app from './ducks/app';
import * as consultationRequests from './ducks/consultationRequests';
import * as consultationsChatRoom from './ducks/consultationsChatRoom';
import * as audioMessages from './ducks/audioMessages';

export const actions = {
  app: app.actions,
  profile: profile.actions,
  chatRooms: chatRooms.actions,
  chatMessages: chatMessages.actions,
  consultationRequests: consultationRequests.actions,
  consultationsChatRoom: consultationsChatRoom.actions,
  audioMessages: audioMessages.actions,
};

export const reducers = combineReducers({
  app: app.reducer,
  profile: profile.profileReducer,
  chatRooms: chatRooms.reducer,
  chatMessages: chatMessages.reducer,
  consultationRequests: consultationRequests.reducer,
  consultationsChatRoom: consultationsChatRoom.reducer,
  audioMessages: audioMessages.reducer,
});

export const selectors = {
  app: app.selectors,
  chatRooms: chatRooms.selectors,
  chatMessages: chatMessages.selectors,
  profile: profile.selectors,
  consultationRequests: consultationRequests.selectors,
  consultationsChatRoom: consultationsChatRoom.selectors,
  audioMessages: audioMessages.selectors,
};

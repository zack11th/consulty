import { configureStore } from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger';
import { reducers } from './ducks';

const logger = createLogger({
  predicate: () => process.env.NEXT_PUBLIC_ENVIRONMENT !== 'production',
  collapsed: true,
});

export const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
  devTools: process.env.NEXT_PUBLIC_ENVIRONMENT !== 'production',
});

export type RootState = ReturnType<typeof reducers>;
export type AppDispatch = typeof store.dispatch;

import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { COOKIE_KEY } from 'common/constants';
import { handleAsyncError, setCookie } from 'utils';
import {
  CreateUserCategoryMetaDto,
  UpdateUserCategoryMetaDto,
  api,
  BulkUpdateUserCategoryMetaDto,
  CreateIndividualContractDto,
  CreateSelfEmployedContractDto,
  NotificationsSetting,
  SigninDto,
  UpdateNotificationsSettingDto,
  UpdateUserDto,
  User,
  Category,
  UpdateUserProfileDto,
  PaymentOperationTypeEnum,
  VerificationRequest,
  CreateVerificationRequestDto,
  Consultation,
  UserCategoryMeta,
  UpdateSelfEmployedContractDto,
} from '../../../api';
import { actions } from './index';
import { selectContentChangeRequest, selectVerificationRequest } from './selectors';
import { RootState } from '../../index';
import { actions as rootActions } from '../../ducks';
import { captureError } from 'utils/captureError';

export const setToken = createAction<string>('user/setToken');

export const fetchMe = createAsyncThunk<User | undefined>('user/fetchMe', async () => {
  try {
    const { data: user } = await api.V1UsersApi.usersControllerGetMe();
    return user;
  } catch (error: any) {
    captureError(error);
    handleAsyncError(error);
  }
});

export const fetchSelfVerificationRequests = createAsyncThunk(
  'profile/fetchSelfVerificationRequests',
  async (payload, { dispatch }) => {
    try {
      const { data } = await api.V1VerificationRequestsApi.verificationRequestsControllerGetUserRequests();

      dispatch(actions.setVerificationRequests(data));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const sendVerificationRequest = createAsyncThunk<void, CreateVerificationRequestDto, { state: RootState }>(
  'profile/sendVerificationRequest',
  async (payload, { dispatch, getState }) => {
    try {
      const verificationRequest = selectVerificationRequest(getState());
      if (!!verificationRequest) {
        await api.V1VerificationRequestsApi.verificationRequestsControllerUpdateOne(verificationRequest.id, payload);
      } else {
        await api.V1VerificationRequestsApi.verificationRequestsControllerCreateOne(payload);
      }

      dispatch(fetchSelfContentChangeRequests());
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchSelfContentChangeRequests = createAsyncThunk(
  'profile/fetchSelfContentChangeRequests',
  async (payload, { dispatch }) => {
    try {
      const { data } =
        await api.V1ContentChangeRequestApi.getManyBaseContentChangeRequestsControllerContentChangeRequest();

      dispatch(actions.setContentChangeRequests(data));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchWallet = createAsyncThunk('profile/fetchWallet', async (payload, { dispatch }) => {
  try {
    const { data } = await api.V1WalletsApi.walletsControllerGetSelfWallet();

    dispatch(actions.setWallet(data));
  } catch (e: any) {
    captureError(e);
    handleAsyncError(e);
  }
});

export const fetchSelfPayments = createAsyncThunk<void, { page: number; type?: PaymentOperationTypeEnum }>(
  'profile/fetchSelfPayments',
  async ({ page, type }, { dispatch }) => {
    try {
      const { data }: any = await api.V1PaymentsApi.paymentsControllerGetManyPayments(10, page, type);

      dispatch(actions.addUserPayments(data));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

interface ContentChangeRequestData {
  aboutSelf?: string;
  avatarUrl?: string;
}
export const fetchContentChangeRequest = createAsyncThunk<void, ContentChangeRequestData, { state: RootState }>(
  'profile/fetchContentChangeRequest',
  async ({ aboutSelf, avatarUrl }, { dispatch, getState }) => {
    try {
      const contentChangeRequest = selectContentChangeRequest(getState());

      if (!contentChangeRequest) {
        const { data } =
          await api.V1ContentChangeRequestApi.createOneBaseContentChangeRequestsControllerContentChangeRequest({
            aboutSelf: aboutSelf,
            avatarUrl: avatarUrl,
          });
        dispatch(actions.setContentChangeRequests([data]));

        return;
      }

      const newContentChangeRequest = {
        ...contentChangeRequest,
        avatarUrl: avatarUrl,
        aboutSelf: aboutSelf,
      };

      const { data } =
        await api.V1ContentChangeRequestApi.updateOneBaseContentChangeRequestsControllerContentChangeRequest(
          contentChangeRequest.id,
          {
            avatarUrl: newContentChangeRequest.avatarUrl,
            aboutSelf: newContentChangeRequest.aboutSelf,
          },
        );

      dispatch(actions.setContentChangeRequests([data]));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const signOut = createAsyncThunk('profile/signOut', (_, { dispatch }) => {
  dispatch(rootActions.chatMessages.resetState());
  dispatch(rootActions.chatRooms.resetState());
  dispatch(rootActions.consultationRequests.resetState());
  dispatch(rootActions.consultationsChatRoom.resetState());
  return true;
});

export const fetchSignInSms = createAsyncThunk<User | undefined, SigninDto>(
  'user/signInSms',
  async (dto, { dispatch }) => {
    try {
      const { data: userData } = await api.V1UsersApi.usersControllerSignInSms(dto);

      setCookie(COOKIE_KEY.accessToken, userData.token);

      dispatch(actions.authUser(userData));

      return userData.user;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);
export const fetchSelfPromocode = createAsyncThunk<boolean | undefined>(
  'user/fetchSelfPromocode',
  async (data, { dispatch }) => {
    try {
      const { data: promocodes } = await api.V1PromocodesApi.promocodesControllerGetSelfPromocodes();

      dispatch(actions.addUserPromocodes(promocodes));

      return true;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchUpdateIam = createAsyncThunk<boolean | undefined, UpdateUserDto>(
  'user/updateIam',
  async (dto, { dispatch }) => {
    try {
      const { data } = await api.V1UsersApi.usersControllerUpdateIAM(dto);
      dispatch(actions.updateUser(data));
      return true;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchUpdateIamProfile = createAsyncThunk<boolean | undefined, UpdateUserProfileDto>(
  'user/updateIam',
  async (dto, { dispatch }) => {
    try {
      const { data } = await api.V1UsersApi.usersControllerUpdateIAMProfile(dto);
      dispatch(actions.updateUser(data));
      return true;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchAddUserToCategory = createAsyncThunk(
  'user/addUserToCategory',
  async ({ category, dto }: { category: Category; dto: CreateUserCategoryMetaDto }, { dispatch }) => {
    try {
      const { data } = await api.V1UserCategoriesApi.userCategoriesMetaControllerCreateOne(dto);
      dispatch(actions.updateUserCategories([{ ...category, ...data }]));
      return true;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchUpdatePriceCategory = createAsyncThunk(
  'user/updatePriceCategory',
  async ({ category, dto }: { category: UserCategoryMeta; dto: UpdateUserCategoryMetaDto }, { dispatch }) => {
    try {
      const { data } = await api.V1UserCategoriesApi.userCategoriesMetaControllerUpdateOne(category.id, dto);
      dispatch(actions.updateUserCategories([{ ...category, ...data }]));
      return true;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchDeleteUserFromCategory = createAsyncThunk<boolean | undefined, number>(
  'user/updatePriceCategory',
  async (id, { dispatch }) => {
    try {
      await api.V1UserCategoriesApi.userCategoriesMetaControllerRemoveOne(id);
      dispatch(actions.deleteUserCategory(id));
      return true;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchGetNotificationSettings = createAsyncThunk<NotificationsSetting | undefined>(
  'user/getNotificationSettings',
  async (_, { dispatch }) => {
    try {
      const { data } = await api.V1NotificationsSettingsApi.notificationsSettingsControllerGetSettings();
      dispatch(actions.updateNotificationSetting(data));
      return data;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchUpdateNotificationSettings = createAsyncThunk<
  NotificationsSetting | undefined,
  UpdateNotificationsSettingDto
>('user/getNotificationSettings', async (dto, { dispatch }) => {
  try {
    const { data } = await api.V1NotificationsSettingsApi.notificationsSettingsControllerUpdateSettings(dto);
    dispatch(actions.updateNotificationSetting(data));
    return data;
  } catch (e: any) {
    captureError(e);
    handleAsyncError(e);
  }
});

export const sendIndividualContract = createAsyncThunk<void, CreateIndividualContractDto>(
  'user/sendIndividualContract',
  async (payload, { dispatch }) => {
    try {
      const { data } = await api.V1ContractsApi.contractsControllerCreateOneIndividual(payload);
      dispatch(actions.setUserContract(data));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const sendSelfEmployedContract = createAsyncThunk<void, CreateSelfEmployedContractDto>(
  'user/sendSelfEmployedContract',
  async (payload, { dispatch }) => {
    try {
      const { data } = await api.V1ContractsApi.contractsControllerCreateOneSelfEmployed(payload);
      dispatch(actions.setUserContract(data));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const fetchSelfContract = createAsyncThunk('user/fetchSelfContract', async (payload, { dispatch }) => {
  try {
    const { data } = await api.V1ContractsApi.contractsControllerGetSelf();
    dispatch(actions.setUserContract(data));
  } catch (e: any) {
    captureError(e);
    handleAsyncError(e);
  }
});

export const fetchSelfPaymentMethods = createAsyncThunk('user/fetchSelfPaymentMethods', async (_, { dispatch }) => {
  try {
    const { data } = await api.V1PaymentMethodsApi.paymentMethodsControllerGetUserPaymentMethods();
    dispatch(actions.updatePaymentMethods(data));

    return data;
  } catch (error: any) {
    captureError(error);
    handleAsyncError(error);
  }
});

export const updateDefaultPaymentMethod = createAsyncThunk(
  'user/updateDefaultPaymentMethod',
  async (payload: number, { dispatch }) => {
    try {
      const { data } = await api.V1PaymentMethodsApi.paymentMethodsControllerUpdateDefaultPaymentMethod({
        paymentMethodId: payload,
      });
      await dispatch(fetchWallet());

      return data;
    } catch (error: any) {
      captureError(error);
      handleAsyncError(error);
    }
  },
);
export const fetchConsultationsWithoutReview = createAsyncThunk<Consultation[] | undefined>(
  'user/getConsultationsWithoutReview',
  async () => {
    try {
      const { data } = await api.V1ConsultationsApi.consultationsControllerGetSelfWithoutReview();
      return data;
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

export const removeConsultationWithReview = createAction<number>('user/deleteConsultationWithReview');

export const updateSelfEmployedContract = createAsyncThunk(
  'user/updateSelfEmployedContract',
  async ({ id, payload }: { id: number; payload: UpdateSelfEmployedContractDto }, { dispatch }) => {
    try {
      const { data } = await api.V1ContractsApi.contractsControllerUpdateOneSelfEmployed(id, payload);
      dispatch(actions.setUserContract(data));
    } catch (e: any) {
      captureError(e);
      handleAsyncError(e);
    }
  },
);

import { RootState } from '../../index';
import { ContentChangeRequestStatusEnum, VerificationRequestStatusEnum } from '../../../api';

export const selectContentChangeRequest = (state: RootState) => {
  if (state.profile.contentChangeRequests.length === 0) {
    return;
  }

  return state.profile.contentChangeRequests.find((r) => r.status === ContentChangeRequestStatusEnum.Pending);
};

export const selectVerificationRequest = (state: RootState) => {
  return state.profile.verificationRequests.find((r) => r.status === VerificationRequestStatusEnum.Pending);
};

export const selectUserCategoryMetaByCategoryId = (id: number) => (state: RootState) => {
  return state.profile.user.categories?.find((category) => category.categoryId === id);
};

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  fetchMe,
  setToken,
  signOut,
  fetchWallet,
  fetchSelfPromocode,
  fetchUpdateIam,
  fetchConsultationsWithoutReview,
  removeConsultationWithReview,
} from './actions';
import {
  AuthUser,
  ContentChangeRequest,
  Contract,
  NotificationsSetting,
  Payment,
  PaymentMethod,
  User,
  UserCategoryMeta,
  UserPromocode,
  VerificationRequest,
  Wallet,
  Consultation,
  ConsultationStatusEnum,
} from '../../../api';
import { RootState } from '../../index';
import { removeCookie } from 'utils';
import { COOKIE_KEY } from 'common/constants';

interface PaginatedPayments {
  data: Payment[];
  count: number;
  page: number;
  total: number;
  pageCount: number | null;
}

interface ProfileState {
  token: string | null;
  user: User;
  notificationsSetting: NotificationsSetting;
  promocodes: UserPromocode[];
  contentChangeRequests: ContentChangeRequest[];
  verificationRequests: VerificationRequest[];
  wallet: Wallet;
  payments: PaginatedPayments;
  paymentMethods: PaymentMethod[];
  consultationsWithoutReview: Consultation[];
  contract: Contract;
}

const initialState: ProfileState = {
  token: '',
  user: {} as User,
  notificationsSetting: {} as NotificationsSetting,
  promocodes: [],
  contentChangeRequests: [],
  wallet: {} as Wallet,
  verificationRequests: [],
  payments: {} as PaginatedPayments,
  paymentMethods: [],
  consultationsWithoutReview: [],
  contract: {} as Contract,
};

export const profileSlice = createSlice({
  name: 'profile',
  initialState: initialState,
  reducers: {
    authUser: {
      reducer: (state, action: PayloadAction<AuthUser>) => {
        state.token = action.payload.token;
        state.user = action.payload.user;
      },
      prepare: (data: AuthUser) => {
        return {
          payload: data,
        };
      },
    },
    updateUser: {
      reducer: (state, action: PayloadAction<Partial<User>>) => {
        state.user = { ...state.user, ...action.payload };
      },
      prepare: (user: Partial<User>) => {
        return {
          payload: user,
        };
      },
    },
    setContentChangeRequests: {
      reducer: (state, action: PayloadAction<ContentChangeRequest[]>) => {
        state.contentChangeRequests = action.payload;
      },
      prepare: (contentChangeRequests: ContentChangeRequest[]) => {
        return {
          payload: contentChangeRequests,
        };
      },
    },

    setWallet: {
      reducer: (state, { payload }: PayloadAction<Wallet>) => {
        state.wallet = payload;
      },
      prepare: (wallet: Wallet) => {
        return {
          payload: wallet,
        };
      },
    },

    updatePaymentMethods: {
      reducer: (state, { payload }: PayloadAction<PaymentMethod[]>) => {
        state.paymentMethods = payload;
      },
      prepare: (paymentMethods: PaymentMethod[]) => {
        return {
          payload: paymentMethods,
        };
      },
    },

    updateUserCategories: {
      reducer: (state, action: PayloadAction<UserCategoryMeta[]>) => {
        if (state.user.categories) {
          state.user.categories.push(...action.payload);
        } else {
          state.user.categories = action.payload;
        }
      },
      prepare: (category: UserCategoryMeta[]) => {
        return {
          payload: category,
        };
      },
    },
    deleteUserCategory: {
      reducer: (state, action: PayloadAction<number>) => {
        state.user.categories = state.user.categories?.filter((c) => c.id !== action.payload) || [];
      },
      prepare: (id: number) => {
        return {
          payload: id,
        };
      },
    },
    updateNotificationSetting: {
      reducer: (state, action: PayloadAction<NotificationsSetting>) => {
        state.notificationsSetting = action.payload;
      },
      prepare: (setting: NotificationsSetting) => {
        return {
          payload: setting,
        };
      },
    },

    addUserPromocodes: {
      reducer: (state, action: PayloadAction<UserPromocode[]>) => {
        state.promocodes = action.payload;
      },
      prepare: (promocodes: UserPromocode[]) => {
        return {
          payload: promocodes,
        };
      },
    },

    setVerificationRequests: {
      reducer: (state, { payload }: PayloadAction<VerificationRequest[]>) => {
        state.verificationRequests = payload;
      },
      prepare: (requests: VerificationRequest[]) => {
        return {
          payload: requests,
        };
      },
    },

    addUserPayments: {
      reducer: (state, action: PayloadAction<PaginatedPayments>) => {
        state.payments = action.payload;
      },
      prepare: (payments: PaginatedPayments) => {
        return {
          payload: payments,
        };
      },
    },

    setUserContract: {
      reducer: (state, action: PayloadAction<Contract>) => {
        state.contract = action.payload;
      },
      prepare: (contract: Contract) => {
        return {
          payload: contract,
        };
      },
    },
  },

  extraReducers: {
    [setToken.type]: (state, { payload }: PayloadAction<string>) => {
      state.token = payload;
    },
    [fetchMe.fulfilled.type]: (state, { payload }: PayloadAction<User>) => {
      state.user = payload;
    },
    [signOut.fulfilled.type]: (state) => {
      state.user = {} as User;
      state.token = '';
      removeCookie(COOKIE_KEY.accessToken);
    },
    [fetchConsultationsWithoutReview.fulfilled.type]: (state, { payload }: PayloadAction<Consultation[]>) => {
      state.consultationsWithoutReview = payload.filter((c) =>
        [
          ConsultationStatusEnum.Ended,
          ConsultationStatusEnum.EndedByClient,
          ConsultationStatusEnum.EndedByExpert,
          ConsultationStatusEnum.MessagesLimitExceeded,
          ConsultationStatusEnum.TimeLimitExceeded,
        ].includes(c.status),
      );
    },
    [removeConsultationWithReview.type]: (state, { payload }: PayloadAction<number>) => {
      const consultationWithReviewIndex = state.consultationsWithoutReview.findIndex((cons) => cons.id === payload);
      state.consultationsWithoutReview.splice(consultationWithReviewIndex, 1);
    },
  },
});

export const { reducer: profileReducer } = profileSlice;
export const actions = {
  ...profileSlice.actions,
  fetchMe,
  signOut,
  setToken,
  fetchSelfPromocode,
  fetchUpdateIam,
  fetchConsultationsWithoutReview,
  removeConsultationWithReview,
  fetchWallet,
};
export const selectors = {
  selectToken: (state: RootState) => state.profile.token,
  selectUser: (state: RootState) => state.profile.user,
  selectIsAuthentication: (state: RootState) => !!state.profile.token,
  selectNotificationSetting: (state: RootState) => state.profile.notificationsSetting,
  selectUserPromocodes: (state: RootState) => state.profile.promocodes,
  selectWallet: (state: RootState) => state.profile.wallet,
  selectPayments: (state: RootState) => state.profile.payments,
  selectPaymentMethods: (state: RootState) => state.profile.paymentMethods,
  selectConsultationsWithoutReview: (state: RootState) => state.profile.consultationsWithoutReview,
  selectIsNeedReview: (state: RootState) =>
    Boolean(
      state.profile.consultationsWithoutReview.length &&
        state.profile.consultationsWithoutReview[0].reviews.filter(
          (review) => review.authorId === state.profile.user.id,
        ),
    ),
  selectContract: (state: RootState) => state.profile.contract,
};

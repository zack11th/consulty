import { RootState } from 'store';
import { ConsultationStatusEnum } from 'api';

export const selectIsReviewModalOpen = (state: RootState) => state.consultationsChatRoom.isReviewOpenModal;
export const selectConsultations = (state: RootState) => state.consultationsChatRoom.consultations;
export const selectActiveConsultation = (state: RootState) =>
  state.consultationsChatRoom.consultations.find((c) => c.status === ConsultationStatusEnum.Active);
export const selectPendingStartConsultation = (state: RootState) =>
  state.consultationsChatRoom.consultations.find((c) => c.status === ConsultationStatusEnum.PendingStart);

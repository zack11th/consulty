import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TypeOptions as NotificationType } from 'react-toastify';
import type { RootState } from '../index';

interface Notification {
  message: string;
  type?: NotificationType;
}

interface AuthModal {
  isWait: boolean;
  counter: number;
  isVisible: boolean;
}

interface JustModal {
  isVisible: boolean;
}

export const appSlice = createSlice({
  name: 'app',
  initialState: {
    notification: {
      message: '',
    } as Notification,
    authModal: {
      isWait: false,
      counter: 60,
      isVisible: false,
    } as AuthModal,
    thanksModal: {
      isVisible: false,
    } as JustModal,
    complainModal: {
      isVisible: false,
    } as JustModal,
    prolongationModal: {
      isVisible: false,
    } as JustModal,
    extraServiceModal: {
      isVisible: false,
    } as JustModal,
    permanentReviewModal: {
      isVisible: false,
    } as JustModal,
    defaultThanksModal: {
      isVisible: false,
    } as JustModal,
  },
  reducers: {
    showNotification(state, action: PayloadAction<Notification>) {
      state.notification = action.payload;
    },
    showAuthModal: {
      prepare: () => {
        return {
          payload: {},
        };
      },
      reducer: (state) => {
        state.authModal.isVisible = true;
      },
    },
    hideAuthModal: {
      prepare: () => {
        return {
          payload: {},
        };
      },
      reducer: (state) => {
        state.authModal.isVisible = false;
      },
    },
    setWait: {
      prepare: (value: boolean) => {
        return {
          payload: value,
        };
      },
      reducer: (state, action: PayloadAction<boolean>) => {
        state.authModal.isWait = action.payload;
      },
    },
    setCounter: {
      prepare: (value: number) => {
        return {
          payload: value,
        };
      },
      reducer: (state, action: PayloadAction<number>) => {
        state.authModal.counter = action.payload;
      },
    },
    setIsVisibleThanksModal(state, { payload }: PayloadAction<boolean>) {
      state.thanksModal.isVisible = payload;
    },
    setIsVisibleComplainModal(state, { payload }: PayloadAction<boolean>) {
      state.complainModal.isVisible = payload;
    },
    setIsVisibleProlongationModal(state, { payload }: PayloadAction<boolean>) {
      state.prolongationModal.isVisible = payload;
    },
    setIsVisibleExtraServiceModal(state, { payload }: PayloadAction<boolean>) {
      state.extraServiceModal.isVisible = payload;
    },
    setIsVisiblePermanentReviewModal(state, { payload }: PayloadAction<boolean>) {
      state.permanentReviewModal.isVisible = payload;
    },
    setIsVisibleDefaultThanksModal(state, { payload }: PayloadAction<boolean>) {
      state.defaultThanksModal.isVisible = payload;
    },
  },
});

export const { reducer } = appSlice;
export const actions = { ...appSlice.actions };

export const selectors = {
  selectNotification: (state: RootState) => state.app.notification,
  selectAuthModalIsVisible: (state: RootState) => state.app.authModal.isVisible,
  selectAuthModalIsWait: (state: RootState) => state.app.authModal.isWait,
  selectAuthModalCounter: (state: RootState) => state.app.authModal.counter,
  selectIsVisibleThanksModal: (state: RootState) => state.app.thanksModal.isVisible,
  selectIsVisibleComplainModal: (state: RootState) => state.app.complainModal.isVisible,
  selectIsVisibleProlongationModal: (state: RootState) => state.app.prolongationModal.isVisible,
  selectIsVisibleExtraServiceModal: (state: RootState) => state.app.extraServiceModal.isVisible,
  selectIsVisiblePermanentReviewModal: (state: RootState) => state.app.permanentReviewModal.isVisible,
  selectIsVisibleDefaultThanksModal: (state: RootState) => state.app.defaultThanksModal.isVisible,
  selectIsModalsOpen: (state:RootState) => state.app.authModal.isVisible || state.app.permanentReviewModal.isVisible || state.app.defaultThanksModal.isVisible || state.app.extraServiceModal.isVisible
};

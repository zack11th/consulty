import { RootState } from 'store';

export const selectCurrentAudioId = (state: RootState) => state.audioMessages.currentAudioId;

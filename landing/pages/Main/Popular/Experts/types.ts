export type ExpertType = {
  about: string;
  age?: string;
  average_price: string;
  city?: string;
  comments_count: string;
  first_name: string;
  id: number;
  last_name: string;
  rating: string;
  userpic?: string;
  userpicRetina?: string;

  achievement: string;
  achievementIcon: string;
};

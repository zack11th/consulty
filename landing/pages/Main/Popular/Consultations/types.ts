export type ConsultationType = {
  name: string;

  ancestors: {
    name: string;
  }[];
};

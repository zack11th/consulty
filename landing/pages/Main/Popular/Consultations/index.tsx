import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../../constants/breakpoints';

import { Consultation } from './Consultation';
import type { ConsultationType } from './types';

const StyledTitle = styled.div({
  fontSize: 21,
  fontWeight: 'bold',
  lineHeight: 1,
  color: '#232832',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    fontSize: 25,
    lineHeight: 1.2,
    marginBottom: 40,
  },
});

const consultations: ConsultationType[] = [
  {
    name: 'Как лучше привлечь клиентов в мой бизнес через интернет?',

    ancestors: [
      {
        name: 'Бизнес',
      },

      {
        name: 'Интернет-маркетинг',
      },

      {
        name: 'Продвижение в Instagram',
      },
    ],
  },

  {
    name: 'Как сделать грамотный договор оказания услуг?',

    ancestors: [
      {
        name: 'Бизнес',
      },

      {
        name: 'Юридические вопросы',
      },

      {
        name: 'Общая консультация',
      },
    ],
  },

  {
    name: 'Как привлекать хороших продавцов на работу?',

    ancestors: [
      {
        name: 'Бизнес',
      },

      {
        name: 'Управление и развитие',
      },

      {
        name: 'Отделом продаж',
      },
    ],
  },

  {
    name: 'Как нам платить меньше налогов, если мы ООО и покупаем много товаров?',

    ancestors: [
      {
        name: 'Бизнес',
      },

      {
        name: 'Бухгалтерия',
      },

      {
        name: 'Оптимизация налогообложения',
      },
    ],
  },
];

export const Consultations: FC = () => (
  <div>
    <StyledTitle>Популярные консультации</StyledTitle>

    <div>
      {consultations.map((consultation, index) => (
        <Consultation consultation={consultation} key={index} />
      ))}
    </div>
  </div>
);

export type CategoryType = {
  id: number;
  name: string;
  icon?: string;
  iconRetina?: string;
  coming_soon: boolean;
  url: string;
  consultations?: number;
};

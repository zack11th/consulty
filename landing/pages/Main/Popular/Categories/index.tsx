import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../../constants/breakpoints';

import { Category } from './Category';

import { CategoryType } from './types';

const StyledTitle = styled.div({
  fontSize: 21,
  fontWeight: 'bold',
  lineHeight: 1,
  color: '#232832',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    fontSize: 25,
    lineHeight: 1.2,
    marginBottom: 40,
  },
});

/* const StyledLoadMoreWrapper = styled.div({
  marginTop: 26,

  [CONDITION_DESKTOP]: {
    marginTop: 35,
  },
});

const StyledLoadMore = styled.button({
  margin: 0,
  padding: 0,
  outline: 'none',
  border: 'none',
  backgroundColor: 'transparent',
  
  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#8b33ff',
  cursor: 'pointer',

  [CONDITION_DESKTOP]: {
    fontSize: 17,
    lineHeight: 1.2,
  },
}); */

const categories: CategoryType[] = [
  {
    id: 1,
    name: 'Бизнес',
    coming_soon: false,
    url: '/',
    consultations: 150,
    icon: '/landing/cup.png',
    iconRetina: '/landing/cup@2x.png',
  },

  {
    id: 2,
    name: 'Спорт',
    coming_soon: true,
    url: '/',
    icon: '/landing/ball.png',
    iconRetina: '/landing/ball@2x.png',
  },

  {
    id: 3,
    name: 'Красота',
    coming_soon: true,
    url: '/',
    icon: '/landing/dress.png',
    iconRetina: '/landing/dress@2x.png',
  },

  {
    id: 4,
    name: 'Образование',
    coming_soon: true,
    url: '/',
    icon: '/landing/books.png',
    iconRetina: '/landing/books@2x.png',
  },

  {
    id: 5,
    name: 'И еще 19 других тем',
    coming_soon: true,
    url: '/',
    icon: '/landing/chat.png',
    iconRetina: '/landing/chat@2x.png',
  },
];

export const Categories: FC = () => (
  <div>
    <StyledTitle>Популярные темы</StyledTitle>

    <div>
      {categories.map((category) => (
        <Category category={category} key={category.id} />
      ))}
    </div>

    {/* <StyledLoadMoreWrapper>
      <StyledLoadMore
        type="button"
      >
        Все темы (еще 16)
      </StyledLoadMore>
    </StyledLoadMoreWrapper> */}
  </div>
);

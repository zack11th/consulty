import React, { useEffect } from 'react';
import type { FC } from 'react';
import Head from 'next/head';
import { Container } from '../../components/Container';
import { BecomeAnExpert } from '../../banners/BecomeAnExpert';
import { TopBlock } from './TopBlock';
import { Popular } from './Popular';
import { Promo } from './Promo';
import { HowItWorks } from '../ExpertLanding/HowItWorks';
import { Guarantee } from './Guarantee';
import { Answers } from '../ExpertLanding/Answers';
import { AdverstingModalWrapper } from './AdverstingModalWrapper';
import { questions } from './questions';
import { MainLayout } from 'components/layouts/main-layout';
import { TFunction, useTranslation } from 'next-i18next';
import { routes } from 'common/routes';
import { useSelector } from 'react-redux';
import { useAppDispatch } from 'hooks/redux';
import { getCookie } from 'utils';
import { COOKIE_KEY } from 'common/constants';
import { actions } from 'store/ducks/profile';

const getNavbar = (t: TFunction) => [
  {
    text: t('defaultNavbar.findExperts'),
    href: routes.topics,
  },
];

export const Main: FC<unknown> = () => {
  const { t } = useTranslation('header.component');

  return (
    <MainLayout
      head={{ title: 'Сервис онлайн-консультаций с экспертами на любые темы - Consulty' }}
      navbar={getNavbar(t)}
      hasFooter
      isLoginButtonVisible
    >
      <Head>
        <meta name="title" content="Сервис онлайн-консультаций с экспертами на любые темы - Consulty" />
        <meta
          name="description"
          content="Consulty – простой сервис, в котором можно найти эксперта в любой теме и заказать у него консультацию по вашему вопросу: бизнес, спорт, недвижимость, красота, дети и другим темам."
        />
        <meta name="og:title" content="Сервис онлайн-консультаций с экспертами на любые темы" />
        <meta name="og:site_name" content="www.consulty.online" />
        <meta name="og:url" content="https://www.consulty.online/" />
        <meta
          name="og:description"
          content="Consulty – простой сервис, в котором можно найти эксперта в любой теме и заказать у него консультацию по вашему вопросу: бизнес, спорт, недвижимость, красота, дети и другим темам."
        />
        <meta name="og:image" content="https://www.consulty.online/landing/seo/seo-girl1.jpg" />
      </Head>
      <TopBlock />
      <Popular />
      <Promo />
      <HowItWorks
        title="Как это работает?"
        description="В вашем распоряжении огромная база экспертов во всех областях — всего пара кликов отделяет вас от них."
        items={[
          {
            title: 'Выберите эксперта',
            description:
              'Сравнивайте экспертов по рейтингу, отзывам, количеству консультаций, опыту и стоимости консультаций. Если не хотите выбирать – просто опубликуйте ваш вопрос, и эксперты откликнутся сами.',
            imageDesktop: '/landing/main/steps/step1.png',
            imageMobile: '/landing/main/steps/step1.png',
          },
          {
            title: 'Получите консультацию',
            description:
              'Эксперт изучит ваш вопрос и даст первоклассную консультацию. Консультация длится 1 час и стоит от 50 рублей, в зависимости от уровня эксперта.',
            imageDesktop: '/landing/main/steps/step2.png',
            imageMobile: '/landing/main/steps/step2.png',
          },
          {
            title: 'Оцените эксперта',
            description:
              'Оцените, насколько хорошо вас проконсультировали. Если вам не понравилось — вернем деньги сразу.',
            imageDesktop: '/landing/main/steps/step3.png',
            imageMobile: '/landing/main/steps/step3.png',
          },
        ]}
        // buttonText="Выбрать эксперта"
        // buttonDescription="и получить консультацию"
        // onButtonClick={() => {
        //   ym(66496642, 'reachGoal', 'Main-how_it_works-choose_expert');
        // }}
      />
      <Guarantee />
      <Container>
        <BecomeAnExpert />
      </Container>
      <Answers questions={questions} />
      {/* <MobileApp /> */}
      <AdverstingModalWrapper />
    </MainLayout>
  );
};

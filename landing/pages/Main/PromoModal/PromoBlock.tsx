import React from 'react';
import type { FC, ReactNode } from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div({
  display: 'flex',

  '& + &': {
    marginTop: 40,
  },
});

const StyledImageWrapper = styled.div({
  marginRight: 20,
});

const StyledImage = styled.img({
  width: 60,
  height: 60,
});

const StyledContent = styled.div({
  flex: 1,
});

const StyledTitle = styled.div({
  fontSize: '19px',
  fontWeight: 'bold',
  lineHeight: 1.05,
  letterSpacing: 'normal',
  color: '#8b33ff',
  marginBottom: 5,
});

const StyledText = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.38,
  color: '#3e4854',
});

type PromoBlockProps = {
  icon: string;
  iconRetina: string;
  title: ReactNode;
  children: ReactNode;
};

export const PromoBlock: FC<PromoBlockProps> = ({ icon, iconRetina, title, children }) => (
  <StyledWrapper>
    <StyledImageWrapper>
      <StyledImage src={icon} srcSet={`${iconRetina} 2x`} alt="" />
    </StyledImageWrapper>

    <StyledContent>
      <StyledTitle>{title}</StyledTitle>

      <StyledText>{children}</StyledText>
    </StyledContent>
  </StyledWrapper>
);

export type BlockType = {
  label: string;
  imageDesktop: string;
  imageMobile: string;
  icon: string;
  iconRetina: string;
};

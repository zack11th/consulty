import React, { memo, useCallback } from 'react';
import styled from 'styled-components';

import { PromoBlock, PromoModal } from './PromoModal';
import { Button } from '../../components/Button';
import { useAppDispatch } from 'hooks/redux';
import { actions, selectors } from 'store/ducks';
import { useRouter } from 'next/router';
import { routes } from 'common/routes';
import { useSelector } from 'react-redux';

const StyledTitleTopBlock = styled.div({
  display: 'flex',
});

const StyledTitle = styled.span({
  fontSize: 25,
  fontWeight: 'bold',
  lineHeight: '31px',
  color: '#ffffff',
});

const StyledTitleIcon = styled.img({
  width: 31,
  height: 31,
  marginLeft: 10,
});

const StyledSubTitle = styled.div({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: 1.43,
  opacity: 0.8,
  color: '#ffffff',
});

const StyledBlocksWrapper = styled.div({
  marginTop: 30,
  marginBottom: 40,
});

const StyledRulesButtonWrapper = styled.div({
  textAlign: 'center',
  marginTop: 20,
});

const StyledRulesButton = styled.a({
  margin: 0,
  padding: 0,
  outline: 'none',
  border: 'none',
  background: 'transparent',

  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.54,
  textAlign: 'center',
  color: '#93969d',
  cursor: 'pointer',
  textDecoration: 'none',
});

type AdverstingModalProps = {
  show: boolean;
  onHide: () => void;
};

export const AdverstingModal = memo<AdverstingModalProps>(({ show, onHide }) => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const isAuthentication = useSelector(selectors.profile.selectIsAuthentication);

  const onClick = useCallback(() => {
      dispatch(actions.app.showAuthModal());
  }, []);

  return (
    <PromoModal
      show={show}
      onHide={onHide}
      title={
        <>
          <StyledTitleTopBlock>
            <StyledTitle>Запуск в январе!</StyledTitle>

            <StyledTitleIcon src="/landing/rocket-1-f-680.png" srcSet="/landing/rocket-1-f-680@2x.png 2x" alt="" />
          </StyledTitleTopBlock>

          <StyledSubTitle>Начнутся консультации по первой теме «Бизнес»</StyledSubTitle>
        </>
      }
    >
      <StyledBlocksWrapper>
        <PromoBlock
          icon="/landing/client.png"
          iconRetina="/landing/client@2x.png"
          title="Первая консультация бесплатно"
        >
          Первые 2 недели раздаем 5 000 бесплатных консультаций — ищите промокоды у блогеров и в СМИ.
        </PromoBlock>

        <PromoBlock icon="/landing/expert.png" iconRetina="/landing/expert@2x.png" title="Особые условия экспертам">
          Пройдите закрытую регистрацию и войдите в круг первых экспертов сервиса на особых условиях.
        </PromoBlock>
      </StyledBlocksWrapper>

      <div>
        {/* {userProfile ? (
          <Button as={Link} to="/cabinet/" $block $size="50" onClick={onHide}>
            Зарегистрироваться
          </Button> */}
        <Button type="button" $block $size="50" onClick={onClick}>
          Зарегистрироваться
        </Button>
      </div>

      <StyledRulesButtonWrapper>
        <StyledRulesButton href="/experts/expert1.png" target="_blank" rel="noreferrer noopener">
          Правила акции
        </StyledRulesButton>
      </StyledRulesButtonWrapper>
    </PromoModal>
  );
});

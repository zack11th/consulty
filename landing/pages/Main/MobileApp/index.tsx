import React, { memo } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { containImageStyle } from '../../../utils/containImageStyle';

import { Form } from './Form';

const StyledWrapper = styled.div({
  position: 'relative',
  overflow: 'hidden',
  backgroundColor: '#8b33ff',
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
  borderBottomLeftRadius: 20,
  paddingTop: 30,
  paddingLeft: 20,
  paddingRight: 20,
  paddingBottom: 314,
  marginTop: 90,
  marginBottom: 90,

  '&::before': {
    content: '""',
    position: 'absolute',
    bottom: -55,
    width: 223,
    height: 320,
    left: '50%',
    marginLeft: -111,
    ...containImageStyle('/landing/main/hand-app.png'),
  },

  [CONDITION_DESKTOP]: {
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    borderBottomLeftRadius: 50,
    paddingTop: 90,
    paddingLeft: 591,
    paddingRight: 30,
    paddingBottom: 87,
    marginTop: 120,
    marginBottom: 120,

    '&::before': {
      bottom: -112,
      width: 449,
      height: 645,
      left: 0,
      marginLeft: 0,
    },
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  fontWeight: 'bold',
  lineHeight: 1.2,
  color: '#ffffff',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 30,
  },
});

const StyledDescription = styled.div({
  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#ffffff',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    fontSize: 19,
    lineHeight: 1.58,
  },
});

const StyledLinksWrapper = styled.div({
  marginBottom: 40,

  [CONDITION_DESKTOP]: {
    marginBottom: 60,
  },
});

const StyledAppLink = styled.a({
  display: 'inline-flex',
  verticalAlign: 'bottom',

  '& + &': {
    marginLeft: 10,
  },
});

const StyledAppImage = styled.img({
  height: 40,

  [CONDITION_DESKTOP]: {
    height: 60,
  },
});

const StyledSMSText = styled.div({
  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#ffffff',
  marginBottom: 15,

  [CONDITION_DESKTOP]: {
    fontSize: 17,
    lineHeight: 1.47,
  },
});

export const MobileApp = memo(() => (
  <Container>
    <StyledWrapper>
      <StyledTitle>С приложением удобнее</StyledTitle>

      <StyledDescription>Скачайте Consulty и эксперты всегда будут рядом с вами.</StyledDescription>

      <StyledLinksWrapper>
        <StyledAppLink href="/">
          <StyledAppImage src="/landing/store-apple.svg" alt="IOs" />
        </StyledAppLink>

        <StyledAppLink href="/">
          <StyledAppImage src="/landing/store-google.svg" alt="Android" />
        </StyledAppLink>
      </StyledLinksWrapper>

      <StyledSMSText>Получите ссылку на приложение в смс:</StyledSMSText>

      <Form />
    </StyledWrapper>
  </Container>
));

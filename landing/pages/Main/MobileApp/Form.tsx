import React, { useState, useCallback, SyntheticEvent } from 'react';
import type { FC, ChangeEventHandler } from 'react';
import styled from 'styled-components';
import MaskedInput from 'react-text-mask';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Button } from '../../../components/Button';

const StyledInputWrapper = styled.div({
  display: 'flex',
  borderRadius: 4,
  backgroundColor: '#ffffff',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    marginBottom: 30,
    maxWidth: 300,
  },
});

const StyledFirstNumber = styled.div({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: 50,
  borderRight: 'solid 1px #eaeaea',

  fontSize: 17,
  fontWeight: 500,
  lineHeight: 1.18,
  color: '#79818c',
});

const StyledInput = styled(MaskedInput)({
  maxWidth: 170,
  height: 50,
  paddingLeft: 15,
  margin: 0,
  outline: 'none',
  border: 'none',

  fontSize: 17,
  fontWeight: 500,
  lineHeight: 1.18,
  color: '#232832',
});

const StyledButtonWrapper = styled.div({
  display: 'flex',
  justifyContent: 'center',

  [CONDITION_DESKTOP]: {
    justifyContent: 'flex-start',
  },
});

const mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

export const Form: FC = () => {
  const [phone, setPhone] = useState('');

  const onPhoneChange = useCallback<ChangeEventHandler<HTMLInputElement>>((event) => {
    setPhone(event.target.value);
  }, []);

  const onSubmit = useCallback((event: SyntheticEvent): void => {
    event.preventDefault();
  }, []);

  return (
    <form onSubmit={onSubmit}>
      <StyledInputWrapper>
        <StyledFirstNumber>+7</StyledFirstNumber>

        <StyledInput mask={mask} value={phone} onChange={onPhoneChange} />
      </StyledInputWrapper>

      <StyledButtonWrapper>
        <Button type="submit" $color="white" $bordered>
          Получить ссылку
        </Button>
      </StyledButtonWrapper>
    </form>
  );
};

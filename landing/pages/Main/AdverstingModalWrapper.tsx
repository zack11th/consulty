import React, { memo, useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { selectors } from 'store/ducks/app';
import {selectors as userSelectors} from 'store/ducks';
import { AdverstingModal } from './AdverstingModal';

export const AdverstingModalWrapper = memo(() => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const isAuthentication = useSelector(userSelectors.profile.selectIsAuthentication);
  const anyAppModalOpen = useSelector(selectors.selectIsModalsOpen)
  console.log('anyAppModalOpen',anyAppModalOpen);
    const openModal = useCallback(() => {
    setIsModalOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, []);

    useEffect(() => {
      const timeout = setTimeout(() => {
        if (Math.random()) {
          openModal();
        }
      }, 5000);
      if(anyAppModalOpen || isAuthentication){
        clearTimeout(timeout);
        isModalOpen? closeModal() : null
      }

    return () => {
      clearTimeout(timeout);
    };
  }, [anyAppModalOpen,isAuthentication]);

  return <AdverstingModal show={isModalOpen} onHide={closeModal} />;
});

import React,{useEffect} from 'react';
import type { FC } from 'react';

import { Container } from '../../components/Container';
import { NotFound } from '../../banners/NotFound';
import { StartEarn } from '../../banners/StartEarn';
import { Ruble } from '../../components/Ruble';

import { Block1 } from './Block1/index';
import { Targets } from './Targets/index';
import { Examples } from './Examples/index';
import { ModalItems } from './ModalItems/index';
import { Categories } from './Categories/index';
import { HowItWorks } from './HowItWorks/index';
import { HowItWorksConsultations } from './HowItWorksConsultations';
import { Salaries } from './Salaries/index';
import { Attract } from './Attract/index';
import { EarnMore } from './EarnMore/index';
import { SpecialOffer } from './SpecialOffer/index';
import { Answers } from './Answers/index';

import { targets } from './targets';
import { questions } from './questions';
import Head from 'next/head';
import { MainLayout } from 'components/layouts/main-layout';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'next-i18next';
import { routes } from 'common/routes';
import { useSelector } from 'react-redux';
import { selectors } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';
import { getCookie } from 'utils';
import { COOKIE_KEY } from 'common/constants';
import { actions } from 'store/ducks/profile';

const examples = [
  {
    title: 'по интернет-маркетингу',
    image: '/landing/expert-slides/cons3-1.png',
  },

  {
    title: 'по налогам',
    image: '/landing/expert-slides/cons3-2.png',
  },

  {
    title: 'по магазинам',
    image: '/landing/expert-slides/cons3-3.png',
  },

  {
    title: 'по маркетингу',
    image: '/landing/expert-slides/cons3-4.png',
  },

  {
    title: 'по косметологии',
    image: '/landing/expert-slides/cons3-5.png',
  },

  {
    title: 'по управлению',
    image: '/landing/expert-slides/cons3-6.png',
  },

  {
    title: 'по запуску бизнеса',
    image: '/landing/expert-slides/cons3-7.png',
  },

  {
    title: 'по договорам',
    image: '/landing/expert-slides/cons3-8.png',
  },

  {
    title: 'по SMM-продвижению',
    image: '/landing/expert-slides/cons3-9.png',
  },

  {
    title: 'по производству',
    image: '/landing/expert-slides/cons3-10.png',
  },

  {
    title: 'по логистике',
    image: '/landing/expert-slides/cons3-11.png',
  },

  {
    title: 'по интернет-маркетингу',
    image: '/landing/expert-slides/cons3-12.png',
  },

  {
    title: 'по CRM и ERP-системам',
    image: '/landing/expert-slides/cons3-13.png',
  },

  {
    title: 'по SMS и E-mail рассылкам',
    image: '/landing/expert-slides/cons3-14.png',
  },

  {
    title: 'по хостингу',
    image: '/landing/expert-slides/cons3-15.png',
  },

  {
    title: 'по ремонту',
    image: '/landing/expert-slides/cons3-16.png',
  },

  {
    title: 'по выбору техники',
    image: '/landing/expert-slides/cons3-17.png',
  },

  {
    title: 'по психологии',
    image: '/landing/expert-slides/cons3-18.png',
  },

  {
    title: 'по страховкам',
    image: '/landing/expert-slides/cons3-19.png',
  },

  {
    title: 'по недвижимости',
    image: '/landing/expert-slides/cons3-20.png',
  },
];

const getNavbar = (t: TFunction) => [
  {
    text: t('defaultNavbar.findExperts'),
    href: routes.topics,
  },
];

export const Landing: FC = () => {
  const { t } = useTranslation('header.component');

  return (
    <MainLayout
      hasFooter
      head={{ title: 'Станьте экспертом и зарабатывайте на онлайн-консультациях - Consulty' }}
      navbar={getNavbar(t)}
    >
      <Head>
        <meta name="title" content="Станьте экспертом и зарабатывайте на онлайн-консультациях - Consulty" />
        <meta
          name="description"
          content="Станьте экспертом Consulty, делитесь опытом и получайте до 5 000 ‎₽/час за онлайн-консультации. Зарабатывайте отзывы, формируйте репутацию эксперта и привлекайте новых клиентов."
        />
        <meta name="og:title" content="Станьте экспертом и зарабатывайте на онлайн-консультациях" />
        <meta name="og:site_name" content="www.consulty.online" />
        <meta name="og:url" content="https://www.consulty.online/expert/" />
        <meta
          name="og:description"
          content="Станьте экспертом Consulty, делитесь опытом и получайте до 5 000 ‎₽/час за онлайн-консультации. Зарабатывайте отзывы, формируйте репутацию эксперта и привлекайте новых клиентов."
        />
        <meta name="og:image" content="https://www.consulty.online/landing/seo/seo-girl2.jpg" />
      </Head>
      <Block1 />
      <Targets title="Кому подходит Consulty" items={targets} />
      <Examples
        title="Примеры консультаций"
        description="Посмотрите примеры консультаций, за которыми приходят пользователи"
        innerLabel="Эксперт"
        items={examples}
      />
      <ModalItems
        title="Тысячам людей нужны ваши знания"
        description="Мы приводим на сервис тысячи людей, которым нужны ответы. Посмотрите темы, в которых ищут экспертов."
        contentDescription="Более 150 тем для консультаций!"
        badge={['Запуск в октябре!']}
      />
      {/* <Categories badge={['В 2020 году', '23 новых темы']} /> */}
      <Container>
        <NotFound />
      </Container>
      <HowItWorks
        title="Как работает Consulty"
        items={[
          {
            title: 'Создайте свой профиль эксперта',
            description:
              'Зарегистрируйтесь, заполните профиль, выберите, по каким темам готовы консультировать и сколько будет стоить консультация.',
            imageDesktop: '/landing/main/steps/step-1@2x.jpg',
            imageMobile: '/landing/main/steps/step-1.jpg',
          },
          {
            title: 'Оказывайте консультации',
            description: <HowItWorksConsultations />,
            imageDesktop: '/landing/main/steps/step-2@2x.jpg',
            imageMobile: '/landing/main/steps/step-2.jpg',
          },
          {
            title: 'Повышайте свой рейтинг и зарабатывайте больше',
            description:
              'Получая хорошие отзывы, вы можете повысить цену на  консультации и зарабатывать больше. Выводите деньги, когда удобно: на банковскую карту или расчетный счет.',
            imageDesktop: '/landing/main/steps/step-3@2x.jpg',
            imageMobile: '/landing/main/steps/step-3.jpg',
          },
        ]}
        buttonText="Стать экспертом в Consulty"
        buttonDescription="Регистрация займет 2 минуты"
        onButtonClick={() => {
          window.scrollTo({
            top: document.documentElement.offsetHeight,
            behavior: 'smooth',
          });
        }}
      />
      <Salaries
        title="Сколько можно заработать с Consulty?"
        description="Вы сами задаете цену консультаций!"
        items={[
          {
            icon: '/landing/female-office-worker-1-f-469-200-d-1-f-4-bc.png',
            iconRetina: '/landing/female-office-worker-1-f-469-200-d-1-f-4-bc@2x.png',
            name: 'Бухгалтер',
            schedule: '1 консультация в день',
            priceDescription: 'от 750 рублей',
            price: (
              <>
                20 475 <Ruble fontWeight={700} />
                /мес.
              </>
            ),
            calculation: [
              {
                title: '30 консультаций в месяц',
                description:
                  'Вы сами решаете, сколько консультаций принимать в месяц. Чем больше ваша компетенция и больше отзывов - тем больше консультаций к вам прийдет.',
              },
              {
                prefix: 'x',
                title: (
                  <>
                    750 <Ruble fontWeight={700} />
                  </>
                ),
                description: 'Вы сами решаете, какую стоимость консультации установить.',
              },
              {
                prefix: '-',
                title: '9% комиссии',
                description:
                  'Это комиссия сервиса, в которую входит работа платформы, прием платежей, привлечение клиентов.',
              },
              {
                prefix: '=',
                title: (
                  <>
                    20 475 <Ruble fontWeight={700} />
                  </>
                ),
                description:
                  'Столько вы заработаете с Consulty, тратя до 1 часа в день. Деньги вы получите на вашу карточку или расчетный счет.',
              },
            ],
          },
          {
            icon: '/landing/male-technologist-1-f-468-200-d-1-f-4-bb.png',
            iconRetina: '/landing/male-technologist-1-f-468-200-d-1-f-4-bb@2x.png',
            name: 'Специалист по выбору техники',
            schedule: '6 консультаций в день',
            priceDescription: 'от 200 рублей',
            price: (
              <>
                32 760 <Ruble fontWeight={700} />
                /мес.
              </>
            ),
            calculation: [
              {
                title: '180 консультаций в месяц',
                description:
                  'Вы сами решаете, сколько консультаций принимать в месяц. Чем больше ваша компетенция и больше отзывов - тем больше консультаций к вам прийдет.',
              },
              {
                prefix: 'x',
                title: (
                  <>
                    200 <Ruble fontWeight={700} />
                  </>
                ),
                description: 'Вы сами решаете, какую стоимость консультации установить.',
              },
              {
                prefix: '-',
                title: '9% комиссии',
                description:
                  'Это комиссия сервиса, в которую входит работа платформы, прием платежей, привлечение клиентов.',
              },
              {
                prefix: '=',
                title: (
                  <>
                    32 760 <Ruble fontWeight={700} />
                  </>
                ),
                description:
                  'Столько вы заработаете с Consulty, тратя до 1 часа в день. Деньги вы получите на вашу карточку или расчетный счет.',
              },
            ],
          },
          {
            icon: '/landing/female-judge-1-f-469-200-d-2696-fe-0-f.png',
            iconRetina: '/landing/female-judge-1-f-469-200-d-2696-fe-0-f@2x.png',
            name: 'Востребованный юрист',
            schedule: '2 консультации в день',
            priceDescription: 'по 2500 рублей',
            price: (
              <>
                136 500 <Ruble fontWeight={700} />
                /мес.
              </>
            ),
            calculation: [
              {
                title: '60 консультаций в месяц',
                description:
                  'Вы сами решаете, сколько консультаций принимать в месяц. Чем больше ваша компетенция и больше отзывов - тем больше консультаций к вам прийдет.',
              },
              {
                prefix: 'x',
                title: (
                  <>
                    2 500 <Ruble fontWeight={700} />
                  </>
                ),
                description: 'Вы сами решаете, какую стоимость консультации установить.',
              },
              {
                prefix: '-',
                title: '9% комиссии',
                description:
                  'Это комиссия сервиса, в которую входит работа платформы, прием платежей, привлечение клиентов.',
              },
              {
                prefix: '=',
                title: (
                  <>
                    136 500 <Ruble fontWeight={700} />
                  </>
                ),
                description:
                  'Столько вы заработаете с Consulty, тратя до 1 часа в день. Деньги вы получите на вашу карточку или расчетный счет.',
              },
            ],
          },
        ]}
      />
      <Attract
        title="Привлекайте новых клиентов с Consulty!"
        items={[
          {
            icon: '/landing/woman-tipping-hand-1-f-481-200-d-2640-fe-0-f.png',
            iconRetina: '/landing/woman-tipping-hand-1-f-481-200-d-2640-fe-0-f@2x.png',
            title: 'Предлагайте ваш продукт или услугу',
            subTitle: 'После качественной консультации клиент закажет ваш основной продукт.',
            descriptionIcon: '/landing/fire-1-f-525.png',
            descriptionIconRetina: '/landing/fire-1-f-525@2x.png',
            description:
              'Если вы бухгалтер или SMM-специалист, который понятно объяснил, как действовать клиенту, либо фитнес-тренер, который составил крутую программу: у вас закажут постоянное сопровождение.',
          },
          {
            icon: '/landing/man-raising-hand-1-f-64-b-200-d-2642-fe-0-f.png',
            iconRetina: '/landing/man-raising-hand-1-f-64-b-200-d-2642-fe-0-f@2x.png',
            title: 'Оказывайте дополнительные услуги',
            subTitle:
              'Часто после консультации клиенту нужна дополнительная услуга: аудит, составление договора, помощь в выборе/настройке программы, сервиса или гаджета.',
            descriptionIcon: '/landing/white-heavy-check-mark-2705.png',
            descriptionIconRetina: '/landing/white-heavy-check-mark-2705@2x.png',
            description:
              'Продайте дополнительную услугу за несколько секунд, выставив счет прямо через сервис в процессе консультации.',
          },
          {
            icon: '/landing/deaf-person-1-f-9-cf.png',
            iconRetina: '/landing/deaf-person-1-f-9-cf@2x.png',
            title: 'Обменивайтесь контактами!',
            subTitle:
              'Проконсультировали клиента по рекламе, и он захотел, чтобы вы ее настроили ему? Посоветовали клиенту, куда полететь отдохнуть зимой, и он захотел купить у вас тур? Подобрали подходящий велотренажер, и клиент хочет купить его в вашем магазине?',
            descriptionIcon: '/landing/waving-hand-sign-1-f-44-b.png',
            descriptionIconRetina: '/landing/waving-hand-sign-1-f-44-b@2x.png',
            description: 'Обменивайтесь телефонами или делитесь ссылками, это разрешено.',
          },
          {
            icon: '/landing/deaf-person-1-f-9-cf-copy-2.png',
            iconRetina: '/landing/deaf-person-1-f-9-cf-copy-2@2x.png',
            title: 'Зарабатывайте на рекомендациях!',
            subTitle:
              'Если вы помогаете клиенту с выбором товаров или услуг, используйте реферальную программу сайтов и сервисов, которые рекомендуете и зарабатывайте дополнительно на отчислениях.',
            descriptionIcon: '/landing/collision-symbol-1-f-4-a-5.png',
            descriptionIconRetina: '/landing/collision-symbol-1-f-4-a-5@2x.png',
            description: 'У нас, кстати, есть своя партнерская программа.',
          },
        ]}
      />
      <EarnMore />
      <SpecialOffer />
      <Answers questions={questions} />
      <StartEarn id="start-earn" />
    </MainLayout>
  );
};

import type {
  ReactNode,
} from 'react';

export type QuestionItemType = {
  question: string;
  answer: ReactNode;
};

export type QuestionGroupType = {
  title?: string;
  questions: QuestionItemType[];
};

import React, { memo } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { containImageStyle } from '../../../utils/containImageStyle';

import { Tariff } from './Tariff';
import { useAppDispatch } from 'hooks/redux';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { actions, selectors } from 'store/ducks';
import { routes } from 'common/routes';

const StyledWrapper = styled.div({
  marginTop: 90,
  marginBottom: 60,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 120,
  },
});

const StyledContent = styled.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const StyledImagesBlock = styled.div({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    marginBottom: 25,
  },
});

const StyledImage = styled.img({
  width: 42,
  height: 50,

  '&:nth-child(2)': {
    width: 70,
    height: 66,
  },

  '& + &': {
    marginLeft: 25,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  fontWeight: 'bold',
  lineHeight: 1.2,
  textAlign: 'center',
  color: '#8b33ff',
  marginBottom: 40,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    marginBottom: 60,
  },
});

const StyledTariffs = styled.div({
  display: 'grid',
  gridTemplateColumns: '1fr',
  gridGap: 40,

  [CONDITION_DESKTOP]: {
    gridTemplateColumns: '1fr 1fr',
    gridGap: 60,
  },
});

const StyledBottomBlock = styled.div({
  position: 'relative',
  boxSizing: 'border-box',
  marginTop: 30,
  maxWidth: 380,
  paddingLeft: 55,
  paddingTop: 16,
  paddingBottom: 19,
  paddingRight: 25,

  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#ffffff',
  borderRadius: 12,
  backgroundImage: 'linear-gradient(to right,#862fff 10%,#9c3efe 40%,#AE4CFE 90%)',

  '&::before': {
    content: '""',
    position: 'absolute',
    top: 20,
    left: 15,
    width: 30,
    height: 30,
    ...containImageStyle('/landing/white-up-pointing-backhand-index-1-f-446.png'),
  },
});

export const SpecialOffer = memo(() => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const user = useSelector(selectors.profile.selectUser);

  const userProfile = {
    categories: [],
    email: 'qwe',
    first_name: 'qwe',
    id: 111,
    last_name: 'qwe',
    registration_finished: true,
    userpic: '',
  };

  return (
    <StyledWrapper>
      <Container>
        <StyledContent>
          <StyledImagesBlock>
            <StyledImage src="/landing/fire-1-f-525.png" srcSet="/landing/fire-1-f-525@2x.png 2x" alt="" />

            <StyledImage
              src="/landing/grinning-face-with-star-eyes-1-f-929.png"
              srcSet="/landing/grinning-face-with-star-eyes-1-f-929@2x.png 2x"
              alt=""
            />

            <StyledImage src="/landing/fire-1-f-525.png" srcSet="/landing/fire-1-f-525@2x.png 2x" alt="" />
          </StyledImagesBlock>

          <StyledTitle>Специальное предложение</StyledTitle>

          <StyledTariffs>
            <Tariff
              badgeColor="#ff3366"
              badge="для 500 отобранных экспертов"
              title="Для друзей Consulty"
              description="Если у вас более 1 000 подписчиков, расскажите им, что вы консультируете в Consulty, разместите ссылку на сервис в шапке профиля и получите специальный тариф до конца 2020 года."
              discount="9"
              innerBadge="Consulty рекомендует!"
              innerBadgeBg="#fff7d7"
              innerBadgeIcon="/landing/like.png"
              innerBadgeIconRetina="/landing/like@2x.png"
              innerBadgeIconStyle={{
                top: -5,
                right: 5,
                width: 25,
                height: 25,
              }}
              notes={[
                'Зачисление на карту или расчетный счет',
                <>
                  Вам поступит <b>91%</b> от оказанных консультаций и услуг
                </>,
                'Для физических лиц, самозанятых, ИП и ООО',
              ]}
              buttonText="Зарегистрируйтесь"
              onButtonClick={() => {
                if (user.id) {
                  if (user.isExpert) {
                    router.push(routes.contract);
                  } else {
                    router.push({ pathname: routes.profile, query: { activeTab: 'expertProfile' } });
                  }
                } else {
                  dispatch(actions.app.showAuthModal());
                }
              }}
              buttonDescription="и подайте заявку на тариф в личном кабинете"
            />

            <Tariff
              badgeColor="#8b33ff"
              badge="до 31 октября"
              title="Специальный тариф"
              description="Пониженная комиссия для первых 5 000 экспертов. Тариф действует до конца 2020 года."
              discount="14.9"
              innerBadge="Для тех, кто регистрируется до 31 октября"
              innerBadgeBg="#f7f6ff"
              innerBadgeIcon="/landing/stopwatch.png"
              innerBadgeIconRetina="/landing/stopwatch@2x.png"
              innerBadgeIconStyle={{
                top: 1,
                right: 11,
                height: 18,
              }}
              notes={[
                'Зачисление на карту или расчетный счет',
                <>
                  Вам поступит <b>85,1%</b> от оказанных консультаций и услуг
                </>,
                'Для физических лиц, самозанятых, ИП и ООО',
              ]}
              buttonText="Выбрать тариф"
              onButtonClick={() => {
                const startEartNode = document.getElementById('start-earn');

                if (!startEartNode) {
                  return;
                }

                // ym(66496642, 'reachGoal', 'Landing_ex-special-choose_rate');

                const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
                const epxertsRect = startEartNode.getBoundingClientRect();

                window.scroll({
                  top: scrollTop + epxertsRect.top,
                  behavior: 'smooth',
                });
              }}
            />
          </StyledTariffs>

          <StyledBottomBlock>Успейте заработать больше других. Регистрация займёт не более 1 минуты</StyledBottomBlock>
        </StyledContent>
      </Container>
    </StyledWrapper>
  );
});

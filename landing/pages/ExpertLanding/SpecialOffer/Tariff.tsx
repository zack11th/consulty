import React, { memo } from 'react';
import type { ReactNode, CSSProperties } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Button } from '../../../components/Button';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  position: 'relative',
  borderRadius: 12,
  boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
  border: 'solid 1px #8b33ff',
  backgroundColor: '#ffffff',
  maxWidth: 380,
  paddingTop: 55,
  paddingLeft: 20,
  paddingRight: 20,

  [CONDITION_DESKTOP]: {
    paddingLeft: 40,
    paddingRight: 40,
  },
});

const StyledBadgeWrapper = styled.div({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
});

const StyledBadge = styled.div({
  fontSize: 11,
  fontWeight: 'bold',
  lineHeight: '25px',
  letterSpacing: 0.5,
  textAlign: 'center',
  color: '#ffffff',
  textTransform: 'uppercase',
  borderBottomLeftRadius: 12,
  borderBottomRightRadius: 12,
  paddingLeft: 26,
  paddingRight: 26,
});

const StyledTitle = styled.div({
  fontSize: 22,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#232832',
  marginBottom: 10,

  [CONDITION_DESKTOP]: {
    fontSize: 22,
  },
});

const StyledDescription = styled.div({
  height: 77,

  fontSize: 12,
  fontWeight: 500,
  lineHeight: 'normal',
  textAlign: 'center',
  color: '#79818c',
});

const StyledPercent = styled.div({
  fontSize: 40,
  fontWeight: 500,
  textAlign: 'center',
  color: '#8b33ff',
  marginBottom: 5,
});

const StyledPercentText = styled.span({
  fontSize: 60,
  fontWeight: 'bold',
  marginRight: 9,
});

const StyledComission = styled.div({
  fontSize: 13,
  fontWeight: 500,
  textAlign: 'center',
  color: '#222d39',
  textTransform: 'uppercase',
  marginBottom: 14,
});

const StyledInnerBadgeBlock = styled.div({
  display: 'flex',
  justifyContent: 'center',
  marginBottom: 30,
});

const StyledInnerBadge = styled.div({
  position: 'relative',

  fontSize: 11,
  lineHeight: '23px',
  borderRadius: 12,
  fontWeight: 500,
  textAlign: 'center',
  backgroundColor: '#fff7d7',
  color: '#222d39',
  paddingLeft: 11,
  paddingRight: 33,
});

const StyledInnerBadgeImage = styled.img({
  position: 'absolute',
});

const StyledItemsBlock = styled.div({
  marginBottom: 30,
});

const StyledItem = styled.div({
  position: 'relative',
  paddingLeft: 30,

  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.54,
  color: '#232832',

  '& + &': {
    marginTop: 10,
  },

  '&::before': {
    position: 'absolute',
    top: 0,
    left: 0,
    content: '""',
    width: 20,
    height: 20,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: 'url(/landing/check-green.svg)',
  },

  [CONDITION_DESKTOP]: {
    paddingLeft: 35,
    fontSize: 14,
    lineHeight: 1.43,

    '& + &': {
      marginTop: 20,
    },
  },
});

const StyledButtonWrapper = styled.div({
  marginTop: 30,
});

const StyledButtonDescription = styled.div({
  boxSizing: 'border-box',
  height: 30,
  paddingTop: 9,

  fontSize: 11,
  fontWeight: 500,
  lineHeight: 'normal',
  color: '#79818c',
  textAlign: 'center',

  [CONDITION_DESKTOP]: {
    height: 40,
  },
});

type TariffProps = {
  badgeColor: string;
  badge: string;
  title: string;
  description: string;
  discount: string;
  innerBadge: string;
  innerBadgeBg: string;
  innerBadgeIcon: string;
  innerBadgeIconRetina: string;
  innerBadgeIconStyle: CSSProperties;
  notes: ReactNode[];
  buttonText: string;
  onButtonClick: () => void;
  buttonDescription?: string;
};

export const Tariff = memo<TariffProps>(
  ({
    badgeColor,
    badge,
    title,
    description,
    discount,
    innerBadge,
    innerBadgeBg,
    innerBadgeIcon,
    innerBadgeIconRetina,
    innerBadgeIconStyle,
    notes,
    buttonText,
    onButtonClick,
    buttonDescription,
  }) => (
    <StyledWrapper>
      <StyledBadgeWrapper>
        <StyledBadge
          style={{
            backgroundColor: badgeColor,
          }}
        >
          {badge}
        </StyledBadge>
      </StyledBadgeWrapper>

      <StyledTitle>{title}</StyledTitle>

      <StyledDescription>{description}</StyledDescription>

      <StyledPercent>
        <StyledPercentText>{discount}</StyledPercentText>%
      </StyledPercent>

      <StyledComission>Комиссия сервиса до конца 2020 года</StyledComission>

      <StyledInnerBadgeBlock>
        <StyledInnerBadge
          style={{
            backgroundColor: innerBadgeBg,
          }}
        >
          {innerBadge}

          <StyledInnerBadgeImage
            src={innerBadgeIcon}
            srcSet={`${innerBadgeIconRetina} 2x`}
            style={innerBadgeIconStyle}
            alt=""
          />
        </StyledInnerBadge>
      </StyledInnerBadgeBlock>

      <StyledItemsBlock>
        {notes.map((note, index) => (
          <StyledItem key={index}>{note}</StyledItem>
        ))}
      </StyledItemsBlock>

      <StyledButtonWrapper>
        <Button type="button" $block onClick={onButtonClick}>
          {buttonText}
        </Button>
      </StyledButtonWrapper>

      <StyledButtonDescription>{buttonDescription}</StyledButtonDescription>
    </StyledWrapper>
  ),
);

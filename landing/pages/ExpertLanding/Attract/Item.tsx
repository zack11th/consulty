import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP, CONDITION_RETINA } from '../../../constants/breakpoints';
import { containImageStyle } from '../../../utils/containImageStyle';

import type { ItemType } from './types';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  minWidth: 280,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  borderRadius: 12,
  boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
  border: 'solid 1px #e5e7eb',
  backgroundColor: '#ffffff',
  paddingTop: 20,
  paddingBottom: 30,
  paddingLeft: 20,
  paddingRight: 20,

  [CONDITION_DESKTOP]: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    boxShadow: 'none',
    border: 'none',
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
});

type StyledIconProps = {
  $icon: string;
  $iconRetina: string;
};

const StyledIcon = styled.div<StyledIconProps>(({ $icon, $iconRetina }) => ({
  width: 70,
  height: 70,
  borderRadius: '50%',
  ...containImageStyle($icon),
  marginBottom: 20,

  [CONDITION_RETINA]: {
    backgroundImage: `url(${$iconRetina})`,
  },

  [CONDITION_DESKTOP]: {
    width: 90,
    height: 90,
    marginBottom: 0,
    marginRight: 33,
  },
}));

const StyledContent = styled.div({
  flex: 1,
});

const StyledTitle = styled.div({
  fontSize: 17,
  lineHeight: 1.18,
  fontWeight: 'bold',
  color: '#232832',
  marginBottom: 10,

  [CONDITION_DESKTOP]: {
    fontSize: 21,
  },
});

const StyledSubTitle = styled.div({
  fontSize: 15,
  lineHeight: 1.33,
  fontWeight: 500,
  color: '#3e4854',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 17,
    lineHeight: 1.47,
    marginBottom: 22,
  },
});

type StyledDescriptionProps = {
  $icon: string;
  $iconRetina: string;
};

const StyledDescription = styled.div<StyledDescriptionProps>(({ $icon, $iconRetina }) => ({
  position: 'relative',

  fontSize: 12,
  lineHeight: 1.25,
  fontWeight: 500,
  color: '#3e4854',
  paddingLeft: 30,

  '&::before': {
    content: '""',
    position: 'absolute',
    top: 0,
    left: 0,
    width: 25,
    height: 25,
    ...containImageStyle($icon),
  },

  [CONDITION_RETINA]: {
    '&::before': {
      ...containImageStyle($iconRetina),
    },
  },

  [CONDITION_DESKTOP]: {
    fontSize: 14,
    lineHeight: 1.43,
    borderRadius: 13,
    border: 'solid 1px #f0e6ff',
    padding: '13px 7px 19px 59px',

    '&::before': {
      content: '""',
      position: 'absolute',
      top: 15,
      left: 15,
    },
  },
}));

type ItemProps = {
  item: ItemType;
};

export const Item: FC<ItemProps> = ({
  item: { icon, iconRetina, title, subTitle, descriptionIcon, descriptionIconRetina, description },
}) => (
  <StyledWrapper>
    <StyledIcon $icon={icon} $iconRetina={iconRetina} />

    <StyledContent>
      <StyledTitle>{title}</StyledTitle>

      <StyledSubTitle>{subTitle}</StyledSubTitle>

      <StyledDescription $icon={descriptionIcon} $iconRetina={descriptionIconRetina}>
        {description}
      </StyledDescription>
    </StyledContent>
  </StyledWrapper>
);

import type {
  ReactNode,
} from 'react';

export type ItemType = {
  icon: string;
  iconRetina: string;
  title: string;
  subTitle: string;
  descriptionIcon: string;
  descriptionIconRetina: string;
  description: ReactNode;
};

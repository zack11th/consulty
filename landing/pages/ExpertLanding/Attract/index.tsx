import React, { memo } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { AnimatedInit } from '../../../components/AnimatedInit';

import { Item } from './Item';

import type { ItemType } from './types';

const StyledWrapper = styled.div({
  marginTop: 90,
  marginBottom: 90,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 120,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  lineHeight: 1.2,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#111111',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    lineHeight: 1.2,
  },
});

const StyledContent = styled.div({
  display: 'grid',
  gridTemplateColumns: 'repeat(4, 1fr)',
  gridGap: 10,
  paddingBottom: 20,
  overflowX: 'auto',
  overflowY: 'hidden',
  marginLeft: -20,
  marginRight: -20,
  paddingLeft: 20,
  paddingRight: 20,
  marginTop: 40,

  [CONDITION_DESKTOP]: {
    gridTemplateColumns: 'repeat(2, 1fr)',
    gridRowGap: 80,
    gridColumnGap: 100,
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginTop: 80,
  },
});

type SalariesProps = {
  title: string;
  items: ItemType[];
};

export const Attract = memo<SalariesProps>(({ title, items }) => (
  <StyledWrapper>
    <Container>
      <StyledTitle>{title}</StyledTitle>

      <StyledContent>
        {items.map((item, index) => (
          <AnimatedInit delayDesktop={(index % 2) * 0.3} delayMobile={index * 0.3} key={index}>
            <Item item={item} />
          </AnimatedInit>
        ))}
      </StyledContent>
    </Container>
  </StyledWrapper>
));

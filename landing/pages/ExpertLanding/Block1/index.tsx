import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { DESKTOP, CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { Button } from '../../../components/Button';
import { Ruble } from '../../../components/Ruble';
import { VideoButton } from '../../../components/VideoButton';

import { Phone } from './Phone';
import { useRouter } from 'next/router';
import { routes } from 'common/routes';
import { useSelector } from 'react-redux';
import { actions, selectors } from 'store/ducks';
import { useAppDispatch } from 'hooks/redux';

const StyledWrapper = styled.div({
  paddingTop: 40,
  backgroundImage: 'linear-gradient(to right,#862fff 10%,#9c3efe 40%,#AE4CFE 90%)',

  [CONDITION_DESKTOP]: {
    minWidth: DESKTOP,
  },
});

const StyledContent = styled.div({
  [CONDITION_DESKTOP]: {
    display: 'flex',
  },
});

const StyledTexts = styled.div({
  [CONDITION_DESKTOP]: {
    paddingTop: 40,
    width: 626,
  },
});

const StyledTitle = styled.div({
  fontSize: 27,
  fontWeight: 'bold',
  color: '#ffffff',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    maxWidth: 550,
    fontSize: 36,
    lineHeight: 1.25,
  },
});

const StyledDescription = styled.div({
  fontSize: 19,
  lineHeight: 1.47,
  fontWeight: 500,
  color: '#ffffff',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    maxWidth: 510,
  },
});

const StyledItem = styled.div({
  position: 'relative',
  paddingLeft: 35,

  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#ffffff',

  '& + &': {
    marginTop: 15,
  },

  '&::before': {
    position: 'absolute',
    top: 0,
    left: 0,
    content: '""',
    width: 20,
    height: 20,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: 'url(/landing/check-green.svg)',
  },
});

const StyledButtonsWrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: 40,

  [CONDITION_DESKTOP]: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 60,
  },
});

const StyledButtonWrapper = styled.div({
  '& + &': {
    marginTop: 40,
  },

  [CONDITION_DESKTOP]: {
    '& + &': {
      marginTop: 0,
      marginLeft: 40,
    },
  },
});

const StylePhoneWrapper = styled.div({
  marginTop: 40,
  display: 'flex',
  justifyContent: 'center',

  [CONDITION_DESKTOP]: {
    marginTop: 0,
    marginLeft: 90,
    marginRight: 0,
  },
});

const title = (
  <>
    Делитесь опытом и получайте от 150 до 5000 <Ruble fontWeight={700} />
    /час
  </>
);
const description =
  'Consulty — сервис платных онлайн-консультаций, где пользователи обращаются к экспертам за их знаниями, чтобы решить свою проблему.';

export const Block1: FC = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const isAuth = useSelector(selectors.profile.selectIsAuthentication);

  const handleNavigateStandExpert = () => {
    if (isAuth) {
      router.push({ pathname: routes.profile, query: { activeTab: 'expertProfile' } });
    } else {
      dispatch(actions.app.showAuthModal());
    }
  };

  return (
    <StyledWrapper>
      <Container>
        <StyledContent>
          <StyledTexts>
            <StyledTitle>{title}</StyledTitle>

            <StyledDescription>{description}</StyledDescription>

            <div>
              <StyledItem>Оказывайте платные консультации</StyledItem>

              <StyledItem>Формируйте репутацию эксперта</StyledItem>

              <StyledItem>Привлекайте новых клиентов на ваш основной продукт или услугу</StyledItem>
            </div>
            <StyledButtonsWrapper>
              <StyledButtonWrapper>
                <Button type="button" $color="white" $size="big" onClick={handleNavigateStandExpert}>
                  Стать экспертом в Consulty
                </Button>
              </StyledButtonWrapper>
              <StyledButtonWrapper>
                <VideoButton url="https://www.youtube.com/embed/XNVnyAPjppw">
                  <div>Посмотрите,как круто</div>
                  <div>быть экспертом</div>
                </VideoButton>
              </StyledButtonWrapper>
            </StyledButtonsWrapper>
          </StyledTexts>
          <StylePhoneWrapper>
            <Phone
              slides={[
                '/landing/expert1/cons2-2.png',
                '/landing/expert1/cons2-3.png',
                '/landing/expert1/cons2-4.png',
                '/landing/expert1/cons2-5.png',
                '/landing/expert1/cons2-6.png',
                '/landing/expert1/cons2-7.png',
              ]}
            />
          </StylePhoneWrapper>
        </StyledContent>
      </Container>
    </StyledWrapper>
  );
};

import React, { useEffect, useState, useRef, useCallback } from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../../constants/breakpoints';
import { containImageStyle } from '../../../../utils/containImageStyle';
import { ProgressButton } from '../../../../components/ProgressButton';

const StyledWrapper = styled.div({
  position: 'relative',
  width: 280,
  height: 448,
  ...containImageStyle('/landing/landing/i-phone.png'),

  [CONDITION_DESKTOP]: {
    width: 395,
    height: 610,
  },
});

const StyledMessagesWrapper = styled.div({
  position: 'relative',
  marginTop: 58,
  marginLeft: 18,
  marginRight: 18,

  [CONDITION_DESKTOP]: {
    marginTop: 90,
    marginLeft: 30,
    marginRight: 30,
  },
});

type StyledMessagesProps = {
  $isCurrent?: boolean;
};

const StyledMessages = styled.img<StyledMessagesProps>(({ $isCurrent }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  opacity: $isCurrent ? 1 : 0,
  transition: 'opacity 0.8s',
}));

const StyledActionsWrapper = styled.div({
  position: 'absolute',
  bottom: 11,
  left: 0,
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',

  [CONDITION_DESKTOP]: {
    bottom: 20,
  },
});

const StyledButtonText = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 'normal',
  color: '#232832',
  marginTop: 5,
  opacity: 0.3,

  [CONDITION_DESKTOP]: {
    marginTop: 8,
  },
});

type PhoneProps = {
  slides: string[];
};

export const Phone: FC<PhoneProps> = ({ slides }) => {
  const setNextTimeoutRef = useRef<NodeJS.Timeout | null>(null);
  const [currentSlide, setCurrentSlide] = useState(0);

  const setNextSlide = () => {
    if (setNextTimeoutRef.current) {
      clearTimeout(setNextTimeoutRef.current);
    }

    setCurrentSlide((prevCurrentSlide) => {
      if (prevCurrentSlide < slides.length - 1) {
        return prevCurrentSlide + 1;
      }

      return 0;
    });

    setNextTimeoutRef.current = setTimeout(() => {
      setNextSlide();
    }, 3000);
  };

  useEffect(() => {
    setNextTimeoutRef.current = setTimeout(() => {
      setNextSlide();
    }, 3000);

    return () => {
      if (setNextTimeoutRef.current) {
        clearTimeout(setNextTimeoutRef.current);
      }
    };
  });

  const onMouseEnter = useCallback(() => {
    if (setNextTimeoutRef.current) {
      clearTimeout(setNextTimeoutRef.current);
    }
  }, []);

  const onMouseLeave = useCallback(() => {
    setNextTimeoutRef.current = setTimeout(() => {
      setNextSlide();
    }, 3000);
  }, []);

  const nextSlide = currentSlide === slides.length - 1 ? 0 : currentSlide + 1;

  const prevSlide = currentSlide === 0 ? slides.length - 1 : currentSlide - 1;

  return (
    <StyledWrapper
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onTouchStart={onMouseEnter}
      onTouchEnd={onMouseLeave}
    >
      <StyledMessagesWrapper>
        {[
          <StyledMessages src={slides[prevSlide]} alt="" key={prevSlide} />,

          <StyledMessages $isCurrent src={slides[currentSlide]} alt="" key={currentSlide} />,

          <StyledMessages src={slides[nextSlide]} alt="" key={nextSlide} />,
        ]}
      </StyledMessagesWrapper>

      <StyledActionsWrapper>
        <div>
          <ProgressButton currentSlide={currentSlide} slidesLength={slides.length} onClick={setNextSlide}>
            Ещё пример
          </ProgressButton>
        </div>

        <StyledButtonText>
          {currentSlide + 1} из {slides.length}
        </StyledButtonText>
      </StyledActionsWrapper>
    </StyledWrapper>
  );
};

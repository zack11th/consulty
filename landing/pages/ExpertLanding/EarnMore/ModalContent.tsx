import React, { memo } from 'react';
import styled from 'styled-components';

import { Ruble } from '../../../components/Ruble';

const StyledWrapper = styled.div({
  padding: '30px 20px',
});

const StyledTitle = styled.div({
  fontSize: 19,
  fontWeight: 'bold',
  lineHeight: 1.05,
  textAlign: 'center',
  color: '#232832',
  marginBottom: 30,
});

const StyledBlock = styled.div({
  '& + &': {
    marginTop: 30,
  },
});

const StyledBlockTitle = styled.div({
  fontSize: 19,
  fontWeight: 'bold',
  lineHeight: 1.05,
  color: '#232832',
  marginBottom: 7,
});

const StyledPlus = styled.span({
  color: '#8b33ff',
});

const StyledBlockText = styled.div({
  fontSize: 12,
  fontWeight: 500,
  lineHeight: 1.25,
  color: '#232832',
});

export const ModalContent = memo(() => (
  <StyledWrapper>
    <StyledTitle>Условия и выплаты по партнерской программе</StyledTitle>

    <StyledBlock>
      <StyledBlockTitle>
        <StyledPlus>+</StyledPlus>
        150 <Ruble fontWeight={700} />
      </StyledBlockTitle>

      <StyledBlockText>За каждого пользователя, оплатившего 2 и более консультации</StyledBlockText>
    </StyledBlock>

    <StyledBlock>
      <StyledBlockTitle>
        <StyledPlus>+</StyledPlus>
        500 <Ruble fontWeight={700} />
      </StyledBlockTitle>

      <StyledBlockText>За экспертов, которые окажут в сервисе 20 и более консультаций</StyledBlockText>
    </StyledBlock>

    <StyledBlock>
      <StyledBlockTitle>
        <StyledPlus>+</StyledPlus>
        2.5%
      </StyledBlockTitle>

      <StyledBlockText>
        От суммы оказанных консультаций привлеченных вами экспертами. В течение полугода.
      </StyledBlockText>
    </StyledBlock>
  </StyledWrapper>
));

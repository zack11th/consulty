import React, { memo, useState, useCallback } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { containImageStyle } from '../../../utils/containImageStyle';
/* import {
  Button,
} from '../../../components/Button'; */
import { Container } from '../../../components/Container';
import { Modal } from '../../../components/Modal';

import { ModalContent } from './ModalContent';
import { Price } from './Price';

const StyledWrapper = styled.div({
  marginTop: 90,
  marginBottom: 90,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 120,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  lineHeight: 1.2,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#111111',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    lineHeight: 1.2,
    marginBottom: 70,
  },
});

const StyledContent = styled.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',

  [CONDITION_DESKTOP]: {
    alignItems: 'flex-start',
    position: 'relative',
    paddingRight: 671,
    minHeight: 614,

    '&::before': {
      content: '""',
      position: 'absolute',
      top: 10,
      right: 0,
      width: 629,
      height: 594,
      ...containImageStyle('/landing/mobile-marketing-rafiki.png'),
    },
  },
});

const StyledDescription = styled.div({
  opacity: 0.9,

  fontSize: 14,
  lineHeight: 1.43,
  fontWeight: 500,
  textAlign: 'center',
  color: '#222d39',
  marginBottom: 40,

  [CONDITION_DESKTOP]: {
    fontSize: 21,
    lineHeight: 1.67,
    textAlign: 'left',
  },
});

const StyledPrices = styled.div({
  display: 'grid',
  gridTemplateColumns: 'repeat(1, 1fr)',
  gridGap: 15,
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    gridTemplateColumns: 'repeat(2, 1fr)',
    gridGap: 20,
  },
});

const StyledModalButtonWrapper = styled.div({
  textAlign: 'center',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    textAlign: 'left',
  },
});

type StyledIconBlockProps = {
  $icon: string;
};

const StyledIconBlock = styled.div<StyledIconBlockProps>(({ $icon }) => ({
  position: 'relative',
  paddingLeft: 40,

  fontSize: 12,
  lineHeight: 1.25,
  fontWeight: 500,
  color: '#232832',
  minHeight: 30,

  '& + &': {
    marginTop: 15,
  },

  '&::before': {
    content: '""',
    position: 'absolute',
    top: 0,
    left: 0,
    width: 30,
    height: 30,
    ...containImageStyle($icon),
  },

  [CONDITION_DESKTOP]: {
    paddingLeft: 50,
    fontSize: 14,
    lineHeight: 1.43,
  },
}));

const StyledModalButton = styled.button({
  margin: 0,
  padding: 0,
  outline: 'none',
  cursor: 'pointer',
  background: 'transparent',

  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.2,
  color: '#8b33ff',
  borderWidth: '0 0 1px',
  borderStyle: 'dotted',
  borderColor: '#8b33ff',
});

/* const StyledDetailButtonWrapper = styled.div({
  marginTop: 40,

  [CONDITION_DESKTOP]: {
    marginTop: 47,
  },
});

const StyledDesktopText = styled.span({
  display: 'none',

  [CONDITION_DESKTOP]: {
    display: 'inline',
  },
}); */

const title = 'Зарабатывайте больше с партнерской программой';
const description = 'Расскажите о Consulty в социальных сетях, на своем канале или в сообществах и заработайте:';

export const EarnMore = memo(() => {
  const [isOpen, setIsOpen] = useState(false);

  const openModal = useCallback(() => {
    // ym(66496642, 'reachGoal', 'Landing_ex-earn_more-see_conditions');
    setIsOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <StyledWrapper>
      <Container>
        <StyledTitle>{title}</StyledTitle>

        <StyledContent>
          <StyledDescription>{description}</StyledDescription>

          <StyledPrices>
            <Price
              price="10 000"
              description="Можно заработать, пригласив знакомых. Достаточно привлечь 20 пользователей и несколько активных экспертов."
            />

            <Price
              price="80 000"
              description="Можно заработать на блоге или канале с аудиторией в 30 000. Достаточно привлечь 150 пользователей и 25 экспертов."
            />
          </StyledPrices>

          <StyledModalButtonWrapper>
            <StyledModalButton type="button" onClick={openModal}>
              Смотреть условия и выплаты
            </StyledModalButton>
          </StyledModalButtonWrapper>

          <div>
            <StyledIconBlock $icon="/landing/bar-chart-1-f-4-ca.png">
              Статистика и детали партнерской программы всегда доступны в личном кабинете
            </StyledIconBlock>

            <StyledIconBlock $icon="/landing/credit-card-1-f-4-b-3.png">
              Деньги можно выводить сразу после выполнения условий рефералом!
            </StyledIconBlock>
          </div>

          {/* <StyledDetailButtonWrapper>
            <Button
              $bordered
              onClikc={() => {
                ym(66496642, 'reachGoal', 'Landing_ex-earn_more-more_info');
              }}
            >
              Подробнее
              <StyledDesktopText>
                {' '}
                о партнерской программе
              </StyledDesktopText>
            </Button>
          </StyledDetailButtonWrapper> */}
        </StyledContent>
      </Container>

      <Modal show={isOpen} onHide={closeModal}>
        <ModalContent />
      </Modal>
    </StyledWrapper>
  );
});

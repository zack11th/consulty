import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { Ruble } from '../../../components/Ruble';

const StyledWrapper = styled.div({
  borderRadius: 12,
  boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
  backgroundColor: '#ffffff',
  padding: 28,
});

const StyledPrice = styled.div({
  fontSize: 32,
  fontWeight: 'bold',
  color: '#8b33ff',
  marginBottom: 10,
});

const StyledDescription = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.54,
  color: '#232832',
});

type PriceProps = {
  price: string;
  description: string;
};

export const Price: FC<PriceProps> = ({ price, description }) => (
  <StyledWrapper>
    <StyledPrice>
      {price} <Ruble fontWeight={700} />
    </StyledPrice>

    <StyledDescription>{description}</StyledDescription>
  </StyledWrapper>
);

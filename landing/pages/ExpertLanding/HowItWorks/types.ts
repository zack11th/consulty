import type {
  ReactNode,
} from 'react';

export type ItemType = {
  title: string;
  description: ReactNode;
  imageDesktop: string;
  imageMobile: string;
};

import React, { memo, useMemo } from 'react';
import styled from 'styled-components';
import { useIsomorphic } from 'react-redux-isomorphic';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { Badge } from '../../../components/Badge';

import { TitleBlock } from './TitleBlock';
import { ModalItem } from './ModalItem';
import { ModalSubItem } from './ModalSubItem';
import { Icon } from './Icon';

import type { CategoryWithChildrenType } from './types';

const StyledWrapper = styled.div({
  marginTop: 60,
  marginBottom: 70,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 70,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  lineHeight: 1.2,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#111111',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    lineHeight: 1.2,
  },
});

const StyledDescription = styled.div({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: 1.43,
  textAlign: 'center',
  color: '#222d39',
  marginBottom: 55,

  [CONDITION_DESKTOP]: {
    fontSize: 21,
    lineHeight: 1.67,
    marginBottom: 75,
    maxWidth: 942,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

const StyledContent = styled.div({
  position: 'relative',
  paddingTop: 30,
  paddingLeft: 30,
  paddingRight: 30,
  paddingBottom: 40,
  borderRadius: 12,
  boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
  border: 'solid 1px #e5e7eb',
  backgroundColor: '#ffffff',

  [CONDITION_DESKTOP]: {
    display: 'flex',
    paddingTop: 50,
    paddingLeft: 65,
    paddingRight: 35,
    paddingBottom: 60,
  },
});

const StyledBadgeWrapper = styled.div({
  position: 'absolute',
  top: -15,
  left: 0,
  width: '100%',
  display: 'flex',
  justifyContent: 'center',

  [CONDITION_DESKTOP]: {
    left: 50,
    width: 'auto',
  },
});

const StyledItemsWrapper = styled.div({
  marginBottom: 35,

  [CONDITION_DESKTOP]: {
    marginBottom: 0,
    width: 230,
    paddingRight: 40,
  },
});

const StyledItemsSetWrapper = styled.div({
  marginBottom: 32,

  [CONDITION_DESKTOP]: {
    marginBottom: 0,
    width: 240,
    paddingRight: 40,
  },
});

const StyledItemsSetTitle = styled.div({
  fontSize: 18,
  fontWeight: 'bold',
  lineHeight: 1.11,
  color: '#2a0065',
  marginBottom: 16,
});

type ModalItemsProps = {
  title: string;
  description: string;
  contentDescription: string;
  badge: string[];
};

export const ModalItems = memo<ModalItemsProps>(({ title, description, contentDescription, badge }) => {
  // const { context: categories } = useIsomorphic<LoadParams, CategoryWithChildrenType[]>(
  //   'expertLanding/modalItems',

  //   async ({ get }) => {
  //     const response = [];

  //     return response;
  //   },
  // );

  const categories = [] as any[];

  const [items, rightBlockRoot] = useMemo<[CategoryWithChildrenType[], CategoryWithChildrenType]>(() => {
    if (!categories || categories.length === 0) {
      return [[], null];
    }

    const { children } = categories[0];

    if (children.length === 0) {
      return [[], null];
    }

    if (children.length === 1) {
      return [children, null];
    }

    const itemsRes = children.slice();
    const rightBlockRootRes = itemsRes.pop();

    return [itemsRes, rightBlockRootRes];
  }, [categories]);

  if (!categories || categories.length === 0) {
    return null;
  }

  return (
    <StyledWrapper>
      <Container>
        <StyledTitle>{title}</StyledTitle>

        <StyledDescription>{description}</StyledDescription>

        <StyledContent>
          <StyledBadgeWrapper>
            <Badge items={badge} />
          </StyledBadgeWrapper>

          <TitleBlock contentTitle={categories[0].category.name} contentDescription={contentDescription} />

          <StyledItemsWrapper>
            {items.map((modalItem) => (
              <ModalItem modalItem={modalItem} key={modalItem.category.id} />
            ))}
          </StyledItemsWrapper>

          <StyledItemsSetWrapper>
            {rightBlockRoot && (
              <>
                <StyledItemsSetTitle>{rightBlockRoot.category.name}</StyledItemsSetTitle>

                <div>
                  {rightBlockRoot.children?.map((modalItem) => (
                    <ModalSubItem modalItem={modalItem} key={modalItem.category.id} />
                  ))}
                </div>
              </>
            )}
          </StyledItemsSetWrapper>

          <Icon />
        </StyledContent>
      </Container>
    </StyledWrapper>
  );
});

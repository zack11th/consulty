import React, { memo, useState, useCallback } from 'react';
import styled from 'styled-components';

import { ModalItemModal } from './ModalItemModal';

import type { CategoryWithChildrenType } from './types';

const StyledWrapper = styled.div({
  '& + &': {
    marginTop: 10,
  },
});

const StyledDash = styled.span({
  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#2a0065',
});

const StyledButton = styled.button({
  display: 'inline',
  textAlign: 'left',
  margin: 0,
  padding: 0,
  outline: 'none',
  cursor: 'pointer',
  background: 'transparent',

  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#2a0065',
  borderWidth: '0 0 1px',
  borderStyle: 'dotted',
  borderColor: '#8b33ff',
});

type ModalSubItemProps = {
  modalItem: CategoryWithChildrenType;
};

export const ModalSubItem = memo<ModalSubItemProps>(({ modalItem }) => {
  const {
    category: { name },
  } = modalItem;

  const [isOpen, setIsOpen] = useState(false);

  const onClick = useCallback(() => {
    setIsOpen(true);
  }, []);

  const onHide = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <StyledWrapper>
      <StyledDash>{'- '}</StyledDash>

      <StyledButton type="button" onClick={onClick}>
        {name}
      </StyledButton>

      <ModalItemModal show={isOpen} onHide={onHide} modalItem={modalItem} />
    </StyledWrapper>
  );
});

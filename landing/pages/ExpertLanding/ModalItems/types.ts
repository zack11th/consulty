export type CategoryType = {
  id: number;
  name: string;
};

export type CategoryWithChildrenType = {
  category: CategoryType;
  children?: CategoryWithChildrenType[];
};

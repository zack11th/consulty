import React, { memo, useState, useCallback } from 'react';
import styled from 'styled-components';

import { ModalItemModal } from './ModalItemModal';

import type { CategoryWithChildrenType } from './types';

const StyledWrapper = styled.div({
  '& + &': {
    marginTop: 30,
  },
});

const StyledButton = styled.button({
  textAlign: 'left',
  margin: 0,
  padding: 0,
  outline: 'none',
  cursor: 'pointer',
  background: 'transparent',

  fontSize: 18,
  fontWeight: 'bold',
  lineHeight: 1.11,
  color: '#2a0065',
  borderWidth: '0 0 1px',
  borderStyle: 'dotted',
  borderColor: '#8b33ff',
});

type ModalItemProps = {
  modalItem: CategoryWithChildrenType;
};

export const ModalItem = memo<ModalItemProps>(({ modalItem }) => {
  const {
    category: { name },
  } = modalItem;

  const [isOpen, setIsOpen] = useState(false);

  const onClick = useCallback(() => {
    setIsOpen(true);
  }, []);

  const onHide = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <StyledWrapper>
      <StyledButton type="button" onClick={onClick}>
        {name}
      </StyledButton>

      <ModalItemModal show={isOpen} onHide={onHide} modalItem={modalItem} />
    </StyledWrapper>
  );
});

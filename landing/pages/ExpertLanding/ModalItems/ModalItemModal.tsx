import React, { memo } from 'react';
import styled from 'styled-components';

import { Modal } from '../../../components/Modal';

import type { CategoryWithChildrenType } from './types';

const StyledModalContent = styled.div({
  paddingTop: 30,
  paddingLeft: 30,
  paddingRight: 30,
  paddingBottom: 40,
});

const StyledModalTitle = styled.div({
  fontSize: 19,
  fontWeight: 'bold',
  lineHeight: 1.05,
  color: '#232832',

  marginBottom: 17,
});

const StyledModalItem = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.15,
  color: '#232832',

  '& + &': {
    marginTop: 10,
  },
});

type ModalItemModalProps = {
  show: boolean;
  onHide: () => void;
  modalItem: CategoryWithChildrenType;
};

export const ModalItemModal = memo<ModalItemModalProps>(
  ({
    show,
    onHide,

    modalItem: {
      category: { name },
      children,
    },
  }) => (
    <Modal show={show} onHide={onHide}>
      <StyledModalContent>
        <StyledModalTitle>{name}</StyledModalTitle>

        {children && children.length > 0 && (
          <div>
            {children.map(({ category: { id, name: categoryName } }) => (
              <StyledModalItem key={id}>{categoryName}</StyledModalItem>
            ))}
          </div>
        )}
      </StyledModalContent>
    </Modal>
  ),
);

import React, { memo } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';

const StyledWrapper = styled.div({
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    width: 125,
    paddingRight: 104,
    marginBottom: 0,
  },
});

const StyledTitle = styled.div({
  fontSize: 22,
  fontWeight: 'bold',
  lineHeight: 1.82,
  color: '#2a0065',

  [CONDITION_DESKTOP]: {
    marginBottom: 6,
  },
});

const StyledDescription = styled.div({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: 1.43,
  color: '#8b33ff',
});

type TitleBlockProps = {
  contentTitle: string;
  contentDescription: string;
};

export const TitleBlock = memo<TitleBlockProps>(({ contentTitle, contentDescription }) => (
  <StyledWrapper>
    <StyledTitle>{contentTitle}</StyledTitle>

    <StyledDescription>{contentDescription}</StyledDescription>
  </StyledWrapper>
));

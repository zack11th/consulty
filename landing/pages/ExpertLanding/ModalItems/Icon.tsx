import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { containImageStyle } from '../../../utils/containImageStyle';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  width: 250,
  height: 250,
  paddingTop: 27,
  paddingLeft: 15,
  paddingRight: 15,
  borderRadius: '50%',
  backgroundImage: 'linear-gradient(to right,#862fff 10%,#9c3efe 40%,#AE4CFE 90%)',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const StyledIcon = styled.div({
  width: 54,
  height: 54,
  marginBottom: 11,
  ...containImageStyle('/landing/banners/popular-rocket.png'),
});

const StyledTitle = styled.div({
  opacity: 0.9,

  fontSize: 17,
  fontWeight: 'bold',
  lineHeight: 1.18,
  textAlign: 'center',
  color: '#ffffff',
  marginBottom: 6,
});

const StyledDescription = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.46,
  textAlign: 'center',
  color: '#ffffff',
});

export const Icon: FC = () => (
  <StyledWrapper>
    <StyledIcon />

    <StyledTitle>
      <div>Запуск сервиса —</div>

      <div>в октябре!</div>
    </StyledTitle>

    <StyledDescription>Зарегистрируйтесь уже сейчас и станьте одним из первых экспертов Consulty.</StyledDescription>
  </StyledWrapper>
);

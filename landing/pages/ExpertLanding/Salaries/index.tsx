import React, { memo } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { AnimatedInit } from '../../../components/AnimatedInit';

import { Salary } from './Salary';

import type { ItemType } from './types';

const StyledWrapper = styled.div({
  marginTop: 90,
  marginBottom: 90,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 120,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  lineHeight: 1.2,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#111111',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    lineHeight: 1.2,
  },
});

const StyledDescription = styled.div({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: 1.43,
  textAlign: 'center',
  color: '#222d39',

  [CONDITION_DESKTOP]: {
    fontSize: 21,
    lineHeight: 1.67,
    marginBottom: 75,
    maxWidth: 942,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});

const StyledContent = styled.div({
  display: 'grid',
  gridTemplateColumns: 'repeat(3, 1fr)',
  gridGap: 10,
  paddingBottom: 20,
  overflowX: 'auto',
  overflowY: 'hidden',
  marginLeft: -20,
  marginRight: -20,
  paddingLeft: 20,
  paddingRight: 20,
  marginTop: 40,

  [CONDITION_DESKTOP]: {
    gridGap: 40,
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginTop: 60,
  },
});

type SalariesProps = {
  title: string;
  description: string;
  items: ItemType[];
};

export const Salaries = memo<SalariesProps>(({ title, description, items }) => (
  <StyledWrapper>
    <Container>
      <StyledTitle>{title}</StyledTitle>

      <StyledDescription>{description}</StyledDescription>

      <StyledContent>
        {items.map((salary, index) => (
          <AnimatedInit delayDesktop={index * 0.3} delayMobile={index * 0.3} key={index}>
            <Salary salary={salary} />
          </AnimatedInit>
        ))}
      </StyledContent>
    </Container>
  </StyledWrapper>
));

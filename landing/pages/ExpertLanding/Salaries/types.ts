import type {
  ReactNode,
} from 'react';

export type CalculationItemType = {
  prefix?: string;
  title: ReactNode;
  description: string;
};

export type ItemType = {
  icon: string;
  iconRetina: string;
  name: string;
  schedule: string;
  priceDescription: string;
  price: ReactNode;
  calculation: CalculationItemType[];
};

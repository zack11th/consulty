import React, { useState, useCallback } from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP, CONDITION_RETINA } from '../../../constants/breakpoints';
import { containImageStyle } from '../../../utils/containImageStyle';
import { Button } from '../../../components/Button';

import type { ItemType } from './types';
import { Modal } from '../../../components/Modal';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  minWidth: 280,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  borderRadius: 12,
  boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
  border: 'solid 1px #e5e7eb',
  backgroundColor: '#ffffff',
  paddingTop: 15,
  paddingBottom: 30,
  paddingLeft: 20,
  paddingRight: 20,

  [CONDITION_DESKTOP]: {
    paddingTop: 25,
    paddingBottom: 40,
    paddingLeft: 40,
    paddingRight: 40,
  },
});

type StyledIconProps = {
  $icon: string;
  $iconRetina: string;
};

const StyledIcon = styled.div<StyledIconProps>(({ $icon, $iconRetina }) => ({
  width: 60,
  height: 60,
  borderRadius: '50%',
  ...containImageStyle($icon),
  marginBottom: 10,

  [CONDITION_RETINA]: {
    backgroundImage: `url(${$iconRetina})`,
  },

  [CONDITION_DESKTOP]: {
    width: 90,
    height: 90,
    marginBottom: 20,
  },
}));

const StyledName = styled.div({
  height: 53,

  fontSize: 17,
  lineHeight: 1.18,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#232832',

  [CONDITION_DESKTOP]: {
    height: 80,
    fontSize: 22,
    lineHeight: 1.36,
  },
});

const StyledDescriptions = styled.div({
  fontSize: 15,
  lineHeight: 1.33,
  fontWeight: 500,
  textAlign: 'center',
  color: '#79818c',
  marginBottom: 17,

  [CONDITION_DESKTOP]: {
    lineHeight: 1.67,
    marginBottom: 25,
  },
});

const StyledSalary = styled.div({
  textAlign: 'center',
  fontSize: 32,
  fontWeight: 'bold',
  lineHeight: 1.41,
  color: '#8b33ff',
  marginBottom: 23,

  [CONDITION_DESKTOP]: {
    marginBottom: 33,
  },
});

const StyledModalContent = styled.div({
  paddingTop: 30,
  paddingLeft: 20,
  paddingRight: 20,
  paddingBottom: 40,
});

const StyledModalBlock = styled.div({
  '& + &': {
    marginTop: 30,
  },
});

const StyledModalBlockTitle = styled.div({
  fontSize: 19,
  lineHeight: 1.05,
  fontWeight: 'bold',
  color: '#232832',
  marginBottom: 7,
});

const StyledModalBlockTitlePrefix = styled.span({
  color: '#8b33ff',
});

const StyledModalBlockDescription = styled.span({
  fontSize: 12,
  fontWeight: 500,
  lineHeight: 1.25,
  color: '#232832',
});

type SalaryProps = {
  salary: ItemType;
};

export const Salary: FC<SalaryProps> = ({
  salary: { icon, iconRetina, name, schedule, priceDescription, price, calculation },
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const openModal = useCallback(() => {
    setIsOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <StyledWrapper>
      <StyledIcon $icon={icon} $iconRetina={iconRetina} />

      <StyledName>{name}</StyledName>

      <StyledDescriptions>
        <div>{schedule}</div>

        <div>{priceDescription}</div>
      </StyledDescriptions>

      <StyledSalary>{price}</StyledSalary>

      <Button $bordered type="button" onClick={openModal}>
        Показать расчет
      </Button>

      <Modal show={isOpen} onHide={closeModal}>
        <StyledModalContent>
          {calculation.map(({ prefix, title, description }, index) => (
            <StyledModalBlock key={index}>
              <StyledModalBlockTitle>
                {prefix && <StyledModalBlockTitlePrefix>{prefix} </StyledModalBlockTitlePrefix>}

                {title}
              </StyledModalBlockTitle>

              <StyledModalBlockDescription>{description}</StyledModalBlockDescription>
            </StyledModalBlock>
          ))}
        </StyledModalContent>
      </Modal>
    </StyledWrapper>
  );
};

import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';

import type { ItemType } from './types';

const StyledWrapper = styled.div({
  display: 'flex',
  alignItems: 'flex-start',
});

const StyledIconWrapper = styled.div({
  paddingRight: 10,

  [CONDITION_DESKTOP]: {
    paddingRight: 25,
  },
});

const StyledContent = styled.div({
  flex: 1,
});

const StyledIcon = styled.img({
  width: 40,
  height: 40,
  borderRadius: 30,

  [CONDITION_DESKTOP]: {
    width: 70,
    height: 70,
    borderRadius: 5,
  },
});

const StyledTitle = styled.div({
  fontSize: 15,
  fontWeight: 'bold',
  lineHeight: 1.33,
  color: '#222d39',
  marginBottom: 5,

  [CONDITION_DESKTOP]: {
    fontSize: 21,
    lineHeight: 1.19,
    marginBottom: 10,
  },
});

const StyledDescription = styled.div({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: 1.43,
  color: '#222d39',

  [CONDITION_DESKTOP]: {
    fontSize: 15,
    lineHeight: 1.33,
  },
});

type ItemProps = {
  item: ItemType;
};

export const Item: FC<ItemProps> = ({ item: { icon, iconRetina, title, description } }) => (
  <StyledWrapper>
    <StyledIconWrapper>
      <StyledIcon src={icon} srcSet={`${iconRetina} 2x`} alt="" />
    </StyledIconWrapper>

    <StyledContent>
      <StyledTitle>{title}</StyledTitle>

      <StyledDescription>{description}</StyledDescription>
    </StyledContent>
  </StyledWrapper>
);

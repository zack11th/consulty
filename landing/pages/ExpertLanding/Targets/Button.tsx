import React, { memo } from 'react';
import type { ReactNode } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';

type StyledButtonProps = {
  $checked: boolean;
};

const StyledButton = styled.button<StyledButtonProps>(({ $checked }) => ({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: '20px',
  textAlign: 'center',
  color: $checked ? '#ffffff' : '#8b33ff',
  paddingTop: 10,
  paddingBottom: 10,
  paddingLeft: 20,
  paddingRight: 20,
  borderRadius: 20,
  backgroundColor: $checked ? '#8b33ff' : '#f7f6ff',
  outline: 'none',
  cursor: 'pointer',
  marginBottom: 15,
  marginLeft: 10,
  marginRight: 10,
  borderWidth: 0,
  transition: 'all 0.8s',

  [CONDITION_DESKTOP]: {
    borderRadius: 30,
    marginBottom: 20,
    fontSize: 19,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30,
  },
}));

type ButtonProps = {
  id: string;
  index: number;
  activeIndex: number;
  setActive: (index: number) => void;
  children: ReactNode;
};

export const Button = memo<ButtonProps>(({ id, index, activeIndex, setActive, children }) => {
  const isChecked = index === activeIndex;
  const onClick = () => {
    if (!isChecked) {
      // history.replace(`${location.pathname}?target=${id}`);
      setActive(index);
    }
  };

  return (
    <StyledButton type="button" $checked={isChecked} onClick={onClick}>
      {children}
    </StyledButton>
  );
});

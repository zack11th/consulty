import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { containImageStyle } from '../../../utils/containImageStyle';
import { AnimatedInit } from '../../../components/AnimatedInit';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  position: 'relative',
  paddingTop: 30,
  paddingBottom: 40,
  paddingLeft: 20,
  paddingRight: 20,

  [CONDITION_DESKTOP]: {
    minHeight: 565,
    paddingTop: 60,
    paddingBottom: 60,
    paddingLeft: 60,
    paddingRight: 125,
  },
});

const StyledImageWrapper = styled.div({
  position: 'absolute',
  zIndex: 0,
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
});

const StyledBackgroundMobile = styled.img({
  [CONDITION_DESKTOP]: {
    display: 'none',
  },
});

const StyledBackgroundDesktop = styled.img({
  display: 'none',

  [CONDITION_DESKTOP]: {
    display: 'block',
  },
});

const StyledItem = styled.div({
  position: 'relative',
  paddingLeft: 35,

  fontSize: 15,
  lineHeight: 1.33,
  fontWeight: 500,
  color: '#ffffff',

  '&::before': {
    position: 'absolute',
    top: 0,
    left: 0,
    content: '""',
    width: 20,
    height: 20,
    ...containImageStyle('/landing/check-green.svg'),
  },

  [CONDITION_DESKTOP]: {
    fontSize: 18,
    lineHeight: 1.39,
    paddingLeft: 45,
  },
});

const StyledAnimatedInit = styled(AnimatedInit)({
  '& + &': {
    marginTop: 25,
  },

  [CONDITION_DESKTOP]: {
    '& + &': {
      marginTop: 40,
    },
  },
});

type LeftBlockProps = {
  activeIndex: number;
  items: string[];
};

export const LeftBlock: FC<LeftBlockProps> = ({ activeIndex, items }) => (
  <StyledWrapper>
    <StyledImageWrapper>
      <StyledBackgroundMobile src="/landing/mask1-mobile.png" alt="" width="100%" height="100%" />

      <StyledBackgroundDesktop src="/landing/mask1-desktop.png" alt="" width="100%" height="100%" />
    </StyledImageWrapper>

    {items.map((item, index) => (
      <StyledAnimatedInit delayDesktop={index * 0.1} delayMobile={index * 0.1} key={`${activeIndex}/${index}`}>
        <StyledItem>{item}</StyledItem>
      </StyledAnimatedInit>
    ))}
  </StyledWrapper>
);

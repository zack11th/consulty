export type ItemType = {
  icon: string;
  iconRetina: string;
  title: string;
  description: string;
};

export type ItemsSetType = {
  id: string;
  buttonText: string;
  leftBlock: string[];
  rightBlock: ItemType[];
};

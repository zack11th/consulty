import React, { memo, useState } from 'react';
import styled from 'styled-components';
import qs from 'qs';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { AnimatedInit } from '../../../components/AnimatedInit';

import { Button } from './Button';
import { LeftBlock } from './LeftBlock';
import { Item } from './Item';
import type { ItemsSetType } from './types';

const StyledWrapper = styled.div({
  marginTop: 60,
  marginBottom: 60,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 120,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  fontWeight: 'bold',
  lineHeight: 1.2,
  textAlign: 'center',
  color: '#111111',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    marginBottom: 60,
  },
});

const StyledButtons = styled.div({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  marginBottom: 15,

  [CONDITION_DESKTOP]: {
    marginBottom: 40,
  },
});

const StyledBlock = styled.div({
  [CONDITION_DESKTOP]: {
    display: 'flex',
  },
});

const StyledLeftBlockWrapper = styled.div({
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    width: 530,
    marginRight: 50,
    marginBottom: 0,
  },
});

const StyledRightBlockWrapper = styled.div<{
  $itemsCount: number;
}>(({ $itemsCount }) => ({
  flex: 1,

  [CONDITION_DESKTOP]: {
    paddingTop: $itemsCount > 3 ? 0 : 60,
    ...($itemsCount > 3
      ? {
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }
      : {}),
  },
}));

const StyledAnimatedInit = styled(AnimatedInit)<{
  $itemsCount: number;
}>(({ $itemsCount }) => ({
  '& + &': {
    marginTop: 30,
  },

  [CONDITION_DESKTOP]: {
    '& + &': {
      marginTop: $itemsCount > 3 ? 40 : 60,
    },
  },
}));

type TargetsProps = {
  title: string;
  items: ItemsSetType[];
};

export const Targets = memo<TargetsProps>(({ title, items }) => {
  const [activeIndex, setActiveIndex] = useState(() => {
    if (process.browser && window) {
      const { target } = qs.parse(location.search, {
        ignoreQueryPrefix: true,
      });

      if (!target) {
        return 0;
      }

      const res = items.findIndex(({ id }) => id === target);

      return res || 0;
    }
    return 0;
  });

  const { leftBlock, rightBlock } = items[activeIndex];

  return (
    <StyledWrapper>
      <Container>
        <StyledTitle>{title}</StyledTitle>

        <StyledButtons>
          {items.map(({ id, buttonText }, index) => (
            <Button id={id} activeIndex={activeIndex} index={index} setActive={setActiveIndex} key={id}>
              {buttonText}
            </Button>
          ))}
        </StyledButtons>

        <StyledBlock>
          <StyledLeftBlockWrapper>
            <LeftBlock activeIndex={activeIndex} items={leftBlock} />
          </StyledLeftBlockWrapper>

          <StyledRightBlockWrapper $itemsCount={rightBlock.length}>
            {rightBlock.map((item, index) => (
              <StyledAnimatedInit
                $itemsCount={rightBlock.length}
                delayDesktop={(leftBlock.length + index) * 0.1}
                delayMobile={(leftBlock.length + index) * 0.1}
                key={`${activeIndex}/${index}`}
              >
                <Item item={item} />
              </StyledAnimatedInit>
            ))}
          </StyledRightBlockWrapper>
        </StyledBlock>
      </Container>
    </StyledWrapper>
  );
});

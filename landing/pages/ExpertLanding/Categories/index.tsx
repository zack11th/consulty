import React, { memo, useMemo } from 'react';
import styled from 'styled-components';
import { useIsomorphic } from 'react-redux-isomorphic';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';
import { Badge } from '../../../components/Badge';
import { Notification } from './Notification';

const StyledWrapper = styled.div({
  marginTop: 70,
  marginBottom: 90,

  [CONDITION_DESKTOP]: {
    marginTop: 70,
    marginBottom: 60,
  },
});

const StyledContent = styled.div({
  position: 'relative',
  paddingTop: 40,
  paddingLeft: 30,
  paddingRight: 30,
  paddingBottom: 40,
  borderRadius: 12,
  boxShadow: '0 0 15px 0 rgba(0, 0, 0, 0.1)',
  border: 'solid 1px #e5e7eb',
  backgroundColor: '#ffffff',

  [CONDITION_DESKTOP]: {
    display: 'flex',
    alignItems: 'flex-start',
    paddingTop: 60,
    paddingLeft: 60,
    paddingRight: 50,
    paddingBottom: 60,
  },
});

const StyledBadgeWrapper = styled.div({
  position: 'absolute',
  top: -15,
  left: 0,
  width: '100%',
  display: 'flex',
  justifyContent: 'center',

  [CONDITION_DESKTOP]: {
    left: 50,
    width: 'auto',
  },
});

const StyledColumnWrapper = styled.div({
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    marginBottom: 0,
    flex: 1,
    paddingRight: 30,
  },
});

const StyledCategory = styled.div({
  fontSize: 17,
  lineHeight: '20px',
  fontWeight: 'bold',
  color: '#2a0065',

  '& + &': {
    marginTop: 15,
  },

  [CONDITION_DESKTOP]: {
    fontSize: 19,
    lineHeight: '24px',

    '& + &': {
      marginTop: 25,
    },
  },
});

type CategoriesProps = {
  badge: string[];
};

type CategoryType = {
  category: {
    id: number;
    name: string;
  };
};

export const Categories = memo<CategoriesProps>(({ badge }) => {
  // const { context: categories } = useIsomorphic<LoadParams, CategoryType[] | null>(
  //   'expertLanding/categories',

  //   async ({ get }) => {
  //     let res: CategoryType[] | null;

  //     try {
  //       res = [];
  //     } catch (e) {
  //       res = null;
  //     }

  //     return res;
  //   },
  // );

  const categories = [] as any[];

  const columns = useMemo<CategoryType[][] | null>(() => {
    if (!categories) {
      return null;
    }

    const breakpoint1 = Math.round(categories.length / 3);
    const breakpoint2 = 2 * breakpoint1;

    return [
      categories.slice(0, breakpoint1),
      categories.slice(breakpoint1, breakpoint2),
      categories.slice(breakpoint2, categories.length),
    ];
  }, [categories]);

  if (!columns) {
    return null;
  }

  return (
    <StyledWrapper>
      <Container>
        <StyledContent>
          <StyledBadgeWrapper>
            <Badge items={badge} />
          </StyledBadgeWrapper>

          {columns.map((column, columnIndex) => (
            <StyledColumnWrapper key={columnIndex}>
              {column.map(({ category: { id, name } }) => (
                <StyledCategory key={id}>{name}</StyledCategory>
              ))}
            </StyledColumnWrapper>
          ))}

          <Notification />
        </StyledContent>
      </Container>
    </StyledWrapper>
  );
});

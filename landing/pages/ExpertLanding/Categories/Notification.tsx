import React, { memo } from 'react';
import styled from 'styled-components';

import { containImageStyle } from '../../../utils/containImageStyle';
import { CONDITION_DESKTOP } from '../../../constants/breakpoints';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  backgroundColor: '#fff6c2',
  borderTopLeftRadius: 30,
  borderBottomLeftRadius: 30,
  borderTopRightRadius: 30,
  paddingTop: 23,
  paddingBottom: 27,
  paddingLeft: 20,
  paddingRight: 20,

  [CONDITION_DESKTOP]: {
    width: 290,
    paddingLeft: 28,
    paddingRight: 25,
  },
});

const StyledTitleBlock = styled.div({
  display: 'flex',
  marginBottom: 17,
});

const StyledTitle = styled.div({
  flex: 1,
  opacity: 0.9,

  fontSize: 17,
  fontWeight: 'bold',
  lineHeight: 1.18,
  color: '#222d39',
  marginRight: 22,

  [CONDITION_DESKTOP]: {
    marginRight: 50,
  },
});

const StyledIcon = styled.div({
  width: 37,
  height: 39,
  ...containImageStyle('/landing/ring.png'),
});

const StyledDescription = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.46,
  color: '#222d39',
});

const title = 'Оповестим о запуске темы';
const description =
  'Чтобы стать экспертом в этих темах, зарегистрируйтесь сейчас, отметьте их в своем профиле, и вам придет уведомление, когда эти темы будут запущены.';

export const Notification = memo(() => (
  <StyledWrapper>
    <StyledTitleBlock>
      <StyledTitle>{title}</StyledTitle>

      <StyledIcon />
    </StyledTitleBlock>

    <StyledDescription>{description}</StyledDescription>
  </StyledWrapper>
));

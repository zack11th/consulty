import React, { memo } from 'react';
import styled from 'styled-components';

import { containImageStyle } from '../../utils/containImageStyle';
import { CONDITION_DESKTOP, CONDITION_RETINA } from '../../constants/breakpoints';

type StyledIconBlockProps = {
  $icon: string;
  $iconRetina: string;
};

const StyledIconBlock = styled.div<StyledIconBlockProps>(({ $icon, $iconRetina }) => ({
  position: 'relative',
  paddingLeft: 45,

  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.33,
  color: '#222d39',
  marginTop: 30,

  '&::before': {
    content: '""',
    position: 'absolute',
    top: 0,
    left: 0,
    width: 30,
    height: 30,
    ...containImageStyle($icon),
  },

  [CONDITION_RETINA]: {
    '&::before': {
      backgroundImage: `url(${$iconRetina})`,
    },
  },
}));

const StyledDescriptionBlock = styled.div({
  padding: 20,
  borderRadius: 12,
  border: 'solid 1px #8b33ff',
  marginTop: 35,

  [CONDITION_DESKTOP]: {
    padding: 30,
  },
});

const StyledDescriptionTitle = styled.div({
  fontSize: 16,
  fontWeight: 'bold',
  lineHeight: 1.25,
  color: '#8b33ff',
  marginBottom: 10,
});

const StyledDescriptionText = styled.div({
  fontSize: 13,
  lineHeight: 1.54,
  fontWeight: 500,
  color: '#222d39',

  [CONDITION_DESKTOP]: {
    fontSize: 14,
    lineHeight: 1.43,
  },
});

const text = 'Когда пользователь закажет консультацию, вам придет уведомление в мобильном приложении или по SMS.';
const descriptionTitle = 'Больше отзывов – больше консультаций!';
const descriptionText =
  'Чтобы заработать первые отзывы, расскажите своей аудитории и знакомым, что вы теперь эксперт-консультант. Либо оказывайте консультации дешевле и быстрее других.';

export const HowItWorksConsultations = memo(() => (
  <>
    <div>{text}</div>

    <StyledIconBlock $icon="/landing/stopwatch-23-f-1.png" $iconRetina="/landing/stopwatch-23-f-1@2x.png">
      Консультация длится до решения вопроса, но не более 1 часа.
    </StyledIconBlock>

    <StyledIconBlock $icon="/landing/glowing-star-1-f-31-f.png" $iconRetina="/landing/glowing-star-1-f-31-f@2x.png">
      После консультации пользователь оставит отзыв.
    </StyledIconBlock>

    <StyledDescriptionBlock>
      <StyledDescriptionTitle>{descriptionTitle}</StyledDescriptionTitle>

      <StyledDescriptionText>{descriptionText}</StyledDescriptionText>
    </StyledDescriptionBlock>
  </>
));

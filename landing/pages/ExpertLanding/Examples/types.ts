export type ItemType = {
  title: string;
  image: string;
};

import React, { useRef, useState, useEffect, useCallback } from 'react';
import type { FC, ReactNode } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';

import { ProgressButton } from '../../../components/ProgressButton';

import { SlideText } from './SlideText';
import type { ItemType } from './types';

const StyledWrapper = styled.div({
  boxSizing: 'border-box',
  position: 'relative',
  borderWidth: 3,
  borderStyle: 'solid',
  borderColor: '#ffcc00',
  borderRadius: 24,
  paddingTop: 25,
  minHeight: 530,

  [CONDITION_DESKTOP]: {
    display: 'flex',
    alignItems: 'center',
    borderWidth: 5,
    paddingTop: 40,
    paddingLeft: 70,
    paddingRight: 40,
    paddingBottom: 40,
  },
});

const StyledInnerLabel = styled.div({
  fontSize: 19,
  fontWeight: 'bold',
  lineHeight: 1.05,
  textAlign: 'center',
  color: '#222d39',
  marginBottom: 5,

  [CONDITION_DESKTOP]: {
    marginBottom: 0,
    fontSize: 35,
    marginRight: 30,
  },
});

const StyledLabelsBlock = styled.div({
  marginBottom: 40,

  [CONDITION_DESKTOP]: {
    overflow: 'hidden',
    position: 'relative',
    width: 470,
    height: 360,
    marginBottom: 0,
  },
});

const StyledImageBlock = styled.div({
  textAlign: 'center',

  [CONDITION_DESKTOP]: {
    boxSizing: 'border-box',
    position: 'relative',
    width: 380,
    minHeight: 421,
    padding: 20,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgba(110, 0, 255, 0.18)',
    backgroundColor: '#fff',
    borderRadius: 27,
    textAlign: 'left',

    '&::before': {
      content: '""',
      position: 'absolute',
      zIndex: 0,
      left: 50,
      top: 6,
      bottom: 3,
      right: -8,
      borderRadius: 27,
      backgroundColor: 'rgba(139, 51, 255, 0.1)',
    },
  },
});

type StyledImageProps = {
  $isPrev: boolean;
};

const StyledImage = styled.img<StyledImageProps>(({ $isPrev }) => {
  if ($isPrev) {
    return {
      display: 'none',
      transition: 'opacity 0.2s',

      [CONDITION_DESKTOP]: {
        display: 'block',
        position: 'absolute',
        left: 20,
        width: 337,
        top: 20,
        opacity: 0,
      },
    };
  }

  return {
    transition: 'opacity 0.2s',
    width: 337,
    maxWidth: '100%',
  };
});

const StyledButtonWrapper = styled.div({
  display: 'flex',
  justifyContent: 'center',
  position: 'absolute',
  bottom: -20,
  left: 0,
  width: '100%',

  [CONDITION_DESKTOP]: {
    bottom: -30,
  },
});

type ImagesProps = {
  innerLabel: string;
  items: ItemType[];
};

export const Images: FC<ImagesProps> = ({ innerLabel, items }) => {
  const setNextTimeoutRef = useRef<NodeJS.Timeout | null>(null);
  const [[currentSlide, prevSlide], setCurrentSlide] = useState<[number, number | null]>([0, null]);

  const setNextSlide = () => {
    if (setNextTimeoutRef.current) {
      clearTimeout(setNextTimeoutRef.current);
    }

    setCurrentSlide(([prevCurrentSlide]) => {
      if (prevCurrentSlide < items.length - 1) {
        return [prevCurrentSlide + 1, prevCurrentSlide];
      }

      return [0, prevCurrentSlide];
    });

    setNextTimeoutRef.current = setTimeout(() => {
      setNextSlide();
    }, 2000);
  };

  useEffect(() => {
    setNextTimeoutRef.current = setTimeout(() => {
      setNextSlide();
    }, 2000);

    return () => {
      if (setNextTimeoutRef.current) {
        clearTimeout(setNextTimeoutRef.current);
      }
    };
  });

  const onMouseEnter = useCallback(() => {
    if (setNextTimeoutRef.current) {
      clearTimeout(setNextTimeoutRef.current);
    }
  }, []);

  const onMouseLeave = useCallback(() => {
    setNextTimeoutRef.current = setTimeout(() => {
      setNextSlide();
    }, 3000);
  }, []);

  const setSlide = useCallback((slideIndex: number): void => {
    setCurrentSlide(([prevCurrentSlide]) => [slideIndex, prevCurrentSlide]);
  }, []);

  const renderedLabels: ReactNode[] = [];

  for (let i = currentSlide - 6, l = currentSlide + 7; i < l; ++i) {
    const slideIndex = i < 0 ? items.length + i : i >= items.length ? i - items.length : i;

    renderedLabels.push(
      <SlideText index={slideIndex} offset={i - currentSlide} setSlide={setSlide} key={slideIndex}>
        {items[slideIndex].title}
      </SlideText>,
    );
  }

  const renderedImages: ReactNode[] = [];

  if (prevSlide !== null) {
    renderedImages.push(<StyledImage $isPrev src={items[prevSlide].image} alt="" key={prevSlide} />);
  }

  renderedImages.push(
    <StyledImage $isPrev={false} src={items[currentSlide].image} alt={items[currentSlide].title} key={currentSlide} />,
  );

  return (
    <StyledWrapper
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onTouchStart={onMouseEnter}
      onTouchEnd={onMouseLeave}
    >
      <StyledInnerLabel>{innerLabel}</StyledInnerLabel>

      <StyledLabelsBlock>{renderedLabels}</StyledLabelsBlock>

      <StyledImageBlock>{renderedImages}</StyledImageBlock>

      <StyledButtonWrapper>
        <ProgressButton currentSlide={currentSlide} slidesLength={items.length} onClick={setNextSlide}>
          Ещё пример
        </ProgressButton>
      </StyledButtonWrapper>
    </StyledWrapper>
  );
};

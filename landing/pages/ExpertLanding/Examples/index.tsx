import React, { memo, useEffect } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';
import { Container } from '../../../components/Container';

import type { ItemType } from './types';

import { Images } from './Images';

import { loadImage } from '../../../utils/loadImage';

const StyledWrapper = styled.div({
  marginTop: 60,
  marginBottom: 60,

  [CONDITION_DESKTOP]: {
    marginTop: 120,
    marginBottom: 120,
  },
});

const StyledTitle = styled.div({
  fontSize: 25,
  lineHeight: 1.2,
  fontWeight: 'bold',
  textAlign: 'center',
  color: '#111111',
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    fontSize: 40,
    lineHeight: 1.2,
  },
});

const StyledDescription = styled.div({
  fontSize: 14,
  fontWeight: 500,
  lineHeight: 1.43,
  textAlign: 'center',
  color: '#222d39',
  marginBottom: 30,

  [CONDITION_DESKTOP]: {
    fontSize: 21,
    lineHeight: 1.67,
    marginBottom: 40,
  },
});

type ExamplesProps = {
  title: string;
  description: string;
  innerLabel: string;
  items: ItemType[];
};

export const Examples = memo<ExamplesProps>(({ title, description, innerLabel, items }) => {
  useEffect(() => {
    items.forEach(({ image }) => {
      loadImage(image);
    });
  }, [items]);

  return (
    <StyledWrapper>
      <Container>
        <StyledTitle>{title}</StyledTitle>

        <StyledDescription>{description}</StyledDescription>

        <Images innerLabel={innerLabel} items={items} />
      </Container>
    </StyledWrapper>
  );
});

import React, { memo } from 'react';
import type { ReactNode } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../../../constants/breakpoints';

type StyledLabelProps = {
  $offset: number;
};

const labelOpacityMap: Record<number, number> = {
  0: 1,
  1: 0.8,
  2: 0.4,
  3: 0.2,
};

const getTop = (offset: number): number => {
  if (offset <= 0) {
    return (3 + offset) * 54;
  }

  return 174 + offset * 54;
};

const StyledLabel = styled.div<StyledLabelProps>(({ $offset }) => ({
  display: $offset === 0 ? 'block' : 'none',

  fontSize: 19,
  fontWeight: 'bold',
  lineHeight: '20px',
  textAlign: 'center',
  color: '#8b33ff',
  transition: 'all 0.25s',
  cursor: 'pointer',

  [CONDITION_DESKTOP]: {
    position: 'absolute',
    left: 0,
    top: getTop($offset),
    display: 'block',
    fontSize: $offset === 0 ? 35 : 23,
    lineHeight: $offset === 0 ? '36px' : '24px',
    opacity: labelOpacityMap[Math.abs($offset)],
    textAlign: 'left',
  },
}));

type SlideTextProps = {
  offset: number;
  index: number;
  setSlide: (index: number) => void;
  children: ReactNode;
};

export const SlideText = memo<SlideTextProps>(({ offset, index, setSlide, children }) => (
  <StyledLabel
    $offset={offset}
    onClick={() => {
      setSlide(index);
    }}
  >
    {children}
  </StyledLabel>
));

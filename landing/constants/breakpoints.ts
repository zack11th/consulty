export const MOBILE = 320;
export const SCROLLABLE_DESKTOP = 600;
export const DESKTOP = 1200;

export const CONDITION_DESKTOP = `@media(min-width: ${SCROLLABLE_DESKTOP}px)`;
export const CONDITION_RETINA = '@media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi)';

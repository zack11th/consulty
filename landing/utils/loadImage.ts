export const loadImage = (url: string): void => {
  const image = new Image();

  image.src = url;
};

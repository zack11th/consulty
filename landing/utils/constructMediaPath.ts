export const constructMediaPath = (
  apiOrigin: string,
  mediaPath: string,
): string => {
  if (mediaPath.startsWith('/')) {
    return `${apiOrigin}${mediaPath}`;
  }

  return mediaPath;
};

import React, {
  useCallback,
} from 'react';
import type {
  FC,
} from 'react';
import {
  useField,
} from 'react-final-form';
import type {
  Props as SelectProps,
} from 'react-select';

import {
  FieldWrapper,
} from '../components/FieldWrapper';
import {
  Select,
} from '../components/Select';

import type {
  BaseFieldProps,
} from './types';

type SelectFieldProps = BaseFieldProps & SelectProps;

export const SelectField: FC<SelectFieldProps> = ({
  name,
  label,
  required,
  ...rest
}) => {
  const {
    input: {
      value,
      onChange,
    },

    meta,
  } = useField(name);

  const onChangeWrapper = useCallback((nextValue) => {
    onChange(nextValue);
  }, [onChange]);

  return (
    <FieldWrapper
      label={label}
      required={required}
      meta={meta}
    >
      <Select
        isDisabled={meta.submitting}
        name={name}
        value={value}
        onChange={onChangeWrapper}
        {...rest}
      />
    </FieldWrapper>
  );
};

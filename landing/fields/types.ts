import type {
  ReactNode,
} from 'react';

export type BaseFieldProps = {
  name: string;
  label?: ReactNode;
  required?: boolean;
  disabled?: boolean;
};

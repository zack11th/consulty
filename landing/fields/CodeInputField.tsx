import React, {
  useCallback,
} from 'react';
import type {
  FC,
  ChangeEventHandler,
} from 'react';
import {
  useField,
} from 'react-final-form';
import styled from 'styled-components';

import {
  FieldWrapper,
} from '../components/FieldWrapper';
import {
  Input,
} from '../components/Input';

import type {
  BaseFieldProps,
} from './types';

const StyledCodeInput = styled(Input)({
  textAlign: 'center',
});

type CodeInputFieldProps = BaseFieldProps & {
  maxLength?: number;
  placeholder?: string;
  handleSubmit?: () => void;
};

export const CodeInputField: FC<CodeInputFieldProps> = ({
  name,
  label,
  maxLength,
  placeholder,
  required,
  handleSubmit,
}) => {
  const {
    input: {
      value,
      onChange,
    },

    meta,
  } = useField<string>(name);

  const onInputChange = useCallback<ChangeEventHandler<HTMLInputElement>>((event) => {
    const {
      target: {
        value: nextValue,
      },
    } = event;

    onChange(nextValue);

    if (handleSubmit && nextValue.length === maxLength) {
      handleSubmit();
    }
  }, [onChange, handleSubmit, maxLength]);

  return (
    <FieldWrapper
      label={label}
      required={required}
      meta={meta}
    >
      <StyledCodeInput
        placeholder={placeholder}
        disabled={meta.submitting}
        name={name}
        value={value}
        maxLength={maxLength}
        onChange={onInputChange}
      />
    </FieldWrapper>
  );
};

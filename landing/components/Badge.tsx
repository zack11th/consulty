import React from 'react';
import type { FC, ReactNode } from 'react';
import styled from 'styled-components';

const StyledWrapper = styled.div({
  display: 'flex',
  paddingLeft: 15,
  paddingRight: 15,
  borderRadius: 16,
  backgroundImage: 'linear-gradient(to right,#862fff 10%,#9c3efe 40%,#AE4CFE 90%)',
  paddingTop: 5,
  paddingBottom: 5,
});

const StyledItem = styled.div({
  position: 'relative',

  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.54,
  color: '#ffffff',

  '& + &': {
    marginLeft: 30,

    '&::before': {
      content: '""',
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: -15,
      width: 1,
      backgroundColor: '#fff',
      opacity: 0.2,
    },
  },
});

type BadgeProps = {
  items: ReactNode[];
};

export const Badge: FC<BadgeProps> = ({ items }) => (
  <StyledWrapper>
    {items.map((item, index) => (
      <StyledItem key={index}>{item}</StyledItem>
    ))}
  </StyledWrapper>
);

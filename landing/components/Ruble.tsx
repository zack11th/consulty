import React, {
  memo,
} from 'react';
import styled from 'styled-components';

type StyledSvgProps = {
  $fontWeight: 300 | 500 | 700;
};

const StyledSvg = styled.svg<StyledSvgProps>(({
  $fontWeight,
}) => {
  let width: number;

  switch ($fontWeight) {
    case 300:
      width = 0.8 * (19 / 26);
      break;

    case 500:
      width = 0.8 * (20 / 26);
      break;

    case 700:
      width = 0.8 * (21 / 26);
      break;

    default:
      throw new Error('Invalid "fontWeight" property in Ruble component');
  }

  return {
    width: `${width}em`,
    height: '0.8em',
  };
});

type RubleProps = {
  fontWeight: 300 | 500 | 700;
};

export const Ruble = memo<RubleProps>(({
  fontWeight,
}) => {
  switch (fontWeight) {
    case 300:
      return (
        <StyledSvg
          $fontWeight={300}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 19 26"
        >
          <path
            fill="currentColor"
            d="M2.644 26v-6.48H.052v-1.98h2.592v-2.7H.052v-2.124h2.592V.692h7.704c4.644 0 7.776 2.7 7.776 7.128 0 4.392-3.348 7.02-7.848 7.02H5.128v2.7h9.108v1.98H5.128V26H2.644zm2.484-13.284h4.896c3.384 0 5.508-1.908 5.508-5.004 0-3.096-2.124-4.896-5.508-4.896H5.128v9.9z"
          />
        </StyledSvg>
      );

    case 500:
      return (
        <StyledSvg
          $fontWeight={500}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 26"
        >
          <path
            fill="currentColor"
            d="M3.464 26v-6.12H.944v-2.52h2.52v-2.52H.944v-2.88h2.52V.584h8.388c4.932 0 7.92 2.772 7.92 7.236s-3.456 7.056-8.064 7.056h-4.68v2.484h9v2.52h-9V26H3.464zm3.564-14.04h4.212c3.132 0 4.86-1.728 4.86-4.284 0-2.592-1.764-4.212-4.788-4.212H7.028v8.496z"
          />
        </StyledSvg>
      );

    case 700:
      return (
        <StyledSvg
          $fontWeight={700}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 21 26"
        >
          <path
            fill="currentColor"
            d="M3.284 26v-5.724H.836V17.18h2.448v-2.34H.836v-3.636h2.448V.44h9.072c5.256 0 8.064 2.88 8.064 7.416 0 4.5-3.528 7.02-8.28 7.02H7.928v2.304h8.928v3.096H7.928V26H3.284zm4.644-14.796h3.564c2.808 0 4.176-1.584 4.176-3.6 0-2.052-1.404-3.492-4.104-3.492H7.928v7.092z"
          />
        </StyledSvg>
      );

    default:
      throw new Error('Invalid "fontWeight" property in Ruble component');
  }
});

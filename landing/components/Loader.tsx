import React, {
  memo,
} from 'react';
import styled from 'styled-components';

const BASE_SIZE = 64;
const HALF_SIZE = BASE_SIZE / 2;
const ANIMATION_DURATION = '1.4s';
const DELAY_RANGE = 0.32;

const StyledWrapper = styled.div({
  display: 'inline-block',
  width: BASE_SIZE * 2,
  textAlign: 'center',
});

type PointProps = {
  $index: number;
};

const StyledPoint = styled.div<PointProps>(({
  $index,
}) => ({
  width: HALF_SIZE,
  height: HALF_SIZE,
  backgroundColor: '#8B33FF',
  borderRadius: HALF_SIZE,
  display: 'inline-block',
  animation: `loader-pulse ${ANIMATION_DURATION} ease-in-out 0s infinite both`,
  animationDelay: `${(($index - 3) * DELAY_RANGE) / 2}s`,
}));

export const Points = memo(() => (
  <StyledWrapper>
    <StyledPoint
      $index={1}
    />

    <StyledPoint
      $index={2}
    />

    <StyledPoint
      $index={3}
    />
  </StyledWrapper>
));

const StyledLoaderWrapper = styled.div({
  paddingTop: 90,
  paddingBottom: 90,
  textAlign: 'center',
});

export const Loader = memo(() => (
  <StyledLoaderWrapper>
    <Points />
  </StyledLoaderWrapper>
));

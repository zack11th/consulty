import React from 'react';
import type { FC, ChangeEventHandler } from 'react';
import styled from 'styled-components';

type LabelProps = {
  $image?: string;
};

const StyledLabel = styled.label<LabelProps>(({ $image }) => ({
  position: 'relative',
  overflow: 'hidden',
  backgroundColor: $image ? 'transparent' : '#8b33ff',
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  backgroundImage: $image ? `url(${$image})` : 'none',
  cursor: 'pointer',

  '&::before': {
    content: $image ? 'none' : '""',
    position: 'absolute',
    left: 24,
    top: 24,
    width: 23,
    height: 20,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundImage: 'url(/landing/camera.svg)',
  },
}));

const StyledInput = styled.input({
  position: 'absolute',
  left: -9999,
  opacity: 0,
  visiblilty: 'hidden',
});

type ImageInputBaseProps = {
  className?: string;
  image?: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
};

export const ImageInputBase: FC<ImageInputBaseProps> = ({ className, image, onChange }) => (
  <StyledLabel className={className} $image={image}>
    <StyledInput type="file" onChange={onChange} accept=".jpg,.jpeg,.png" />
  </StyledLabel>
);

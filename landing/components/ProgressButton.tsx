import React, { memo } from 'react';
import type { ReactNode } from 'react';
import styled from 'styled-components';

import { CONDITION_DESKTOP } from '../constants/breakpoints';
import { containImageStyle } from '../utils/containImageStyle';

const StyledProgressButton = styled.button({
  position: 'relative',

  fontSize: 16,
  fontWeight: 500,
  fontStretch: 'normal',
  fontStyle: 'normal',
  lineHeight: '20px',
  letterSpacing: 'normal',
  textAlign: 'center',
  color: '#fff',
  paddingTop: 10,
  paddingBottom: 10,
  paddingLeft: 49,
  paddingRight: 21,
  borderRadius: 20,
  backgroundColor: '#e9003a',
  boxShadow: '0 0 15px 0 rgba(255, 51, 102, 0.5)',
  borderWidth: 0,
  outline: 'none',
  cursor: 'pointer',
  userSelect: 'none',

  '&::after': {
    content: '""',
    position: 'absolute',
    top: 10,
    left: 24,
    width: 16,
    height: 18,
    ...containImageStyle('/landing/refresh.svg'),
  },

  [CONDITION_DESKTOP]: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 45,
    borderRadius: 25,

    '&::after': {
      top: 15,
      left: 20,
    },
  },
});

const StyledOverlay = styled.div({
  position: 'absolute',
  zIndex: 1,
  top: 0,
  right: 0,
  bottom: 0,
  backgroundColor: '#fff',
  opacity: 0.5,
  transition: 'width 0.15s',
  borderTopRightRadius: 20,
  borderBottomRightRadius: 20,

  [CONDITION_DESKTOP]: {
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
  },
});

type ProgressButtonProps = {
  currentSlide: number;
  slidesLength: number;
  onClick: () => void;
  onMouseEnter?: () => void;
  onMouseLeave?: () => void;
  children: ReactNode;
};

export const ProgressButton = memo<ProgressButtonProps>(
  ({ currentSlide, slidesLength, onClick, onMouseEnter, onMouseLeave, children }) => (
    <StyledProgressButton
      type="button"
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onTouchStart={onMouseEnter}
      onTouchEnd={onMouseLeave}
    >
      <StyledOverlay
        style={{
          width: `${((1 - (currentSlide + 1) / slidesLength) * 100).toFixed(2)}%`,
        }}
      />

      {children}
    </StyledProgressButton>
  ),
);

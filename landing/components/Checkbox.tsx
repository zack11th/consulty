import React, { useCallback } from 'react';
import type { FC, ReactNode } from 'react';
import styled from 'styled-components';

type CheckboxProps = {
  checked: boolean;
  onChange: (nextValue: boolean) => void;
  children?: ReactNode;
};

const StyledWrapper = styled.label({
  display: 'flex',
  margin: 0,
  cursor: 'pointer',
});

const StyledInput = styled.input({
  display: 'none',
});

type StyledBoxProps = {
  $checked: boolean;
};

const StyledBox = styled.div<StyledBoxProps>(({ $checked }) => ({
  position: 'relative',
  boxSizing: 'border-box',
  width: 20,
  height: 20,
  borderRadius: 4,
  borderStyle: 'solid',
  borderWidth: $checked ? 0 : 1,
  borderColor: '#c1c7cd',
  backgroundColor: $checked ? '#8b33ff' : '#ffffff',
}));

const StyledCheckImg = styled.img({
  position: 'absolute',
  top: 4,
  left: 4,
  width: 11,
  height: 11,
});

const StyledText = styled.div({
  flex: 1,
  marginLeft: 10,

  fontSize: 12,
  fontWeight: 500,
  lineHeight: 1.25,
  color: '#333333',
});

export const Checkbox: FC<CheckboxProps> = ({ checked, onChange, children }) => {
  const onInputChange = useCallback(() => {
    onChange(!checked);
  }, [checked, onChange]);

  return (
    <StyledWrapper>
      <StyledInput type="checkbox" checked={checked} onChange={onInputChange} />

      <StyledBox $checked={checked}>{checked && <StyledCheckImg src="/landing/check-white.svg" alt="" />}</StyledBox>

      <StyledText>{children}</StyledText>
    </StyledWrapper>
  );
};

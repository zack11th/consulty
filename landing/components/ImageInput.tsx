import React from 'react';
import type {
  FC,
  ChangeEventHandler,
} from 'react';
import styled from 'styled-components';

import {
  ImageInputBase,
} from './ImageInputBase';

const StyledWrapper = styled.div({
  textAlign: 'center',
  marginBottom: 10,
});

const StyledBaseInput = styled(ImageInputBase)({
  display: 'inline-block',
  verticalAlign: 'bottom',
  width: 70,
  height: 70,
  borderTopLeftRadius: 10,
  borderTopRightRadius: 10,
  borderBottomLeftRadius: 10,
  borderBottomRightRadius: 2,
});

type ImageInputProps = {
  image?: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
};

export const ImageInput: FC<ImageInputProps> = ({
  image,
  onChange,
}) => (
  <StyledWrapper>
    <StyledBaseInput
      image={image}
      onChange={onChange}
    />
  </StyledWrapper>
);

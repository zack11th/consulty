import type { FC } from 'react';
import type { IndicatorProps } from 'react-select';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
//@ts-ignore
const IndicatorSeparator: FC<IndicatorProps<any>> = () => null;

export default IndicatorSeparator;

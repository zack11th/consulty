import React from 'react';
import type { FC } from 'react';
import styled from 'styled-components';

import type { ControlProps } from 'react-select';

type StyledControlProps = {
  $isDisabled: boolean;
};

const StyledControl = styled.div<StyledControlProps>(({ $isDisabled }) => ({
  boxSizing: 'border-box',
  alignItems: 'center',
  cursor: $isDisabled ? 'default' : 'pointer',
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'space-between',
  outline: 0,
  position: 'relative',
  transition: 'all 100ms',
  borderWidth: 1,
  borderStyle: 'solid',
  borderColor: '#c1c7cd',

  backgroundColor: '#fff',
  minHeight: 50,
  borderRadius: 12,
}));

// eslint-disable-next-line @typescript-eslint/no-explicit-any
//@ts-ignore
const Control: FC<ControlProps<any>> = (props) => {
  const { children, isDisabled, innerRef, innerProps, className } = props;
  return (
    <StyledControl ref={innerRef} className={className} $isDisabled={isDisabled} {...innerProps}>
      {children}
    </StyledControl>
  );
};

export default Control;

import type { SelectComponentsConfig } from 'react-select';

import Control from './Control';
import IndicatorSeparator from './IndicatorSeparator';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
//@ts-ignore
const createComponents = (): SelectComponentsConfig<any> => ({
  Control,
  IndicatorSeparator,
});

export const syncComponents = createComponents();
export const asyncComponents = createComponents();

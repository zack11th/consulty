import type { Styles } from 'react-select';

import { valueContainer } from './valueContainer';

//@ts-ignore
export const styles: Styles = {
  valueContainer,
};

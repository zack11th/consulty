import type { CSSProperties } from 'react';

export const valueContainer = (): CSSProperties => ({
  fontSize: 15,
  fontWeight: 500,
  lineHeight: 1.67,
  alignItems: 'center',
  display: 'flex',
  flex: 1,
  flexWrap: 'wrap',
  WebkitOverflowScrolling: 'touch',
  position: 'relative',
  overflow: 'hidden',
  padding: '6px 15px',
  color: '#232832',
});

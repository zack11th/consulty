import React from 'react';
import type {
  FC,
} from 'react';
import BaseSelect from 'react-select';
import type {
  Props,
} from 'react-select';

import {
  syncComponents,
} from './components';
import {
  styles,
} from './styles';

export const Select: FC<Props> = (props) => (
  <BaseSelect
    {...props}
    components={syncComponents}
    styles={styles}
  />
);

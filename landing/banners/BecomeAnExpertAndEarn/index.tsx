import React, { memo } from 'react';

import { Button } from '../../components/Button';

import { Item } from './Item';
import { BannerBase } from '../BannerBase';
import ym from 'react-yandex-metrika';
import { ENVIRONMENT_TYPE } from 'common/constants';

const sendDataToMetrics = () => {
  if (ENVIRONMENT_TYPE === 'production') {
    ym('reachGoal', ['Main-become_expert-more_info']);
    window.gtag('event', 'Main-become_expert-more_info');
    window.fbq('trackCustom', 'Main-become_expert-more_info');
  }
};

export const BecomeAnExpertAndEarn = memo(() => (
  <BannerBase
    title="Станьте экспертом и зарабатывайте с Consulty"
    actions={
      <Button $block onClick={sendDataToMetrics}>
        Подробнее
      </Button>
    }
    bubbleImage="/landing/banners/become-an-expert/bubble@2x.png"
    bubbleImageRetina="/landing/banners/become-an-expert/bubble@3x.png"
    mainImageDesktop="/landing/banners/become-an-expert/girl@2x.png"
    mainImageMobile="/landing/banners/become-an-expert/girl.png"
    bgColor="#ffc910"
  >
    <Item>Зарабатывайте на консультациях</Item>
    <Item>Получите доказательство своей экспертности</Item>
    <Item>Монетизируйте аудиторию своего блога</Item>
  </BannerBase>
));

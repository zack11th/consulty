import React, { memo } from 'react';

import { Button } from '../../components/Button';

import { Item } from './Item';
import { BannerBase } from '../BannerBase';
import { useRouter } from 'next/router';
import { routes } from 'common/routes';

export const BecomeAnExpert = memo(() => {
  const router = useRouter();

  return (
    <BannerBase
      title="Станьте экспертом в Consulty"
      actions={
        <Button
          $block
          onClick={() => {
            router.push(routes.expertLanding);
          }}
        >
          Подробнее
        </Button>
      }
      bubbleImage="/landing/banners/become-an-expert/bubble@2x.png"
      bubbleImageRetina="/landing/banners/become-an-expert/bubble@3x.png"
      mainImageDesktop="/landing/banners/become-an-expert/girl_mask.png"
      mainImageMobile="/landing/banners/become-an-expert/girl.png"
      bgColor="#ffc910"
    >
      <Item>Зарабатывайте на консультациях</Item>

      <Item>Привлекайте бесплатно новых клиентов</Item>

      <Item>Зарабатывайте на партнерской программе</Item>
    </BannerBase>
  );
});

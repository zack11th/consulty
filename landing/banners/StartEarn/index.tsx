import React, { memo } from 'react';

import { Button } from '../../components/Button';

import { Item } from './Item';
import { BannerBaseBig } from '../BannerBaseBig';
import { useAppDispatch } from 'hooks/redux';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { actions, selectors } from 'store/ducks';
import { routes } from 'common/routes';

type StartEarnProps = {
  id?: string;
};

export const StartEarn = memo<StartEarnProps>(({ id }) => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const isAuth = useSelector(selectors.profile.selectIsAuthentication);

  return (
    <BannerBaseBig
      id={id}
      title="Начните зарабатывать с Consulty"
      subTitle="Создайте ваш профиль эксперта за одну минуту. "
      actions={
        <Button
          $color="violet-gradient"
          $size="medium"
          type="button"
          onClick={() => {
            if (isAuth) {
              router.push({ pathname: routes.profile, query: { activeTab: 'expertProfile' } });
            } else {
              dispatch(actions.app.showAuthModal());
            }
          }}
        >
          Стать экспертом в Consulty
        </Button>
      }
      mainImageDesktop="/landing/banners/become-an-expert/girl_mask.png"
      mainImageMobile="/landing/banners/become-an-expert/girl_mask.png"
      bgColor="#ffc910"
    >
      <Item>Формируйте репутацию эксперта</Item>
      <Item>Оказывайте платные консультации</Item>
      <Item>Привлекайте новых клиентов</Item>
    </BannerBaseBig>
  );
});

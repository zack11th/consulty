import React, { memo } from 'react';
import type { ReactNode } from 'react';
import styled from 'styled-components';

import { Container } from '../components/Container';
import { DESKTOP, CONDITION_DESKTOP } from '../constants/breakpoints';
import { containImageStyle } from '../utils/containImageStyle';

type StyledWrapperProps = {
  $bgColor: string;
};

const StyledWrapper = styled.div<StyledWrapperProps>(({ $bgColor }) => ({
  boxSizing: 'border-box',
  overflow: 'hidden',
  position: 'relative',
  backgroundColor: $bgColor,

  [CONDITION_DESKTOP]: {
    minWidth: DESKTOP,
  },
}));

const StyledTitleBlock = styled.div({
  marginBottom: 20,

  [CONDITION_DESKTOP]: {
    marginBottom: 30,
  },
});

const StyledTitle = styled.div({
  fontWeight: 700,
  fontSize: 20,
  lineHeight: 1.2,
  color: '#232832',

  [CONDITION_DESKTOP]: {
    fontSize: 35,
  },
});

const StyledSubTitle = styled.div({
  fontWeight: 500,
  fontSize: 14,
  lineHeight: 1.43,
  color: '#232832',
  marginTop: 10,

  [CONDITION_DESKTOP]: {
    fontSize: 17,
    marginTop: 20,
  },
});

const StyledButtonWrapper = styled.div({
  marginTop: 40,
});

const StyledContent = styled.div({
  position: 'relative',
  boxSizing: 'border-box',
  paddingTop: 30,
  paddingBottom: 282,

  [CONDITION_DESKTOP]: {
    height: 450,
    paddingTop: 80,
  },
});

type StyledBubbleProps = {
  $bubbleImage: string;
};

const StyledBubble = styled.div<StyledBubbleProps>(({ $bubbleImage }) => ({
  position: 'absolute',
  right: 109,
  bottom: 150,
  width: 172,
  height: 100,
  ...containImageStyle($bubbleImage),

  [CONDITION_DESKTOP]: {
    right: 341,
    bottom: 156,
    width: 243,
    height: 129,
  },
}));

type StyledMainImageProps = {
  $mainImageDesktop: string;
  $mainImageMobile: string;
};

const StyledMainImage = styled.div<StyledMainImageProps>(({ $mainImageDesktop, $mainImageMobile }) => ({
  position: 'absolute',
  right: 0,
  bottom: 0,
  borderRadius: 12,
  width: '100%',
  height: 229,
  ...containImageStyle($mainImageMobile),

  [CONDITION_DESKTOP]: {
    right: -100,
    width: 658,
    height: 450,
    backgroundImage: `url(${$mainImageDesktop})`,
  },
}));

type BannerBaseBigProps = {
  id?: string;
  title: ReactNode;
  subTitle?: ReactNode;
  actions?: ReactNode;
  mainImageDesktop: string;
  mainImageMobile: string;
  bubbleImage?: string;
  bgColor: string;
  children: ReactNode;
};

export const BannerBaseBig = memo<BannerBaseBigProps>(
  ({ id, title, subTitle, actions, mainImageDesktop, mainImageMobile, bubbleImage, bgColor, children }) => (
    <StyledWrapper id={id} $bgColor={bgColor}>
      <Container>
        <StyledContent>
          <StyledMainImage $mainImageDesktop={mainImageDesktop} $mainImageMobile={mainImageMobile} />

          <StyledTitleBlock>
            <StyledTitle>{title}</StyledTitle>

            {subTitle && <StyledSubTitle>{subTitle}</StyledSubTitle>}
          </StyledTitleBlock>

          <div>{children}</div>

          {actions && <StyledButtonWrapper>{actions}</StyledButtonWrapper>}

          {bubbleImage && <StyledBubble $bubbleImage={bubbleImage} />}
        </StyledContent>
      </Container>
    </StyledWrapper>
  ),
);

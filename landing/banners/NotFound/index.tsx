import React, { memo, useState, useCallback } from 'react';
import styled from 'styled-components';

import { Button } from '../../components/Button';
import { Modal } from '../../components/Modal';

import { BannerBase } from './BannerBase';
import { ModalContent } from './ModalContent';

const StyledText = styled.div({
  fontSize: 17,
  fontWeight: 500,
  lineHeight: 1.47,
  color: '#222d39',
});

const text = 'Напишите нам, какой темы не хватает. Добавим её, если она нужна пользователям. ';

export const NotFound = memo(() => {
  const [isOpen, setIsOpen] = useState(false);

  const openModal = useCallback(() => {
    setIsOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsOpen(false);
  }, []);

  return (
    <BannerBase
      title="Не нашли нужную тему для консультаций?"
      actions={
        <Button $size="50" type="button" onClick={openModal}>
          Предложить тему
        </Button>
      }
      bubbleImage="/landing/banners/not-found/bubble.png"
      bubbleImageRetina="/landing/banners/not-found/bubble@2x.png"
      mainImageDesktop="/landing/banners/not-found/pic@2x.png"
      mainImageMobile="/landing/banners/not-found/pic.png"
      bgColor="#ffd200"
    >
      <StyledText>{text}</StyledText>

      <Modal show={isOpen} onHide={closeModal}>
        <ModalContent />
      </Modal>
    </BannerBase>
  );
});

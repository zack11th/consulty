import React, { memo, useCallback, useState } from 'react';
import styled from 'styled-components';
import { Form } from 'react-final-form';

import { Button } from '../../components/Button';
import { InputField } from '../../fields/InputField';
import { SUPPORT_EMAIL } from 'common/constants';

const StyledWrapper = styled.div({
  padding: '30px 20px 45px',
});

const StyledTitle = styled.div({
  fontSize: 19,
  fontWeight: 'bold',
  lineHeight: 1.05,
  textAlign: 'center',
  color: '#232832',
  marginBottom: 15,
});

const StyledDescription = styled.div({
  fontSize: 13,
  fontWeight: 500,
  lineHeight: 1.54,
  textAlign: 'center',
  color: '#232832',
});

const StyledForm = styled.form({
  marginTop: 30,
});

const StyledButtonWrapper = styled.div({
  marginTop: 15,
});

export const ModalContent = memo(() => {
  const [isSent, setIsSent] = useState(false);

  const onSubmit = useCallback(
    async (values: {
      name: string;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    }): Promise<any> => {
      window.open(`mailto:${SUPPORT_EMAIL}?subject='Предложить тему'&body=${encodeURIComponent(values.name)}`);
      setIsSent(true);
    },
    [],
  );

  if (isSent) {
    return (
      <StyledWrapper>
        <StyledTitle>Отправлено</StyledTitle>

        <StyledDescription>Мы расмотрим предложенную тему и добавим, если она нужна пользователям.</StyledDescription>
      </StyledWrapper>
    );
  }

  return (
    <StyledWrapper>
      <StyledTitle>Предложить тему</StyledTitle>

      <StyledDescription>Если считаете, что не хватает востребованной темы - напишите нам</StyledDescription>

      <Form onSubmit={onSubmit}>
        {({ handleSubmit, submitting, values }) => (
          <StyledForm onSubmit={handleSubmit}>
            <InputField name="name" placeholder="Например астрология" />

            <StyledButtonWrapper>
              <Button type="submit" $block disabled={submitting || !values.name}>
                Предложить
              </Button>
            </StyledButtonWrapper>
          </StyledForm>
        )}
      </Form>
    </StyledWrapper>
  );
});

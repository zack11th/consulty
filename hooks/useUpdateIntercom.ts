import { selectors } from '../store/ducks';
import { useEffect } from 'react';
import { useIntercom } from 'react-use-intercom';
import { useAppSelector } from './redux';

export const useUpdateIntercom = () => {
  const user = useAppSelector(selectors.profile.selectUser);
  const userId = user?.id?.toString();
  const { update, shutdown, boot } = useIntercom();

  const updateIntercom = () => {
    if (user.id && user.firstName && user.lastName) {
      shutdown();
      boot();
      update({
        email: user?.email,
        userId,
        name: user?.firstName,
        phone: user?.phone,
        unsubscribedFromEmails: undefined,
        //TODO: add i18n dependency language
        // languageOverride: 'ru',
      });
      return;
    }
    shutdown();
    boot();
  };

  useEffect(() => {
    updateIntercom();
  }, [user]);

  return { updateIntercom };
};

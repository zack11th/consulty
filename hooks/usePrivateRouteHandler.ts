import { useSelector } from 'react-redux';
import { selectors } from '../store/ducks/profile';
import { useAppDispatch } from './redux';
import { actions } from '../store/ducks/app';

export const usePrivateRouteHandler = () => {

  const token = useSelector(selectors.selectToken);
  const dispatch = useAppDispatch();

  return (handler: () => void) => () => {
    if(!!token){
      handler();
      return;
    }
    dispatch(actions.showAuthModal());
  };
};
import { api } from 'api';
import httpClient from 'api/httpClient';
import { MAX_FILE_SIZE } from 'common/constants';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { getUrl } from 'utils';
import imageCompression from 'browser-image-compression';
import { captureError } from 'utils/captureError';

export const usePrepareImageToUpload = () => {
  const { t } = useTranslation('utils');
  const selectFile = (
    files: FileList | null,
    onloadCallback: ({ readerResult, file }: { readerResult?: string; file?: File }) => void,
  ) => {
    let isValidFile = false;
    if (files) {
      const File = files.item(0);
      if (File) {
        const reader = new FileReader();
        const { size, type } = File;
        if (size < MAX_FILE_SIZE && (type === 'image/jpeg' || type === 'image/png')) {
          isValidFile = true;
        }
        if (!isValidFile) {
          throw new Error(t('incorrectFile'));
        }
        reader.onload = async () => {
          if (typeof reader.result === 'string' && isValidFile) {
            const resizedFile = await resizeImageFn(File);
            onloadCallback({ readerResult: reader.result, file: resizedFile });
          }
        };
        reader.readAsDataURL(File);
      }
    }
  };

  const uploadFileToCloud = async (file: File) => {
    try {
      const { data: signUrl } = await api.V1StorageApi.storageControllerSignFileUrl({
        contentType: file.type,
        fileKey: file.name,
      });
      await httpClient.put(signUrl, file, {
        headers: {
          'Content-Type': file.type,
          'Access-Control-Allow-Methods': 'GET, POST, PUT, OPTIONS, DELETE',
          'Access-Control-Allow-Origin': '*',
        },
      });
      return getUrl(signUrl);
    } catch (error) {
      captureError(error);
      toast.error(t('somethingWrong'));
    }
  };

  const resizeImageFn = async (file: File) => {
    const options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 490,
      useWebWorker: true,
    };
    try {
      const compressedFile = await imageCompression(file, options);

      return compressedFile;
    } catch (error) {
      captureError(error);
      toast.error(t('somethingWrong'));
    }
  };

  return { selectFile, uploadFileToCloud };
};

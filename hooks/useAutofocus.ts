import { useEffect } from 'react';
import { Path, UseFormSetFocus } from 'react-hook-form';

export function useAutofocus<T>(field: Path<T>, setFocus: UseFormSetFocus<T>, delay = 0) {
  useEffect(() => {
    const timeout = setTimeout(() => setFocus(field), delay);
    return () => clearTimeout(timeout);
  }, [setFocus]);
}

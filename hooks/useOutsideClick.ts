import { RefObject, useEffect } from 'react';

type Props = {
  ref: RefObject<HTMLDivElement>;
  onOutsideClick: () => void;
};

export const useOutsideClick = ({ ref, onOutsideClick }: Props) => {
  useEffect(() => {
    const handleClickOutside = (event: Event) => {
      if (ref.current && !ref.current.contains(event.target as Node)) {
        onOutsideClick();
      }
    };

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref, onOutsideClick]);
};

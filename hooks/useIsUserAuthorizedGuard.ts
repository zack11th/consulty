import { actions } from 'store/ducks/app';
import { useAppDispatch } from './redux';
import { useSelector } from 'react-redux';
import { selectors } from 'store/ducks/profile';

export function useIsUserAuthorizedGuard<T extends CallableFunction>(callback: T) {
  const dispatch = useAppDispatch();
  const isAuthorized = useSelector(selectors.selectIsAuthentication);

  const showModal = () => {
    dispatch(actions.showAuthModal());
  };

  const guardedCallback = new Proxy(callback, {
    apply: (target, _, args) => {
      if (!isAuthorized) {
        showModal();
        return;
      }

      target(...args);
    },
  });

  return { guardedCallback, isAuthorized };
}

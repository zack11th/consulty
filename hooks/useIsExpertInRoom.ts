import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { selectors } from 'store/ducks';
import { ChatRoomExtended } from 'types';

export const useIsExpertInRoom = (chatRoom: ChatRoomExtended | undefined) => {
  const user = useSelector(selectors.profile.selectUser);
  const [isExpertInRoom, setIsExpertInRoom] = useState(false);

  useEffect(() => {
    setIsExpertInRoom(user.id === chatRoom?.expertId);
  }, [user, chatRoom]);

  return isExpertInRoom;
};

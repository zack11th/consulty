import { ChatToast } from 'components/ui/chat-toast';
import { toast } from 'react-toastify';

type useChatToastProps = {
  icon?: string;
  title: string;
  content: string;
  onClick: () => void;
};

export const useChatToast = () => {
  return ({ icon, title, content, onClick }: useChatToastProps) =>
    toast(<ChatToast icon={icon} title={title} content={content} />, {
      onClick,
      containerId: 'chatToastContainer',
    });
};

import { ENVIRONMENT_TYPE } from 'common/constants';
import { useEffect } from 'react';

export const useFacebookPixel = () => {
  useEffect(() => {
    if (ENVIRONMENT_TYPE === 'production') {
      import('react-facebook-pixel')
        .then((x) => x.default)
        .then((FacebookPixel) => {
          FacebookPixel.init('1053459231378310');
        });
    }
  });
};

import { ConsultationStatusEnum, Message } from 'api';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { pusherEvents } from 'common/constants';
import pusherService from 'services/pusher-service';
import { actions, selectors } from 'store/ducks';
import { useAppDispatch } from './redux';

const createPrivateChannel = (chatRoomId: number) => `private-chat_${chatRoomId}`;

export const useChatPrivateCannel = () => {
  const dispatch = useAppDispatch();
  const accessToken = useSelector(selectors.profile.selectToken);
  const chatRoomId = useSelector(selectors.chatRooms.selectCurrentChatRoomId);

  useEffect(() => {
    const pusher = pusherService.init(accessToken);
    const privateChatCannel = chatRoomId && createPrivateChannel(chatRoomId);

    if (pusher && privateChatCannel) {
      const privateChannel = pusher.subscribe(privateChatCannel);

      privateChannel.bind(pusherEvents.messages.received, (data: { message: Message }) => {
        dispatch(actions.chatMessages.receiveChatMessage(data.message));

        if (data.message.consultation) {
          dispatch(actions.consultationsChatRoom.updateExpertMessageCount(data.message.consultation));
          if (data.message.consultation.status === ConsultationStatusEnum.MessagesLimitExceeded) {
            dispatch(actions.consultationsChatRoom.setIsReviewModalOpen(true));
          }
        }
      });

      return () => {
        privateChannel.unbind();
        pusher.unsubscribe(privateChatCannel);
      };
    }
  }, [accessToken, chatRoomId]);
};

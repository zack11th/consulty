import { COOKIE_KEY } from 'common/constants';
import { routes } from 'common/routes';
import { useRouter } from 'next/router';
import { FC, useEffect } from 'react';
import { getCookie } from 'utils';

export const withAuthClientSide = <T extends Record<string, any>>(Component: FC<T>) => {
  return function withAuthClientSideComponent(props: T): JSX.Element {
    const router = useRouter();
    const accessToken = process.browser && getCookie(COOKIE_KEY.accessToken);

    useEffect(() => {
      if (!accessToken) {
        router.replace(routes.topics);
      }
    }, [accessToken]);

    return <Component {...props} />;
  };
};

import { GetServerSideProps, GetServerSidePropsContext } from 'next';

export function withAuthGSSP(gssp: GetServerSideProps) {
  return async (context: GetServerSidePropsContext) => {
    const { req, res } = context;
    
    const token: string = req.cookies.token;

    if (!token) {
      // Redirect to home page
      res.writeHead(301, { Location: '/' });
      res.end();
      return {};
    }

    return await gssp(context); // Continue on to call `getServerSideProps` logic
  };
}

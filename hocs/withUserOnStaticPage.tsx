import { COOKIE_KEY } from 'common/constants';
import { useAppDispatch } from 'hooks/redux';
import { FC, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { actions, selectors } from 'store/ducks';
import { getCookie } from 'utils';

export const withUserOnStaticPage = <T extends Record<string, unknown>>(Component: FC<T>) => {
  return function withUserOnStaticPageComponent(props: T): JSX.Element {
    const dispatch = useAppDispatch();
    const isAuthenticate = useSelector(selectors.profile.selectIsAuthentication);

    useEffect(() => {
      if (!isAuthenticate) {
        const localToken = process.browser && getCookie(COOKIE_KEY.accessToken);
        if (localToken) {
          dispatch(actions.profile.setToken(localToken));
          dispatch(actions.profile.fetchMe());
          dispatch(actions.profile.fetchWallet());
          dispatch(actions.profile.fetchSelfPromocode());
          dispatch(actions.profile.fetchConsultationsWithoutReview());
        }
      }
    }, []);

    return <Component {...props} />;
  };
};

import { ChatConsultationOffer, ChatRoom, Consultation, Message } from 'api';

export type LinkType = {
  text: string;
  href: string;
};

export type LoadingStatus = 'idle' | 'pending' | 'fulfilled' | 'rejected';
export interface MessageExtended extends Partial<Message> {
  status?: 'error' | 'pending' | 'sent';
}
export type MessageVariants = 'me' | 'companion' | 'system';

export interface ChatRoomExtended extends ChatRoom {
  isTempRoom?: boolean;
  isCompanionWritten?: boolean;
}

export interface ConsultationWithOffer extends Consultation {
  consultationOffer?: ChatConsultationOffer;
}

export interface GetMessagesAxiosResponse {
  count: number;
  data: Message[];
  page: number;
  pageCount: number | null;
  total: number;
}

export interface Params {
  fields?: Array<string>;
  s?: string;
  filter?: Array<string>;
  or?: Array<string>;
  sort?: Array<string>;
  join?: Array<string>;
  limit?: number;
  offset?: number;
  page?: number;
  cache?: number;
  options?: any;
}

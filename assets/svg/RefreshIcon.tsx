import * as React from 'react';

function SvgRefreshIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={16} height={18} {...props}>
      <defs>
        <path
          id="refreshIcon_svg__a"
          d="M12.803 5.016A7.398 7.398 0 007.5 2.79H6.4L8.165.995 7.182 0 3.745 3.494l3.392 3.498.99-.988-1.752-1.807H7.5c3.37 0 6.111 2.78 6.111 6.197 0 3.418-2.741 6.198-6.111 6.198-3.37 0-6.111-2.78-6.111-6.198V9.69H0v.704c0 2.032.78 3.942 2.197 5.378C3.613 17.21 5.497 18 7.5 18s3.887-.791 5.303-2.228A7.607 7.607 0 0015 10.394a7.61 7.61 0 00-2.197-5.378z"
        />
      </defs>
      <g fill="none" fillRule="evenodd">
        <mask id="refreshIcon_svg__b" fill="#fff">
          <use xlinkHref="#refreshIcon_svg__a" />
        </mask>
        <use fill="#FFF" fillRule="nonzero" xlinkHref="#refreshIcon_svg__a" />
        <path fill="#FFF" d="M0 0h15.158v18H0z" mask="url(#refreshIcon_svg__b)" />
      </g>
    </svg>
  );
}

export default SvgRefreshIcon;

import * as React from 'react';

function SvgShare(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width={18} height={17} {...props}>
      <defs>
        <path
          id="Share_svg__a"
          d="M18 7.749L10.52 0v4.623H8.93C3.997 4.623 0 8.399 0 13.058v2.45l.706-.732c2.402-2.485 5.8-3.902 9.363-3.902h.45v4.623L18 7.75z"
        />
      </defs>
      <g transform="translate(0 .023)" fill="none" fillRule="evenodd">
        <mask id="Share_svg__b" fill="#fff">
          <use xlinkHref="#Share_svg__a" />
        </mask>
        <use fill="#93969D" xlinkHref="#Share_svg__a" />
        <path fill="#93969D" d="M0 0h18v16.615H0z" mask="url(#Share_svg__b)" />
      </g>
    </svg>
  );
}

export default SvgShare;

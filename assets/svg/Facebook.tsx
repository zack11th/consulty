import * as React from 'react';

function SvgFacebook(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={20} height={20} {...props}>
      <path
        d="M11.34 19v-8.21h2.655l.397-3.2h-3.05V5.546c0-.927.248-1.558 1.526-1.558H14.5V1.124a20.999 20.999 0 00-2.378-.125c-2.35 0-3.962 1.49-3.962 4.23v2.36H5.5v3.2h2.66V19h3.18z"
        fill="#000"
        fillRule="nonzero"
      />
    </svg>
  );
}

export default SvgFacebook;

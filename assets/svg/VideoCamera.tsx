import * as React from 'react';

function SvgVideoCamera(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width={40} height={40} xmlns="http://www.w3.org/2000/svg" {...props}>
      <defs>
        <linearGradient x1="5.442%" y1="35.24%" x2="100%" y2="35.24%" id="VideoCamera_svg__a">
          <stop stopColor="#812DF6" offset="0%" />
          <stop stopColor="#993DFA" offset="45.328%" />
          <stop stopColor="#AE4CFE" offset="100%" />
        </linearGradient>
      </defs>
      <g fill="none" fillRule="evenodd">
        <circle fill="url(#VideoCamera_svg__a)" cx={20} cy={20} r={20} />
        <g fill="#FFF" fillRule="nonzero">
          <path d="M22.41 14.022h-9.203A2.214 2.214 0 0011 16.23v7.438c0 1.214.993 2.207 2.207 2.207h9.204a2.214 2.214 0 002.207-2.207v-7.438a2.2 2.2 0 00-2.207-2.207zM29.54 15.236a1.124 1.124 0 00-.376.154l-3.443 1.987v5.12l3.465 1.987c.64.375 1.435.154 1.81-.486.11-.199.177-.42.177-.662v-6.82c0-.817-.773-1.479-1.634-1.28z" />
        </g>
      </g>
    </svg>
  );
}

export default SvgVideoCamera;

import * as React from 'react';

function SvgStar(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={19} height={19} {...props}>
      <path
        d="M9.499 14.957l-5.87 3.515 1.551-6.631L0 7.382l6.83-.584L9.499.528l2.671 6.271 6.83.583-5.18 4.459 1.551 6.631z"
        fill="#EE8835"
        fillRule="evenodd"
      />
    </svg>
  );
}

export default SvgStar;

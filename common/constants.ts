// api
export const API_BASE_URL = process.env.NEXT_PUBLIC_API_URL;
export const ENVIRONMENT_TYPE: 'development' | 'staging' | 'production' = process.env.NEXT_PUBLIC_ENVIRONMENT as
  | 'development'
  | 'staging'
  | 'production';
// export const API_BASE_URL = 'http://193.32.219.9';
// export const API_BASE_URL = 'http://localhost:5000';

// breakpoints
export const MOBILE = 320;
export const SCROLLABLE_DESKTOP = 600;
export const DESKTOP = 1180;

export const CONDITION_DESKTOP = `@media(min-width: ${SCROLLABLE_DESKTOP}px)`;

// header
export const HEADER_HEIGHT_DESKTOP = '80px';
export const HEADER_HEIGHT_MOBILE = '60px';

// pusher
export const pusherEvents = {
  chatRoom: {
    created: 'CHAT_CREATED',
    companionWritingStart: 'client-COMPANION_WRITING_START',
    companionWritingEnd: 'client-COMPANION_WRITING_END',
  },
  messages: {
    received: 'MESSAGE_RECEIVED',
  },
  consultationOffers: {
    created: 'CONSULTATION_OFFER_CREATED',
  },
  consultationRequests: {
    created: 'CONSULTATION_REQUEST_CREATED',
  },
  consultation: {
    starts: 'CONSULTATION_STARTS',
    pendingStart: 'CONSULTATION_PENDING_START',
    timeExceeded: 'CONSULTATION_TIME_EXCEEDED',
    messagesLimitExceeded: 'CONSULTATION_MESSAGES_LIMIT_EXCEEDED',
    endedByClient: 'CONSULTATION_ENDED_BY_CLIENT',
    endedByExpert: 'CONSULTATION_ENDED_BY_EXPERT',
    prolongationRequest: 'CONSULTATION_PROLONGATION_REQUEST',
    prolonged: 'CONSULTATION_PROLONGED',
    prolongationRequestDeclined: 'CONSULTATION_PROLONGATION_REQUEST_DECLINED',
    paymentExpired: 'CONSULTATION_PAYMENT_EXPIRED',
  },
  chatConsultationOffer: {
    created: 'CREATED',
    rejected: 'REJECTED',
  },
  extraServices: {
    created: 'EXTRA_SERVICE_CREATED',
    paid: 'EXTRA_SERVICE_PAID',
    rejected: 'EXTRA_SERVICE_REJECTED',
  },
  blocking: {
    block: 'USER_BLOCKED',
  },
};

// other
export const DEFAULT_AVATAR = '/img/default-avatar.png';
export const DEFAULT_CATEGORY = '/img/default-category.png';
export const DEFAULT_REQUEST_CHAT_TOAST_ICON = '/img/coin60@3x.png';
export const DEFAULT_RESPONSE_CHAT_TOAST_ICON = '/img/thumb-up60@3x.png';
export const DEFAULT_THINK_CHAT_TOAST_ICON = '/img/think-for-banner@3x.png';

export const COOKIE_KEY = {
  accessToken: 'token',
};

export const LIMIT_CHAT_MESSAGES = 35;
export const MAX_FILE_SIZE = 5242880;

export const SUPPORT_EMAIL = 'support@consulty.online';

export const AUDIO_MESSAGE_EXT = 'aac';

export const INTERCOM_APP_ID = process.env.NEXT_PUBLIC_INTERCOM_APP_ID!;

export enum routes {
  home = '/',
  expertLanding = '/expert',
  topics = '/app/topics',
  category = '/app/topics/:id',
  experts = '/app/experts',
  expert = '/app/expert/:id',
  profile = '/app/profile',
  chat = '/app/chat',
  contract = '/app/contract',
  contractCorrect = '/app/contract/correct',
}

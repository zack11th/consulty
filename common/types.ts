export type ContractSignSmsCodePayload = {
  smsCode: string;
  timestampEnterSmsCode: string;
};
